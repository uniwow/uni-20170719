## 任务名称： 账号分类拉取

## 任务描述：
```
该任务的目的是把指标计算所需的账号分类以及账号推荐所需数据保存成csv格式
```

## 部署地址：

```
gitlab:
生产环境:139.129.196.32/home/admin/ETL_PYTHON_3
```

## 执行方式 ：
```
cd /home/admin/ETL_PYTHON_3 && /usr/local/python3/bin/python3 -m media_weixin_control --func func_94 >> /dev/null 2>&1 &
```

## 注意事项：

### 输出文件: 

#### 账号分类参数：
格式如[账号分类参数](./account_tags.csv) 
```
格式说明：
nhdbg520,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0
微信公众号id,其余为26个分类的状态矩阵
1：表示属于该分类
0：不属于该分类
```

分类详见 [分类映射](./category.json) 


### 接口描述：
```
media_weixin_account_tags((CHECK_CSV_PATH, 'account_tags.csv', 'ab'),(CHECK_CSV_PATH, 'media_weixin_basic_data_export.csv', 'ab'))

account_tags.csv ：该项目所需文件
media_weixin_basic_data_export.csv ：账号计算所需数据，登龙很熟悉，不做解释

```
详见 [公众号推荐依赖文件](./media_weixin_basic_data_export.csv)

### 依赖项目：
```
无
```