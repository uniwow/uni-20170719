## 任务名称： DTS 指标计算

## 任务描述：
```
spark 指标计算项目的主要任务是计算依托微信公账号发文信息设计的各种指数。结果保存到mongdb及hdfs
```

## 任务需求：
```
spark 指标计算项目的主要目的是计算依托微信公账号发文信息设计的各种指数。
```

## 部署地址：

```
gitlab:
生产环境:192.168.0.117/qmg/jar/compute.jar
```

## 执行方式 
```
/usr/local/spark/bin/spark-submit --class compute.WeiXinAccountIndexCompute --master spark://spider-host-003:7077 --deploy-mode client --driver-memory 10g --executor-memory 24g --total-executor-cores 24 /qmg/jar/compute.jar >/qmg/bin/nohup.out 2>&1 &
```

## 注意事项：

### 指数清单: 

[spark指数清单](./spark指标清单.md) 


### spark 接口描述：
```
lazy val dataframeComputed = rdd_compute
      .filter(map_filter(_))        筛选出最近两个月发布的文章
      .map(map_split_by_object_id_upgrade(_))       把数据转换为以object_id为键的键值对
      .reduceByKey((_ ++ _))        对键做聚类，为筛选重复数据最准备
      .map(reduce_split_by_object_id_upgrade(_))        筛选出一篇文章的最近一次抓取，并把数据转换为以public_id为键的键值对
      .reduceByKey(_ ++ _)      对键做聚类，为筛选重复数据做准备
      //      .filter(account_in_media_weixin(_))
      .map(deduplication_articles(_))       去除因不同平台文章发布日期时间差造成的数据重复
      .map(reduce_split_upgrade(_))     指标统计
      .flatMap(_.toList)        结果展开转换为列表
      .reduceByKey(_ ++ _)     把数据以周期为键做聚类
      .map(rank_by_wom_index(_))        计算周期排名
      .flatMap(_.toList)        结果展开转换为列表
      .reduceByKey(_ ++ _)      把数据以账号为键做聚类
      .map(media_weixin_account_rank_inc(_))        计算每个分类下的排名变化
      .map(transfer_to_mongo_frame(_))      把结果转换为目标json格式
      .map(row => ComputedData(row._1, row._2, row._3, row._4))
      .toDF.as[ComputedData].persist(StorageLevel.MEMORY_AND_DISK_SER)
```

### 依赖项目：

#### 指标计算所需文件上传到hdfs 

详见 [指标计算所需参数](./指标计算所需参数.md) 






