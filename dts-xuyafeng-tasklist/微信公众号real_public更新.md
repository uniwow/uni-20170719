## 任务名称： 微信公众号real_public更新

## 任务描述：
```
更新real_public 以及账号基本信息到mysql.media_weixin
```

## 任务需求：
```
由于后台录入微信公众号public_id等基本信息可能有误，不能满足需要准确public_id的业务场景，故需要根据爬虫抓取的准确结果更新real_public 字段
```

## 部署地址：

```
gitlab:
生产环境:139.129.196.32/home/admin/ETL_PYTHON_3
```

## 执行方式 :
```
0 7 * * * root cd /home/admin/ETL_PYTHON_3 && /usr/local/python35/bin/python3 -m media_weixin_control --func func_67

```
详见 [定时任务文件](./crontab.txt) 

## 注意事项：

### 接口描述：
```
basic_info_and_index_compute_data_to_mysql(self, source_mongo_args_1, source_mongo_args_2, source_mysql_args, target_mysql_args):

source_mongo_args_1: mongodb 连接参数
source_mongo_args_2：mongodb 连接参数
source_mysql_args:mysql 连接参数
target_mysql_args：mysql连接参数
```

### 依赖项目：

#### 指标入库账号文件拉取:
```
无
```
