## 任务名称： 指标入库账号文件拉取 

## 任务描述：
```
将保存在mongodb上的media_weixin_info collection 账号导出到多文件
```

## 任务需求：
```
为了满足基本的多进程处理任务，将public_id字段随机导出到多个文件，用于多进程数据入库
```

## 部署地址：

```
gitlab:
生产环境:139.129.196.32/home/admin/ETL_PYTHON_3
```

## 执行方式 :
```
0 7 * * * root cd /home/admin/ETL_PYTHON_3 && /usr/local/python35/bin/python3 -m media_weixin_control --func func_107

```
详见 [定时任务文件](./crontab.txt) 

## 注意事项：

### 接口描述：
```
media_weixin_info_accounts_split_to_five_files(source_mongo_args,file_nums)
source_mongo_args：mongodb 连接参数
file_nums：被分成的文件数
```

### 文件格式
详见 [账号列表](./head media_weixin_info_influence_public_id_1.csv)

### 依赖项目：

#### :spark 指标计算 
详见 [spark 指标计算](./spark指标计算.md)
