## 任务名称： 文章参数拉取 

## 任务描述：
```
该任务的目的是把指标计算所需的文章参数保存成csv格式，以及把文章导入elasticsearch和mysql.weixin_artile
```

## 任务需求：
```
该任务的目的是把指标计算所需的文章参数保存成csv格式，以及把文章导入elasticsearch和mysql
```

## 部署地址：

```
gitlab:
生产环境:139.129.196.32/home/admin/ETL_PYTHON_3
```

## 执行方式 
```
*/50 * * * * root cd /usr/local/supervisor && /usr/bin/supervisorctl restart atricle_trend:*

```
详见 [进程管理配置文件](./supervisord.conf) 及其 [定时任务文件](./crontab.txt) 
## 注意事项：

### 输出文件: 

#### 文章参数： 
格式如[文章参数](./article_20171111.csv) 
```
格式说明：
'weixin_id','post_time','article_type','article_pos','page_view_num','page_like_num','comment_num','updtae_time','_id' 
微信公众号id,发布时间,文章类型,文章位置,文章阅读数,文章点赞数,文章评论数,更新日期,object_id 
```

#### ES 参数：
格式如[elasticsearch参数](./elasticsearch.json)
```
es.index(index="qmg_dts_sogou_weixin",
         doc_type="media_weixin_article",
         id=mongo_obj_id,
         body={
             "public_id": weixin_id,
             "public_name": str(message_dict["weixin_name"]),
             "mongo_object_id": mongo_obj_id,
             "title": title,
             "content": content,
             "digest": short_desc,
             "article_pos": article_pos,
             "is_origin": is_origin,
             "post_date": datetime_to_str(current_stamp_to_datetime(post_time)),
         },
```

### 接口描述：
```
def weixin_article_consumer_from_grow_trend_info_random(self,groups_num,group_id,source_mongo_args,target_mysql_args):

groups_num: 账号数值化取余数
group_id  : 当前进程所负责消费的余数
source_mongo_args: mongodb 连接参数
target_mysql_args：mysql连接参数
```

### 依赖项目：
```
无
```



