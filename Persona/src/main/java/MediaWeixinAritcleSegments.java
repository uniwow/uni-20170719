/**
 * Created by uni on 2017/6/29 0029.
 */
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.tokenizer.NotionalTokenizer;
import com.hankcs.hanlp.tokenizer.SpeedTokenizer;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import org.bson.Document;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;


import java.util.List;

public class MediaWeixinAritcleSegments {

    public static void main(String[] args) throws IOException {
        /*
        * 1.默认情况下是开启名词识别模式的
        * 2.可以通过自定义词性，筛选出品牌词，并进行词频统计
        * 3.批量处理，叠加操作
        * */
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://dts-datawarehouse-admin:6ghednsdf1xde0bqq@118.190.175.5:27011/?authSource=wom-dts-datawarehouse"));
        MongoDatabase database = mongoClient.getDatabase("wom-dts-datawarehouse");
        MongoCollection<Document> coll = database.getCollection("media_weixin_article");
        MongoCollection<Document> coll_tag = database.getCollection("media_weixin_brands");
        final LineIterator it = FileUtils.lineIterator(new File("F:\\shared-dir\\data-etl\\ETL_PYTHON_3\\csvs\\media_weixin_accounts_segment.csv"), "UTF-8");
        try {
            while (it.hasNext()) {
                final String line = it.nextLine();
                Map<String, Integer> segmentCounter = new HashMap<String, Integer>();
                System.out.println(line);
                Block<Document> segmentAndFrequence = new Block<Document>() {
                    @Override
                    public void apply(final Document document) {
                        try{
                            List<Term> termList = NotionalTokenizer.segment(document.getString("content"));
                            for(Term term:termList){
                                if(term.nature.toString().equals("tag_brand")){
                                    if (segmentCounter.containsKey(term.word)){
                                        segmentCounter.put(term.word,segmentCounter.get(term.word)+1);
                                    }else{
                                        segmentCounter.put(term.word,1);
                                    }
                                }
                            }
                        }catch (Exception e){
                            System.out.println("不能被正确分词");
                        }finally{
                        }
                    }
                };
                coll.find(eq("weixin_id", line)).projection(new Document("content", 1))
                        .forEach(segmentAndFrequence);
                Comparator<Map.Entry<String, Integer>> valueComparator = new Comparator<Map.Entry<String,Integer>>(){
                    @Override
                    public int compare(Map.Entry<String, Integer> o1,
                                       Map.Entry<String, Integer> o2) {
                        return o2.getValue()-o1.getValue();
                    }
                };
                List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String,Integer>>(segmentCounter.entrySet());
                Collections.sort(list,valueComparator);
                System.out.println(list);
                Document document_inner = new Document();
                for (Map.Entry<String, Integer> entry : list) {
                    try {
                        if (!entry.getKey().contains(".") && !entry.getKey().contains("&")&& !entry.getKey().contains("#")){
                            document_inner.append(entry.getKey(),entry.getValue());
                        }

                    } catch (Exception e) {
                        System.out.println("Invalid BSON field");
                    }
                }
                Document document = new Document("public_id", line)
                        .append("brand_segment",Arrays.asList(document_inner));
                coll_tag.insertOne(document);
            }
        } finally {
            it.close();
            mongoClient.close();
        }

        Segment segment = HanLP.newSegment().enableNameRecognize(true);

//        for (String sentence : testCase)
//        {
//            List<Term> termList = segment.seg(sentence);
//            List<Term> termList = NotionalTokenizer.segment(sentence);
//            System.out.println(termList);
//        }

    }
}
