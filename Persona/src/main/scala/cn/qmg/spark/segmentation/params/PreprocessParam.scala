package cn.qmg.spark.segmentation.params

import cn.qmg.spark.segmentation.untils.Conf
import scala.collection.mutable

/**
  * 预处理过程使用参数
  */
class PreprocessParam extends Serializable  {
  val kvMap: mutable.LinkedHashMap[String, String] = Conf.loadConf("src/main/resources/preprocess.properties")
 //停用词词库
  val stopwordFilePath: String = kvMap.getOrElse("stopword.file.path", "dictionaries/hanlp/data/dictionary/stopwords.txt")
  //品牌词词库
  val brandwordFilePath: String = kvMap.getOrElse("brandword.file.path", "dictionaries/hanlp/data/dictionary/brandwords.txt")
  val segmentType: String = kvMap.getOrElse("segment.type", "StandardSegment")    //分词方式
  val delNum: Boolean = kvMap.getOrElse("is.delete.number", "false").toBoolean     //是否去除数字
  val delEn: Boolean = kvMap.getOrElse("is.delete.english", "false").toBoolean    //是否去除英语单词
  val addNature: Boolean = kvMap.getOrElse("add.nature", "false").toBoolean   //是否添加词性
  val minTermLen: Int = kvMap.getOrElse("min.term.len", "1").toInt      //最小词长度
  val minTermNum: Int = kvMap.getOrElse("min.term.num", "3").toInt      //行最小词数
  val vocabSize: Int = kvMap.getOrElse("vocab.size", "10000").toInt   //特征词汇表大小

}
