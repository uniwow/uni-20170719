package cn.qmg.spark.segmentation.preprocess

import java.io.File

import cn.qmg.spark.segmentation.params.PreprocessParam
import org.apache.spark.ml.feature._
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * 数据预处理
  */
class Preprocessor extends Serializable {

//  def train(filePath: String, spark: SparkSession):DataFrame = {
//    val params = new PreprocessParam
//
//    val cleanDF = this.clean(filePath, spark)   //清洗数据
//    val trainDF = this.segment(cleanDF, params)   //分词
//    trainDF
//  }

  /**
    * 清洗步骤
    *首先从文件加载数据到RDD，然后按分割符进行切分
    */
  def clean(filePath: String, spark: SparkSession): DataFrame = {
    import spark.implicits._
    val textDF = spark.sparkContext.textFile(filePath).flatMap { line =>
      val fields = line.split("\u00EF")
        val title = fields(0)
        val weixin_id = fields(1)
        val content = fields(2)
        if (!weixin_id.equals(" ")) Some(title, weixin_id, content) else None

    }.toDF("title", "weixin_id","content")
    textDF
  }

  /**
    * 分词过程，包括"分词", "去除停用词"
    */
  def segment(data: DataFrame, params: PreprocessParam): DataFrame = {
    val spark = data.sparkSession

    //=== 分词
    val segmenter = new Segmenter()
      .isDelEn(params.delEn)
      .isDelNum(params.delNum)
      .setSegmentType(params.segmentType)
      .addNature(params.addNature)
      .setMinTermLen(params.minTermLen)
      .setMinTermNum(params.minTermNum)
      .setInputCol("content")    //传入文章
      .setOutputCol("tokens")
    val segDF = segmenter.transform(data)

    //=== 去除停用词
    val stopwordArray = spark.sparkContext.textFile(params.stopwordFilePath).collect()
    val remover = new StopWordsRemover()
      .setStopWords(stopwordArray)
      .setInputCol(segmenter.getOutputCol)
      .setOutputCol("removed")
    val removedDF = remover.transform(segDF)
    removedDF
  }

  /**
    * 匹配品牌词，包括统计词频
    * @param data
    * @param params
    * @return
    */
  def matching(data: DataFrame, params: PreprocessParam): Array[String] = {

    val vectorizer = new CountVectorizer()
      .setVocabSize(params.vocabSize)
      .setInputCol("removed")
      .setOutputCol("features")
    val parentVecModel = vectorizer.fit(data)

    //过滤词汇表
    val numPattern = "[0-9]+".r
    val vocabulary: Array[String] = parentVecModel.vocabulary.flatMap { term =>
      if (term.length == 1 || term.matches(numPattern.regex)) None else Some(term)

    }
//    val vecDF = new CountVectorizerModel(Identifiable.randomUID("cntVec"), vocabulary)
//      .setInputCol("removed")
//      .setOutputCol("features")
//    vecDF
    vocabulary
  }


//
//    val spark = data.sparkSession
//    val brandwordArray: Array[String] = spark.sparkContext.textFile(params.brandwordFilePath).collect()
//
//    val matchDF =
//
//    matchDF
//  }
}
