import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.tokenizer.NotionalTokenizer;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.body.Body;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Administrator on 2017/6/30 0030.
 */
public class WeixinArticleHanlpTest {
    public static void main(String[] args) throws IOException {
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://dts-datawarehouse-admin:6ghednsdf1xde0bqq@118.190.175.5:27011/?authSource=wom-dts-datawarehouse"));
        MongoDatabase database = mongoClient.getDatabase("wom-dts-datawarehouse");
        MongoCollection<Document> coll = database.getCollection("media_weixin_article");
        MongoCollection<Document> coll_tag = database.getCollection("weixin_article_brands");
        final LineIterator it = FileUtils.lineIterator(new File("F:\\shared-dir\\text-mining-toolkit\\brand_crawl\\weixin_articles_urls.txt"), "UTF-8");
        try {
            while (it.hasNext()) {
                /*
                * 完成request请求
                *
                * */
                final String line = it.nextLine();
                System.out.println(line);

                System.out.println(line);
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(line).build();
                Response response = client.newCall(request).execute();

                String htmlStr = response.body().string(); // 含html标签的字符串
                String textStr = "";
                java.util.regex.Pattern p_script;
                java.util.regex.Matcher m_script;
                java.util.regex.Pattern p_style;
                java.util.regex.Matcher m_style;
                java.util.regex.Pattern p_html;
                java.util.regex.Matcher m_html;
                java.util.regex.Pattern p_special;
                java.util.regex.Matcher m_special;
                String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>";
                //定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style>
                String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";
                // 定义HTML标签的正则表达式
                String regEx_html = "<[^>]+>";
                // 定义一些特殊字符的正则表达式 如：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                String regEx_special = "\\&[a-zA-Z]{1,10};";
                p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
                m_script = p_script.matcher(htmlStr);
                htmlStr = m_script.replaceAll(""); // 过滤script标签
                p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
                m_style = p_style.matcher(htmlStr);
                htmlStr = m_style.replaceAll(""); // 过滤style标签
                p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
                m_html = p_html.matcher(htmlStr);
                htmlStr = m_html.replaceAll(""); // 过滤html标签
                p_special = Pattern.compile(regEx_special, Pattern.CASE_INSENSITIVE);
                m_special = p_special.matcher(htmlStr);
                htmlStr = m_special.replaceAll(""); // 过滤特殊标签
                textStr = htmlStr;
                System.out.println(textStr);
//                System.out.println(response.body().string().replace("'","").replace("\"","").replace("\n","").replace("\r","").replace("\t","").replace(" ",""));
                /*
                * 分词操作
                * */
                List<Term> termList = NotionalTokenizer.segment(textStr.replace("'","").replace("\"","").replace("\n","").replace("\r","").replace("\t","").replace(" ",""));
                Map<String, Integer> segmentCounter = new HashMap<String, Integer>();
                for(Term term:termList){
                    if(term.nature.toString().equals("tag_brand")){
                        if (segmentCounter.containsKey(term.word)){
                            segmentCounter.put(term.word,segmentCounter.get(term.word)+1);
                        }else{
                            segmentCounter.put(term.word,1);
                        }
                    }
                }
//                System.out.println(line);
                Comparator<Map.Entry<String, Integer>> valueComparator = new Comparator<Map.Entry<String,Integer>>(){
                    @Override
                    public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                        return o2.getValue()-o1.getValue();
                    }
                };
                List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String,Integer>>(segmentCounter.entrySet());
                Collections.sort(list,valueComparator);
                System.out.println(list);
                Document document_inner = new Document();
                for (Map.Entry<String, Integer> entry : list) {
                    try {
                        if (!entry.getKey().contains(".") && !entry.getKey().contains("&")&& !entry.getKey().contains("#")){
                            document_inner.append(entry.getKey(),entry.getValue());
                        }

                    } catch (Exception e) {
                        System.out.println("Invalid BSON field");
                    }
                }

                Document document = new Document("article_url", line)
                        .append("brand_segment",Arrays.asList(document_inner));
                coll_tag.insertOne(document);

            }
        } finally {
            it.close();
            mongoClient.close();
        }

    }
}
