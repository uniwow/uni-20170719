package cn.qmg.spark.segmentation

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import play.api.libs.json.Json


/**
  * Created by mhfka on 2017/6/14.
  */
object Test {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
    val warehouseLocation = "D:\\Software\\IEDA\\Workspaces\\Persona\\spark-warehouse"
    val spark = SparkSession
      .builder
      .master("local[2]")
      .appName("Test")
      .config("spark.sql.warehouse.dir", warehouseLocation)
      .getOrCreate()

    val input = spark.sparkContext.parallelize(List( """{"title":"读书","public_name":"shidiandushu","content":"读书。。。。。"}""","""{"name":"过往记忆"}"""))
    val parsed = input.map(Json.parse)
    case class Info(title: String, weixin_id: String,content: String) {
       override def toString: String = title + "\t" + weixin_id + "\t" + content
    }
    implicit val personReads = Json.format[Info]
//    implicit val userFormat: OFormat[Info] = Json.using[Json.WithDefaultValues].format[Int]
    val result = parsed.flatMap(record => personReads.reads(record).asOpt)
    result.collect

  }

}