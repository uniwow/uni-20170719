package cn.qmg.spark.segmentation

import cn.qmg.spark.segmentation.preprocess.{Preprocessor, Segmenter}
import com.hankcs.hanlp.HanLP
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.feature.StopWordsRemover
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._

import scala.io.Source
import scala.collection.immutable.Map

/**
  * 测试
  */
object PreprocessDemo extends Serializable {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)
       HanLP.Config.enableDebug()
    val warehouseLocation ="F:\\shared-dir\\Persona\\spark-warehouse"
    val spark = SparkSession
      .builder
      .master("local[2]")
      .appName("Preprocess Demo")
      .config("spark.sql.warehouse.dir", warehouseLocation)
      .getOrCreate()

    val filePath = "F:\\shared-dir\\Persona\\data\\weixin/train/duhaoshu.json"
    val stopwordPath = "F:\\\\shared-dir\\Persona\\dictionaries\\hanlp\\data\\dictionary\\stopwords.txt"
    val brandwordPath = "F:\\shared-dir\\Persona\\dictionaries\\hanlp\\data\\dictionary\\custom\\MyBrandWord.txt"
    //数据清洗
val textDF = spark.read.json(filePath)
    //分词
    // val segmenter = new Segmenter(spark)
    val segmenter = new Segmenter()
      .setSegmentType("StandardSegment")
      .addNature(false)
      //      .enableNature(false)
      .setInputCol("content")
      .setOutputCol("tokens")
    val segDF = segmenter.transform(textDF)

    //去除停用词
    val stopwordArray = spark.sparkContext.textFile(stopwordPath).collect()
    val remover = new StopWordsRemover()
      .setStopWords(stopwordArray)
      .setInputCol("tokens")
      .setOutputCol("removed")
    val removedDF = remover.transform(segDF)
         removedDF.show()
    //匹配品牌
//    val brandwordDF = spark.read.textFile(brandwordPath).toDF("brandword")
//    val remover removedDF.schema.fields
//    for ()
//    val seriali = removedDF.join(brandwordDF,removedDF("removed")=== brandwordDF("brandword"),"inner")
//
//    println(seriali)

//    val brandword = removedDF.toString().replaceAll("\\,", "").replaceAll("\\/[a-z]+,", "")
//    val brandwordArray= spark.sparkContext.textFile(brandwordPath).collect()
//    val matcher = new StopWordsRemover()
//      .setStopWords(brandwordArray)
//      .setInputCol("removed")
//      .setOutputCol("matched")
//    val brandDF = matcher.transform(removedDF)
//
//    brandDF.show()

    }




}

