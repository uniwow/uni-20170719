package cn.qmg.spark.segmentation
import com.hankcs.hanlp.HanLP
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary
import com.hankcs.hanlp.seg.Segment
import com.hankcs.hanlp.seg.common.Term
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.conf.Configured
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.IntWritable
import org.apache.hadoop.io.LongWritable
import org.apache.hadoop.io.Text
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.Mapper
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat
import org.apache.hadoop.util.GenericOptionsParser
import org.apache.hadoop.util.Tool
import org.apache.hadoop.util.ToolRunner
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SparkSession}
import com.hankcs.hanlp.HanLP
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary
import com.hankcs.hanlp.seg.Segment
import org.apache.hadoop.conf.Configured
import org.apache.hadoop.io.IntWritable
import org.apache.hadoop.io.LongWritable
import java.io.IOException

import cn.qmg.spark.segmentation.params.PreprocessParam

object SentenceSeg extends Configured {
  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)
    val warehouseLocation = "D:\\Software\\IEDA\\Workspaces\\Persona\\spark-warehouse"
    val spark = SparkSession
      .builder
      .master("local[2]")
      .appName("Test")
      .config("spark.sql.warehouse.dir", warehouseLocation)
      .getOrCreate()

    val filePath = "D:\\Software\\IEDA\\Workspaces\\Persona\\data\\weixin/train/duhaoshu.json"
    val stopwordPath = "D:\\Software\\IEDA\\Workspaces\\Persona\\dictionaries\\hanlp\\data\\dictionary\\stopwords.txt"
    val brandwordPath = "D:\\Software\\IEDA\\Workspaces\\Persona\\dictionaries\\hanlp\\data\\dictionary\\custom\\MyBrandWord.txt"
    val textDF= spark.read.json(filePath)

  }

      def map(data: DataFrame, filePath: String, params: PreprocessParam): DataFrame = {
        val spark = data.sparkSession

        val textDF= spark.read.json(filePath)
        val textSentence = new Text()
        val line = data.toString
        val strSplits = line.split("\t")
        if (strSplits.length == 2) {
          val label = strSplits(0).toInt
          val sentence = strSplits(1)
          val segment = HanLP.newSegment.enablePartOfSpeechTagging(false)
          val segWords = segment.seg(sentence)
          CoreStopWordDictionary.apply(segWords)
          val strSegWords = segWords.toString.replaceAll("[\\[\\]]", "").replaceAll("\\/[a-z]+,", "")
          textSentence.set(strSegWords.toString)
          System.out.println("---------------------------")
          System.out.println(segWords.size)
          System.out.println(strSegWords)
          System.out.println("---------------------------")
//          sc.write(new IntWritable(label), textSentence)

        }
        textDF
      }


}
