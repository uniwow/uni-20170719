from multiprocessing.pool import ThreadPool

thread_pool = ThreadPool(20)

redis_record = {
    "host": "118.178.187.31",
    "port": 6379,
    "password": "f628c98b21dbeaa4314da2b710970ce6",
    "db": 8
}

abuyun_proxy = {
    "server": "proxy.abuyun.com",
    "port": 9010,
    "tunnels": [
        {
            "user": "H7U2365U2IZPJ6XP",
            "password": "694E0E53ED72AAD0"
        },
        {
            "user": "HZQ776SX9D33D39P",
            "password": "CE63E0B1198FDB3A"
        },
        {
            "user": "HT7127N93262415P",
            "password": "FFD7FA05C02F8B7F"
        }
    ],
}
headers = {}
headers["Host"] = "weixin.sogou.com"
headers["Referer"] = "http://weixin.sogou.com/weixin?type=1&s_from=input&query=woshitongdao"
headers["Connection"] = "keep-alive"
headers["Upgrade-Insecure-Requests"] = "1"
headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36"
headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
headers["Accept-Encoding"] = "gzip, deflate, sdch"
headers["Accept-Language"] = "zh-CN,zh;q=0.8"
cookies = {}
cookies["SUID"] = "E73551652028940A00000000591028D6"
cookies["weixinIndexVisited"]="1"
cookies["SUV"] = "00BA7A57655135E7591028D8C8601125"
cookies["CXID"] = "0AAE7EB9489E280479A4CCC03D913FEF"
cookies["_ga"] = "GA1.2.427300628.1494297158"
cookies["ssuid"] = "5336228900"
cookies["dt_ssuid"] = "5915942300"
cookies["pgv_pvi"] = "4892420096"
cookies["SUID"] = "E73551655412940A00000000591028D7"
cookies["SMYUV"] = "1496633317303784"
cookies["UM_distinctid"] = "15c764c17bcd1-0cf0c7773d2a3b-541a321f-15f900-15c764c17bdad8"
cookies["sw_uuid"] = "7880404070"
cookies["sg_uuid"] = "9166314400"
cookies["ld"] = "kkllllllll2B9BVVlllllVOUbLDlllllaU8VJZllll9lllll4ylll5@@@@@@@@@@"
cookies["ad"] = "Tyllllllll2BqPknlllllVOQplclllllNnwNAlllll9lllll4klll5@@@@@@@@@@"
cookies["ABTEST"] = "0|1499652649|v1" 
cookies["SNUID"] = "0F9B64F28783D56B91F1AA48873788EB"
cookies["JSESSIONID"] = "aaa0zH5ZgkNLEabf_1OZv"
cookies["sct"] = "232"
cookies["IPLOC"] = "CN3100"

proxy_list = [{"ping_time":0.061577,"ip:port":"36.99.206.211:808","http_type":"HTTP"},
             {"ping_time":0.571605,"ip:port":"134.35.251.245:8080","http_type":"HTTP"},
             {"ping_time":0.45169,"ip:port":"103.228.152.12:80","http_type":"HTTP\/HTTPS"},
             {"ping_time":0.17354,"ip:port":"185.89.216.15:10200","http_type":"HTTP"},
             {"ping_time":0.237857,"ip:port":"147.75.208.113:10200","http_type":"HTTP"},
             {"ping_time":0.164906,"ip:port":"149.202.49.42:80","http_type":"HTTP"},
             {"ping_time":0.032291,"ip:port":"60.184.175.26:808","http_type":"HTTP\/HTTPS"},
             {"ping_time":0.025319,"ip:port":"120.77.183.34:8888","http_type":"HTTP"},
             {"ping_time":0.158295,"ip:port":"113.77.24.79:9000","http_type":"HTTP\/HTTPS"},
             {"ping_time":0.656316,"ip:port":"183.153.18.110:808","http_type":"HTTP\/HTTPS"},
             {"ping_time":0.194618,"ip:port":"42.115.88.42:53281","http_type":"HTTP\/HTTPS"},
             {"ping_time":0.110325,"ip:port":"61.6.41.111:53281","http_type":"HTTP"},
             {"ping_time":0.582383,"ip:port":"190.206.173.255:8080","http_type":"HTTP"},
             {"ping_time":0.05062,"ip:port":"47.90.46.132:3128","http_type":"HTTP\/HTTPS"},
             {"ping_time":999999,"ip:port":"62.244.190.88:80","http_type":"HTTP"},
             {"ping_time":0.458449,"ip:port":"36.66.124.237:53281","http_type":"HTTP\/HTTPS"}]