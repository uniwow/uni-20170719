from peewee import *
from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import RetryOperationalError

class AutoConnectPooledMySQLDatabase(RetryOperationalError, PooledMySQLDatabase):
    pass
    
database = AutoConnectPooledMySQLDatabase('dts_ops', max_connections=10,**{'user': 'wom', 'port': 3306, 'password': 'Qmg2016mw#semys$', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com'})


class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class DtsWeixinAccountMonitorTask(BaseModel):
    end_time = IntegerField()
    frequency = IntegerField()
    last_post_time = IntegerField()
    public = CharField(db_column='public_id')
    start_time = IntegerField()
    status = IntegerField()
    updated_at = DateTimeField()
    uuid = CharField(unique=True)
    scheduler_uuid = CharField(unique=True)

    class Meta:
        db_table = 'dts_weixin_account_monitor_task'

