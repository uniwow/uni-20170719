### 微信账号监控
--------------------------------------


#### 简介

账号监控用于 尽快得到账号即将发布的文章，然后创建文章监测任务监测文章；

#### 程序部署以及启动方式

1. 程序启动方式

        python3 -m start

2. 使用supervisor管理

        [program:dts_weixin_account_monitor]
        command=python3 -m start --address=118.178.187.31 --port=8887 --log-file-num-backups=1 --log-file-prefix=runtime/server/server.log  --logging=info
        process_name=%(program_name)s_%(process_num)02d
        numprocs=1
        numprocs_start=0
        autostart=False
        autorestart=True
        startretries=3
        user=root
        stdout_logfile=NONE
        directory=/alidata1/workspace/dts/dts-weixin-account-monitor

3.  程序部署位置

        118.178.187.31:22:/alidata1/workspace/dts/dts-weixin-account-monitor

#### 接口描述

1. 监测任务

        GET http://118.178.187.31:8887/weixin-account-monitor?uuid=1111111


