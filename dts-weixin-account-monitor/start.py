#!/usr/bin/env python3
# encoding: utf-8
import sys


from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.options import options, define
from tornado.log import gen_log


from handlers.weixin_account_monitor import WeixinAccountMonitor


if __name__ == "__main__":

    if sys.platform.startswith("win"):
        gen_log.error("The service is only allowed to run on the unix platform.")
        sys.exit(-1)

    define("port", default=9886, help="Specify port number for server", metavar="9886")
    define("address", default="192.168.0.113", help="Specify ip address for server", metavar="192.168.0.113")
    options.parse_command_line()

    handlers = [
        (r"/weixin-account-monitor", WeixinAccountMonitor)
    ]

    application = Application(handlers=handlers,
                              autoreload=True,
                              debug=False)
    application.listen(int(options.port), xheaders=True, address=options.address)
    IOLoop.current().start()
