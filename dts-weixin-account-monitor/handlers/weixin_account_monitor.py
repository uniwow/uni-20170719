# encoding: utf-8

import random
import re
import datetime

from tornado.web import RequestHandler
from pyquery import PyQuery
import requests

from static.qmg_setting import headers,cookies,abuyun_proxy,redis_record,thread_pool,proxy_list
from dts_weixin_account_monitor_model import DtsWeixinAccountMonitorTask


def submit_article_monitor_task(article_urls: list, *, user ="1185694600@qq.com", password="111111"):

    login_url = "http://www.womdata.com/site/login"
    submit_api = "http://www.womdata.com/weixin/article-monitor/create-task"

    with requests.session() as session:
        r = session.get(login_url)
        pq = PyQuery(r.text)

        session.post(login_url, data={
            "_csrf-frontend": pq.find("#w0 input[name='_csrf-frontend']").attr("value"),
            "LoginForm[username]": user,
            "LoginForm[password]": password,
            "LoginForm[rememberMe]": "1"
        })

        session.get("http://www.womdata.com/weixin/article-monitor/task-list")

        for article_url in article_urls:
            session.post(submit_api, data={
                "article_url": article_url,
                "task_end_time": 86400,
                "task_interval_freq": 600,
                "point": 0
            })


def stamp_to_str(stamp):
    return datetime.datetime.fromtimestamp(int(stamp)).strftime('%Y%m%d')


def weixin_account_monitor(*args):
    """
    微信公众号最新文章检测
    """
    (uuid,) = args
    retry_count = 5
    # print(uuid[1].decode("utf-8"))
    try:
        record = DtsWeixinAccountMonitorTask.get(DtsWeixinAccountMonitorTask.uuid == uuid)
    except DtsWeixinAccountMonitorTask.DoesNotExist:
        print(uuid,"not exists...")
        return
    
    print(uuid,record.public)
    while True and retry_count > 0:
        print("http://weixin.sogou.com/weixin?type=1&s_from=input&query=%s" % record.public)
        # proxy = proxy_list[random.choice(list(range(len(proxy_list))))]
        # proxies = {"http":proxy["ip:port"]}
        
        proxyHost = abuyun_proxy["server"]
        proxyPort = abuyun_proxy["port"]
        proxy_user= abuyun_proxy["tunnels"][random.choice([0,1,2])]
        proxyUser = proxy_user["user"]
        proxyPass = proxy_user["password"]
        proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
              "host" : proxyHost,
              "port" : proxyPort,
              "user" : proxyUser,
              "pass" : proxyPass,
            }
        proxies = {"http":proxyMeta}
        print(proxies)
        response = requests.get(
            "http://weixin.sogou.com/weixin?type=1&s_from=input&query=%s&ie=utf8&_sug_=n&_sug_type_=" % record.public,
            headers=headers, proxies=proxies,timeout=10)
        doc = PyQuery(response.text)
        if len(doc("label[name=em_weixinhao]")) == 0:
            retry_count = retry_count - 1
            print("抓取失败，正在重试...")
            if retry_count == 0:
                pass
                # TODO(......................)
            continue
        else:
            item = doc("label[name=em_weixinhao]")[0]
            """如果是该公众号，进入主页"""
            if item.text_content() == record.public:
                if len(doc("span>script")) > 0:
                    last_publish_time = int(re.search(r"\d+",doc("span>script")[0].text_content()).group(0))
                    if record.last_post_time == -1 or \
                            (stamp_to_str(last_publish_time) != stamp_to_str(record.last_post_time)):
                        retry_count = 5
                        while True or retry_count > 0:
                            proxyHost = abuyun_proxy["server"]
                            proxyPort = abuyun_proxy["port"]
                            proxy_user= abuyun_proxy["tunnels"][random.choice([0,1,2])]
                            proxyUser = proxy_user["user"]
                            proxyPass = proxy_user["password"]
                            proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
                                  "host" : proxyHost,
                                  "port" : proxyPort,
                                  "user" : proxyUser,
                                  "pass" : proxyPass,
                                }
                            proxies = {"http":proxyMeta}
                            # proxy = proxy_list[random.choice(list(range(len(proxy_list))))]
                            # proxies = {"http":proxy["ip:port"]}
                            print(proxies)
                            headers["Host"] = "mp.weixin.qq.com"
                            response = requests.get(doc("a[uigs=account_name_0]")[0].attrib["href"], headers = headers,cookies = cookies,timeout=10)
                            # print(response.text)
                            # input("........................")
                            """返回验证码，重试"""
                            if "验证" in response.text:
                                retry_count = retry_count - 1
                                if retry_count == 0:
                                    pass
                                    # TODO(......................)
                                continue
                            else:
                                for line in response.text.split("\n"):
                                    if "var msgList" in line:
                                        article_url_list = []
                                        post_time_list = []
                                        article_list = eval(line.split(" = ")[1].replace(";",""))["list"]
                                        if len(article_list) == 0:
                                            break
                                        article_url_pre = "https://mp.weixin.qq.com"
                                        for article in article_list:
                                            """如果最近文章列表有大于最近发布时间的，发送给文章监控，并更新最近发布时间"""
                                            if int(article["comm_msg_info"]["datetime"]) > record.last_post_time and record.start_time < int(article["comm_msg_info"]["datetime"]):
                                                post_time_list.append(int(article["comm_msg_info"]["datetime"]))
                                                article_url_list.append(article_url_pre + article["app_msg_ext_info"]["content_url"].replace("&amp","&"))
                                                if "multi_app_msg_item_list" in article["app_msg_ext_info"]:
                                                    for sub_article in article["app_msg_ext_info"]["multi_app_msg_item_list"]:
                                                        article_url_list.append(article_url_pre + sub_article["content_url"].replace("&amp","&"))
                                        """ 获取文章列表 """
                                        submit_article_monitor_task(article_url_list)
                                        print(article_url_list,post_time_list)
                                        DtsWeixinAccountMonitorTask.update(last_post_time = max(post_time_list)).where(
                                            DtsWeixinAccountMonitorTask.uuid == uuid).execute()
                                        # input(".........................")
                                break
                    else:
                        print("最近文章已抓取")
                else:
                    print("没有显示最近发布文章")
            break


class WeixinAccountMonitor(RequestHandler):
    """
    接收执行任务所需参数，放入任务队列
    uuid : 任务唯一标识
    """
    def get(self):
        """
        接收uuid 直接传送给异步线程池
        """
        uuid = self.get_argument("uuid")
        if type(uuid) == type(b"aa"):
            uuid = uuid.decode("utf-8")
        thread_pool.apply_async(weixin_account_monitor, (uuid,))
        self.write(uuid.encode("utf-8"))
        """
        redis_conn = redis.Redis(host=redis_record["host"],
                            port=redis_record["port"],
                            password=redis_record["password"],
                            db=redis_record["db"])
        redis_conn.lpush("uni_test",self.get_argument("uuid"))
        订阅者模式
        # pubsub_channel = "task:weixin_account_monitor"
        # redis_conn.publish(pubsub_channel, self.get_argument("uuid"))
        # redis_conn.pubsub().subscribe('task:pubsub:channel')
        # redis_conn.publish(pubsub_channel, 1)

        ps = redis_conn.pubsub()
        ps.subscribe('task')
        for i in ps.listen():
            if i['type'] == 'message':
                print("Task get", i['data'])
        """