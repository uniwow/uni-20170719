###  QMG平台服务
--------------------------------------

启动方式：`python3 -m QMGPlatformService.application`

代码更新之后不虚手动重启，代码还自动重新加载


使用supervisor管理进程，启动了四个服务，默认分别是8885，8886,8887,8888四个端口

使用Nginx作为负载均衡器，配置文件详细请看：`nginx.conf`

使用案例：

`curl -XPOST -d '{"ad_budget": 100000, "ad_aim": 2}'  http://admin:admin@server_ip:8088/recommend?pretty=1`