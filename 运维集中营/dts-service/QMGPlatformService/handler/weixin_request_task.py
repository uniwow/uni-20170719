#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: weixin_request_task.py
@time: 2017/5/3 13:21
"""
from urllib import parse
import json
import pytz

from tornado.web import RequestHandler

from QMGPlatformService.common.pyredis import Redis
from QMGPlatformService import ConfigReader

timezone = pytz.timezone("Asia/Shanghai")

TASK_QUEUE_NAME = "WeixinRequestTaskQueue"


class WeixinRequestTaskHandler(RequestHandler):

    def post(self):
        """
        微信cookie pool 构建
        :return: 
        """

        method = self.get_argument("method")
        url = self.get_argument("url")
        headers = self.get_argument("headers")
        body = self.get_argument("body")
        headers = dict(list(map(lambda item: tuple(item.split("=")), headers.split("&"))))
        url_parse = parse.urlparse(url)
        url_parse_params = parse.parse_qs(url_parse.query)
        url_parse_params = {key: value[0] for key, value in url_parse_params.items()}

        write_result = {
            "success": True
        }

        if "action" in url_parse_params and url_parse_params["action"] == "home":

            pass_ticket = url_parse_params["pass_ticket"]
            redis = Redis(**dict(ConfigReader.items("redis")))
            redis.connection.rpush(TASK_QUEUE_NAME, json.dumps({
                "headers": headers,
                "url": url,
                "method": method,
                "body": body,
                "pass_ticket": pass_ticket
            }))

        self.write(write_result)
