#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
微信舆情，根据条件统计文章和账号数
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: wx_public_option.py
@time: 2017/4/10 16:36
"""

import json
from QMGPlatformService.common.ops_models import SogouWeixinAccountInitTask
from tornado.web import RequestHandler
from peewee import Does


class DtsMediaSearch(RequestHandler):
    def get(self):
        status = "success"
        message = "Success, Congratulations!"
        public_ids = self.get_query_argument("public_ids")
        public_id_list = public_ids.split(",")
        public_id_list = list(filter(lambda public_id: public_id is not None and public_id != "", public_id_list))
        SogouWeixinAccountInitTask.update(account_level=1).where(
            SogouWeixinAccountInitTask.account << public_id_list).execute()
        self.write(json.dumps({
            "status": status,
            "message": message
        }))

