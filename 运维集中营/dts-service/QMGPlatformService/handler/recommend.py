#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

import json

from tornado.web import RequestHandler

from QMGPlatformService.common.algorithm import SimpleAlgorithm


class RecommendHandler(RequestHandler):

    LAST_ACCOUNTS_LOAD_TIMESTAMP = -1

    def get(self):
        self.write("""
            <pre>
                POST 参数：
                    ad_aim: 广告目的，默认为 1 品牌宣传， 2 广告销售
                    media_cate: 账号分类 默认为：26 其他，也就是不限制分类，多个分类以,区分
                    media_area：账号所在区域，默认为：0 全国，对地域不做限制，多个地域以,区分
                    media_count: 要求推荐的账号数目，默认：12
                    ad_budget: 广告预算，默认为：100000
                    max_price_float：最大价格浮动，默认为2.0
                    per_price_float：每次价格浮动，默认为0.05
                    reference_from：参考方向 1 价格  2 阅读数
                URL 参数：
                    pretty 是否结果美化
                例子：
                    curl -XPOST -d '{"ad_aim": 2}'  http://admin:pwd@139.129.237.240:8088/recommend?pretty
            </pre>
        \n""")

    def post(self):
        """
        params:
            ad_aim: 广告目的，默认为 1 品牌宣传， 2 广告销售
            media_cate: 账号种类 默认为：26 其他，也就是不限制种类
            media_area：账号所在区域，默认为：0 全国，对地域不做限制
            media_count: 要求推荐的账号数目，默认：12
            ad_budget: 广告预算，默认为：100000
            max_price_float：最大价格浮动，默认为2.0
            per_price_float：每次价格浮动，默认为0.05
            reference_from：参考方向 1 价格  2 阅读数
        :return: 
        """
        pretty = bool(self.get_query_argument("pretty", default=False))

        params = {
            "ad_aim": self.get_argument("ad_aim", 1),
            "media_cate": self.get_argument("media_cate", "26"),
            "media_area": self.get_argument("media_area", "0"),
            "media_count": self.get_argument("media_count", 12),
            "ad_budget": self.get_argument("ad_budget", 10 ** 5),
            "max_price_float": self.get_argument("max_price_float", 2.0),
            "per_price_float": self.get_argument("per_price_float", 0.05),
            "reference_from": self.get_argument("reference_from", 1),
        }

        algorithm = SimpleAlgorithm(**params)
        result = algorithm.run()
        total_price = total_read_num = 0
        for index, account in enumerate(result):

            if params["reference_from"] == 1:
                total_price += account[account["price_from"]]
            else:
                for item in ("multi_1_price", "multi_2_price", "single_price"):
                    if account[item] > 0:
                        total_price += account[item]
                        break
            total_read_num += account["head_view_media_num"]
            if result[index]["fans_num"] <= 0:
                result[index]["fans_num"] = "--"
            else:
                result[index]["fans_num"] = "%.1f万" % float(result[index]["fans_num"] / 10000)
            result[index]["wom_index"] = format(result[index]["wom_index"], ",")
            result[index]["head_view_media_num"] = format(result[index]["head_view_media_num"], ",").replace(".0", "")
            result[index]["head_like_media_num"] = format(result[index]["head_like_media_num"], ",").replace(".0", "")

        self.__write_cors_headers()

        self.write(json.dumps({
            "success": True,
            "accounts": result,
            "total_price": round(total_price, 2),
            "total_read_num": int(total_read_num),
        }, ensure_ascii=False, indent=None if pretty is False else 4))

    def options(self):
        self.__write_cors_headers()

    def __write_cors_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, HEAD')

