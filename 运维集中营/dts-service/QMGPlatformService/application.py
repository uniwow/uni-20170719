#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.options import options, define

from QMGPlatformService.handler import *
from QMGPlatformService import package_name


if __name__ == "__main__":

    define("port", default=8886, help="Specify port number for server", metavar="8886")
    options.parse_command_line()

    handlers = [
        (r"/recommend", RecommendHandler),
        (r"/wxpo", WXPublicOption),
        # (r"/wordle", WordleHandler),
        (r"/mediasearch", DtsMediaSearch),
        (r"/wx-request-task", WeixinRequestTaskHandler),
    ]

    application = Application(handlers=handlers,
                              static_path="./%s/static/" % package_name,
                              autoreload=True)
    application.listen(int(options.port), xheaders=True, address="127.0.0.1")
    IOLoop.current().start()
