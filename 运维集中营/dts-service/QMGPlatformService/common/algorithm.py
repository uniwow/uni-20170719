#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
智能推荐算法实现
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: algorithm.py
@time: 2017/4/1 11:00
"""
from abc import ABCMeta, abstractmethod
from pprint import pprint
import math
import random

import pandas as pd

from QMGPlatformService import package_name


class BaseAlgorithm(object):

    """
    智能推荐算法基类
    """

    __metaclass__ = ABCMeta

    def __init__(self, **kwargs):

        # 推荐算法的基本因子
        self.ad_aim = int(kwargs.get("ad_aim", 1))
        self.media_cate = "|".join(list(map(lambda s: "(#%s#)" % s,
                                            filter(lambda s: len(s) > 0,
                                                   kwargs.get("media_cate", "26").strip().split(",")))))
        self.media_area = "|".join(list(map(lambda s: "(#%s#)" % s,
                                            filter(lambda s: len(s) > 0,
                                                   kwargs.get("media_area", "0").strip().split(",")))))
        self.media_count = int(kwargs.get("media_count", 12))
        self.ad_budget = int(kwargs.get("ad_budget", 10 ** 5))
        self.max_price_float = float(kwargs.get("max_price_float", 2.0))
        self.per_price_float = float(kwargs.get("per_price_float", 0.05))
        self.reference_from = int(kwargs.get("reference_from", 1))

        self.price_from = ("multi_1_price",
                           "multi_2_price", "single_price") if self.reference_from == 1 else ("head_view_media_num",)

        # 计算每种类别下账号的数量
        self.__calculate_account_number()

        # 计算配价因子
        self.price_factor = random.randint(int(self.media_count / 2) - 1, int(self.media_count / 2) + 1)
        self.price_factor = self.price_factor if self.price_factor > 0 else 1

        self.accounts = pd.read_csv("./%s/static/wx_public_option/media_weixin.csv" % package_name)
        if self.media_area != "(#0#)":
            self.accounts = self.accounts[self.accounts.media_area.str.contains(self.media_area)]
        if self.media_cate != "(#26#)":
            self.accounts = self.accounts[self.accounts.media_cate.str.contains(self.media_cate)]

    def __calculate_account_number(self):
        """
        最终推荐给用户的账号分为：
            主推账号（chief），次推账号(secondary)，搭配账号(collocation)，纠正账号(correct)
        :return: 
        """

        chief = int(self.media_count / self.media_count)
        secondary = math.ceil(self.media_count / 7)
        collocation = math.ceil(self.media_count / 5)
        correct = self.media_count - chief - secondary - collocation
        correct = correct if correct > 0 else 0

        self.chief_num = chief
        self.secondary_num = secondary
        self.collocation_num = collocation
        self.correct_num = correct

    def choose_accounts(self, base_price, account_number, recommend_accounts,
                        choose_order=("multi_1_price", "multi_2_price", "single_price")):
        """
        根据用提供的基准价格，每次上下浮动float_point个百分点，选择account_number个账号，
          按照多图文头条(multi_1_price)，多图文次条(multi_2_price)，单图文的顺序(single_price)
        :param base_price: 
        :param account_number: 
        :param choose_order
        :param recommend_accounts 已经推荐的账号
        :return: 
        """
        assert isinstance(recommend_accounts, list)
        accounts = []

        base_price = 500 if base_price <= 0 else base_price

        if base_price > 0:
            for price_column in choose_order:

                def add_column(item):
                    item["price_from"] = price_column
                    item["avatar"] = "http://open.weixin.qq.com/qr/code/?username=%s" % item["public_id"]
                    return item

                if len(accounts) < account_number:
                    price_float = 0.0
                    while price_float <= self.max_price_float and len(accounts) < account_number:
                        price_float += self.per_price_float
                        price_top_line = base_price + base_price * price_float
                        price_down_line = base_price - base_price * price_float
                        chosen_accounts = self.accounts.loc[
                            (self.accounts[price_column] > 0)
                            &
                            (self.accounts[price_column] >= price_down_line)
                            &
                            (self.accounts[price_column] <= price_top_line)
                            ].to_dict(orient="records")

                        chosen_accounts = list(sorted(chosen_accounts,
                                                      reverse=True,
                                                      key=self.account_compare))[0:account_number * 5]

                        chosen_accounts = list(map(add_column, chosen_accounts))
                        needed_account_number = account_number - len(accounts)
                        while len(chosen_accounts) > 0 and needed_account_number >= 0 and len(accounts) < account_number:
                            random_index = random.choice(range(len(chosen_accounts)))
                            account = chosen_accounts.pop(random_index)
                            if account["public_id"] not in recommend_accounts:
                                accounts.append(account)
                                recommend_accounts.append(account["public_id"])
                                needed_account_number -= 1
                else:
                    break
        return accounts

    @staticmethod
    def sum_account_money(accounts):
        money = 0
        for account in accounts:
            money += account[account["price_from"]]
        return money

    def calculate_base_price(self, rest_budget):
        base_price = 0
        if rest_budget > 0:
            base_price = (rest_budget / self.media_count) * self.price_factor
        return base_price

    @abstractmethod
    def result(self) -> list:
        pass

    def account_compare(self, item):

        head_avg_view_num = float(item["head_view_media_num"]) + 1
        head_avg_like_num = float(item["head_like_media_num"]) + 1

        head_avg_view_num = head_avg_view_num if head_avg_view_num > 1 else 1
        head_avg_like_num = head_avg_like_num if head_avg_like_num > 1 else 1

        if self.ad_aim == 1:
            return 0.7 * math.log(head_avg_view_num) + 0.5 * math.log(head_avg_like_num)
        elif self.ad_aim == 2:
            return 0.5 * math.log(head_avg_view_num) + 0.5 * math.log(head_avg_like_num)

    def run(self):
        result = self.result()
        handle_result = list(map(self.__class__.__translate_account_cate, result))
        return handle_result

    @staticmethod
    def __translate_account_cate(account: dict) -> dict:
        categories = {
            "1": "新闻资讯", "2": "生活", "3": "段子手", "4": "汽车", "5": '时尚', "6": '美容美妆', "7": 'IT/互联网',
            "8": '电商', "9": '科技数码', "10": '母婴/育儿', "11": '教育培训', "12": '健康养生', "13": '家居房产',
            "14": '美食', "15": '游戏动漫', "16": '娱乐八卦', "17": '旅游/摄影', "18": '影视音乐', "19": '金融财经',
            "20": '广告/营销', "21": '职场/管理', "22": '名人/艺人', "23": '情感/星座', "24": '文化创意',
            "25": '体育健身', "26": '其他'
        }
        areas = {
            "0": '全国', "1": '北京', "2": '天津', "3": '河北', "4": '山西', "5":  '内蒙古', "6": '辽宁', "7": '吉林',
            "8": '黑龙江', "9": '上海', "10": '江苏', "11": '浙江', "12": '安徽', "13": '福建', "14": '江西',
            "15": '山东', "16": '河南', "17": '湖北', "18": '湖南', "19": '广东', "20": '广西', "21": '海南',
            "22": '重庆', "23": '四川', "24": '贵州', "25": '云南', "26": '西藏', "27": '陕西', "28": '甘肃', "29": '青海',
            "30": '宁夏', "31": '新疆', "32": '台湾', "33": '香港', "34": '澳门', "35": '海外', "36": "其他",
            "289": '广州', "291": '深圳', "162": '南京', "175": '杭州', "385": '成都', "73": '石家庄', "84": '太原',
            "107": '沈阳', "108": '大连', "121": '长春', "130": '哈尔滨', "166": '苏州', "163": '无锡', "176": '宁波',
            "186": '合肥', "203": '福州', "204": '厦门', "212": '南昌', "223": '济南', "224": '青岛', "240": '郑州',
            "258": '武汉', "275": '长沙', "310": '南宁', "324": '海口',"406": '贵阳', "415": '昆明', "438": '西安',
            "448": '兰州', "462": '西宁'
        }
        if not isinstance(account["media_cate"], list):
            cate_list = []
            for number in list(filter(lambda x: len(x) > 0, account["media_cate"].split("#"))):
                if number in categories:
                    cate_list.append(categories[number])
                else:
                    cate_list.append("其他")
            if len(cate_list) > 0:
                account["media_cate"] = cate_list

        if not isinstance(account["media_area"], list):
            area_list = []
            for number in list(filter(lambda x: len(x) > 0, account["media_area"].split("#"))):
                if number in areas:
                    area_list.append(areas[number])
                else:
                    area_list.append("全国")
            if len(area_list) > 0:
                account["media_area"] = area_list
        return account


class SimpleAlgorithm(BaseAlgorithm):

    def __init__(self, **kwargs):
        super(SimpleAlgorithm, self).__init__(**kwargs)

    def result(self):

        result = []
        accounts_list = []
        rest_budget = self.ad_budget
        # 选择主推账号
        chief_accounts = self.choose_accounts(base_price=self.calculate_base_price(rest_budget),
                                              account_number=self.chief_num,
                                              choose_order=self.price_from,
                                              recommend_accounts=accounts_list)
        result += chief_accounts
        # 选择次推账号
        rest_budget -= self.sum_account_money(chief_accounts)
        secondary_accounts = self.choose_accounts(base_price=self.calculate_base_price(rest_budget),
                                                  account_number=self.secondary_num,
                                                  choose_order=self.price_from,
                                                  recommend_accounts=accounts_list)
        result += secondary_accounts
        # 搭配账号
        rest_budget -= self.sum_account_money(secondary_accounts)
        collocation_accounts = self.choose_accounts(base_price=self.calculate_base_price(rest_budget),
                                                    account_number=self.collocation_num,
                                                    choose_order=self.price_from,
                                                    recommend_accounts=accounts_list)
        result += collocation_accounts
        # 纠正账号
        rest_budget -= self.sum_account_money(collocation_accounts)
        correct_accounts = self.choose_accounts(base_price=self.calculate_base_price(rest_budget),
                                                account_number=self.correct_num,
                                                choose_order=self.price_from,
                                                recommend_accounts=accounts_list)
        result += correct_accounts

        return result


if __name__ == '__main__':

    recommend = SimpleAlgorithm()
    pprint(recommend.run())
