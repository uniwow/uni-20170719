#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
继承自 tornado.web.RequestHandler 用于对api请求做访问控制
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: BaseRequestHandler.py
@time: 2017/3/31 11:51
"""

from functools import wraps
import base64

import tornado.web

from QMGPlatformService.users import USERS


def authenticate(module_name="default"):

    def decorate(api):

        @wraps(api)
        def wrapper(*args, **kwargs):
            instance = args[0]
            assert isinstance(instance, tornado.web.RequestHandler)
            if "Authorization" in instance.request.headers:
                signature = instance.request.headers["Authorization"].split(" ")[1]
                user, pwd = base64.b64decode(signature.encode()).decode("utf-8").split(":")
                if module_name in USERS:
                    for item in USERS[module_name]:
                        if user == item[0] and pwd == item[1]:
                            return api(*args, **kwargs)
            instance.clear()
            instance.set_status(401)
            instance.add_header("WWW-Authenticate",  "Basic realm=\"user and password?\"")

        return wrapper

    return decorate

