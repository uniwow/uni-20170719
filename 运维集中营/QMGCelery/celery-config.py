from kombu import Queue
from datetime import timedelta
beat_schedule = {
    "add":{
        "task":"QMGCelery.tasks.add",
        "schedule":timedelta(seconds=10),
        "args":(16,16)
    }
}
task_queues = (Queue("default", routing_key = "task.#"),
    Queue("web_tasks",routing_key = "web.#"),
    )
task_default_exchange = "tasks"
task_default_exchange_type = "topic"
task_default_routing_key = "task.default"
task_routes = {
    "QMGCelery.tasks.add":{
        "queue":"web_tasks",
        "routing_key":"web.add",
    }
}
broker_url = "redis://139.129.196.32:6379/1"
result_backend = "redis://139.129.196.32:6379/2"
# task_serializer = "msgpck"
# result_serializer = "json"
result_expires = 60 * 60 *24
accept_content = ["json","msgpack"]