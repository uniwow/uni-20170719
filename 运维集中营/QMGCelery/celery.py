from __future__ import absolute_import
from celery import Celery 

app = Celery("QMGCelery",include=["QMGCelery.tasks"])
app.config_from_object("QMGCelery.celery-config")

if __name__ == '__main__':
    app.start()