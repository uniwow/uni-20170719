from cement.core.foundation import CementApp
from cement.core import hook
from cement.utils.misc import init_defaults
from cement.core.controller import CementBaseController, expose
from cement.core.interface import Interface, Attribute

defaults = init_defaults("qmgapp")
defaults["qmgapp"]["debug"] = False
defaults["qmgapp"]["version"] = "1.0.0"
defaults["qmgapp"]["func"] = "func_0"

def my_cleanup_hook(qmgapp):
    print(".......................")
    pass


class MyBaseController(CementBaseController):
    class Meta:
        label = 'base'
        description = "My Application does amazing things!"
        arguments = [
            ( ['-C',"--certain"],
              dict(action='store_true', help='the big C option') ),
            ]

    @expose(hide=True)
    def default(self):
        self.app.log.info('Inside MyBaseController.default()')
        if self.app.pargs.certain:
            print("Recieved option: foo => %s" % self.app.pargs.foo)

    @expose(help="this command does relatively nothing useful")
    def command1(self):
        self.app.log.info("Inside MyBaseController.command1()")

    @expose(aliases=['cmd2'], help="more of nothing")
    def command2(self):
        self.app.log.info("Inside MyBaseController.command2()")


# class MySecondController(CementBaseController):
#     class Meta:
#         label = 'second'
#         stacked_on = 'base'

#     @expose(help='this is some command', aliases=['some-cmd'])
#     def second_cmd1(self):
#         self.app.log.info("Inside MySecondController.second_cmd1")

class QMGApp(CementApp):
    class Meta:
        label = 'qmgapp'
        config_defaults = defaults
        base_controller = MyBaseController
        handlers = [MyBaseController]
        extensions = ['daemon', 'memcached', 'json', 'yaml']
        hooks = [
            ('pre_close', my_cleanup_hook),
        ]

with QMGApp() as qmgapp:
    # add arguments to the parser
    qmgapp.args.add_argument('-f', '--func', action='store', metavar='STR',
                          help='the label of function')
    # qmgapp.log.debug("About to run my myapp application!")
    qmgapp.run()
    if qmgapp.pargs.func:
        qmgapp.log.info("Received option: foo => %s" % qmgapp.pargs.func)