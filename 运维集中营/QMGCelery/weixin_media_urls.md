[账号主页链接 GET ](https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MjM5MjE0MTcyMA==)  
备注：  
1. 用户主页  
2. get 请求  
3. 需要cookie  
4. 不需要关注  
5. 加入&file=json偶尔可以返回json结构，但不稳定。暂时不予考虑  

[认证信息链接 GET ](http://mp.weixin.qq.com/mp/getverifyinfo?__biz=MjM5MjE0MTcyMA==)  
备注：  
1. 认证消息页  
2. get 请求  
3. 需要cookie  
4. 不需要关注  

[历史文章页链接 GET ](https://mp.weixin.qq.com/mp/profile_ext?action=getmsg&__biz=MjM5MjE0MTcyMA==&f=json&frommsgid=1000001324&count=10)  
备注：  
1. 历史文章页  
2. get 请求  
3. 需要cookie  
4. 需要关注  
5. 每次只能获取十次群发数据  

[文章评论页链接 GET ](https://mp.weixin.qq.com/mp/appmsg_comment?action=getcomment&__biz=MjM5MjE0MTcyMA==&comment_id=1762003255&offset=0&limit=100)  
备注：文章评论页  
1. 用户主页  
2. get 请求  
3. 需要cookie  
4. 不需要关注  
https://mp.weixin.qq.com/mp/getappmsgext?__biz=MjM5MjE0MTcyMA==&mid=2673258822&idx=1

[阅读点赞页链接 POST ](https://mp.weixin.qq.com/mp/getappmsgext?__biz=MjM5MjE0MTcyMA==&mid=2673258822&sn=df0183329a0d787b75199b7deb4cf5be&idx=1)  
备注：  
1. 阅读点赞页  
2. post 请求  
3. 需要cookie  
https://mp.weixin.qq.com/mp/getappmsgext?__biz=MzAwMDAyMzY3OA==&mid=2664450890&sn=df0183329a0d787b75199b7deb4cf5be&idx=1
var biz = ""||"MzAwMDAyMzY3OA==";
    var sn = "" || ""|| "";
    var mid = "" || ""|| "2664450890";
    var idx = "" || "" || "1";
    window.__allowLoadResFromMp = true; 


post data

```
is_only_read=1&req_id=03161tquEUWtm8aKhkXepTf3&pass_ticket=L1%25252BpMMZuGWujUvxHmcxJpZQzlNaagO1y0Tbeoj5GENjKXb2e4yN3nhtzyWZn1ykA&is_temp_url=0
```

```
headers:
User-Agent,Origin,Cookie必须添加,其他可以去掉

Host:mp.weixin.qq.com
User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 MicroMessenger/6.5.2.501 NetType/WIFI WindowsWechat QBCore/3.43.373.400 QQBrowser/9.0.2524.400
Origin:https://mp.weixin.qq.com
Referer:https://mp.weixin.qq.com/s?__biz=MjM5MjE0MTcyMA==&mid=2673258822&idx=1&sn=df0183329a0d787b75199b7deb4cf5be&chksm=bc08c9b18b7f40a7d3b53454ef8e8ccc8b82e404bf592ce1bc27f4565cc1bdc267b768f31652&scene=27&key=d96819ed7aeab1eb4fa7886ba930adedad69eb9e32ce23584f14153ed2e826d44aece0824049a83773827f6429c8636038129204f07d47fdacb5a4588ef33d915111abc6030bea3762271726c62318cc&ascene=1&uin=MjQ2OTkzNjMyNw%3D%3D&devicetype=Windows+8&version=6204014f&pass_ticket=L1%2BpMMZuGWujUvxHmcxJpZQzlNaagO1y0Tbeoj5GENjKXb2e4yN3nhtzyWZn1ykA&winzoom=1
Cookie:rewardsn=6981eeb4b8d9fba35d97; wap_sid=CMf54JkJEkB4MW1kUnlSbHhKcXI5UjNyTmdhbVUtLXlDS3RhTGlCWV9tYzVPZG5MMEp4SWN1TkhFbHVuNjBuNDNtb0Zrd2MwGAQg/BEomN/U9Agw/6qmyAU=; wap_sid2=CMf54JkJEnA4WkFGRUg1Rjk5M2F1MmYwZ2tsT0ktM2tYeWV5NnA3bUtISFh4NWcyLWs1cHdPTDNySWp6R0xRdzVqMWJNRGJ5eXlDWnlqQ0hpSDFCcEtIU1F6Rm9qNVM3YzZEbkVqS1lvWVF4c1JYTWZSbUhBd0FBMP+qpsgF; pass_ticket=L1+pMMZuGWujUvxHmcxJpZQzlNaagO1y0Tbeoj5GENjKXb2e4yN3nhtzyWZn1ykA; wxtokenkey=351e68bda96b87f6d88f130b85adeb62d6a30924b2d0b3c9f376eac4c1de33f4; wxticket=2760021606; wxticketkey=e8115969fe7c777717c64e47c6677385d6a30924b2d0b3c9f376eac4c1de33f4
```

截止目前我们了解的参数  
1. __biz 微信公众号唯一标识  
2. comment_id 和文章唯一对应的评论页参数  
3. mid 一次群发唯一标识  
4. ids 一次群发消息位置  
5. sn 一串解决历史问题的随机码  
6. scene 场景参数  
7. "copyright_stat":11 原创


headers={
        "Host":"mp.weixin.qq.com",
        "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 MicroMessenger/6.5.2.501 NetType/WIFI WindowsWechat QBCore/3.43.373.400 QQBrowser/9.0.2524.400",
        "Origin":"https://mp.weixin.qq.com"
        }
        
cookies = {
        "pgv_info":"ssid=s5251259248",
        "pgv_pvid":"9543468272",
        "wxtokenkey":"1eabf221acf19081973824523848eec3daf3f6a6228fc4f7e14495803f42e624",
        "wxticket":"2300074243",
        "wxticketkey":"bcb47b27edf6c7af82227157cd709904daf3f6a6228fc4f7e14495803f42e624",
        "wap_sid":"CMf54JkJEkBVM1hIVzFVUGxrQ0J6X2gzU0tBWndtcTFvaEVmQXAtU1lOdVVSUlBIM0tYS2lwakdXZVZuNEx0NG5yZ190RXVDGAQg/BEomN/U9Agws52wyAU=",
        "wap_sid2":"CMf54JkJElxmS1h5c19fX2xFUWU4X3A0Q3pTZkEwZ2RDTk5ValFYWEtscDdSckhESFBWNGNOcEhTdENPbldONVRVUHNRWGxEc1prTWdYelZLZnluOUN6ZHdMYm9pSWNEQUFBfjCznbDIBQ==",
        "pass_ticket":"H6bRYnkCORJblyRzA0ZnzHfanxCahrxcvNsZtIqZyseGN2QaK6z/ErZqoQXQtx68",
        "wap_sid":"CMf54JkJEkByZ1JHM2Jnb25hdnBQZEJLN2xEX0dsMjEwYk1oRjFySFBvOGhHQUhwUk5hR0ZaQ0hseU5iTjVpbVZxVzVZbkR0GAQgpBQorbKaxQsw3MywyAU=",
        "wap_sid2":"CMf54JkJElxUdklBaVliRnBJMGlVT0M2V2tyWHBTLTM0SkJjcEZlSGZ5Q29jYTQxVjViZEV4WUJPaWdqTXJkMGZ6NkY1UVphZExhcFFVSzc4RTFEYWJtMFNieFBTWWNEQUFBfjDczLDIBQ==",
        "pass_ticket":"H6bRYnkCORJblyRzA0ZnzHfanxCahrxcvNsZtIqZyseGN2QaK6z/ErZqoQXQtx68"
        }
url="https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MjM5MjE0MTcyMA=="
url="https://mp.weixin.qq.com/mp/appmsg_comment?action=getcomment&__biz=MjM5MjE0MTcyMA==&comment_id=1762003255&offset=0&limit=100"
url="https://mp.weixin.qq.com/mp/getappmsgext?__biz=MjM5MjE0MTcyMA==&mid=2673258822&sn=df0183329a0d787b75199b7deb4cf5be&idx=1"
data={
        "is_only_read":1,
        "req_id":"03161tquEUWtm8aKhkXepTf3",
        "pass_ticket":"H6bRYnkCORJblyRzA0ZnzHfanxCahrxcvNsZtIqZyseGN2QaK6z%25252FErZqoQXQtx68",
        "is_temp_url":0,
    }

result=requests.get(url=url,
            headers=headers,
            cookies=cookies)
result=requests.post(url=url,
            headers=headers,
            data=data,
            cookies=cookies)

urls=["http://mp.weixin.qq.com/mp/getverifyinfo?__biz=MjM5MjE0MTcyMA=="]
rs = (grequests.post(url=u,
            headers=headers,
            data=data,
            cookies=cookies) for u in urls)
result=grequests.map(rs)
urls=["http://mp.weixin.qq.com/mp/getverifyinfo?__biz=MjM5MjE0MTcyMA=="]
rs = (grequests.get(url=u,
            headers=headers,
            cookies=cookies
            ) for u in urls)
result=grequests.map(rs)

result=requests.get(url=url,
            headers=headers,
            cookies=cookies)
url="http://mp.weixin.qq.com/mp/getverifyinfo?__biz=MjM5MjE0MTcyMA=="

import aiohttp
import asyncio
html = None
async def fetch(client):
    async with client.get(url,allow_redirects=True) as resp:
        assert resp.status == 200
        return  await resp.text()

async def main(loop,headers,cookies):
    async with aiohttp.ClientSession(loop=loop,headers=headers,
            cookies=cookies) as client:
        html = await fetch(client)
        for line in html.split("\n"):
            if "var jsonData" in line:
                print(line)

loop = asyncio.get_event_loop()
loop.run_until_complete(main(loop,headers,cookies))




wxtokenkey=9c60360e0626b305ce4b148d9da30a62035d44e2450543f9dad0a65b9f93d63a;wxticket=1266502935;wxticketkey=cd52eca0fe54544c9e85d6fd2865db1e035d44e2450543f9dad0a65b9f93d63a;wap_sid=CMf54JkJEkBkYWw3WElFQUFqVzMwTFAyb3Nic0NUalZlZzdlQWc4THFFM093QWs0OFprOTRsdEpWWW5oeHpTc3A1SEY4eGZRGAQg/BEomN/U9Agwmbm6yAU=;wap_sid2=CMf54JkJElxaVXZYVnUwa3N2RUJlOFhJVGtLdFdYbnRyZ3hLSnRPNGZya0d1NlFNbklndzlGd1M2NS1NQnFvenY1WkZFakpZYzRERlI3eURwNjhOMkhKRVBkSExkb2NEQUFBfjCZubrIBQ==;pass_ticket=O/9MZgZaMihmN9ikaMeOh9zXT2QaY8xXcXRE7irUuLbbmYK8ZeYgSXRKGxYOHWkl


POST https://mp.weixin.qq.com/mp/getappmsgext?__biz=MjM5MjE0MTcyMA==&appmsg_type=9&mid=2673258981&sn=66abbeaa0ec4626214f305853e501b70&idx=1&scene=27&title=%E5%A4%8F%E6%97%A5%E6%B4%BE%E5%AF%B9%20%7C%20%E7%A9%BF%E6%88%90%E8%BF%99%E6%A0%B7%E6%89%8D%E6%98%AF%E9%9F%B3%E4%B9%90%E8%8A%82%E5%B7%A1%E6%B8%B8%E8%80%85%EF%BC%81&ct=1493891166&abtest_cookie=&devicetype=Windows%208&version=/mmbizwap/zh_CN/htmledition/js/appmsg/index358e2e.js&f=json&r=0.1659126803278923&is_need_ticket=1&is_need_ad=0&comment_id=2882266163&is_need_reward=0&both_ad=1&reward_uin_count=0&msg_daily_idx=1&uin=MjQ2OTkzNjMyNw%253D%253D&key=66acbcb3d7f816b02ecf55ba8a40380ace1c3de92ae5b4bdd164fde677909b75e444c7181452f6d806b9b149759c0e90d597c47e01fbf3bf4c8a6c5d39031e37b6e496e30a4dc58d9fc55e3e50aa37a4&pass_ticket=O%25252F9MZgZaMihmN9ikaMeOh9zXT2QaY8xXcXRE7irUuLbbmYK8ZeYgSXRKGxYOHWkl&wxtoken=4078436317&devicetype=Windows%26nbsp%3B8&clientversion=6204014f&x5=0&f=json HTTP/1.1
Host: mp.weixin.qq.com
Connection: keep-alive
GET https://mp.weixin.qq.com/s?__biz=MjM5MjE0MTcyMA==&mid=2673258982&idx=1&sn=66abbeaa0ec4626214f305853e501b70&scene=27&key=37a4&ascene=1&uin=MjQ2OTkzNjMyNw%3D%3D&devicetype=Windows+8&version=6204014f&pass_ticket=O%2F9MZgZaMihmN9ikaMeOh9zXT2QaY8xXcXRE7irUuLbbmYK8ZeYgSXRKGxYOHWkl&winzoom=1
/s?__biz=MjM5MjE0MTcyMA==&mid=2673258981&idx=1&comment_id=420845864&scene=27&key=3ea6c9e46a21bfa9b1d8bc0db915023a56e110da77abc055006079eb8de7e49bf65e9790c3144385fe328345c0c1616f70e232ace0e61d38454f68db149948cfdaac48d3509cd1548c35627b9387e&ascene=1&uin=MjQ2OTkzNjMyNw%3D%3D&devicetype=Windows+8&version=6204014f&pass_ticket=O%2F9MZgZaMihmN9ikaMeOh9zXT2QaY8xXcXRE7irUuLbbmYK8ZeYgSXRKGxYOHWkl&winzoom=1 HTTP/1.1

var biz = ""||"MzAwMDAyMzY3OA==";  
 var sn = "" || ""|| "";  
 var mid = "" || ""|| "2664450963";  
 var idx = "" || "" || "1";  
 window.__allowLoadResFromMp = true; 

  https://mp.weixin.qq.com/s?__biz=MjM5MjE0MTcyMA==&mid=2673258981&idx=1&CMf54JkJElxMV3pTSmFSNkxUSTRzTW9VSFJBYUUtVXdyTjlGb0pMLWxTTjl6WGFjbjdkMzVFUnZqaWlVUHJGamZlbFNMTlBDOFVYeEc0TlcyTEZQRmN3Nk1rZVZqb2NEQUFBfjDDw8TIBQ
  chksm

  https://mp.weixin.qq.com/s?timestamp=1494295334&src=3&ver=1&signature=s6xzwDqgl4-LvsGHt-qQpFgg7NZUxa1G0C8MuURxaqUX3EHr1qkAcoETS0FTz6TEdEPiJRNH5*ezdSN9*z*ulOVYWYUqyQNwIZu2Picxr7af7toIHSpdDSDn5oWBzoAJZxenr9jRr4lKaCpV2nD0xpyHToMHfRNtwWGUGCcpDdY=&uin=MjQ2OTkzNjMyNw%3D%3D&key=cc28ec1c0927c0f062af29bbf057e0170769b6e764333d2458688365f3fa530c8134e432ded012ce3f3760f35c2ab73619754710a8b2b4c1630b55bec472fe73b27d440608102f51fcc48c211cf63688&devicetype=Windows+8&version=6204014f&lang=zh_CN&a8scene=1&pass_ticket=Tw28TZ7QCfPIu1%2FOHCRONcuLID%2FQ4wuFEVPwRLktZYmU9YzG%2FX%2Bt1zDtmXJCpS3u&winzoom=1
  https://mp.weixin.qq.com/s?timestamp=1494295465&src=3&ver=1&signature=s6xzwDqgl4-LvsGHt-qQpLDxejgOCoAcrzsit-9IeI3maxlQJmmkuPxNf10zeo0enhMs26Tqljq1gedVAnGz4eDItmeIWtNRYTuu46fUeuR5QbWDv7GcOzAicfmOtyVzv1K6JIuNyMLjnw7dRgFbdlLQexRXxPrdvi1av*2VCCc=&wxtokenkey=4826e6db2b0607bf6ff144e43a47dd2b178477a4799bae40ad6c47c5ac727a50&wxticket=828707087&wxticketkey=35c176e9c72c09603acccee27b9a5132178477a4799bae40ad6c47c5ac727a50&wap_sid=CMf54JkJEkBGNjFINTY0S3RCOTJsdjZXU0JtbW9RU0JWU3NvZDIwRFBzYVQ0clVXV3FPaVJLdXVKVERvRkJ0M3p5RDJDWUlaGAQgpBQomN/U9Agww8PEyAU=&wap_sid2=CMf54JkJElxMV3pTSmFSNkxUSTRzTW9VSFJBYUUtVXdyTjlGb0pMLWxTTjl6WGFjbjdkMzVFUnZqaWlVUHJGamZlbFNMTlBDOFVYeEc0TlcyTEZQRmN3Nk1rZVZqb2NEQUFBfjDDw8TIBQ==&pass_ticket=Tw28TZ7QCfPIu1/OHCRONcuLID/Q4wuFEVPwRLktZYmU9YzG/X+t1zDtmXJCpS3u&uin=MjQ2OTkzNjMyNw%3D%3D


http://mp.weixin.qq.com/s?src=3&timestamp=1494297726&ver=1&signature=4b*L0VKZMplVN9mWp4FhXwMOrxJ*sGBTN9rcEjxVTsi*UAarxQysAJal77EThAoZrqPwxaV2odpemtOdCsGJ4wRULlO7WzLjfwPqbhfwgHYH34SEJ6PaHflMjwKm6LHBKq*b2cxKQowhHlyldg*EGK4pJbTE69OfdGCWGB32Q3U=&uin=MjQ2OTkzNjMyNw%3D%3D&pass_ticket=Tw28TZ7QCfPIu1/OHCRONcuLID/Q4wuFEVPwRLktZYmU9YzG/X+t1zDtmXJCpS3u