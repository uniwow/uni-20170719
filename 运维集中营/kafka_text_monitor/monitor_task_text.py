#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pymysql
from pykafka import KafkaClient
import json
import requests


KAFKA_HOSTS = "139.129.196.32:9092,139.129.196.32:9093,139.129.196.32:9094"
ZOOKEEPER_HOST = "139.129.196.32:2181,139.129.196.32:2182,139.129.196.32:2183"
MYSQL_DEV_CONFIG = {
    'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com',
     'user': 'wom',
     'password': 'Qmg2016mw#semys$',
     'db': 'wom_prod',
     'charset': 'utf8mb4'
}


class MysqlConnect:
    def __init__(self):
        try:
            self.connection = pymysql.connect(
                 host=MYSQL_DEV_CONFIG['host'],
                 user=MYSQL_DEV_CONFIG['user'],
                 password=MYSQL_DEV_CONFIG['password'],
                 db=MYSQL_DEV_CONFIG['db'],
                 charset=MYSQL_DEV_CONFIG['charset'],
                 cursorclass=pymysql.cursors.DictCursor)
        except Exception as e:
            print(e)
            pass


def main():
    while True:
        print('------------------开始一次循环')
        try:
            client = KafkaClient(hosts=KAFKA_HOSTS, socket_timeout_ms=3 * 1000)
            topic = client.topics[bytes('weixin_article_monitor_task_word_segment_topic', encoding='utf-8')]
            consumer = topic.get_balanced_consumer(
                reset_offset_on_start=True,
                consumer_group=bytes('mmm', encoding='utf-8'),
                auto_commit_enable=False,
                auto_commit_interval_ms=1000,
                zookeeper_connect=ZOOKEEPER_HOST
            )

            if consumer is not None:
                for message in consumer:
                    if message is not None:
                        msg = json.loads(message.value.decode('utf-8', 'ignore'))
                        print(msg)
                        task_uuid = msg['task_uuid']
                        article_title = msg['article_title']
                        article_content = msg['article_content']
                        article_digest = msg['article_digest']

                        wordle_json_orig = requests.post("http://114.215.110.161:9999/wordle", data={
                            'userId': 'test',
                            'secretId': 'test',
                            'title': str(article_title).replace('\n', '').strip(),
                            'content': str(article_content).replace('\n', '').strip(),
                            'digest': str(article_digest).replace('\n', '').strip(),
                        })
                        wordle_json = wordle_json_orig.text[1:-1].split(',')
                        wordle = []
                        try:
                            if len(article_content) > 1:
                                for tuple in wordle_json:
                                    wordle.append({
                                        "word": tuple.split(':')[0].strip()[1:-1],
                                        "value": tuple.split(':')[1].strip()
                                    })
                        except Exception as wordle_exception:
                            print('wordle has error.')
                            print(wordle_exception)
                            pass
                        wordle_json = json.dumps(wordle, ensure_ascii=False)
                        try:
                            print('start connect mysql')
                            mysql_connect = MysqlConnect()
                            mysql_connection = mysql_connect.connection
                            print('start connect mysql success')
                            with mysql_connection.cursor() as cursor:
                                sql = "UPDATE dts_weixin_article_monitor_task_output SET keyword_list = %s WHERE task_uuid = %s"
                                cursor.execute(sql, (wordle_json, task_uuid))
                                mysql_connection.commit()

                                print('update to mysql success where task uuid is %s.' % task_uuid)

                        except Exception as ee:
                            print(ee)
                    else:
                        print('msg of topic is None')
            else:
                print('msg of topic is None')
        except Exception as e:
            print('kafka connect error.')
            print(e)
        finally:
            mysql_connection.close()

if __name__ == '__main__':
    main()
