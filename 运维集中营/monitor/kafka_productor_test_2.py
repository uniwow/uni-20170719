# coding=utf-8
from pykafka import KafkaClient
import time
client = KafkaClient(
    hosts="kafka-1:9092, kafka-2:9092, kafka-3:9092")  # 可接受多个Client这是重点
topic = client.topics[b'server-161-2']



def kafka_producer():
    with topic.get_sync_producer(min_queued_messages=1000,auto_start=False,) as producer:

        while True:
            producer.start()
            for i in range(1, 100):
                for j in range(1, 100):
                    kafka_str=('test message ' + str(i) + ":" + str(j))
                    print(kafka_str)
                    producer.produce(kafka_str.encode("utf-8",'ignore'))
            producer.stop()
if __name__ == '__main__':
    kafka_producer()