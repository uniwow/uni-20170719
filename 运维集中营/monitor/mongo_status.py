# coding=utf-8
import json
import subprocess
import getopt
import sys
import os
import re


def mongo_server_stats(ip_port, user, pwd, db):
    cmd = "/usr/bin/echo 'db.serverStatus()' |mongo %s -u '%s' -p '%s' --authenticationDatabase '%s' --quiet" % (ip_port, user, pwd,  db)

    # result=os.system(cmd)
    # print(type(result))
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    result = p.stdout.readlines()

    # print(str(result).replace("\\t","").replace("\\n","").replace("'",""))
    index = -1
    result_str = ""
    for item in result:
        index += 1
        item = item.decode("utf-8", "ignore")
        # m = re.match(r"NumberLong\(\d+\)", item)
        # if m is not None:
        #     item = m.group(0)
        #     print(item)
        result[index] = item.replace("NumberLong","long")
        if "Timestamp" in item or "ISODate" in item:
            continue

        result_str += result[index]

        print(result[index])
    result_json = json.loads(result_str)
    print(result_str)

    # print(json.dumps(result,sort_keys=True, indent=4))


def mongo_server():
    pass


def main(argv):
    func_name = None
    server_ip = None
    try:
        opts, args = getopt.getopt(
            argv[1:], 'h', ['help=', 'func_name=', 'server_ip='])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ('-h', '--help'):
            sys.exit(1)
        elif o in ('-f', '--func_name'):
            func_name = a
        elif o in ('-s', '--server_ip'):
            server_ip = a
    if func_name == "server_stats" and server_ip == "114.215.140.70:27011":
        mongo_server_stats("114.215.140.70:27011",
                           "wom-dts", "wom-dts", "admin")
if __name__ == '__main__':
    main(sys.argv)
