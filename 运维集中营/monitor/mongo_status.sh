#!/bin/bash

output=$(/bin/echo "db.serverStatus().$4" |mongo -u$1 -p$2 $3 --quiet)
echo $output
