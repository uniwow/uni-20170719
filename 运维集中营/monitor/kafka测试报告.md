# kafka集群测试报告   
## kafka配置环境(内网模式)  
### zookeeper  
1. zookeeper=10.163.167.52:2181，10.30.216.52:2181，10.30.176.5:2181  
2. path=/alidata1/zookeeper  

### kafka  
1. broker=10.163.167.52:9092，10.30.216.52:9092，10.30.176.5:9092  
2. path=/alidata1/kafka  

## 错误消息:  
pykafka can not connect kafka broker  
Message not delivered!! SocketDisconnectedError()
exception pykafka.exceptions.SocketDisconnectedError  

## kafka配置环境(外网模式)  
### zookeeper  

1. zookeeper=114.215.110.161:2181，121.42.253.156:2181，118.190.77.56:2181  
2. path=/alidata1/zookeeper  

### kafka  
1. broker=114.215.110.161:9092，121.42.253.156:9092，118.190.77.56:9092  
2. path=/alidata1/kafka  

## 测试条件  
### 3个topic，2个consumer group  
topics(consumer group(consumer numbers))  
server-161-1(server-161-1(2),server-161-2(2)),server-161-2(server-161-2(3)) server-161-3(server-161-1(2))  

## 指标统计  
### topic  

|![topic-1-in](./topic-1-OneMinuteRate-in-03-28-14-30.jpg)    |
|![topic-1-out](./topic-1-OneMinuteRate-out-03-28-14-30.jpg)  |
|![topic-2-in](./topic-2-OneMinuteRate-in-03-28-14-30.jpg)    |
|![topic-2-out](./topic-2-OneMinuteRate-out-03-28-14-30.jpg)  |
|![topic-3-in](./topic-3-OneMinuteRate-in-03-28-14-30.jpg)    |
|![topic-3-out](./topic-3-OneMinuteRate-out-03-28-14-30.jpg)  |
|-------------------------------------------------------------|
|![topic-1-in](./topic-1-OneMinuteRate-in-03-28-17-30.jpg)    |
|![topic-1-out](./topic-1-OneMinuteRate-out-03-28-17-30.jpg)  |
|![topic-2-in](./topic-2-OneMinuteRate-in-03-28-17-30.jpg)    |
|![topic-2-out](./topic-2-OneMinuteRate-out-03-28-17-30.jpg)  |
|![topic-3-in](./topic-3-OneMinuteRate-in-03-28-17-30.jpg)    |
|![topic-3-out](./topic-3-OneMinuteRate-out-03-28-17-30.jpg)  |
|-------------------------------------------------------------|
|![topic-1-in](./topic-1-OneMinuteRate-in-03-29-12-00.jpg)    |
|![topic-1-out](./topic-1-OneMinuteRate-out-03-29-12-00.jpg)  |
|![topic-2-in](./topic-2-OneMinuteRate-in-03-29-12-00.jpg)    |
|![topic-2-out](./topic-2-OneMinuteRate-out-03-29-12-00.jpg)  |
|![topic-3-in](./topic-3-OneMinuteRate-in-03-29-12-00.jpg)    |
|![topic-3-out](./topic-3-OneMinuteRate-out-03-29-12-00.jpg)  |
|-------------------------------------------------------------|

### consumer  

|![consumer-group-1](./consumer-1-lag-03-28-14-30.jpg)   |
|![consumer-group-2](./consumer-2-lag-03-28-14-30.jpg)   |
|--------------------------------------------------------|
|![consumer-group-1](./consumer-1-lag-03-28-17-30.jpg)   |
|![consumer-group-2](./consumer-2-lag-03-28-17-30.jpg)   |

###############################################################
生产环境测试
cost 40 minute to make 1000000 messages
cost 40 minute to make 1000000 messages
cost 40 minute to make 1000000 messages

test message 6:50;count:3206164
cost 1347 mintue




