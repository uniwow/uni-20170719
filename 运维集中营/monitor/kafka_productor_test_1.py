# coding=utf-8
from pykafka import KafkaClient,SslConfig
import time
# config= SslConfig(cafile="/usr/local/kafka/ssl/client.truststore.jks", certfile="/usr/local/kafka/ssl/client.keystore.jks", keyfile="/usr/local/kafka/ssl/ca-key", password="server-70")
# client = KafkaClient(
#     hosts="server-70:9091, server-70:9092, server-70:9093",ssl_config=config)
# client = KafkaClient(
#     hosts="139.129.237.240:9092, 139.129.237.241:9092, 139.129.238.1:9093")
client = KafkaClient(
    hosts="server-32:9092, server-32:9093, server-32:9094")
topic = client.topics[b'uni']



def kafka_producer():
    # with topic.get_sync_producer(min_queued_messages=1000,auto_start=False,) as producer:
    with topic.get_producer(min_queued_messages=1000,auto_start=False,sync=False) as producer:
        while True:
            producer.start()
            for i in range(1, 100):
                for j in range(1, 100):
                    kafka_str=('test message ' + str(i) + ":" + str(j))
                    print(kafka_str)
                    producer.produce(kafka_str.encode("utf-8",'ignore'))
            producer.stop()
if __name__ == '__main__':
    kafka_producer()
