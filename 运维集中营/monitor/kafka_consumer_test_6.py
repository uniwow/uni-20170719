from pykafka import KafkaClient
import time
client = KafkaClient(
    hosts="kafka-1:9092, kafka-2:9092, kafka-3:9092")  # 可接受多个Client这是重点
topic = client.topics[b'server-161-3']

def kafka_consumer():
    consumer = topic.get_balanced_consumer(
        consumer_group=b"server-161-1", auto_commit_enable=False, zookeeper_connect="kafka-1:2181,kafka-2:2181,kafka-3:2181")
    while True:
        message=consumer.consume() 
        print(message.value.decode("utf-8","ignore"))
        consumer.commit_offsets()
if __name__ == '__main__':
    kafka_consumer()