
## kafka security

### 伪集群模式
1. 生成证书

#!/bin/bash
PASSWORD=server-70
VALIDITY=365
keytool -keystore server.keystore.jks -alias server-70 -validity $VALIDITY -genkey
openssl req -new -x509 -keyout ca-key -out ca-cert -days $VALIDITY
keytool -keystore server.truststore.jks -alias CARoot -import -file ca-cert
keytool -keystore client.truststore.jks -alias CARoot -import -file ca-cert
keytool -keystore server.keystore.jks -alias server-70 -certreq -file cert-file
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days $VALIDITY -CAcreateserial -passin pass:$PASSWORD
keytool -keystore server.keystore.jks -alias CARoot -import -file ca-cert
keytool -keystore server.keystore.jks -alias server-70 -import -file cert-signed
keytool -keystore client.keystore.jks -alias server-70 -validity $VALIDITY -genkey
keytool -keystore client.keystore.jks -alias server-70 -certreq -file cert-file
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days $VALIDITY -CAcreateserial -passin pass:$PASSWORD
keytool -keystore client.keystore.jks -alias CARoot -import -file ca-cert
keytool -keystore client.keystore.jks -alias server-70 -import -file cert-signed

2. kafka server.property productor.property 配置  
3. 启动kafka  
4. keytool -list -v -keystore server.keystore.jks 查看是否成功  
5. 启动生产者、消费者进程 ./bin/kafka-console-producer.sh --broker-list server-70:9092 --topic uni  --producer.config ./config/producer.properties  
6. ./bin/kafka-console-consumer.sh --bootstrap-server spider-kafka-1:9092 --topic uni --new-consumer --consumer.config ./config/producer.properties
openssl s_client -debug -connect spider-kafka-1:9092 -tls1
## 配置pykafka ssl  
1. keytool -exportcert -alias localhost -keystore kafka.client.keystore.jks -rfc -file certificate.pem

2. keytool -v -importkeystore -srckeystore kafka.client.keystore.jks -srcalias server-70 -destkeystore cert_and_key.p12 -deststoretype PKCS12  
3. openssl pkcs12 -in cert_and_key.p12 -nocerts -nodes  
4. 粘贴from -- end to ‘key.pem’
5. keytool -exportcert -alias CARoot -keystore client.keystore.jks -rfc -file CARoot.pem  


### 集群模式

1. 在一台机器上生成证书（kafka-1）  

同上1 

2. 在另一台主机上执行如下命令，生成证书（kafka-2）  

keytool -keystore client.keystore.241.jks -alias server-241 -validity 365 -genkey  
keytool -keystore client.keystore.241.jks -alias server-241 -certreq -file cert-file-241  
3. 将第1步中生成的文件scp到本服务器并执行一下命令  
 openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file-241  -out cert-signed-241  -days 365 -CAcreateserial -passin pass:server-240  
 keytool -keystore client.keystore.241.jks -alias CARoot -import -file ca-cert  
 keytool -keystore client.keystore.241.jks -alias server-241 -import -file cert-signed-241  

 4. 在kafka 3 上重做步骤2  
 5. 



## 参考文件

1. [使用SSL加密和认证](http://orchome.com/171)
2. [使用ssl加密认证](http://blog.csdn.net/zbdba/article/details/52458654)
3. [使用ssl加密认证](https://github.com/zbdba/Kafka-SSL-config)
4. [pykafka ssl 加密认证](http://maximilianchrist.com/blog/connect-to-apache-kafka-from-python-using-ssl)  
5. [burrow ssl](http://studygolang.com/wr?u=http%3a%2f%2fblog.csdn.net%2fnewsyoung1%2farticle%2fdetails%2f39472903)
6. [burrow ssl 讨论帖](https://github.com/linkedin/Burrow/issues/68)