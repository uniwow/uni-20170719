from pykafka import KafkaClient,SslConfig
import time
config= SslConfig(cafile="/usr/local/kafka/ssl/CARoot.pem",certfile="/usr/local/kafka/ssl/certificate.pem",keyfile="/usr/local/kafka/ssl/key.pem")
client = KafkaClient(
    hosts="server-70:9091, server-70:9092, server-70:9093",ssl_config=config)
topic = client.topics[b'uni']

def kafka_consumer():
    consumer = topic.get_balanced_consumer(
        consumer_group=b"server-161-1", auto_commit_enable=False, zookeeper_connect="kafka-1:2181,kafka-2:2181,kafka-3:2181")
    while True:
        message=consumer.consume() 
        print(message.value.decode("utf-8","ignore"))
        consumer.commit_offsets()
if __name__ == '__main__':
    kafka_consumer()

