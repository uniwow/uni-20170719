#!/usr/bin/env python
# coding: utf-8

from pymongo import MongoClient
import requests
from bson import ObjectId
import re
import time
from pathlib import Path
import os
import json
import fcntl


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=mongo_config['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e


if __name__ == '__main__':

    MONGO_OPS = {
        "host": "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-ops",
        "db": "wom-dts-ops",
        "connectTimeoutMS": 50000,
        "socketTimeoutMS": 10000,
        "waitQueueTimeoutMS": 10000
    }

    MONGO_WOM = {
        'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse",
        "connectTimeoutMS": 50000,
        "socketTimeoutMS": 10000,
        "waitQueueTimeoutMS": 10000,
    }

    '''
    连接wom-dts-ops的当kafka出错时的临时表kafka_conventional_text_info
    '''
    mongo_ops = MongoConnect(MONGO_OPS)
    db_ops = mongo_ops.db
    col_kafka_conventional_text_info = db_ops['kafka_conventional_text_info']

    '''
        连接wom-dts-datawarehouse的文本表media_weixin_article_text_info
    '''
    mongo_datawarehouse = MongoConnect(MONGO_WOM)
    db_datawarehouse = mongo_datawarehouse.db
    col_datawarehouse_text = db_datawarehouse['media_weixin_article_text_info']

    '''
        维护一个文件
    '''
    # file = open('aaa%s.csv'
    file = open('/alidata1/dts/export_from_mongo/article_wordle/wordle-%s.csv'
                % time.strftime('%Y-%m-%d', time.localtime(time.time())), 'ab')
    file_create_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))


    is_last_exec_time = False
    while True:
        # print('is last execute time: %s' % is_last_exec_time)
        '''
        查找一条记录
        '''
        one = {}
        object_id_delete = ''
        mongo_object_id = ''
        all_msg = col_kafka_conventional_text_info.find_one()
        kafka_message = all_msg['kafka_message'].decode('utf-8')
        object_id_delete = all_msg['_id']
        if "*|233|*|" in kafka_message:
            print(len(str(kafka_message).split("*|233|*|")))
            print(True)
            msg_arr = str(kafka_message).split("*|233|*|")
            object_id = ''
            weixin_id = ''
            title = ''
            content = ''
            digest = ''
            msg_len = len(msg_arr)
            if msg_len > 0:
                mongo_object_id = msg_arr[0]
            if msg_len > 1:
                one["weixin_id"] = msg_arr[1]
            if msg_len > 2:
                one["title"] = msg_arr[2]
            if msg_len > 3:
                one["content"] = msg_arr[3]
            if msg_len > 4:
                one["digest"] = msg_arr[4]
        else:
            print(False)
            j = json.loads(kafka_message, encoding='utf-8')
            mongo_object_id = j['mongo_object_id']
            one["weixin_id"] = j["weixin_id"]
            one["content"] = j["content"]
            one["digest"] = j["digest"]
            one["title"] = j["title"]
        if one:
            # object_id = str(one['_id'])
            weixin_id = one['weixin_id']
            print(object_id_delete)
            # print(one['there is no doc in collection.(empty collection)'])
            title = re.sub(r'\s+', ' ', one['title']).strip()
            content = re.sub(r'\s+', ' ', one['content']).strip()
            digest = ''
            try:
                digest = re.sub(r'\s+', ' ', one['digest']).strip()
            except Exception as e:
                pass
            segment = requests.post("http://114.215.110.161:9999/segment", data={
                'userId': 'test',
                'secretId': 'test',
                'withFlag': 'false',
                'text': title,
                'text_1': content,
                'text_2': digest,
            }).text.split('*|233|*|')
            title_segment = ''
            content_segment = ''
            digest_segment = ''

            if len(segment) > 0:
                title_segment = segment[0]
            if len(segment) > 1:
                content_segment = segment[1]
            if len(segment) > 2:
                digest_segment = segment[2]
            wordle_json = requests.post("http://114.215.110.161:9999/wordle", data={
                'userId': 'test',
                'secretId': 'test',
                'title': title,
                'content': content,
                'digest': digest,
            }).text[1:-1].split(',')
            wordle = []
            try:
                if len(content) > 10:
                    for tuple in wordle_json:
                        wordle.append({
                            "word": tuple.split(':')[0].strip()[1:-1],
                            "value": tuple.split(':')[1].strip()
                        })
            except Exception as wordle_exception:
                print('wordle has error.')
            post = {
                "_id": ObjectId(mongo_object_id),
                "weixin_id": weixin_id,
                "text_info": [
                    {
                        "weixin_id": weixin_id,
                        "title_segment": title_segment,
                        "content_segment": content_segment,
                        "digest_segment": digest_segment,
                        "create_time": int(time.time())
                    }
                ],
                "wordle_info": [
                    {
                        "wordle": wordle,
                        "create_time": int(time.time())
                    }
                ]
            }
                # print(post)
            try:
                col_datawarehouse_text.insert_one(post)
            except Exception as e:
                print(e)
                print("This ObjectId %s is already existed.\nTrying to update it..." % mongo_object_id)
                col_datawarehouse_text.update_one({'_id': ObjectId(mongo_object_id)}, {
                    '$set': {
                        "text_info": {
                            "weixin_id": weixin_id,
                            "title_segment": title_segment,
                            "content_segment": content_segment,
                            "digest_segment": digest_segment,
                            "create_time": int(time.time())
                        },
                        "wordle_info": {
                            "wordle": wordle,
                            "create_time": int(time.time())
                        }
                    }
                })

            '''
            删除这条记录
            '''
            print('ObjectId for delete is %s' % object_id_delete)
            if object_id_delete != '':
                col_kafka_conventional_text_info.delete_one({'_id': ObjectId(object_id_delete)})
            else:
                print('delete successful')

            '''
            切换日期并切换文件
            '''
            if file_create_date != time.strftime('%Y-%m-%d', time.localtime(time.time())):
                file.close()
                file_create_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
                # file = open('/aaa%s.csv'
                file = open('/alidata1/dts/export_from_mongo/article_wordle/wordle-%s.csv'
                            % time.strftime('%Y-%m-%d', time.localtime(time.time())), 'ab')

            import json

            json_temp = {
                "weixin_id": str(weixin_id),
                "wordle": wordle
            }
            wordle_weixin_id = json.dumps(json_temp, ensure_ascii=False)
            '''
                文件锁开始执行
            '''
            fcntl.flock(file, fcntl.LOCK_EX)
            file.write(bytes(wordle_weixin_id, encoding='utf-8'))
            file.write(bytes('\n', encoding='utf-8'))
            fcntl.flock(file, fcntl.LOCK_UN)

        else:
            print('it is none')
            time.sleep(3)
            reconnect_file_path_name = './_SUCCESS'
            reconnect_file = Path(reconnect_file_path_name)
            if reconnect_file.is_file() or is_last_exec_time:
                print('INFO: _SUCCESS file is found...')
                if reconnect_file.is_file():
                    os.remove(reconnect_file_path_name)
                if is_last_exec_time:
                    # continue
                    print('_SUCCESS file found and there is no doc in collection, Process finished and exit.')
                    break
                is_last_exec_time = True
                # continue
            else:
                print('INFO: no _SUCCESS file found...')
                continue



    '''
    关闭数据库连接
    '''
    mongo_datawarehouse.connection.close()
    mongo_ops.connection.close()
