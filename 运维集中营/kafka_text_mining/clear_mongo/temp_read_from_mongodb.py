#!/usr/bin/env python
# coding: utf-8

from pymongo import MongoClient
import requests
from bson import ObjectId
import re
import time
from pathlib import Path
import os


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=mongo_config['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e


if __name__ == '__main__':

    MONGO_OPS = {
        "host": "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-ops",
        "db": "wom-dts-ops",
        "connectTimeoutMS": 50000,
        "socketTimeoutMS": 10000,
        "waitQueueTimeoutMS": 10000
    }

    MONGO_WOM = {
        'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse",
        "connectTimeoutMS": 50000,
        "socketTimeoutMS": 10000,
        "waitQueueTimeoutMS": 10000,
    }

    '''
    连接wom-dts-ops的当kafka出错时的临时表kafka_conventional_text_info
    '''
    mongo_ops = MongoConnect(MONGO_OPS)
    db_ops = mongo_ops.db
    col_kafka_conventional_text_info = db_ops['kafka_conventional_text_info']

    '''
        连接wom-dts-datawarehouse的文本表media_weixin_article_text_info
    '''
    mongo_datawarehouse = MongoConnect(MONGO_WOM)
    db_datawarehouse = mongo_datawarehouse.db
    col_datawarehouse_text = db_datawarehouse['media_weixin_article_text_info']

    is_last_exec_time = False
    while True:
        # print('is last execute time: %s' % is_last_exec_time)
        '''
        查找一条记录
        '''
        one = None
        object_id_delete = ''
        try:
            one_orig = col_kafka_conventional_text_info.find_one()
            object_id_delete = one_orig['_id']
            # print(object_id_delete)
            # print(one_orig['kafka_message'].decode('utf-8'))
            kafka_message = col_kafka_conventional_text_info.find_one()['kafka_message'].decode('utf-8').strip().split("*|233|*|")
            one = dict(zip(['_id', 'weixin_id', 'title', 'content', 'digest'], kafka_message))
            # print(one)
        except Exception as e:
            print(e)
        if one is not None:
            object_id = str(one['_id'])
            weixin_id = one['weixin_id']
            # print(one['there is no doc in collection.(empty collection)'])
            title = re.sub(r'\s+', ' ', one['title']).strip()
            content = re.sub(r'\s+', ' ', one['content']).strip()
            digest = ''
            try:
                digest = re.sub(r'\s+', ' ', one['digest']).strip()
            except Exception as e:
                pass
            segment = requests.post("http://115.28.162.102:9999/segment", data={
                'userId': 'test',
                'secretId': 'test',
                'withFlag': 'false',
                'text': title,
                'text_1': content,
                'text_2': digest,
            }).text.split('*|233|*|')
            title_segment = ''
            content_segment = ''
            digest_segment = ''

            if len(segment) > 0:
                title_segment = segment[0]
            if len(segment) > 1:
                content_segment = segment[1]
            if len(segment) > 2:
                digest_segment = segment[2]
            wordle_json = requests.post("http://115.28.162.102:9999/wordle", data={
                'userId': 'test',
                'secretId': 'test',
                'title': title,
                'content': content,
                'digest': digest,
            }).text[1:-1].split(',')
            wordle = []
            try:
                if len(content) > 10:
                    for tuple in wordle_json:
                        wordle.append({
                            "word": tuple.split(':')[0].strip()[1:-1],
                            "value": tuple.split(':')[1].strip()
                        })
            except Exception as e:
                print('wordle exception is occupied, object id is %s.' % object_id)
                pass
            try:
                post = {
                    "_id": ObjectId(object_id),
                    "weixin_id": weixin_id,
                    "text_info": [
                        {
                            "weixin_id": weixin_id,
                            "title_segment": title_segment,
                            "content_segment": content_segment,
                            "digest_segment": digest_segment,
                            "create_time": int(time.time())
                        }
                    ],
                    "wordle_info": [
                        {
                            "wordle": wordle,
                            "create_time": int(time.time())
                        }
                    ]
                }
                # print(post)
                col_datawarehouse_text.insert_one(post)
                print('ObjectId is %s, insert successful' % object_id)
            except Exception as e:
                # print("This ObjectId %s is already existed.\nTrying to update it..." % object_id)
                col_datawarehouse_text.update_one({'_id': ObjectId(object_id)}, {
                    '$set': {
                        "text_info": {
                            "weixin_id": weixin_id,
                            "title_segment": title_segment,
                            "content_segment": content_segment,
                            "digest_segment": digest_segment,
                            "create_time": int(time.time())
                        },
                        "wordle_info": {
                            "wordle": wordle,
                             "create_time": int(time.time())}}})
                print('ObjectId is %s, update successful' % object_id)
                pass

            '''
            删除这条记录
            '''
            if object_id_delete != '':
                col_kafka_conventional_text_info.delete_one({'_id': ObjectId(object_id_delete)})
            else:
                print('ObjectId is %s, delete successful' % object_id)
        else:
            print('mongodb is empty.')
            time.sleep(3)
            reconnect_file_path_name = './_SUCCESS'
            reconnect_file = Path(reconnect_file_path_name)
            if reconnect_file.is_file() or is_last_exec_time:
                print('INFO: _SUCCESS file is found...')
                if reconnect_file.is_file():
                    os.remove(reconnect_file_path_name)
                if is_last_exec_time:
                    # continue
                    print('_SUCCESS file found and there is no doc in collection, Process finished and exit.')
                    break
                is_last_exec_time = True
                # continue
            else:
                print('INFO: no _SUCCESS file found...')
                continue



    '''
    关闭数据库连接
    '''
    mongo_datawarehouse.connection.close()
    mongo_ops.connection.close()
