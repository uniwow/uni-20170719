#!/usr/bin/env python
# coding: utf-8

import requests
import re
from bson import ObjectId
from pymongo import MongoClient
import time
import os


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=mongo_config['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as exc:
            print('error occupied while connect to mongodb')
            raise exc


def main():
    pass
    os.chdir('/alidata1/dts/export_from_mongo/machine_learning/')
    file_dir = os.listdir()
    print(file_dir)
    file_pattern = re.compile('article_20161129.*\\.csv')
    file_name_list = list(filter(lambda x: file_pattern.match(str(x)) is not None, file_dir))
    print(file_name_list)

    for file_name in file_name_list:
        MONGO = {
            'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                    "@115.28.26.225:27011/wom-dts-datawarehouse",
            "db": "wom-dts-datawarehouse",
        }
        print('hello')

        '''
            连接mongodb的文本表
        '''
        db = MongoConnect(MONGO).db
        collection = db['test_init']

        input_file = open(file_name, 'rb').readlines()
        num = 0
        for article in input_file:
            # print(article.decode('utf-8'))
            num += 1

            msg = article.decode(encoding='utf-8').split("*|233|*|")
            object_id = ''
            weixin_id = ''
            title = ''
            content = ''
            digest = ''
            msg_len = len(msg)
            if msg_len > 0:
                object_id = msg[0]
            if msg_len > 1:
                weixin_id = msg[1]
            if msg_len > 2:
                title = msg[2]
            if msg_len > 3:
                content = msg[3]
            if msg_len > 4:
                digest = msg[4]
            if len(object_id) == 24:
                segment = requests.post("http://115.28.162.102:9999/segment", data={
                    'userId': 'test',
                    'secretId': 'test',
                    'withFlag': 'false',
                    'text': title,
                    'text_1': content,
                    'text_2': digest,
                }).text.split('*|233|*|')
                segment_len = len(segment)
                title_segment = ''
                content_segment = ''
                digest_segment = ''
                if segment_len > 0:
                    title_segment = re.sub(r'\s+', ' ', segment[0]).strip()
                if segment_len > 1:
                    content_segment = re.sub(r'\s+', ' ', segment[1]).strip()
                if segment_len > 2:
                    digest_segment = re.sub(r'\s+', ' ', segment[2]).strip()

                wordle_json_orig = t1 = requests.post("http://115.28.162.102:9999/wordle", data={
                    'userId': 'test',
                    'secretId': 'test',
                    'title': title.strip(),
                    'content': content.strip(),
                    'digest': digest.strip(),
                })
                wordle_json = wordle_json_orig.text[1:-1].split(',')
                wordle = []
                try:
                    if len(content) > 10:
                        for tuple in wordle_json:
                            wordle.append({
                                "word": tuple.split(':')[0].strip()[1:-1],
                                "value": tuple.split(':')[1].strip()
                            })
                except Exception as wordle_exception:
                    print('wordle has error.')
                    pass
                try:
                    post = {
                        "_id": ObjectId(object_id),
                        "weixin_id": weixin_id,
                        "text_info": [
                            {
                                "weixin_id": weixin_id,
                                "title_segment": title_segment,
                                "content_segment": content_segment,
                                "digest_segment": digest_segment,
                                "create_time": int(time.time())
                            }
                        ],
                        "wordle_info": [
                            {
                                "wordle": wordle,
                                "create_time": int(time.time())
                            }
                        ]
                    }
                    collection.insert_one(post)
                    # print(post)
                    print("num is %s, ObjectId is %s, operation is create." %
                          (num, object_id))
                except Exception as e:
                    # print(e)
                    print("This ObjectId %s is already existed.\nTrying to update it..." % object_id)
                    collection.update_one({'_id': ObjectId(object_id)}, {
                        '$set': {
                            "text_info": [
                                {
                                    "weixin_id": weixin_id,
                                    "title_segment": title_segment,
                                    "content_segment": content_segment,
                                    "digest_segment": digest_segment,
                                    "create_time": int(time.time())
                                }
                            ],
                            "wordle_info": [
                                {
                                    "wordle": wordle,
                                    "create_time": int(time.time())
                                }
                            ]}})
                    pass
            else:
                print('ObjectId: %s is not a valid ObjectId.' % object_id)


if __name__ == '__main__':
    main()
