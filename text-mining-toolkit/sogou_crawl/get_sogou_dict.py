#!/usr/bin/env python
# coding=utf-8

import requests
import os
import sys


def main():
    if len (sys.argv) != 3:
                print ("Unknown Option \n usage: python %s 词库id 词库名称" %(sys.argv[0]))
                exit (1)
    file = requests.get("http://download.pinyin.sogou.com/dict/download_cell.php?id=%s&name=%s" % (sys.argv[1], sys.argv[2]))
    file_name = sys.argv[2]
    with open("%s.scel" % file_name, "wb") as f:
        for c in file.iter_content(chunk_size=1024):
            if c:
                f.write(c)
            f.flush()
    #print(file.text.encode("utf-8"))
    os.system('python scel2txt.py %s.scel %s.txt' % (file_name, file_name))
    os.system('rm -f %s.scel' % file_name)

if __name__ == '__main__':
    main()

