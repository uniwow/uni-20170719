#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import time
import requests
from lxml import etree
from pymongo import MongoClient
from bson import ObjectId
import collections

PARENT_CODE = {
    "电器数码办公": "A",
    "母婴文教图书": "B",
    "美容打扮化妆": "C",
    "女士女装内衣": "D",
    "男士运动户外": "E",
    "箱包穿着打扮": "F",
    "名牌时尚奢侈": "G",
    "车/建工/农具": "H",
    "日常生活服务": "I",
    "家居建材五金": "J",
    "家饰家纺房产": "K",
    "日用百货居家": "L",
    "食品茶水特产": "M",
    "人群医药保健": "N",
}


MONGO = {
        #'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
         #       "@115.28.26.225:27011/wom-dts-datawarehouse",
        #"db": "wom-dts-datawarehouse",
        'host': "127.0.0.1:27017",
        #       "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "test",
    }

BRAND_SEARCH = {
    'url': 'http://10.maigoo.com/search/?catid=',
    'retry_times': 5,
    'product_stopwords': ["按", "国", "市", "省", "筛选", "品牌"]
}


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=MONGO['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e


class BrandSpider:
    pattern_cn = re.compile('[\u4e00-\u9fa5]')
    pattern_en = re.compile('[^\u4e00-\u9fa5]')

    def __init__(self, task_cate_dict):
        self.task_cate_dict = task_cate_dict
        self.task_log = open('/Users/apple/Codes/Py/text-mining-toolkit/brand_crawl/task.log', 'a+')
        self.brand_list = open('/Users/apple/Codes/Py/text-mining-toolkit/brand_crawl/brand_list.txt', 'a+')
        pass

    @staticmethod
    def get_brand_name(page):
        return page.xpath("//li/div[2]/div[1]/a/text()")

    @staticmethod
    def get_brand_img(page):
        return page.xpath("//li/div[3]/div[1]/div[1]/img/@src")

    @staticmethod
    def get_brand_company(page):
        return list(map(lambda x: str(x).strip()[1:-1] if len(str(x).strip()) > 2 else '',
                        page.xpath("//li/div[3]/div[2]/div[1]/div[1]/text()[2]")))

    @staticmethod
    def get_brand_products(page, item_len, is_first_page):
        products_list = []
        for item in range(1, item_len + 1):
            item_products_list = list(map(lambda x: str(x).strip(),
                                          page.xpath("//li[%d]//div[@class='desc']//a[@class='yellow']//text()"
                                                     % ((item + 1) if is_first_page else item))))
            item_products_list_copy = item_products_list.copy()
            for product in item_products_list_copy:
                for stop_word in BRAND_SEARCH["product_stopwords"]:
                    if stop_word in product and product in item_products_list:
                        item_products_list.remove(product)
            products_list.append(list(map(lambda x: {'product': x}, item_products_list)))
        return products_list

    @staticmethod
    def get_brand_phone_and_desc(page):
        return list(map(lambda x: str(x).strip().replace('\n', '').replace(' ', ''),
                        page.xpath("//li/div[3]/div[2]/div[1]/div[@class='desc']/text()[1]")))

    def parse_to_mongodb(self, page, collection, cat_id, is_first_page):
        brand_name_list_first = list(self.get_brand_name(page))[:10]
        page_length_first = len(brand_name_list_first)
        brand_name_en_list_first = list(map(lambda x: ''.join(self.pattern_cn.split(x)), brand_name_list_first))
        brand_name_cn_list_first = list(map(lambda x: ''.join(self.pattern_en.split(x)), brand_name_list_first))
        brand_img_list_first = self.get_brand_img(page)[:10]
        brand_company_list_first = self.get_brand_company(page)[:10]
        brand_products_list_first = self.get_brand_products(page, page_length_first, is_first_page)
        brand_phone_and_desc_list_first = self.get_brand_phone_and_desc(page)[:10]
        brand_phone_list_first = list(map(lambda x: x.split(')')[0][1:] if ')' in x else "",
                                          brand_phone_and_desc_list_first))
        brand_desc_list_first = list(map(lambda x: x.split(')(')[1][:-1] if ')(' in x else "",
                                         brand_phone_and_desc_list_first))
        for item in range(page_length_first):
            insert_post = {
                'name_cn': brand_name_cn_list_first[item],
                'name_en': brand_name_en_list_first[item],
                'company': brand_company_list_first[item],
                'img_url': brand_img_list_first[item],
                'phone': str(brand_phone_list_first[item]).replace("电话", "").replace("：", ""),
                'desc': brand_desc_list_first[item],
                'products': brand_products_list_first[item],
                'create_time': int(time.time()),
                'update_time': int(time.time()),
                'status': 1,
                'cate': [
                    {
                        'name': self.task_cate_dict[str(cat_id)][0],
                        'code': str(cat_id),
                        'parent_name': self.task_cate_dict[str(cat_id)][1],
                        'parent_code': PARENT_CODE[self.task_cate_dict[str(cat_id)][1]]
                    }
                ]
            }
            find_result = collection.find_one({'$or': [{
                'name_cn': brand_name_cn_list_first[item],
                'company': brand_company_list_first[item],
                'img_url': brand_img_list_first[item]}, {
                'name_en': brand_name_en_list_first[item],
                'company': brand_company_list_first[item],
                'img_url': brand_img_list_first[item]}]})

            if find_result is None:
                collection.insert_one(insert_post)
                self.brand_list.write('%s\n' % (insert_post['name_cn']
                                                if insert_post['name_cn'] != '' else insert_post['name_en']))
                print('brand %s%s insert success.' % (insert_post['name_cn'], insert_post['name_en']))
            elif cat_id not in set(map(lambda x: dict(x)['code'], find_result['cate'])):
                _id = dict(find_result).get('_id')
                collection.update_one({'_id': ObjectId(_id)}, {
                    '$push': {
                        'cate': {
                            'name': self.task_cate_dict[str(cat_id)][0],
                            'code': str(cat_id),
                            'parent_name': self.task_cate_dict[str(cat_id)][1],
                            'parent_code': PARENT_CODE[self.task_cate_dict[str(cat_id)][1]]
                        }
                    }
                })
                print('doc is already exist, update success.')
            else:
                print('cate code is already exist. so do not have to update')
                pass
            # print(insert_post)

    @staticmethod
    def get_page_from_url(url):
        response = requests.get(url)
        retry_times_first = BRAND_SEARCH['retry_times']
        if response.status_code != 200:
            print('requests error, retry ...')
        else:
            while retry_times_first > 0:
                retry_times_first -= 1
                response = requests.get(url)
                if response.status_code != 200:
                    # TODO:
                    print('connection error occupied.')
                    pass
        if response.content.decode('utf-8').strip() == '':
            return ''
        else:
            page = etree.HTML(response.content.decode('utf-8', 'ignore'))
            return page

    def process(self, cat_id, collection):
        first_url = BRAND_SEARCH['url'] + cat_id
        page_first = self.get_page_from_url(first_url)
        self.parse_to_mongodb(page_first, collection, cat_id, is_first_page=True)
        page_num_start = 2
        while 1:
            page_url = first_url + '&action=ajax&getac=brand&page=' + str(page_num_start)
            print(page_url)
            page = self.get_page_from_url(page_url)
            if page == '':
                break
            else:
                self.parse_to_mongodb(page, collection, cat_id, is_first_page=False)
            page_num_start += 1
            # time.sleep(10)
        pass

    def start_crawl(self, cat_id_list, collection):
        for cat_id in cat_id_list:
            print(cat_id)
            self.process(cat_id, collection)
            self.task_log.write('task %s is finished.\n' % cat_id)

    def destroy(self):
        self.task_log.close()


def main():
    print('hello brand')
    task_file = list(filter(lambda x: str(x).strip() != '', open('/Users/apple/Codes/Py/text-mining-toolkit/brand_crawl/task.txt', 'r', encoding='utf-8').readlines()))
    task_list_orig = map(lambda x: str(x).strip().split(' ')[0], task_file)
    task_cate = map(lambda x: str(x).strip().split(' ')[1:], task_file)

    # print(len(list(task_list_orig)))
    # print(len(list(task_cate)))

    task_cate_dict_ordered = collections.OrderedDict(zip(task_list_orig, task_cate))

    task_list = list(task_cate_dict_ordered.keys())
    task_cate_dict = dict(task_cate_dict_ordered)

    #print(task_cate_dict)

    mongo_connect = MongoConnect(MONGO)
    collection = mongo_connect.db['brand_lib_test']

    brand_spider = BrandSpider(task_cate_dict)
    brand_spider.start_crawl(task_list, collection)

    mongo_connect.connection.close()


if __name__ == '__main__':
    main()
