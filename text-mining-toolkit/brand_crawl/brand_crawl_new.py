#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import time
import requests
from lxml import etree
from pymongo import MongoClient
from bson import ObjectId
import collections
import pyquery


MONGO = {
        #'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
         #       "@115.28.26.225:27011/wom-dts-datawarehouse",
        #"db": "wom-dts-datawarehouse",
        'host': "127.0.0.1:27017",
        #       "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "test",
    }

BRAND_SEARCH = {
    'url': 'http://10.maigoo.com/search/?catid=',
    'retry_times': 5,
    'product_stopwords': ["按", "国", "市", "省", "筛选", "品牌"]
}


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=MONGO['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e

class BrandSpider:

    def __init__(self, task_cate_dict):
        self.task_cate_dict = task_cate_dict
        pass

    def getInfoCat(self):
        cat_list = self.task_cate_dict
        for url_cat in cat_list:
            url = BRAND_SEARCH['url'] + url_cat
            req = requests.get(url)
            reg_html = pyquery.PyQuery(req.text)
            brand_list = reg_html.find('.b-brand-nlist ul .trans100')
            print(brand_list)
            print(cat_list[url_cat][0])
            print(url_cat)
            exit()

    def run(self):
        BrandSpider.getInfoCat(self)

def main():
    mongo_connect = MongoConnect(MONGO)
    collection = mongo_connect.db['brand_lib_test']

    task_file = list(filter(lambda x: str(x).strip() != '',open('/Users/apple/Codes/Py/text-mining-toolkit/brand_crawl/brand_list.txt', 'r',encoding='utf-8').readlines()))
    task_list_orig = map(lambda x: str(x).strip().split(' ')[0], task_file)
    task_cate = map(lambda x: str(x).strip().split(' ')[1:], task_file)

    task_cate_dict_ordered = collections.OrderedDict(zip(task_list_orig, task_cate))

    task_list = list(task_cate_dict_ordered.keys())
    #分类类型
    task_cate_dict = dict(task_cate_dict_ordered)


    #print(task_cate_dict)
    #exit()

    brand_spider = BrandSpider(task_cate_dict)
    brand_spider.run()

    mongo_connect.connection.close()

if __name__ == '__main__':
    main()