# -*- coding: utf-8 -*-

import re
import time
import requests
from lxml import etree
from pymongo import MongoClient
from bson import ObjectId
import collections
import pyquery
import argparse
import grequests
import mysql.connector
import json
import urllib
import numpy as np
import matplotlib
from pypinyin import pinyin, lazy_pinyin
matplotlib.use('Agg')
import matplotlib.font_manager as font_manager
font = font_manager.FontProperties(fname="/mnt/shared-dir/simsunb.ttf")
import matplotlib.pyplot as plt 
# plt.rcParams['font.sans-serif']=['SimHei']
# plt.rcParams['font.family']='sans-serif'
# plt.rcParams['axes.unicode_minus']=False
cnames = {'aqua':'#00FFFF','aquamarine':'#7FFFD4','black':'#000000','blanchedalmond':'#FFEBCD','blue':'#0000FF','blueviolet':'#8A2BE2','brown':'#A52A2A','burlywood':'#DEB887','cadetblue':'#5F9EA0','chartreuse':'#7FFF00','chocolate':'#D2691E','coral':'#FF7F50','cornflowerblue':'#6495ED','cornsilk':'#FFF8DC','crimson':'#DC143C','cyan':'#00FFFF','darkblue':'#00008B','darkcyan':'#008B8B','darkgoldenrod':'#B8860B','darkgray':'#A9A9A9','darkgreen':'#006400','darkkhaki':'#BDB76B','darkmagenta':'#8B008B','darkolivegreen':'#556B2F','darkorange':'#FF8C00','darkorchid':'#9932CC','darkred':'#8B0000','darksalmon':'#E9967A','darkseagreen':'#8FBC8F','darkslateblue':'#483D8B','darkslategray':'#2F4F4F','darkturquoise':'#00CED1','darkviolet':'#9400D3','deeppink':'#FF1493','deepskyblue':'#00BFFF','dimgray':'#696969','dodgerblue':'#1E90FF','firebrick':'#B22222','floralwhite':'#FFFAF0','forestgreen':'#228B22','fuchsia':'#FF00FF','gainsboro':'#DCDCDC','ghostwhite':'#F8F8FF','gold':'#FFD700','goldenrod':'#DAA520','gray':'#808080','green':'#008000','greenyellow':'#ADFF2F','honeydew':'#F0FFF0','hotpink':'#FF69B4','indianred':'#CD5C5C','indigo':'#4B0082','ivory':'#FFFFF0','khaki':'#F0E68C','lavender':'#E6E6FA','lavenderblush':'#FFF0F5','lawngreen':'#7CFC00','lemonchiffon':'#FFFACD','lightblue':'#ADD8E6','lightcoral':'#F08080','lightcyan':'#E0FFFF','lightgoldenrodyellow':'#FAFAD2','lightgreen':'#90EE90','lightgray':'#D3D3D3','lightpink':'#FFB6C1','lightsalmon':'#FFA07A','lightseagreen':'#20B2AA','lightskyblue':'#87CEFA','lightslategray':'#778899','lightsteelblue':'#B0C4DE','lightyellow':'#FFFFE0','lime':'#00FF00','limegreen':'#32CD32','linen':'#FAF0E6','magenta':'#FF00FF','maroon':'#800000','mediumaquamarine':'#66CDAA','mediumblue':'#0000CD','mediumorchid':'#BA55D3','mediumpurple':'#9370DB','mediumseagreen':'#3CB371','mediumslateblue':'#7B68EE','mediumspringgreen':'#00FA9A','mediumturquoise':'#48D1CC','mediumvioletred':'#C71585','midnightblue':'#191970','mintcream':'#F5FFFA','mistyrose':'#FFE4E1','moccasin':'#FFE4B5','navajowhite':'#FFDEAD','navy':'#000080','oldlace':'#FDF5E6','olive':'#808000','olivedrab':'#6B8E23','orange':'#FFA500','orangered':'#FF4500','orchid':'#DA70D6','palegoldenrod':'#EEE8AA','palegreen':'#98FB98','paleturquoise':'#AFEEEE','palevioletred':'#DB7093','papayawhip':'#FFEFD5','peachpuff':'#FFDAB9','peru':'#CD853F','pink':'#FFC0CB','plum':'#DDA0DD','powderblue':'#B0E0E6','purple':'#800080','red':'#FF0000','rosybrown':'#BC8F8F','royalblue':'#4169E1','saddlebrown':'#8B4513','salmon':'#FA8072','sandybrown':'#FAA460','seagreen':'#2E8B57','seashell':'#FFF5EE','sienna':'#A0522D','silver':'#C0C0C0','skyblue':'#87CEEB','slateblue':'#6A5ACD','slategray':'#708090','snow':'#FFFAFA','springgreen':'#00FF7F','steelblue':'#4682B4','tan':'#D2B48C','teal':'#008080','thistle':'#D8BFD8','tomato':'#FF6347','turquoise':'#40E0D0','violet':'#EE82EE','wheat':'#F5DEB3','white':'#FFFFFF','whitesmoke':'#F5F5F5','yellow':'#FFFF00','yellowgreen':'#9ACD32'}



mongo_dict = {
    'host': '118.190.175.5',
    'port': 27011,
    'db': 'wom-dts-datawarehouse',
    'user': 'dts-datawarehouse-admin',
    'password': '6ghednsdf1xde0bqq',
}
wom_dev_mysql = {
    "host":"rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com",
    "db":"wom_dev", 
    "user":"wom",
    "password":"Qmg2016mw#semys$"
}   

def get_args():
    parse =argparse.ArgumentParser()
    parse.add_argument('-s',type=str)
    parse.add_argument('-w',type=str)
    args = parse.parse_args()
    return vars(args)

def get_href():
    args = get_args()
    spider_name = args['s']
    if spider_name == "detail_by_level":
        maigoo_brand_cate_detail_scrawl_by_level()
    elif spider_name == "detail_by_indu":
        maigoo_brand_cate_detail_scrawl()
    elif spider_name == "home_page_by_level":
        maigoo_brand_cate_detail_by_level_home_page_scrawl()
    elif spider_name == "wechat_index":
        search_words = args['w']
        wechat_index_crawl(search_words)

def request_error_handler(request,exception):
    print("resuest error...")

def wechat_index_crawl(search_words):
    query = "SELECT `id`,`openid`,`search_key`,`headers` FROM `wechat_search_index` WHERE `status` = 1 AND  `froms` is not NULL AND `headers` IS NOT NULL"
    
    cnx = mysql.connector.connect(user=wom_dev_mysql["user"],
                                password=wom_dev_mysql["password"],
                                host=wom_dev_mysql["host"],
                                database=wom_dev_mysql["db"],
                                charset='utf8')
    cursor = cnx.cursor(buffered=True)
    cursor.execute(query)
    row = cursor.fetchone()
    if row is None:
        print("search key is used out...")
    else:
        _id,openid,search_key,headers = row

        print(dict([(item.split("=")[0],(item.split("=")[1])) for item in headers.split("&")]))
        # input("..................")
        headers = dict([(item.split("=")[0],(item.split("=")[1])) for item in headers.split("&")])
        """
        任务开始，把状态设置为2
        """
        query = "UPDATE `wechat_search_index` SET `status` = 2 WHERE `id` = %d" % _id
        cursor.execute(query)
        cnx.commit()
        rs = []
        result_list = []
        for word in search_words.split("#"):
            print(word)
            rs.append(grequests.get("https://search.weixin.qq.com/cgi-bin/searchweb/wxindex/querywxindexgroup?wxindex_query_list=%s&openid=%s&search_key=%s" % (word,openid,search_key),headers = headers))
        response_list = grequests.map(rs,exception_handler = request_error_handler)
        success_flag = 1

        for response in response_list:
            """
            结果错误，把状态改为0
            """
            if json.loads(response.text)["msg"] == 'invalid auth' or json.loads(response.text)["errcode"] == 1008:
                print("search key is expired")
                query = "UPDATE `wechat_search_index` SET `status` = 0 WHERE `search_key` = '%s'" % search_key
                cursor.execute(query)
                cnx.commit()
                cnx.close()
                success_flag =0
                break
            else:
                # print(type(response.text))
                result_list.append(json.loads(response.text)["data"]["group_wxindex"][0])
        if success_flag == 0:
            rs = []
            result_list = []
            wechat_index_crawl(search_words)
        else:
            print(result_list)
            plt.figure(figsize=(8,6), dpi=800)
            ax = plt.subplot(111)
            ax.spines['right'].set_color('none')
            ax.spines['top'].set_color('none')
            y_list = []
            # color_list = ["red","blue","yellow","green",""]
            color_init = -1
            
            for item in result_list:
                color_init += 1
                color = sorted(cnames.keys())[color_init]
                print(color)
                
                x=list(range(90))
                y=[0 for i in range(90)]
                if item["wxindex_str"].replace(" ","") != "":
                    y = [int(i) for i in item["wxindex_str"].split(",")]
                    y_list = y_list + y
                plt.plot(x,y,label = " ".join(lazy_pinyin(item["query"])),color = color, linewidth=2.5, linestyle="-",alpha=1.00)
                plt.fill_between(x, y, 0,(y-1) > 200,color=color, alpha=.25)
                plt.scatter(x[y.index(max(y))],max(y), 20,color = color)
                plt.annotate("(%d,%d)" % (x[y.index(max(y))],max(y)),xy=(x[y.index(max(y))],max(y)), xycoords='data',xytext=(+15, +15), textcoords='offset points', fontsize=6,arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=.1"))
                plt.plot([x[y.index(max(y))],x[y.index(max(y))],],[0,max(y)],linewidth=1.5, linestyle="--",color = color)
            plt.legend(loc='upper left')
            plt.xlabel("time(d)")
            plt.ylabel("index")
            plt.ylim(min(y_list)*1.1, max(y_list)*1.1)
            plt.xticks(np.linspace(0,89,10,endpoint=True))
            plt.yticks(np.linspace(0,max(y_list),10,endpoint=True)) #,[r'$-1$', r'$0$', r'$+1$']
            plt.title(" ".join(lazy_pinyin('wechat index')),fontproperties = font)
            plt.savefig("%s.png" % "wechat_index")
                # plt.figure()
            query = "UPDATE `wechat_search_index` SET `status` = 1 WHERE `id` = %d" % _id
            cursor.execute(query)
            cnx.commit()
            cnx.close()




def maigoo_brand_cate_scrawl():
    # response = requests.get("http://10.maigoo.com/search/")
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com")
    brand_list = []
    for item in response_pyquery("dd")[1][:-2]:
        brand_item = {}
        brand_item["catid"] = item.attrib["tempid"]
        brand_item["name"] = item.text
        brand_list.append(brand_item)
        sub_response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s" % item.attrib["tempid"])
        sub_brand_list = []
        for sub_item in sub_response_pyquery("dd")[1][1:-1]:
            sub_brand_item = {}
            sub_brand_item["catid"] = sub_item.attrib["tempid"]
            sub_brand_item["name"] = sub_item.text
            sub_brand_list.append(sub_brand_item)
        brand_item["sub_brand_list"] = sub_brand_list
    print(brand_list)
    mongo_database["brand_classify_qmg"].insert({"classify_dimension":"industry",
                                                "desc":"按照行业分类",
                                                "target":"http://10.maigoo.com",
                                                "classify":brand_list})
def maigoo_brand_cate_scrawl_by_brand_level():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com")
    brand_list = []
    for item in response_pyquery("dd")[4]:
        brand_item = {}
        brand_item["catid"] = item.attrib["tempid"]
        brand_item["name"] = item.text
        brand_list.append(brand_item)
        # sub_response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s" % item.attrib["tempid"])
        # sub_brand_list = []
        # for sub_item in sub_response_pyquery("dd")[1][1:-1]:
        #     sub_brand_item = {}
        #     sub_brand_item["catid"] = sub_item.attrib["tempid"]
        #     sub_brand_item["name"] = sub_item.text
        #     sub_brand_list.append(sub_brand_item)
        # brand_item["sub_brand_list"] = sub_brand_list
    print(brand_list)
    mongo_database["brand_classify_qmg"].insert({"classify_dimension":"level",
                                                "desc":"按照品牌等级",
                                                "target":"http://10.maigoo.com",
                                                "classify":brand_list})

def maigoo_brand_cate_detail_home_page_scrawl():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    classify_list = mongo_database["brand_classify_qmg"].find_one({"classify_dimension":"industry","target":"http://10.maigoo.com"},{"classify":1})
    # print(classify_list)
    for classify in classify_list["classify"]:
        # print(classify)
        # for sub_classify in classify["sub_brand_list"]:
        brand_message = {}
        brand_message["brand_cate"] = {}
        brand_message["brand_cate"]["catid"] = classify["catid"]
        brand_message["brand_cate"]["cate_name"] = classify["name"]
        brand_message["brand_cate"]["sup_catid"] = "-1"
        brand_message["brand_cate"]["sup_cate_name"] = "-1"
        page_count = 1
        try:
            response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s" % (classify["catid"],))
            item_count = 0
            brand_message_list = []
            for i in range(1,11):
                brand_message_list.append({})
            for item in response_pyquery("a.dhidden")[:10]:
                brand_message_list[item_count]["company_home_page"] = item.attrib["href"]
                if re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()) is None:
                    brand_message_list[item_count]["brand_name_cn"] = "未知"
                else:
                    brand_message_list[item_count]["brand_name_cn"] = re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()).groups()[0]
                if (re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is None) and (re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()) is None):
                    brand_message_list[item_count]["brand_name_en"] = "未知"
                else:
                    brand_message_list[item_count]["brand_name_en"] = re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()).groups()[0] if re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is not None else re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()).groups()[0] 
                item_count += 1
            # print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.td3>div.dhidden")[:10]:
                brand_message_list[item_count]["company_desc"] = item.text_content().split("\n")[2].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                company_phone = item.text_content().split("\n")[1].split("(")
                if (len(company_phone) >1) & ("电话" in item.text_content().split("\n")[1].split("(")[-1]):
                        brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                        brand_message_list[item_count]["company_phone"] = item.text_content().split("\n")[1].split("(")[-1].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                else:
                    brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                    brand_message_list[item_count]["company_phone"] = "未知"
                item_count += 1
            print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.tbcell>img")[:10]:
                brand_message_list[item_count]["company_logo"] = item.attrib["src"]
                item_count += 1
            # print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.desc")[:10]:
                brand_message_list[item_count]["company_products"] = []
                for product in item:
                    brand_message_list[item_count]["company_products"].append(product.text_content())
                item_count += 1
            for item in brand_message_list:
                print(item)
                mongo_database["brand_warehouse_qmg"].update({"company":item["company"],"brand_name_cn":item["brand_name_cn"],"brand_name_en":item["brand_name_en"]},{"$set":item,"$addToSet":{"brand_cate":brand_message["brand_cate"]}},True)
            print("current classify is %s; current sub_classify is %s;current page is %d" % (brand_message["brand_cate"]["sup_catid"],brand_message["brand_cate"]["catid"],page_count))
            page_count += 1
            
        except Exception as e:
            print("the page is over")

def maigoo_brand_cate_detail_by_level_home_page_scrawl():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    classify_list = mongo_database["brand_classify_qmg"].find_one({"classify_dimension":"level","target":"http://10.maigoo.com"},{"classify":1})
    # print(classify_list)
    for classify in classify_list["classify"]:
        # print(classify)
        # for sub_classify in classify["sub_brand_list"]:
        brand_message = {}
        brand_message["brand_cate"] = {}
        brand_message["brand_cate"]["catid"] = classify["catid"]
        brand_message["brand_cate"]["cate_name"] = classify["name"]
        page_count = 1
        try:
            response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s" % (classify["catid"],))
            item_count = 0
            brand_message_list = []
            for i in range(1,11):
                brand_message_list.append({})
            for item in response_pyquery("a.dhidden")[:10]:
                brand_message_list[item_count]["company_home_page"] = item.attrib["href"]
                if re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()) is None:
                    brand_message_list[item_count]["brand_name_cn"] = "未知"
                else:
                    brand_message_list[item_count]["brand_name_cn"] = re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()).groups()[0]
                if (re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is None) and (re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()) is None):
                    brand_message_list[item_count]["brand_name_en"] = "未知"
                else:
                    brand_message_list[item_count]["brand_name_en"] = re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()).groups()[0] if re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is not None else re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()).groups()[0] 
                item_count += 1
            # print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.td3>div.dhidden")[:10]:
                brand_message_list[item_count]["company_desc"] = item.text_content().split("\n")[2].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                company_phone = item.text_content().split("\n")[1].split("(")
                if (len(company_phone) >1) & ("电话" in item.text_content().split("\n")[1].split("(")[-1]):
                        brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                        brand_message_list[item_count]["company_phone"] = item.text_content().split("\n")[1].split("(")[-1].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                else:
                    brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                    brand_message_list[item_count]["company_phone"] = "未知"
                item_count += 1
            print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.tbcell>img")[:10]:
                brand_message_list[item_count]["company_logo"] = item.attrib["src"]
                item_count += 1
            # print(brand_message_list)
            item_count = 0
            for item in response_pyquery("div.desc")[:10]:
                brand_message_list[item_count]["company_products"] = []
                for product in item:
                    brand_message_list[item_count]["company_products"].append(product.text_content())
                item_count += 1
            for item in brand_message_list:
                print(item)
                mongo_database["brand_warehouse_qmg_by_level"].update({"company":item["company"],"brand_name_cn":item["brand_name_cn"],"brand_name_en":item["brand_name_en"]},{"$set":item,"$addToSet":{"brand_cate":brand_message["brand_cate"]}},True)
            print("current classify is %s;current page is %d" % (brand_message["brand_cate"]["catid"],page_count))
            page_count += 1
            
        except Exception as e:
            print("the page is over")
def maigoo_brand_cate_detail_scrawl():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    classify_list = mongo_database["brand_classify_qmg"].find_one({"classify_dimension":"industry","target":"http://10.maigoo.com"},{"classify":1})
    # print(classify_list)
    for classify in classify_list["classify"]:
        # print(classify)
        for sub_classify in classify["sub_brand_list"]:
            brand_message = {}
            brand_message["brand_cate"] = {}
            brand_message["brand_cate"]["catid"] = sub_classify["catid"]
            brand_message["brand_cate"]["cate_name"] = sub_classify["name"]
            brand_message["brand_cate"]["sup_catid"] = classify["catid"]
            brand_message["brand_cate"]["sup_cate_name"] = classify["name"]
            page_count = 2
            while True:
                try:
                    response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s&action=ajax&getac=brand&page=%d" % (sub_classify["catid"],page_count))
                    item_count = 0
                    brand_message_list = []
                    for i in range(1,len(response_pyquery("a.dhidden"))+1):
                        brand_message_list.append({})
                    for item in response_pyquery("a.dhidden"):
                        brand_message_list[item_count]["company_home_page"] = item.attrib["href"]
                        if re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()) is None:
                            brand_message_list[item_count]["brand_name_cn"] = "未知"
                        else:
                            brand_message_list[item_count]["brand_name_cn"] = re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()).groups()[0]
                        if (re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is None) and (re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()) is None):
                            brand_message_list[item_count]["brand_name_en"] = "未知"
                        else:
                            brand_message_list[item_count]["brand_name_en"] = re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()).groups()[0] if re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is not None else re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()).groups()[0] 
                        item_count += 1
                    # print(brand_message_list)
                    item_count = 0
                    for item in response_pyquery("div.td3>div.dhidden"):
                        brand_message_list[item_count]["company_desc"] = item.text_content().split("\n")[2].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                        company_phone = item.text_content().split("\n")[1].split("(")
                        if (len(company_phone) >1) & ("电话" in item.text_content().split("\n")[1].split("(")[-1]):
                            brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                            brand_message_list[item_count]["company_phone"] = item.text_content().split("\n")[1].split("(")[-1].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                        else:
                            brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                            brand_message_list[item_count]["company_phone"] = "未知"
                        item_count += 1
                    print(brand_message_list)
                    item_count = 0
                    for item in response_pyquery("div.tbcell>img"):
                        brand_message_list[item_count]["company_logo"] = item.attrib["src"]
                        item_count += 1
                    # print(brand_message_list)
                    item_count = 0
                    for item in response_pyquery("div.desc"):
                        brand_message_list[item_count]["company_products"] = []
                        for product in item:
                            brand_message_list[item_count]["company_products"].append(product.text_content())
                        item_count += 1
                    for item in brand_message_list:
                        print(item)
                        mongo_database["brand_warehouse_qmg"].update({"company":item["company"],"brand_name_cn":item["brand_name_cn"],"brand_name_en":item["brand_name_en"]},{"$set":item,"$addToSet":{"brand_cate":brand_message["brand_cate"]}},True)
                    print("current classify is %s; current sub_classify is %s;current page is %d" % (brand_message["brand_cate"]["sup_catid"],brand_message["brand_cate"]["catid"],page_count))
                    page_count += 1
                    
                except Exception as e:
                    print("the page is over")
                    break

def maigoo_brand_cate_detail_scrawl_by_level():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    classify_list = mongo_database["brand_classify_qmg"].find_one({"classify_dimension":"level","target":"http://10.maigoo.com"},{"classify":1})
    # print(classify_list)
    for classify in classify_list["classify"]:
        # print(classify)
        
        brand_message = {}
        brand_message["brand_cate"] = {}
        brand_message["brand_cate"]["catid"] = classify["catid"]
        brand_message["brand_cate"]["cate_name"] = classify["name"]
        
        page_count = 2
        while True:
            try:
                response_pyquery = pyquery.PyQuery(url = "http://10.maigoo.com/search/?catid=%s&action=ajax&getac=brand&page=%d" % (classify["catid"],page_count))
                item_count = 0
                brand_message_list = []
                for i in range(1,len(response_pyquery("a.dhidden"))+1):
                    brand_message_list.append({})
                for item in response_pyquery("a.dhidden"):
                    brand_message_list[item_count]["company_home_page"] = item.attrib["href"]
                    if re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()) is None:
                        brand_message_list[item_count]["brand_name_cn"] = "未知"
                    else:
                        brand_message_list[item_count]["brand_name_cn"] = re.search(u"([\u4e00-\u9fa5]+([a-zA-Z0-9&.]+)?[\u4e00-\u9fa5]+)",item.text_content()).groups()[0]
                    # print("hello ")

                    if (re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is None) and (re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()) is None):
                        brand_message_list[item_count]["brand_name_en"] = "未知"
                    else:
                        brand_message_list[item_count]["brand_name_en"] = re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()).groups()[0] if re.search(u"(^[a-zA-Z0-9&.]+)",item.text_content()) is not None else re.search(u"([a-zA-Z0-9&.]+$)",item.text_content()).groups()[0]
                    item_count += 1
                # print(brand_message_list)
                item_count = 0
                for item in response_pyquery("div.td3>div.dhidden"):
                    brand_message_list[item_count]["company_desc"] = item.text_content().split("\n")[2].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                    company_phone = item.text_content().split("\n")[1].split("(")
                    if (len(company_phone) >1) & ("电话" in item.text_content().split("\n")[1].split("(")[-1]):
                        brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                        brand_message_list[item_count]["company_phone"] = item.text_content().split("\n")[1].split("(")[-1].replace("(","").replace(")","").replace(" ","").replace("\n","").replace("\t","")
                    else:
                        brand_message_list[item_count]["company"] = "(".join(item.text_content().split("\n")[1].split("(")[:-1]).replace(" ","").replace("\n","").replace("\t","")
                        brand_message_list[item_count]["company_phone"] = "未知"
                    item_count += 1
                print(brand_message_list)
                item_count = 0
                for item in response_pyquery("div.tbcell>img"):
                    brand_message_list[item_count]["company_logo"] = item.attrib["src"]
                    item_count += 1
                # print(brand_message_list)
                item_count = 0
                for item in response_pyquery("div.desc"):
                    brand_message_list[item_count]["company_products"] = []
                    for product in item:
                        brand_message_list[item_count]["company_products"].append(product.text_content())
                    item_count += 1
                for item in brand_message_list:
                    print(item)
                    mongo_database["brand_warehouse_qmg_by_level"].update({"company":item["company"],"brand_name_cn":item["brand_name_cn"],"brand_name_en":item["brand_name_en"]},{"$set":item,"$addToSet":{"brand_cate":brand_message["brand_cate"]}},True)
                print("current classify is %s; current page is %d" % (brand_message["brand_cate"]["catid"],page_count))
                page_count += 1
                
            except Exception as e:
                print("the page is over")
                break

    



if __name__ == '__main__':
    get_href()

