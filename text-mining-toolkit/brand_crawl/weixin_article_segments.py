import jieba
import jieba.analyse
import jieba.posseg as pseg
from collections import Counter
jieba.load_userdict("/home/admin/ETL_PYTHON_3/csvs/sogou_scel_dicts.txt")
jieba.analyse.set_stop_words("/mnt/shared-dir/text-mining-toolkit/brand_crawl/hgd_stop_words.txt")
jieba.add_word('石墨烯')
jieba.del_word('自定义词')
jieba.suggest_freq(('中', '将'), True)
def content_cut_and_segment_frequencies(content,cut_all=False):
    """
    分词及词数统计
    也可以转换为tf 统计
    分词操作中不包含去除停用词

    """
    word_count = {}
    seg_list = jieba.cut(content,cut_all)
    segment_count = Counter(seg_list)
    sorted_list = sorted(segment_count.items(), key=lambda pair: pair[1], reverse=True)
    # segment_count.most_common()
    file = open("/mnt/shared-dir/text-mining-toolkit/brand_crawl/hgd_stop_words.txt","rb")
    stop_words_list = file.read().decode("utf-8").split("\n")
    for key,value in sorted_list:
        if key not in stop_words_list:
            print("in :",key,value)
        else:
            print("not in :",key,value)
        


def segment_tf_idf(content):
    """
    选出关键词
    根据tf-idf
    该算法确实是对文本求tf 然后用默认的idf算出tf-idf权重
    """
    # jieba.analyse.set_idf_path()
    jieba.analyse.extract_tags(content, topK=20, withWeight=False, allowPOS=())

def segment_pseg(content):
    """
    词性标注
    """

# def segment_frequencies():

#     pass

    