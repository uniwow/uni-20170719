#!/usr/bin/env python
# coding: utf-8

from pykafka import KafkaClient
from bson import ObjectId
from pymongo import MongoClient
import requests
import re
import time
import fcntl
import subprocess


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=mongo_config['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e

if __name__ == '__main__':

    ARTICLE_ATTRS = ['mongo_object_id', 'weixin_id', 'title', 'content', 'digest']

    MONGO = {
        'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse",
        "connectTimeoutMS": 50000,
        "socketTimeoutMS": 10000,
        "waitQueueTimeoutMS": 10000,
    }

    KAFKA_HOSTS = "139.129.196.32:9092,139.129.196.32:9093,139.129.196.32:9094"
    ZOOKEEPR_HOST = "139.129.196.32:2181,139.129.196.32:2182,139.129.196.32:2183"
    '''
    连接mongodb的文本表
    '''
    db = MongoConnect(MONGO).db
    collection = db['media_weixin_article_text_info']

    connect_error_occupied = False
    second_script_executing = False
    kafka_has_error = False
    while True:
        print('------------------开始一次循环')
        consumer = []

        if connect_error_occupied:
            second_script_executing = True
        try:
            client = KafkaClient(hosts=KAFKA_HOSTS, socket_timeout_ms=3 * 1000)
            topic = client.topics[bytes('media_weixin_article', encoding='utf-8')]
            # consumer = topic.get_simple_consumer()
            consumer = topic.get_balanced_consumer(
                reset_offset_on_start=False,
                consumer_group=bytes('article_text', encoding='utf-8'),
                auto_commit_enable=True,
                auto_commit_interval_ms=1000,
                zookeeper_connect=ZOOKEEPR_HOST
                )

            if consumer is not None:
                print('consumer is not none...')

                file = open('/alidata1/dts/export_from_mongo/article_wordle/wordle-%s.csv' % time.strftime('%Y-%m-%d', time.localtime(time.time())), 'ab')
                file_create_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))

                for message in consumer:
                    '''
                    当kafka连接恢复正常时，发送标志文件告知备用程序。
                    '''
                    if kafka_has_error:
                        kafka_has_error = False
                        print('kafka is reconnected successfully')
                        reconnect_file = open('./_SUCCESS', 'w+')
                        reconnect_file.close()
                    if message is not None:
                        # print(message.offset, message.value.decode(encoding='utf-8'))
                        msg = message.value.decode(encoding='utf-8').split("*|233|*|")
                        object_id = ''
                        weixin_id = ''
                        title = ''
                        content = ''
                        digest = ''
                        msg_len = len(msg)
                        if msg_len > 0:
                            object_id = msg[0]
                        if msg_len > 1:
                            weixin_id = msg[1]
                        if msg_len > 2:
                            title = msg[2]
                        if msg_len > 3:
                            content = msg[3]
                        if msg_len > 4:
                            digest = msg[4]
                        if len(object_id) == 24:
                            segment = requests.post("http://115.28.162.102:9999/segment", data={
                                'userId': 'test',
                                'secretId': 'test',
                                'withFlag': 'false',
                                'text': title,
                                'text_1': content,
                                'text_2': digest,
                            }).text.split('*|233|*|')
                            segment_len = len(segment)
                            title_segment = ''
                            content_segment = ''
                            digest_segment = ''
                            if segment_len > 0:
                                title_segment = re.sub(r'\s+', ' ', segment[0]).strip()
                            if segment_len > 1:
                                content_segment = re.sub(r'\s+', ' ', segment[1]).strip()
                            if segment_len > 2:
                                digest_segment = re.sub(r'\s+', ' ', segment[2]).strip()

                            wordle_json_orig = t1 = requests.post("http://115.28.162.102:9999/wordle", data={
                                'userId': 'test',
                                'secretId': 'test',
                                'title': title.strip(),
                                'content': content.strip(),
                                'digest': digest.strip(),
                            })
                            wordle_json = wordle_json_orig.text[1:-1].split(',')
                            wordle = []
                            try:
                                if len(content) > 3:
                                    for tuple in wordle_json:
                                        wordle.append({
                                            "word": tuple.split(':')[0].strip()[1:-1],
                                            "value": tuple.split(':')[1].strip()
                                        })
                            except Exception as wordle_exception:
                                print('wordle has error.')
                                pass
                            try:
                                post = {
                                    "_id": ObjectId(object_id),
                                    "weixin_id": weixin_id,
                                    "text_info": [
                                        {
                                            "weixin_id": weixin_id,
                                            "title_segment": title_segment,
                                            "content_segment": content_segment,
                                            "digest_segment": digest_segment,
                                            "create_time": int(time.time())
                                        }
                                    ],
                                    "wordle_info": [
                                        {
                                            "wordle": wordle,
                                            "create_time": int(time.time())
                                        }
                                    ]
                                }
                                collection.insert_one(post)
                                print("message_offset is %s, ObjectId is %s, operation is create." %
                                      (message.offset, object_id))
                            except Exception as e:
                                print(e)
                                print("This ObjectId %s is already existed.\nTrying to update it..." % object_id)
                                collection.update_one({'_id': ObjectId(object_id)}, {
                                        '$set': {
                                            "text_info": [
                                                {
                                                    "weixin_id": weixin_id,
                                                    "title_segment": title_segment,
                                                    "content_segment": content_segment,
                                                    "digest_segment": digest_segment,
                                                    "create_time": int(time.time())
                                                }
                                            ],
                                            "wordle_info": [
                                                {
                                                    "wordle": wordle,
                                                     "create_time": int(time.time())
                                                }
                                            ]}})
                                print("message_offset is %s, ObjectId is %s, operation is update." %
                                      (message.offset, object_id))

                            if file_create_date != time.strftime('%Y-%m-%d', time.localtime(time.time())):
                                file.close()
                                file_create_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
                                file = open('/alidata1/dts/export_from_mongo/article_wordle/wordle-%s.csv' % time.strftime('%Y-%m-%d', time.localtime(time.time())), 'ab')

                            import json
                            json_temp = {
                                "weixin_id": str(weixin_id),
                                "wordle": wordle
                            }
                            wordle_weixin_id = json.dumps(json_temp, ensure_ascii=False)

                            fcntl.flock(file, fcntl.LOCK_EX)
                            file.write(bytes(wordle_weixin_id, encoding='utf-8'))
                            file.write(bytes('\n', encoding='utf-8'))
                            fcntl.flock(file, fcntl.LOCK_UN)


                                # print(message.offset, message.value.decode(encoding='utf-8'))
                        else:
                            print('ObjectId: %s is not a valid ObjectId.' % object_id)
                    else:
                        print('none')
        except Exception as ee:
            print(ee)
            '''
            连接错误，如果备用脚本不在运行，启动备用脚本。
            '''
            # consumer.stop()
            connect_error_occupied = True
            if not second_script_executing:
                print('INFO: call second script')
                kafka_has_error = True
                # subprocess.Popen('python3 temp_read_from_mongodb.py', stdin=None, stdout=None, stderr=None)
            time.sleep(10)
