#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: mongo.py
@time: 2017/2/15 10:45
"""

import pymongo

from .. import ConfigReader


class Mongo(object):

    def __init__(self, db):
        db = "mongo-dts-" + db
        host = ConfigReader.get(db, "host")
        database = ConfigReader.get(db, "db")
        self.__connection = pymongo.MongoClient(host=host)
        self.db = self.__connection[database]

    def __del__(self):
        try:
            if hasattr(self, "__connection"):
                self.__connection.close()
        except Exception:
            pass

#if __name__ == "__main__":
    #mongo = Mongo(db="datawarehouse")
    #collection = mongo.db["media_weixin"]
    #print(collection.count())
