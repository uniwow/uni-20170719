#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8


def format_seconds(second):
    """
    将秒数格式化成d天h时m分s秒的格式
    :param second:
    :return:
    """
    second = int(second)
    d = int(second / 86400)
    rest = second - d * 86400
    h = int(rest / 3600)
    rest -= h * 3600
    m = int(rest / 60)
    s = rest - m * 60
    return "%s天%s时%s分%s秒" % (d, h, m, s)


if __name__ == "__main__":
    pass
    
    