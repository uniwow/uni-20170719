#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: __base.py
@time: 2017/3/14 10:52
"""

from abc import ABCMeta, abstractmethod
import os

from ..common import Logger


class BaseWorker(object):

    __metaclass__ = ABCMeta

    def __init__(self, logger):
        assert isinstance(logger, Logger)
        self.logger = logger

    @abstractmethod
    def run(self):
        pass

    def combine_es_dict(self, target_file):
        if os.path.isfile(target_file):
            # 备份原有词典
            project_dir = "/data/wwwroot/dict.womdata.com"
            target_host = "root@114.215.140.70"
            cmd_prefix = "ssh %s -i ~/.ssh/qmg_spider_id_rsa" % target_host
            os.system('{cmd_prefix} "{workdir};{ls};{rm_bak};{bak};{ls};{wc}"'.format(
                cmd_prefix=cmd_prefix,
                workdir="cd %s" % project_dir,
                ls="ls -al",
                bak="cp -f womdata-dict.txt womdata-dict.txt.bak",
                wc="wc womdata-dict.txt",
                rm_bak="/bin/rm -f womdata-dict.txt.bak"
            ))
            # 上传新文件
            scp_cmd_prefix = "scp -i ~/.ssh/qmg_spider_id_rsa"
            os.system("{scp_cmd_prefix} {target_file} {target_host}:{project_dir}".format(
                target_host=target_host,
                target_file=target_file,
                project_dir=project_dir,
                scp_cmd_prefix=scp_cmd_prefix
            ))
            # 合并新旧词典并且替换
            os.system('{cmd_prefix} "{workdir};{ls};{combine};{rm_old};{add_new};{rm_temp};{rm_dict}"'.format(
                cmd_prefix=cmd_prefix,
                workdir="cd %s" % project_dir,
                ls="ls -al",
                combine="cat womdata-dict.txt dict.txt | sort | uniq | cat >> temp.txt",
                rm_old="/bin/rm -f womdata-dict.txt",
                add_new="mv temp.txt womdata-dict.txt",
                rm_temp="/bin/rm -f temp.txt",
                rm_dict="/bin/rm -f dict.txt"
            ))

        self.logger.debug("合并线上词典成功")
