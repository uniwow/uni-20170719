#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
获取搜狗拼音最新热词
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: get_sogou_popular_words.py
@time: 2017/3/13 15:49
"""

from datetime import datetime
from time import sleep
import sys
import os
import traceback
from urllib.parse import urlparse


import requests
import pyquery

from ..common import Logger, transform_and_save , transform ,Mongo , Config
from .. import package_name
from .__base import BaseWorker


class PopularWordsGetter(BaseWorker):

    SOGOU_PINYIN_HOME_PAGE = "http://pinyin.sogou.com/dict/"

    HTTP_HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/56.0.2924.87 Safari/537.36",
        "Host": "pinyin.sogou.com",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Referer": "http://pinyin.sogou.com/dict/detail/index/60447?rf=dictindex&pos=slidebanner"
    }

    def __init__(self, logger_,connect):

        super(PopularWordsGetter, self).__init__(logger=logger)

        self.today_date = datetime.now().strftime("%Y%m%d")
        self.sougouDict = connect.db['sougou_dict']

    #获取所有的分类下面的所有的下载链接
    def __getSougouUrlCatAll(self):
        response = requests.get(url=self.__class__.SOGOU_PINYIN_HOME_PAGE,
                                headers=self.__class__.HTTP_HEADERS)
        pq = pyquery.PyQuery(response.text)

        #所有的末级分类
        texts = []
        def getAlink(i,e):
            e = pq(e)
            insert_link = "http://pinyin.sogou.com"+e.attr('href')
            parse_link = urlparse(insert_link)
            insert_parse_link = "http://pinyin.sogou.com"+parse_link.path
            append_info = {
                'name' : e.text(),
                'link' : insert_link,
                'page_link': insert_parse_link
            }
            texts.append(append_info)
        pq.find(".catewords a").each(getAlink)

        #获取末级分类下的下载链接
        down_link_all = []
        for textlist in texts:
            if "dictindex" in textlist['link']:
                links_get = PopularWordsGetter.getPageDownloadLink(self,page_url = textlist['link'])
                insert = {
                    "cat_name" : textlist['name'],
                    "cat_detail" : links_get
                }
                down_link_all.append(insert)
                #PopularWordsGetter.addWord(insert_info,textlist['name'])
                #insert_info.append(textlist['name'])
                print(textlist['name'])
            page = 1
            while(page >0):
                page += 1
                url_add = textlist['page_link']+'/default/'+str(page)
                req = requests.get(url_add)
                req_html = pyquery.PyQuery(req.text)
                req_detail = req_html.find('.dict_detail_block')
                links_get = PopularWordsGetter.getPageDownloadLink(self, page_url=url_add)
                insert = {
                    "cat_name": textlist['name'],
                    "cat_detail": links_get
                }
                down_link_all.append(insert)
                print(textlist['name'])
                print(page)
                if len(req_detail) == 0:
                    break

        back_detail_link = []
        #插入mongo
        for insert in down_link_all:
            for detail_link_info in insert['cat_detail']:
                back_detail_link.append(detail_link_info['detail_link'])

        return back_detail_link

    #获取详情页的链接
    def getPageDownloadLink(self,page_url):
        response = requests.get(page_url)
        pq  = pyquery.PyQuery(response.text)
        texts = []
        def getAlink(i, e):
            e = pq(e)
            append_info = {
                'dict_name': e.text(),
                'detail_link': "http://pinyin.sogou.com" + e.attr('href')
            }
            texts.append(append_info)
        pq.find('.detail_title a').each(getAlink)
        return texts

    def __get_popular_words(self):
        success = False

        while not success:
            response = requests.get(url=self.__class__.SOGOU_PINYIN_HOME_PAGE,
                                    headers=self.__class__.HTTP_HEADERS)
            if response.status_code == 200:
                success = True
                pq = pyquery.PyQuery(response.text)

                # 爬取热词
                words = []
                uls = pq.find("#hot_word_list ul")
                for index,element in enumerate(range(0, uls.length)):
                    ul = uls.eq(index)
                    lis = ul.find("li")
                    for i in range(0, lis.length):
                        li = lis.eq(i)
                        word = li.find("a").remove("span").text().strip()
                        self.logger.debug(word)
                        words.append(word)
                #将热词放入mongo
                today_mark = datetime.now(Config['tz']).strftime("%Y-%m-%d");
                hot_words = {
                    "dict_cat": '热词指数',
                    "dict_name": "hot_words",
                    "word_list": words,
                    "update_time": datetime.now(Config['tz']).strftime("%Y-%m-%d %H:%M:%S")
                }
                hasSougouDict = self.sougouDict.find_one({"dict_name":"hot_words"})
                if hasSougouDict == None:
                    self.sougouDict.insert(hot_words)
                else:
                    timeCheck = hasSougouDict['update_time'].split(' ')
                    if today_mark != timeCheck[0]:
                        self.sougouDict.update({"dict_name":"hot_words"},{'$push':{"word_list":words}})

                # 流行词库页面urls
                popular_dict_page_urls = []
                carousels = pq.find("#newdict_show #slider li")
                print(carousels.length)
                for i in range(0, carousels.length):
                    div = carousels.eq(i)
                    popular_dict_page_urls.append(div.find("a").attr("href"))
                today_date = self.today_date
                dir_name = "./%s/runtime/scels/source/%s" % (package_name, today_date)
                if not os.path.isdir(dir_name):
                    os.mkdir(dir_name)
                # 推荐词库URL
                recommend_dict_urls = []
                recommend_dict_lis = pq.find("#dict_rcmd_list ul li")
                for index in range(0, recommend_dict_lis.length):
                    recommend_dict_urls.append("http://pinyin.sogou.com%s" %
                                               pq.find("#rcmd_pic_%s a" % index).attr("href"))


                #获取所有的分类链接下的下载链接
                getDetailUrl = self.__getSougouUrlCatAll()

                for url in popular_dict_page_urls + recommend_dict_urls + getDetailUrl:
                    try:
                        response = requests.get(url=url, headers=self.__class__.HTTP_HEADERS)
                        if response.status_code == 200:
                            self.logger.debug("进入页面:%s", url)
                            pq = pyquery.PyQuery(response.text)
                            #查找
                            dict_cat = pq.find('.cate_no_child.select_now a').text();
                            print(dict_cat)

                            if url.find("search_list") > 0:
                                dict_sets = pq.find("#dict_detail_list .dict_detail_block")
                                for i in range(dict_sets.length):
                                    dict_ele = dict_sets.eq(i)
                                    dict_url = dict_ele.find(".dict_detail_show .dict_dl_btn a").attr("href")
                                    dict_name = dict_ele.find(".dict_detail_title_block .detail_title a").text().strip()
                                    self.__download_dict(dir_name, dict_url, dict_name)

                            else:
                                dict_url = pq.find("#dict_dl_btn").find("a").attr("href")
                                dict_name = pq.find("#dict_detail_block .dict_detail_title").text().strip()
                                print(dict_name)
                                self.__download_dict(dir_name, dict_url, dict_name)

                            #将词典存入mongo
                            open_target = "%s/%s.scel" % (dir_name, dict_name)
                            wordsDict = []
                            for word_ in transform(source_file_or_dir=open_target):
                                wordsDict.append(word_)
                            download_dict = {
                                "dict_cat": dict_cat,
                                "dict_name": dict_name,
                                "word_list": wordsDict,
                                "update_time": datetime.now(Config['tz']).strftime("%Y-%m-%d %H:%M:%S")
                            }
                            hasSougouDict = self.sougouDict.find_one({"dict_name": dict_name})
                            if hasSougouDict == None:
                                self.sougouDict.insert(download_dict)
                            else:
                                pass
                    except Exception as e:
                        self.logger.error("进入页面失败：%s", url)
                        exc_type, exc_value, exc_tb = sys.exc_info()
                        self.logger.error(".".join(traceback.format_exception(exc_type, exc_value, exc_tb)))

                # 爬取推荐词库

                # 转换scel词库为txt文件
                target_dir = "./%s/runtime/scels/target/%s" % (package_name, today_date)
                if not os.path.isdir(target_dir):
                    os.mkdir(target_dir)
                target_file = "%s/dict.txt" % target_dir
                transform_and_save(dir_name, target_file)
                self.logger.debug("转换词库成功")

                # 合并词库
                with open(target_file, "a+") as f:
                    f.write("\n".join(words))
                self.logger.debug("合并词库成功")

                return target_file

    def run(self):
        target_file = self.__get_popular_words()
        if target_file is not None:
            self.combine_es_dict(target_file)

    def __download_dict(self, dir_name, dict_url, dict_name):
        response = requests.get(dict_url)
        if response.status_code == 200:
            with open("%s/%s.scel" % (dir_name, dict_name), "wb") as f:
                f.write(response.content)
            self.logger.debug("下载词库:%s 成功", dict_name)


if __name__ == '__main__':
    logger = Logger(log_file="popular-words-getter.log",
                    app_name="PopularWordsGetter",
                    app_type=0)
    #连接mongo数据库
    mongoConnect = Mongo(db="local")

    getter = PopularWordsGetter(logger_=logger, connect=mongoConnect)
    getter.run()
