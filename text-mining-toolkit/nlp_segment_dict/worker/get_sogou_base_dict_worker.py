#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
搜狗输入法基本词典爬取，http://pinyin.sogou.com/dict/cate/index/
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: get_sogou_base_dict.py
@time: 2017/3/14 11:20
"""
from time import sleep
import sys
import traceback

import requests
import pyquery

from .__base import BaseWorker
from .. import package_name
from ..common import Logger, transform_and_save

# 我们采取手动设搜狗词典的分类及分类下面页数的信息，这里只包含大类
SOGOU_DICT_CONF = [
    (167, 10), (1, 31), (76, 49), (96, 79), (127, 10), (132, 31), (436, 108), (154, 20), (389, 79), (367, 17),
    (31, 106), (403, 154)
]


class BaseSogouDictGetter(BaseWorker):

    HTTP_HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/56.0.2924.87 Safari/537.36",
        "Host": "pinyin.sogou.com",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Referer": "http://pinyin.sogou.com/dict/detail/index/60447?rf=dictindex&pos=slidebanner"
    }

    def __init__(self, logger_, skip=0):
        super(BaseSogouDictGetter, self).__init__(logger=logger_)
        self.dir_name = "./%s/runtime/scels/source/base" % package_name
        self.target_file = "./%s/runtime/scels/target/base/dict.txt" % package_name
        self.skip = skip

    def __generate_dict_cate_and_page(self):
        """
        生成搜狗词典对应的分类和分类下词典的页数
        :return:
        """
        self.logger.debug("生成搜狗词典配置文件")
        yield from SOGOU_DICT_CONF

    def __execute_task(self):

        count = 0
        for item in self.__generate_dict_cate_and_page():
            cate = item[0]
            for page in range(1, int(item[1]) + 1):
                url = "http://pinyin.sogou.com/dict/cate/index/%s/default/%s" % (cate, page)
                self.__class__.HTTP_HEADERS["Referer"] = url
                try:
                    response = requests.get(url=url,
                                            headers=self.__class__.HTTP_HEADERS)
                    if response.status_code == 200:
                        self.logger.debug("成功进入页面：%s", url)
                        pq = pyquery.PyQuery(response.text)
                        eles = pq.find("#dict_detail_list .dict_detail_block")
                        for i in range(0, eles.length):
                            count += 1
                            if count > self.skip:
                                ele = eles.eq(i)
                                title = ele.find(".dict_detail_title_block .detail_title a").text().strip()
                                url = ele.find(".dict_detail_show .dict_dl_btn a").attr("href")
                                self.__download_dict(url, title)
                            sleep(1)
                except Exception:
                    self.logger.error("进入页面失败：%s", url)
                    exc_type, exc_value, exc_tb = sys.exc_info()
                    self.logger.error(".".join(traceback.format_exception(exc_type, exc_value, exc_tb)))

    def __download_dict(self, dict_url, dict_name):
        try:
            response = requests.get(dict_url)
            if response.status_code == 200:
                with open("%s/%s.scel" % (self.dir_name, dict_name), "wb") as f:
                    f.write(response.content)
                self.logger.debug("下载词库:%s 成功", dict_name)
        except Exception:
            self.logger.debug("下载词库:%s 失败", dict_name)
            exc_type, exc_value, exc_tb = sys.exc_info()
            self.logger.error(".".join(traceback.format_exception(exc_type, exc_value, exc_tb)))

    def __generate_txt_dict(self):
        transform_and_save(self.dir_name, self.target_file)
        self.logger.debug("生成词典成功")

    def run(self):
        # self.__execute_task()
        # self.__generate_txt_dict()
        self.combine_es_dict(self.target_file)


if __name__ == '__main__':
    logger = Logger(log_file="base-dict-getter.log",
                    app_name="BaseSogouDictGetter",
                    app_type=0)
    worker = BaseSogouDictGetter(logger_=logger, skip=30)
    worker.run()
