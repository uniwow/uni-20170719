#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

import pkgutil
from configparser import ConfigParser

__version__ = "0.1"

package_name = "nlp_segment_dict"
module_name = "NLPSegmentDict"

ConfigReader = ConfigParser(allow_no_value=True)
ConfigReader.read_string(pkgutil.get_data(package_name, 'config.ini').decode("utf-8"))
