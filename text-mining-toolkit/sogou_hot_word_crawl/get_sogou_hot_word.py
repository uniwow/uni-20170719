#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import time
import requests
from pathlib import Path
from bs4 import BeautifulSoup
if __name__ == '__main__':
    start_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
    file_log = open('.\\get_new_word_log.txt', 'a+')
    url = 'http://pinyin.sogou.com/dict/'
    req = requests.get(url)
    soup = BeautifulSoup(req.text, 'lxml')
    r = soup.find_all('div', attrs={'id': "hot_word_show_left"})
    if r == None:
        print('something unexpected happened.')
        end_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        file_log.write('start at %s end with %s ERROR con not get response from %s by requests.get()\n'
                       % (start_time, end_time, url))
    else:
        file_read_path = '.\\new_word.txt'
        my_file = Path(file_read_path)
        if not my_file.is_file():
            f_ = open(file_read_path, 'w+')
            f_.close()
        file_read = open(file_read_path, 'r').readlines()
        old_word = set()
        new_word_num = 0
        for old in file_read:
            old_word.add(old.split(' ')[0])
        file_output_name = '.\\new_word.txt'
        file = open(file_output_name, 'a+')
        for i in r:
            for ii in i.find_all('li'):
                for iii in re.findall('t">.*</span>.*</a>', str(ii.a)):
                    tmp = iii[3:-4].split('</span>')
                    if not tmp[1] in old_word:
                        file.write('%s %s n\n' % (tmp[1], tmp[0]))
                        new_word_num += 1
        end_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        file_log.write('start at %s end with %s INFO found and added %d new word(s) into file %s\n'
                       % (start_time, end_time, new_word_num, file_output_name))


