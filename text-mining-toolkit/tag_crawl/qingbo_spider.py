
from base_tag_spider import BaseTagSpider
import collections
from pymongo import MongoClient


MONGO = {
        'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse",
    }


class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=MONGO['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e


class Qingb(BaseTagSpider):

    def parse_etree_from_get(self, etree):
        weixin_id_etree = etree.xpath("//li/div[@class='number-txt']")
        weixin_id = list(
            map(lambda x: x.xpath("./p[@class='wx-tt']/span[@class='wx-width']/span/text()")[0], weixin_id_etree))
        tags = list(map(lambda x: x.xpath("./p[@class='wx-sp']/span//a[@class='tags']/text()"), weixin_id_etree))
        return collections.OrderedDict(zip(weixin_id, tags))


def main():
    mongo_connect = MongoConnect(MONGO)
    collection = mongo_connect.db['newrank_test']

    weixin_id_list = ["rmrbwx", "suqunbasketball", "zhangzhaozhong45"]

    qingb = Qingb("qingbo", collection=collection)
    qingb.start_crawl(weixin_id_list,
                      prefix_url="http://www.gsdata.cn/query/wx?q=",
                      postfix_url="",
                      page="&page=")

if __name__ == '__main__':
    main()
