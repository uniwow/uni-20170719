from base_tag_spider import BaseTagSpider
import collections
from pymongo import MongoClient
from lxml import etree


MONGO = {
        'host': "mongodb://wom-dts-datawarehouse-admin:wom-dts-datawarehouse-admin"
                "@115.28.26.225:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse",
    }



class MongoConnect:
    def __init__(self, mongo_config):
        try:
            self.connection = MongoClient(host=MONGO['host'])
            self.db = self.connection[mongo_config['db']]
        except Exception as e:
            print('error occupied while connect to mongodb')
            raise e


class NewRankSpider(BaseTagSpider):

    def parse_etree_from_get(self, etree):
        weixin_id = [str(str(etree.xpath("//p[@class='info-detail-head-weixin-account clearfix']/span/text()")[0])
                         .replace("微信号：", ""))]
        print(weixin_id)
        tags = [etree.xpath("//ul[@class='tag-name-list']//text()")]
        print(weixin_id)
        print(tags)
        print(collections.OrderedDict(zip(weixin_id, tags)))
        return collections.OrderedDict(zip(weixin_id, tags))


def main():
    splash_server = "http://139.129.196.32:8050/execute"
    lua_script = """
            function main(splash)
                splash.js_enabled = true
                splash.images_enabled = false
                splash.private_mode_enabled = false
                splash.plugins_enabled = true
                splash:set_custom_headers({
                    ["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
                })
                splash:go(splash.args.url)
                splash:wait(5)
                return splash:html()
            end
        """
    # response = requests.get("%s/execute" % splash_server, params={
    #     'lua_source': lua_script,
    #     'timeout': 60,
    #     'url': url
    # })

    metho_params = {
        'lua_source': lua_script,
        'timeout': 60,
    }

    weixin_id_list = ["suqunbasketball", "zhangzhaozhong45"]

    mongo_connect = MongoConnect(MONGO)
    collection = mongo_connect.db['newrank_test']

    newr = NewRankSpider("newrank", splash_server=splash_server, method_params=metho_params, collection=collection)
    print("---")
    newr.start_crawl(weixin_id_list,
                      prefix_url="http://www.newrank.cn/public/info/detail.html?account=")


if __name__ == '__main__':
    main()
