#!/usr/bin/env python
# coding: utf-8

import abc
import requests
from lxml import etree
import random
import time


class BaseTagSpider:

    __metaclass__ = abc.ABCMeta

    def __init__(self, name,
                 method="Get",
                 collection=None,
                 page_size_max=5,
                 retry_times=5,
                 char_set="utf-8",
                 post_data=None,
                 cookies=None):
        """init the base tag spider

        :param name: (required) spider name.
        :param method: (optional) Request method. Default to ''Get''.
        :param collection: (required) Collection: mongodb collection.
        :param page_size_max: (optional) Int. the max number of url page we want to crawl. Defaults to ''5''.
        :param retry_times: (optional) Int. the max retry times of request. Defaults to ''5''.
        :param char_set: (optional) str. the char-set of web page. Defaults to ''utf-8''.
        :param post_data: (optional) Dictionary of request post data. the char-set of web page. Defaults to None.
        :param cookies: (optional) Dictionary mapping Cookies to the cookies.
        """
        self.name = name
        self.method = method.lower()
        self.collection = collection
        self.page_size_max = page_size_max
        self.retry_times = retry_times
        self.char_set = char_set
        self.post_data = post_data
        if cookies is None:
            self.cookies = {
                "Cookies": ""
            }
        else:
            self.cookies = cookies

    def insert2mongodb(self, weixin_id, weixin_dict):
        """ insert weixin dict to mongodb

        :param weixin_id: str.
        :param weixin_dict: Dictionary mapping weixin_id(str) to tags(list).
        :return: None
        """
        if weixin_dict is not None and weixin_id in weixin_dict:
            print(type(weixin_dict[weixin_id]))
            cate_list = list(map(lambda x: {"name": x}, weixin_dict[weixin_id]))
            self.collection.update_one({"weixin_id": weixin_id}, {
                "$set": {
                    "cate.%s" % str(self.name): cate_list
                }
            }, upsert=True)

    @abc.abstractmethod
    def parse_etree_from_get(self, etree):
        """Parse etree to dict.
        :param etree: etree from lxml.
        :return: dict('weixin_id': ['tag0', 'tag1', ...])
        """
        raise NotImplemented("not implement this abstract method")

    @abc.abstractmethod
    def parse_from_post(self, response):
        """Parse array to dict.
        :param response: Dictionary of post method's response.
        :return: dict('weixin_id': ['tag0', 'tag1', ...])
        """
        raise NotImplemented("not implement this abstract method")

    def request_from_url(self, url):
        """send a request with input url.

        :param url: Url for request's get method.
        :return: Dictionary mapping weixin_id to tags
        """
        user_agent = [
            "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0",
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
            'Chrome/53.0.2785.116 Safari/537.36',
            'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) '
            'Chrome/47.0.2526.80 Safari/537.36 Core/1.47.933.400 QQBrowser/9.4.8699.400',
        ]
        get_params = {
            "User-Agent": random.choice(user_agent),
        }
        if self.method == "get":
            response = requests.get(url=url, headers=get_params, cookies=self.cookies)
            while response.status_code != 200 and self.retry_times > 0:
                print('requests error, retry times remain %s ...' % self.retry_times)
                self.retry_times -= 1
                time.sleep(3)
                response = requests.get(url)

            if response.status_code != 200:
                # TODO:
                print('connection error occupied.')
                pass
            _etree = etree.HTML(response.content.decode('utf-8', 'ignore'))
            return _etree

        elif self.method == "post":
            response = requests.post(url=url, headers=get_params, cookies=self.cookies, data=self.post_data)
            while response.status_code != 200 and self.retry_times > 0:
                print('requests %s error, retry times remain %s ...' % (response.status_code, self.retry_times))
                self.retry_times -= 1
                time.sleep(3)
                response = requests.post(url=url, headers=get_params, cookies=self.cookies, data=self.post_data)
            else:
                if response.status_code != 200:
                    # TODO:
                    print('connection error occupied.')
                    pass
            # print(response.content.decode('utf-8'))
            print(response.status_code)
            return response

    def process(self, weixin_id, prefix_url, postfix_url, page):
        weixin_dict = dict()
        if self.method == "get":
            for page_num in range(1, self.page_size_max):
                if page == "":
                    page_num = ""
                query_url = prefix_url + weixin_id + postfix_url + page + str(page_num)
                page_num += 1
                print(query_url)
                etree = self.request_from_url(query_url)
                weixin_id_tag_dict = self.parse_etree_from_get(etree)
                weixin_dict.update(weixin_id_tag_dict)
                if page == "" or len(weixin_id_tag_dict) == 0:
                    break
            return weixin_dict
        elif self.method == "post":
            query_url = prefix_url + postfix_url
            response = self.request_from_url(query_url)
            weixin_dict = self.parse_from_post(response=response)
            return weixin_dict

    def start_crawl(self, weixin_id_list, prefix_url, postfix_url, page=""):
        for weixin_id in weixin_id_list:
            weixin_dict = self.process(weixin_id, prefix_url=prefix_url, postfix_url=postfix_url, page=page)
            self.insert2mongodb(weixin_id, weixin_dict)
            # self.task_log.write('task %s is finished.\n' % weixin_id)

    def destroy(self):
        self.task_log.close()
