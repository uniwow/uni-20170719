#### 用于接收文章内容并返回品牌词云
--------------------------------------
启动方式: 
python3 start.py 


验证方式: 
```
POST : http://192.168.0.113:9886/article-brand-wordle 
header : Content-Type:application/x-www-form-urlencoded 
```
参数 :
``` 
title:美的宝马大众喜马拉雅FM是我们的最爱保时捷保时捷保时捷 
content:喜马拉雅FM是我们的最爱保时捷保时捷保时捷娃哈哈 
digest:喜马拉雅FM是我们的最爱保时捷保时捷保时捷宝马 
```
返回值 : 
```
{"success": true, "wordle": [{"name": "保时捷", "value": 9}, {"name": "喜马拉雅FM", "value": 3}, {"name": "宝马", "value": 2}, {"name": "娃哈哈", "value": 1}, {"name": "美的", "value": 1}, {"name": "大众", "value": 1}]}
```
