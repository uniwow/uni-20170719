#!/usr/bin/env python3
# encoding: utf-8
import pytz
from collections import UserDict

__config = {
    # mongo 数据仓库
    "mongo-datawarehouse": {
        "host": "mongodb://dts-datawarehouse-operator:LCLEspiddaomzCLEots89d@118.190.175.5:27011/wom-dts-datawarehouse",
        "db": "wom-dts-datawarehouse"
    },
    # 配置时区
    "timezone": pytz.timezone("Asia/Shanghai"),
    "timezone-name": "Asia/Shanghai",
    # 是否开一调试模式
    "debug": True,
    # 词云api
    "word_cloud_api": "http://admin:ada1dfa29d@114.215.110.161:8088/wordle"
}


class Config(UserDict):

    def __getattr__(self, item):
        return self.data[item]

config = Config(dict=__config)

