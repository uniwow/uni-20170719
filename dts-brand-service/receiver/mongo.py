#!/usr/bin/env python3
# encoding: utf-8
import pymongo


class Mongo(object):

    """
    mongo connector
    example:
        mongo = Mongo(host="mongodb://dts-ops-operator:871304byarmp32@115.28.26.225:27011/wom-dts-ops",
                      db="wom-dts-ops")
    """
    def __init__(self, host, db):
        self.__connection = pymongo.MongoClient(host=host)
        self.db = self.__connection[db]

    def __del__(self):
        try:
            self.__connection.close()
        except RecursionError:
            pass
