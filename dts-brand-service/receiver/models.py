import json

from peewee import *
from playhouse.shortcuts import RetryOperationalError
from datetime import datetime
import requests

from receiver.config import config


class AutoReconnectMysql(RetryOperationalError, MySQLDatabase):
    pass

database = AutoReconnectMysql('wom_prod', **{
    'user': 'wom',
    'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com',
    'password': 'Qmg2016mw#semys$',
    'charset': 'utf8mb4'
})


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class DtsTaskOverview(BaseModel):
    account_uuid = CharField()
    consume_point = IntegerField()
    enable = IntegerField()
    first_monitor_time = IntegerField()
    is_url_effective = IntegerField()
    refund_point = IntegerField()
    task_code = CharField()
    task_create_time = IntegerField(null=True)
    task_cycle = IntegerField()
    task_name = CharField()
    task_status_config = TextField(null=True)
    task_uuid = CharField()
    uuid = CharField(unique=True)

    class Meta:
        db_table = 'dts_task_overview'


class DtsWeixinArticleMonitorTask(BaseModel):
    account_biz_code = CharField(null=True)
    account_uuid = CharField()
    article_metadata = CharField()
    article_url = TextField()
    article_url_detect_time = IntegerField(null=True)
    enqueue_status = IntegerField()
    first_monitor_time = IntegerField()
    init_status = IntegerField()
    is_url_effective = IntegerField()
    last_update_time = IntegerField()
    latest_crawl_time = IntegerField()
    monitor_status = IntegerField()
    scheduler_uuid = CharField(null=True)
    task_create_time = IntegerField()
    task_cycle = IntegerField()
    task_end_time = IntegerField()
    task_interval_freq = IntegerField(null=True)
    task_status = IntegerField(null=True)
    uuid = CharField(unique=True)

    class Meta:
        db_table = 'dts_weixin_article_monitor_task'

    def update_task(self, record: dict):

        # 数据第一次返回时需要更改的
        if not DtsWeixinArticleMonitorTaskOutput.exists(record["task_uuid"]):
            read_like_trend = record["read_like_trend"]
            read_like_trend = list(sorted(read_like_trend, key=lambda item: item["crawl_time"]))

            self.article_url_detect_time = read_like_trend[0]["crawl_time"]
            self.is_url_effective = 1
            self.first_monitor_time = read_like_trend[0]["crawl_time"]
            self.init_status = 1

            # 更新任务总览
            model = DtsTaskOverview.get(DtsTaskOverview.task_uuid == record["task_uuid"])
            model.task_name = record["article_title"]
            model.is_url_effective = 1
            model.first_monitor_time = read_like_trend[0]["crawl_time"]
            model.save()

        self.latest_crawl_time = record["latest_crawl_time"]

        self.last_update_time = record["latest_crawl_time"]
        self.save()


class DtsWeixinArticleMonitorTaskOutput(BaseModel):
    article_content = TextField(null=True)
    article_digest = TextField(null=True)
    article_dissemination_data = TextField(null=True)
    article_fixed_access_link = TextField(null=True)
    article_is_origin = IntegerField(null=True)
    article_publish_time = IntegerField()
    article_title = TextField()
    created_at = IntegerField()
    current_like_num = IntegerField()
    current_read_num = IntegerField()
    keyword_list = TextField(null=True)
    monitor_begin_like_num = IntegerField()
    monitor_begin_read_num = IntegerField()
    monitor_cnt = IntegerField()
    task_uuid = ForeignKeyField(db_column='task_uuid', rel_model=DtsWeixinArticleMonitorTask, to_field='uuid')
    updated_at = IntegerField()
    weixin = CharField(db_column='weixin_id', null=True)
    weixin_name = CharField()

    class Meta:
        db_table = 'dts_weixin_article_monitor_task_output'

    @classmethod
    def exists(cls, task_uuid):
        return bool(cls.select().where(cls.task_uuid == task_uuid).count())

    @classmethod
    def update_record(cls, task_uuid, record):

        if cls.exists(task_uuid):
            model = cls.get(cls.task_uuid == task_uuid)
        else:
            model = cls()

        # 新建的记录
        if model.id is None:
            model.task_uuid = record["task_uuid"]
            model.weixin = record["account_id"]
            model.weixin_name = record["account_nickname"]
            model.article_title = record["article_title"]
            model.article_digest = record["article_short_desc"]
            model.article_content = record["article_content"]
            model.article_fixed_access_link = record["article_url"]
            model.article_is_origin = record["is_orig"]
            model.article_publish_time = record["article_post_time"]

            # 获取词云
            try:
                response = requests.post(url=config.word_cloud_api, data={
                    "title": record["article_title"],
                    "content": record["article_content"],
                    "digest": record["account_desc"],
                }).json()

                if "wordle" in response:
                    model.keyword_list = json.dumps([{"value": value, "word": word}
                                                     for word, value in response["wordle"].items()],
                                                    ensure_ascii=False)

            except (requests.exceptions.RequestException, Exception) as e:
                print(e)
                model.keyword_list = "[]"

            model.created_at = int(datetime.now(config.timezone).timestamp())

        model.updated_at = int(datetime.now(config.timezone).timestamp())
        read_like_trend = record["read_like_trend"]
        read_like_trend = list(sorted(read_like_trend, key=lambda item: item["crawl_time"]))

        model.monitor_begin_read_num = read_like_trend[0]["read_num"]
        model.monitor_begin_like_num = read_like_trend[0]["like_num"]
        model.current_read_num = read_like_trend[-1]["read_num"]
        model.current_like_num = read_like_trend[-1]["like_num"]
        model.monitor_cnt = len(record["read_like_trend"])

        article_dissemination_data = []
        if len(record["read_like_trend"]) > 0:
            first = record["read_like_trend"][0]
            article_dissemination_data.append({
                "comment_num": 0,
                "incre_comment_num": 0,
                "incre_read_num": first["read_num"],
                "incre_like_num": first["like_num"],
                "read_num": first["read_num"],
                "like_num": first["like_num"],
                "ratio_like_read": round(first["like_num"] / first["read_num"], 3),
                "crawl_time": first["crawl_time"]
            })
            for index in range(1, len(record["read_like_trend"])):
                current = record["read_like_trend"][index]
                prev = record["read_like_trend"][index - 1]
                article_dissemination_data.append({
                    "comment_num": 0,
                    "incre_comment_num": 0,
                    "incre_read_num": current["read_num"] - prev["read_num"],
                    "incre_like_num": current["like_num"] - prev["like_num"],
                    "read_num": current["read_num"],
                    "like_num": current["like_num"],
                    "ratio_like_read": round(current["like_num"] / current["read_num"], 3),
                    "crawl_time": current["crawl_time"]
                })
        model.article_dissemination_data = json.dumps(article_dissemination_data)
        model.save()
