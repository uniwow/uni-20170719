#!/usr/bin/env python3
# encoding: utf-8
import sys

from tornado.ioloop import IOLoop
from tornado.web import Application
from tornado.options import options, define
from tornado.log import gen_log

from receiver.config import config
from receiver.article_brand_wordle import BrandWordleHandler


if __name__ == "__main__":

    if sys.platform.startswith("win"):
        gen_log.error("The service is only allowed to run on the unix platform.")
        sys.exit(-1)

    define("port", default=9886, help="Specify port number for server", metavar="9886")
    define("address", default="192.168.0.113", help="Specify ip address for server", metavar="192.168.0.113")
    options.parse_command_line()

    handlers = [
        # (r"/article-monitor-data-receiver", ReceiverHandler),
        (r"/article-brand-wordle",BrandWordleHandler)
    ]

    application = Application(handlers=handlers,
                              static_path="./static/",
                              autoreload=True,
                              debug=config.debug)

    application.listen(int(options.port), xheaders=True, address=options.address)
    IOLoop.current().start()
