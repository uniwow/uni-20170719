class MyException(Exception):
    '''异常父类'''


class CookieInvalid(MyException):
    '''cookie 失效异常'''
    def __str__(self):
         return "cookie invalid"

class CookieUsedUp(MyException):
    '''cookie 使用完毕'''
    def __str__(self):
    	return "cookie used up"

class AccountBans(MyException):
	'''account bans'''
	def __str__(self):
		return "account bans"

class KafkaCrash(MyException):
	"""docstring for KafkaCrash"""
	def __str__(self):
		return "kafka crashed"
		