# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_mysql import MysqlTool
from tools.tool_file import FileTool
from tools.settings import WOM_DEV_MYSQL_TUPLE,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, BACKUP_CSV_PATH_BIG_DATA, BACKUP_INDEX_COMPUTE_PATH, ZOOKEEPER, KAFKA, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, CHECK_CSV_PATH, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO
import multiprocessing
import time
import signal
from libs.media_weixin_check import MediaWeiXinCheck
from libs.media_weixin_storage import MediaWeiXinStorage
from libs.media_weixin_backups import MediaWeixinBackups
from libs.media_weixin_kafka_transform import MediaWeiXinKafkaTransform
from tools.tool_time import str_to_time_stamp
from tools.tool_mail import send_mail
import os
import sys
from tools.tool_system import get_pid
from spiders.native_spider import huajiao_video_crawler
from pymongo.errors import BulkWriteError
import json
from bson.objectid import ObjectId
from tools.tool_string import strip_and_replace_all,strip_and_replace
from my_exceptions.my_exceptions import KafkaCrash

def test_system():
    get_pid('python')


def test_hanyu():
    a = "俺去也"
    b = "去也"
    print(str(b in a))


def test_str_to_time_stamp():
    time_str = "2016-10-01 00:00:01"
    print(str(str_to_time_stamp(time_str)))


def media_weixin_mongo_storage():
    '''入库程序 1:'''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_storage_mongo()


def media_weixin_backups(backup_path, ip, port, suffix, collection_name, source_file_args):
    '''根据账号备份程序 2:'''
    media_weixin_backups_obj = MediaWeixinBackups()
    media_weixin_backups_obj.media_weixin_backups(
        backup_path, ip, port, suffix, collection_name, source_file_args)


def media_weixin_recovery(ip, port, user, pwd, db, collection_name, source_file_args):
    '''json文件入库程序 3:'''
    media_weixin_backups_obj = MediaWeixinBackups()
    media_weixin_backups_obj.media_weixin_recovery(
        ip, port, user, pwd, db, collection_name, source_file_args)


def media_weixin_mongo_collected(source_file_args, source_mongo_args, source_collection_args, target_file_args_list):
    '''账号列表是否抓取 4:'''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.file_mongo(
        source_file_args, source_mongo_args, source_collection_args, target_file_args_list)


def media_weixin_mysql_exists(source_file_args, source_mysql_args, source_table_args, target_file_args_list):
    '''账号是否存在mysql中 5:'''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.file_mysql(
        source_file_args, source_mysql_args, source_table_args, target_file_args_list)


def media_weixin_id_list(source_mongo_args, source_collection_args, target_file_args_list):
    '''满足条件的账号id 6:'''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.mongo_duplicate(
        source_mongo_args, source_collection_args, target_file_args_list)


def media_weixin_article_count(source_mongo_args, source_file_args, target_file_args):
    '''对应账号的文章总数 7:'''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_article_count(
        source_mongo_args, source_file_args, target_file_args)


def update_price_data():
    '''更改价格信息8:'''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_pub_type_update()


def media_weixin_id_1000():
    media_weixin_check_obj = MediaWeiXinCheck()
    pass


def worker_1(interval):
    count = 0
    count += 1
    print("worker_1")
    time.sleep(interval)
    print("end worker_1")
    print("count %d , %d " % (count, interval))


def worker_2(interval):
    print("worker_2")
    time.sleep(interval)
    print("end worker_2")


def worker_3(interval):
    print("worker_3")
    time.sleep(interval)
    print("end worker_3")


def mongo_test():
    # mongo_tool = MongoTool(*WOM_DTS_OPS_MONGO_TUPLE)
    # documents = mongo_tool.database['kafka_conventional_grow_trend_info'].find({}, {
    #                                                                            "kafka_message": 1})

    # print(str(documents.count()))
    # print(str(type(documents)))
    # for document in documents:
    #     kafka_message = json.loads(
    #         document['kafka_message'].decode("utf-8", 'ignore'))
    #     print(str(type(kafka_message)) + "----------------type")
    #     print(str(document['kafka_message'].decode("utf-8", 'ignore')))
    #     print(str(documents.count()))
    #     input('.............')
    # mongo_tool=MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    # document_cursor=mongo_tool.database['media_weixin'].find({},{"public_id":1}).limit(5)
    # document_cursor.batch_size(500)
    # target_file_tool=FileTool(*("/alidata1/dts/export_from_mongo/weixin_article","test.csv","ab"))
    # while mongo_tool.cursor_has_next(document_cursor):
    #     document=document_cursor.next()
    #     target_file_tool.write_database_line_to_file(document['public_id']+'\n')
    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    document = mongo_tool.database['media_weixin_article'].find_one(
        {'weixin_id':'123' })
    print(str(document is None))
    object_id = strip_and_replace_all(
        str(document["_id"]))
    mongo_obj_id = object_id
    weixin_id = strip_and_replace_all(
        str(document["weixin_id"]))
    title = ''
    if "title" in document:
        title = strip_and_replace_all(
            strip_and_replace(str(document["title"])))
    article_url = ''
    if "fixed_url" in document:
        article_url = strip_and_replace_all(
            strip_and_replace(str(document["fixed_url"])))
    short_desc = ''
    if "digest" in document:
        short_desc = strip_and_replace_all(
            strip_and_replace(str(document["digest"])))
    article_type = -1
    if "article_type" in document:
        article_type = int(document["article_type"])
    article_pos = -1
    if "article_pos" in document:
        article_pos = int(document["article_pos"])
    read_num = 0
    if "page_view_num" in document:
        read_num = int(document["page_view_num"])
    like_num = 0
    if "page_like_num" in document:
        like_num = int(document["page_like_num"])
    post_time = -1
    if "post_time" in document:
        post_time = int(document["post_time"])
    query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
        mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
    print(query)
    # while mongo_tool.cursor_has_next(document_cursor):
    #     document = document_cursor.next()
    #     target_file_tool.write_database_line_to_file(
    #         document['public_id'] + '\n')


def mysql_test():
    mysql_helper = MysqlTool(
        WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL['db'], WOM_PROD_MYSQL['user'], WOM_PROD_MYSQL['password'])
    '''
    unique_id_list = mysql_helper.get_unique_id_list(
        "video_media_huajiao", "media_name", [["grade", ">", 3], ["follower_num", "IS", "NOT NULL"]])
    for unique_id in unique_id_list[:1]:
        row = mysql_helper.find_one("video_media_huajiao", [
            "media_id", "verify_status", "follower_num"], "media_name", unique_id)
        print(str(type(row)))
        # mysql_helper.update_one("video_media_huajiao",{"grade":18,"signature":"我草啊,xxx"},"media_name",unique_id)
        # mysql_helper.insert_one("video_media_huajiao", {
        #                        "a": 1, "b": "aaa'bbb"})
    '''
    query = ("SELECT * FROM `media_weixin` WHERE `public_id`='11111111111111111'")
    mysql_helper.cursor.execute(query)
    row = mysql_helper.cursor.fetchone()
    print(str(row))


def test_bulk():
    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    bulk_test_coll = mongo_tool.database['meipai_user_info']
    print(str(type(bulk_test_coll.count())))
    print(bulk_test_coll.count())
    bulk = mongo_tool.unordered_bulk_options(bulk_test_coll)
    bulk.insert({'a': 0})
    bulk.insert({'a': 1})
    bulk.insert({'a': 2})
    bulk.insert({'a': 3})
    bulk.insert({'a': 4})
    bulk.insert({'a': 5})
    bulk.insert({'a': 6})
    bulk.insert({'a': 7})
    bulk.insert({'a': 8})
    bulk.insert({'a': 9})
    bulk.insert({'a': 10})
    bulk.insert({'a': 11})
    bulk.insert({'a': 12})
    bulk.insert({'a': 13})
    bulk.insert({'a': 14})
    bulk.insert({'a': 15})
    bulk.insert({'a': 16})
    bulk.insert({'a': 17})
    bulk.insert({'a': 18})
    input('..............')
    try:
        bulk.execute({'w': 3, 'wtimeout': 1})
        bulk.execute({'w': 3, 'wtimeout': 1})
    except BulkWriteError as bwe:
        # pass
        print(bwe.details)
    print(str(type(bulk_test_coll.count())))


def kafka_producer_test():
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.producer_test(KAFKA)


def kafka_consumer_test():
    # media_weixin_kafka_transform=MediaWeiXinKafkaTransform()
    # media_weixin_kafka_transform.consumer_test((BACKUP_CSV_PATH_BIG_DATA,"kafka_test.csv","ab"),KAFKA)
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_csv_consumer(
        WOM_DTS_OPS_MONGO_TUPLE, WOM_DEV_MYSQL_TUPLE, KAFKA)


def python_signal_test():
    signal.signal(signal.SIGTERM, handler)
    time.sleep(10)
    print('Program Ends.')

def handler(sig, frame):
    print('Got signal: ', sig)

def exception_test():
    try:
        try:

            print("hello exception")
            raise KafkaCrash
        except Exception as e:
            print("hello exception 1")
        finally:
            pass
    except Exception as e:
        print("hello exception 2")
    finally:
        pass
    pass

def media_weixin_data_transfer():
    
if __name__ == "__main__":
    # python_signal_test()
    # exception_test()
    # kafka_producer_test()
    # kafka_consumer_test()
    mongo_test()
    # test_bulk()
    # huajiao_crawler("http://webh.huajiao.com/User/getUserFeeds?uid=33283658")
    # test_system()
    # send_mail()
    # mysql_test()
    # update_price_date()

    '''
    p1 = multiprocessing.Process(target=worker_1, args=(2,))
    p2 = multiprocessing.Process(target=worker_1, args=(3,))
    p3 = multiprocessing.Process(target=worker_1, args=(4,))
    p1.start()
    p2.start()
    p3.start()

    print("The number of CPU is:" + str(multiprocessing.cpu_count()))
    for p in multiprocessing.active_children():
        print("child   p.name:" + p.name + "\tp.id" + str(p.pid))
    print("END!!!!!!!!!!!!!!!!!")
    
    # media_weixin_mongo_collected((CHECK_CSV_PATH, 'shouye_weixin_id_list.csv', 'rb'), (WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO[
    #                             'user'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['password']), ('media_weixin_article', {}, ["weixin_id", ]), ([1, CHECK_CSV_PATH, 'shouye_weixin_id_has_article_list.csv', 'ab'], [0, CHECK_CSV_PATH, 'media_weixin_id_not_collected_test.csv', 'ab'], [0, ]))
    # media_weixin_mysql_exists((CHECK_CSV_PATH, 'media_weixin_id_not_collected_test.csv', 'rb'),(DTS_OPT_MYSQL['host'],DTS_OPT_MYSQL['db'],DTS_OPT_MYSQL['user'],DTS_OPT_MYSQL['password']),('sogou_weixin_account_init_task', 'account_id', [['account_status', '=', 0]]),([1, CHECK_CSV_PATH, 'media_weixin_id_invalid_test.csv', 'ab'], [0, CHECK_CSV_PATH, 'media_weixin_id_not_collected_test.csv', 'ab'], [0, ]))
    # media_weixin_id_list((WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['user'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['password']),('media_weixin', {"data_analysis": {"$exists": True}}, ["public_id", ]),([1, CHECK_CSV_PATH, 'media_weixin_id_has_article.csv', 'ab'], [0, CHECK_CSV_PATH, 'media_weixin_id_not_collected_test.csv', 'ab'], [0, ]))
    # media_weixin_article_count((WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['user'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO[
    #                           'password']), (CHECK_CSV_PATH, 'shouye_weixin_id_list.csv', 'rb'), ([1, CHECK_CSV_PATH, 'media_weixin_id_article_count.csv', 'ab'], [0, CHECK_CSV_PATH, 'media_weixin_id_not_collected_test.csv', 'ab'], [0, ]))
    # media_weixin_backups('/home/admin/backups/json', WOM_DTS_DATAWAREHOSE_70_MONGO['host'],
    #                     27017, 70, 'media_weixin_article', (CHECK_CSV_PATH, 'media_weixin_id_has_article.csv', 'rb'))
    # media_weixin_recovery(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['user'],WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['password'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], 'media_weixin_article_temp',('/home/admin/backups/json','backup_files.txt', 'rb'))
    # media_weixin_mongo_storage()
    # test_str_to_time_stamp()
    #test_hanyu()
    '''
