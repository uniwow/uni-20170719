from peewee import *

database = MySQLDatabase('wom_dev', **{'user': 'wom', 'password': 'Qmg2016mw#semys$', 'port': 3306, 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class MediaVendor(BaseModel):
    account_uuid = CharField(index=True)
    active_end_time = IntegerField(null=True)
    balance = DecimalField()
    bank_account = CharField(null=True)
    bank_account_1 = CharField(null=True)
    bank_account_2 = CharField(null=True)
    bank_name = CharField(null=True)
    comment = TextField(null=True)
    comp_address = CharField(null=True)
    comp_industry = CharField(null=True)
    comp_website = CharField(null=True)
    contact1 = CharField()
    contact2 = CharField(null=True)
    contact_info = TextField(null=True)
    contact_person = CharField(null=True)
    default_vendor = IntegerField(null=True)
    etl_status = IntegerField(null=True)
    is_assigned = IntegerField()
    last_update_time = IntegerField()
    location = CharField(null=True)
    name = CharField(null=True)
    nickname = CharField(null=True)
    pay_type = IntegerField(null=True)
    pay_user = CharField(null=True)
    qq = CharField(null=True)
    register_type = IntegerField()
    type = IntegerField(null=True)
    uuid = CharField(unique=True)
    video_media_cnt = IntegerField()
    weibo_media_cnt = IntegerField()
    weixin = CharField(null=True)
    weixin_media_cnt = IntegerField()
    withdraw_amount = DecimalField(null=True)

    class Meta:
        db_table = 'media_vendor'

