from peewee import *

database = MySQLDatabase('wom_prod', **{'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'password': 'Qmg2016mw#semys$', 'port': 3306, 'user': 'wom'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class DtsMediaGroup(BaseModel):
    create_time = IntegerField()
    create_uuid = CharField()
    name = CharField()
    uuid = CharField(unique=True)

    class Meta:
        db_table = 'dts_media_group'

class DtsMediaGroupMap(BaseModel):
    group_uuid = ForeignKeyField(db_column='group_uuid', rel_model=DtsMediaGroup, to_field='uuid')
    rank = IntegerField(db_column='rank_id')
    weixin = CharField(db_column='weixin_id')
    weixin_name = CharField()

    class Meta:
        db_table = 'dts_media_group_map'

