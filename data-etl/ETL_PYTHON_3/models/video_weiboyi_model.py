from peewee import *

database = MySQLDatabase('wom_prod', **{'user': 'wom', 'password': 'Qmg2016mw#semys$', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'port': 3306})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class MediaVideo(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    address = CharField(null=True)
    coop_remark = TextField(null=True)
    create_time = IntegerField()
    main_platform = IntegerField()
    media_cate = TextField(null=True)
    name = CharField()
    nickname = CharField()
    sex = IntegerField()
    update_time = IntegerField()
    uuid = CharField()

    class Meta:
        db_table = 'media_video'

class VendorVideoBind(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    account_period = IntegerField()
    active_end_time = IntegerField()
    belong_type = IntegerField()
    cooperate_level = IntegerField()
    create_time = IntegerField()
    is_pref_vendor = IntegerField()
    status = IntegerField()
    update_time = IntegerField()
    uuid = CharField()
    vendor_uuid = CharField()
    video_uuid = CharField()

    class Meta:
        db_table = 'vendor_video_bind'


class VideoPlatformCommonInfo(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    account = CharField(db_column='account_id')
    account_name = CharField()
    audit_time = IntegerField()
    auth_type = IntegerField()
    avatar = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    fail_reason = TextField(null=True)
    follower_num = IntegerField()
    is_push = IntegerField()
    is_put = IntegerField()
    is_top = IntegerField()
    person_sign = CharField(null=True)
    platform_type = IntegerField()
    remark = TextField(null=True)
    status = IntegerField()
    update_time = IntegerField()
    url = CharField(null=True)
    uuid = CharField()
    video_uuid = CharField()

    class Meta:
        db_table = 'video_platform_common_info'

class VideoVendorPrice(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    create_time = IntegerField()
    is_main_platform = IntegerField()
    platform_type = IntegerField()
    platform_uuid = CharField()
    price_config = TextField(null=True)
    price_execute_one = DecimalField()
    price_execute_two = DecimalField()
    price_orig_one = DecimalField()
    price_orig_two = DecimalField()
    price_retail_one = DecimalField()
    price_retail_two = DecimalField()
    update_time = IntegerField()
    uuid = CharField()
    vendor_bind_uuid = CharField()

    class Meta:
        db_table = 'video_vendor_price'

class WeiboyiLiveTalentData(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    advert_price = DecimalField(null=True)
    area = CharField(null=True)
    avatar = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    follower_num = IntegerField(null=True)
    is_auth = IntegerField()
    is_original = IntegerField()
    live_price = DecimalField(null=True)
    name = CharField(null=True)
    person_intro = CharField(null=True)
    platform = CharField(null=True)
    platform_uuid = CharField(null=True)
    sex = IntegerField()
    url = CharField(null=True)

    class Meta:
        db_table = 'weiboyi_live_talent_data'

