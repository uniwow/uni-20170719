from peewee import *
from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import RetryOperationalError

class AutoConnectPooledMySQLDatabase(RetryOperationalError, PooledMySQLDatabase):
    pass
dev_database = AutoConnectPooledMySQLDatabase('wom_dev', max_connections=10,**{'port': 3306, 'user': 'wom', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'password': 'Qmg2016mw#semys$'})
   
prod_database = AutoConnectPooledMySQLDatabase('wom_prod', max_connections=10,**{'port': 3306, 'password': 'Qmg2016mw#semys$', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'user': 'wom'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = prod_database

class MediaVendorBind(BaseModel):
    comment = TextField(null=True)
    coop_level = IntegerField()
    create_time = IntegerField()
    is_pref_vendor = IntegerField()
    media_ownership = IntegerField()
    media_uuid = CharField(index=True)
    pay_period = IntegerField(null=True)
    status = IntegerField()
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)
    has_direct_pub = IntegerField()
    has_origin_pub = IntegerField()

    class Meta:
        db_table = 'media_vendor_bind'

class MediaVendorWeixinPriceList(BaseModel):
    active_end_time = IntegerField(null=True)
    bind_uuid = CharField()
    execute_price_m_1 = DecimalField()
    execute_price_m_2 = DecimalField()
    execute_price_m_3 = DecimalField()
    execute_price_s = DecimalField()
    m_1_pub_type = IntegerField()
    m_2_pub_type = IntegerField()
    m_3_pub_type = IntegerField()
    orig_price_m_1_max = DecimalField()
    orig_price_m_1_min = DecimalField()
    orig_price_m_2_max = DecimalField()
    orig_price_m_2_min = DecimalField()
    orig_price_m_3_max = DecimalField()
    orig_price_m_3_min = DecimalField()
    orig_price_s_max = DecimalField()
    orig_price_s_min = DecimalField()
    prmt_execute_price_m_1 = DecimalField()
    prmt_execute_price_m_2 = DecimalField()
    prmt_execute_price_m_3 = DecimalField()
    prmt_execute_price_s = DecimalField()
    prmt_orig_price_m_1 = DecimalField()
    prmt_orig_price_m_2 = DecimalField()
    prmt_orig_price_m_3 = DecimalField()
    prmt_orig_price_s = DecimalField()
    prmt_price_m_1 = DecimalField()
    prmt_price_m_2 = DecimalField()
    prmt_price_m_3 = DecimalField()
    prmt_price_s = DecimalField()
    pub_config = TextField(null=True)
    retail_price_m_1_max = DecimalField()
    retail_price_m_1_min = DecimalField()
    retail_price_m_2_max = DecimalField()
    retail_price_m_2_min = DecimalField()
    retail_price_m_3_max = DecimalField()
    retail_price_m_3_min = DecimalField()
    retail_price_s_max = DecimalField()
    retail_price_s_min = DecimalField()
    s_pub_type = IntegerField()
    uuid = CharField()

    class Meta:
        db_table = 'media_vendor_weixin_price_list'

class WeixinVendorBind(BaseModel):
    account_period = IntegerField(null=True)
    active_end_time = IntegerField(null=True)
    belong_type = IntegerField(null=True)
    cooperate_level = IntegerField()
    create_time = IntegerField(null=True)
    has_direct_pub = IntegerField()
    has_origin_pub = IntegerField()
    is_pref_vendor = IntegerField()
    m_1_execute_price = DecimalField(null=True)
    m_1_orig_price = DecimalField(null=True)
    m_1_pub_type = IntegerField(null=True)
    m_1_retail_price = DecimalField(null=True)
    m_2_execute_price = DecimalField(null=True)
    m_2_orig_price = DecimalField(null=True)
    m_2_pub_type = IntegerField(null=True)
    m_2_retail_price = DecimalField(null=True)
    m_3_execute_price = DecimalField(null=True)
    m_3_orig_price = DecimalField(null=True)
    m_3_pub_type = IntegerField(null=True)
    m_3_retail_price = DecimalField(null=True)
    media_uuid = CharField(index=True)
    s_execute_price = DecimalField(null=True)
    s_orig_price = DecimalField(null=True)
    s_pub_type = IntegerField(null=True)
    s_retail_price = DecimalField(null=True)
    status = IntegerField()
    update_time = IntegerField(null=True)
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)

    class Meta:
        db_table = 'weixin_vendor_bind'


class DtsMediaSearch(BaseModel):
    weixin = CharField(db_column='weixin_id', primary_key=True)
    weixin_name = CharField()

    class Meta:
        db_table = 'dts_media_search'

class DtsMediaGroup(BaseModel):
    create_time = IntegerField()
    create_uuid = CharField()
    name = CharField()
    uuid = CharField(unique=True)

    class Meta:
        db_table = 'dts_media_group'

class DtsMediaGroupMap(BaseModel):
    group_uuid = ForeignKeyField(db_column='group_uuid', rel_model=DtsMediaGroup, to_field='uuid')
    rank = IntegerField(db_column='rank_id')
    weixin = CharField(db_column='weixin_id')
    weixin_name = CharField()

    class Meta:
        db_table = 'dts_media_group_map'

class WomAdminBaseConf(BaseModel):
    conf_json = TextField(null=True)
    conf_name = CharField()
    conf_type = IntegerField()
    create_time = IntegerField()
    is_update = IntegerField()
    update_time = IntegerField()
    uuid = CharField()

    class Meta:
        db_table = 'wom_admin_base_conf'

class BindVideoVendorPrice(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    create_time = IntegerField()
    is_main_platform = IntegerField()
    platform_type = IntegerField()
    platform_uuid = CharField()
    price_config = TextField(null=True)
    price_execute_one = DecimalField()
    price_execute_two = DecimalField()
    price_orig_one = DecimalField()
    price_orig_two = DecimalField()
    price_retail_one = DecimalField()
    price_retail_two = DecimalField()
    update_time = IntegerField()
    uuid = CharField(unique=True)
    vendor_bind_uuid = CharField(index=True)

    class Meta:
        db_table = 'video_vendor_price'

class WeiboVendorBind(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    account_period = IntegerField()
    active_end_time = IntegerField()
    belong_type = IntegerField()
    cooperate_level = IntegerField()
    create_time = IntegerField()
    is_pref_vendor = IntegerField()
    micro_direct_price_execute = DecimalField()
    micro_direct_price_orig = DecimalField()
    micro_direct_price_retail = DecimalField()
    micro_transfer_price_execute = DecimalField()
    micro_transfer_price_orig = DecimalField()
    micro_transfer_price_retail = DecimalField()
    soft_direct_price_execute = DecimalField()
    soft_direct_price_orig = DecimalField()
    soft_direct_price_retail = DecimalField()
    soft_transfer_price_execute = DecimalField()
    soft_transfer_price_orig = DecimalField()
    soft_transfer_price_retail = DecimalField()
    status = IntegerField()
    update_time = IntegerField()
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)
    weibo_uuid = CharField(index=True)

    class Meta:
        db_table = 'weibo_vendor_bind'

