from peewee import *
from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import RetryOperationalError

class AutoConnectPooledMySQLDatabase(RetryOperationalError, PooledMySQLDatabase):
    pass
    
database_kaikai = AutoConnectPooledMySQLDatabase('wom_dev', max_connections=10,**{'user': 'wom', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'port': 3306, 'password': 'Qmg2016mw#semys$'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database_kaikai

class MongoMediaWeiXin(BaseModel):
    account_short_desc = CharField(null=True)
    media_cate = CharField(null=True)
    wom_media_cate = CharField()
    public = CharField(db_column='public_id', unique=True)
    public_name = CharField()
    media_area = TextField(null=False)
    media_tags = TextField(null=False)

    class Meta:
        db_table = 'weixin_cate'

