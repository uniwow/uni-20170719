from peewee import *

database = MySQLDatabase('wom_prod', **{'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'password': 'Qmg2016mw#semys$', 'port': 3306, 'user': 'wom'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class WeixinVendorBind(BaseModel):
    account_period = IntegerField(null=True)
    active_end_time = IntegerField(null=True)
    belong_type = IntegerField(null=True)
    cooperate_level = IntegerField()
    create_time = IntegerField(null=True)
    has_direct_pub = IntegerField()
    has_origin_pub = IntegerField()
    is_pref_vendor = IntegerField()
    m_1_execute_price = DecimalField(null=True)
    m_1_orig_price = DecimalField(null=True)
    m_1_pub_type = IntegerField(null=True)
    m_1_retail_price = DecimalField(null=True)
    m_2_execute_price = DecimalField(null=True)
    m_2_orig_price = DecimalField(null=True)
    m_2_pub_type = IntegerField(null=True)
    m_2_retail_price = DecimalField(null=True)
    m_3_execute_price = DecimalField(null=True)
    m_3_orig_price = DecimalField(null=True)
    m_3_pub_type = IntegerField(null=True)
    m_3_retail_price = DecimalField(null=True)
    media_uuid = CharField(index=True)
    s_execute_price = DecimalField(null=True)
    s_orig_price = DecimalField(null=True)
    s_pub_type = IntegerField(null=True)
    s_retail_price = DecimalField(null=True)
    status = IntegerField()
    update_time = IntegerField(null=True)
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)

    class Meta:
        db_table = 'weixin_vendor_bind'

