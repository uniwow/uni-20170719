from peewee import *

database = MySQLDatabase('wom_prod', **{'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'password': 'Qmg2016mw#semys$', 'user': 'wom', 'port': 3306})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class VideoVendorPrice(BaseModel):
    id = PrimaryKeyField(db_column='Id')
    create_time = IntegerField()
    is_main_platform = IntegerField()
    platform_type = IntegerField()
    platform_uuid = CharField()
    price_config = TextField(null=True)
    price_execute_one = DecimalField()
    price_execute_two = DecimalField()
    price_orig_one = DecimalField()
    price_orig_two = DecimalField()
    price_retail_one = DecimalField()
    price_retail_two = DecimalField()
    update_time = IntegerField()
    uuid = CharField(unique=True)
    vendor_bind_uuid = CharField(index=True)

    class Meta:
        db_table = 'video_vendor_price'

class WeiboVendorBind(BaseModel):
    id = PrimaryKeyField(db_column='Id')
    account_period = IntegerField()
    active_end_time = IntegerField()
    belong_type = IntegerField()
    cooperate_level = IntegerField()
    create_time = IntegerField()
    is_pref_vendor = IntegerField()
    micro_direct_price_execute = DecimalField()
    micro_direct_price_orig = DecimalField()
    micro_direct_price_retail = DecimalField()
    micro_transfer_price_execute = DecimalField()
    micro_transfer_price_orig = DecimalField()
    micro_transfer_price_retail = DecimalField()
    soft_direct_price_execute = DecimalField()
    soft_direct_price_orig = DecimalField()
    soft_direct_price_retail = DecimalField()
    soft_transfer_price_execute = DecimalField()
    soft_transfer_price_orig = DecimalField()
    soft_transfer_price_retail = DecimalField()
    status = IntegerField()
    update_time = IntegerField()
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)
    weibo_uuid = CharField(index=True)

    class Meta:
        db_table = 'weibo_vendor_bind'

class WeixinVendorBind(BaseModel):
    account_period = IntegerField(null=True)
    active_end_time = IntegerField(null=True)
    belong_type = IntegerField(null=True)
    cooperate_level = IntegerField()
    create_time = IntegerField(null=True)
    has_direct_pub = IntegerField()
    has_origin_pub = IntegerField()
    is_pref_vendor = IntegerField()
    m_1_execute_price = DecimalField(null=True)
    m_1_orig_price = DecimalField(null=True)
    m_1_pub_type = IntegerField(null=True)
    m_1_retail_price = DecimalField(null=True)
    m_2_execute_price = DecimalField(null=True)
    m_2_orig_price = DecimalField(null=True)
    m_2_pub_type = IntegerField(null=True)
    m_2_retail_price = DecimalField(null=True)
    m_3_execute_price = DecimalField(null=True)
    m_3_orig_price = DecimalField(null=True)
    m_3_pub_type = IntegerField(null=True)
    m_3_retail_price = DecimalField(null=True)
    media_uuid = CharField(index=True)
    s_execute_price = DecimalField(null=True)
    s_orig_price = DecimalField(null=True)
    s_pub_type = IntegerField(null=True)
    s_retail_price = DecimalField(null=True)
    status = IntegerField()
    update_time = IntegerField(null=True)
    uuid = CharField(unique=True)
    vendor_uuid = CharField(index=True)

    class Meta:
        db_table = 'weixin_vendor_bind'

class WomAdminBaseConf(BaseModel):
    conf_json = TextField(null=True)
    conf_name = CharField()
    conf_type = IntegerField()
    create_time = IntegerField()
    is_update = IntegerField()
    update_time = IntegerField()
    uuid = CharField()

    class Meta:
        db_table = 'wom_admin_base_conf'

