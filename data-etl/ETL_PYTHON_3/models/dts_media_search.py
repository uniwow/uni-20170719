from peewee import *

database = MySQLDatabase('wom_prod', **{'port': 3306, 'password': 'Qmg2016mw#semys$', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'user': 'wom'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class DtsMediaSearch(BaseModel):
    weixin = CharField(db_column='weixin_id', primary_key=True)
    weixin_name = CharField()

    class Meta:
        db_table = 'dts_media_search'
