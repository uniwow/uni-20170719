from peewee import *

database = MySQLDatabase('wom_prod', **{'password': 'Qmg2016mw#semys$', 'user': 'wom', 'port': 3306, 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class MediaWeixin(BaseModel):
    account_cert = IntegerField()
    account_cert_info = TextField(null=True)
    account_short_desc = TextField(null=True)
    account_type = IntegerField()
    advantage_account = IntegerField()
    avatar_big_img = CharField(null=True)
    avatar_small_img = CharField(null=True)
    come_from = IntegerField()
    create_time = IntegerField()
    follower_area = TextField(null=True)
    follower_area_machine = TextField(null=True)
    follower_num = IntegerField()
    follower_num_machine = IntegerField(null=True)
    follower_screenshot = TextField(null=True)
    head_like_median = IntegerField(null=True)
    head_read_median = IntegerField()
    is_dts_rank = IntegerField()
    is_need_bd = IntegerField()
    is_push = IntegerField()
    is_put = IntegerField()
    is_top = IntegerField()
    latest_article_post_date = IntegerField()
    m_10w_article_total_cnt = IntegerField()
    m_avg_like_cnt = IntegerField()
    m_avg_price_pv = DecimalField()
    m_avg_view_cnt = IntegerField()
    m_head_avg_like_cnt = IntegerField()
    m_head_avg_view_cnt = IntegerField()
    m_release_cnt = IntegerField(null=True)
    m_wmi = FloatField()
    media_cate = TextField(null=True)
    media_cate_machine = TextField(null=True)
    media_level = IntegerField()
    oldest_article_post_date = IntegerField()
    promote_index = FloatField()
    public = CharField(db_column='public_id', unique=True)
    public_name = CharField(index=True, null=True)
    qrcode_img = CharField(null=True)
    rank_in_dts = IntegerField(null=True)
    real_public = CharField(db_column='real_public_id')
    resource_comment = TextField(null=True)
    sales_index = FloatField()
    show_in_51wom = IntegerField()
    show_in_dts_rank = IntegerField()
    status = IntegerField()
    total_article_cnt = IntegerField()
    update_time = IntegerField()
    uuid = CharField(unique=True)
    weixin_media_type = IntegerField(null=True)

    class Meta:
        db_table = 'media_weixin'

