from peewee import *

from playhouse.pool import PooledMySQLDatabase
from playhouse.shortcuts import RetryOperationalError

class AutoConnectPooledMySQLDatabase(RetryOperationalError, PooledMySQLDatabase):
    pass

dev_database = AutoConnectPooledMySQLDatabase('wom_dev', max_connections=10,**{'port': 3306, 'user': 'wom', 'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'password': 'Qmg2016mw#semys$'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = dev_database

class MediaVideo(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    address = CharField(null=True)
    coop_remark = TextField(null=True)
    create_time = IntegerField()
    main_platform = IntegerField()
    media_cate = TextField(null=True)
    name = CharField()
    nickname = CharField()
    sex = IntegerField()
    update_time = IntegerField()
    uuid = CharField()

    class Meta:
        db_table = 'media_video'

class VendorVideoBind(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    account_period = IntegerField()
    active_end_time = IntegerField()
    belong_type = IntegerField()
    cooperate_level = IntegerField()
    create_time = IntegerField()
    is_pref_vendor = IntegerField()
    status = IntegerField()
    update_time = IntegerField()
    uuid = CharField()
    vendor_uuid = CharField()
    video_uuid = CharField()

    class Meta:
        db_table = 'vendor_video_bind'

class VideoMediaBaseInfo(BaseModel):
    advantage = TextField(null=True)
    area = CharField()
    avatar_img = CharField(null=True)
    avg_watch_num = IntegerField()
    birth_date = IntegerField(null=True)
    case_desc = TextField(null=True)
    create_time = IntegerField()
    current_address = CharField(null=True)
    cust_sort = IntegerField()
    graduate_school = CharField(null=True)
    height = IntegerField(null=True)
    is_video_platform = IntegerField()
    last_update_time = IntegerField()
    main_platform = IntegerField(null=True)
    main_platform_active_price = DecimalField(null=True)
    main_platform_follower_num = IntegerField()
    main_platform_online_price = DecimalField(null=True)
    main_platform_video_original_price = DecimalField(null=True)
    main_platform_video_straight_price = DecimalField(null=True)
    main_platform_video_transpond_price = DecimalField(null=True)
    measurements = CharField(null=True)
    mobile_phone = BigIntegerField(null=True)
    name_cn = CharField(null=True)
    name_en = CharField(null=True)
    platform_conf = TextField(null=True)
    pref_bind_uuid = CharField(null=True)
    pref_vendor_uuid = CharField(null=True)
    put = IntegerField()
    qq = IntegerField(null=True)
    remark = TextField(null=True)
    retail_price_max = DecimalField(null=True)
    retail_price_min = DecimalField(null=True)
    sex = IntegerField(null=True)
    stage_name = CharField()
    status = IntegerField()
    style = CharField(null=True)
    tag = CharField()
    top = IntegerField()
    _type = IntegerField(null=True,db_column='type')
    uuid = CharField(unique=True)
    weight = IntegerField(null=True)
    weixin = CharField(null=True)

    class Meta:
        db_table = 'video_media_base_info'

class VideoMediaDouyu(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = IntegerField(null=True)
    platform_money2 = IntegerField(null=True)
    put_up = IntegerField()
    remark = CharField(null=True)
    room_num = CharField()
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    video_media_douyucol = CharField(null=True)

    class Meta:
        db_table = 'video_media_douyu'

class VideoMediaHani(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField(null=True)
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = CharField(null=True)
    put_up = IntegerField()
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    verify_status = IntegerField()
    xp_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_hani'

class VideoMediaHuajiao(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    like_num = IntegerField(null=True)
    live_time = IntegerField(null=True)
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = CharField()
    put_up = IntegerField()
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    verify_status = IntegerField(null=True)
    xp_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_huajiao'

class VideoMediaMeipai(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    concern_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    like_num = IntegerField(null=True)
    media = CharField(db_column='media_id')
    media_name = CharField()
    put_up = IntegerField()
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    verify_status = IntegerField()
    xp_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_meipai'

class VideoMediaMiaopai(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    last_update_time = IntegerField()
    like_num = IntegerField(null=True)
    media = CharField(db_column='media_id', null=True)
    media_name = CharField()
    platform_money1 = IntegerField(null=True)
    put_up = IntegerField()
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    verify_status = IntegerField()
    video_media_meipaicol = CharField(null=True)
    video_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_miaopai'

class VideoMediaPanda(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    live_type = TextField(null=True)
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = IntegerField(null=True)
    platform_money2 = IntegerField(null=True)
    put_up = IntegerField()
    remark = CharField(null=True)
    room_num = CharField()
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    xp_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_panda'

class VideoMediaTaobao(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    image_text_num = IntegerField(null=True)
    last_update_time = IntegerField()
    media_name = CharField(unique=True)
    put_up = IntegerField(null=True)
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField()
    uuid = CharField(unique=True)
    verify_status = IntegerField(null=True)
    video_num = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_taobao'

class VideoMediaYingke(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    live_num = CharField(null=True)
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = IntegerField(null=True)
    put_up = IntegerField()
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField()
    total_vendor_uuid = TextField(null=True)
    url = TextField(null=True)
    uuid = CharField(unique=True)
    verify_status = IntegerField()

    class Meta:
        db_table = 'video_media_yingke'

class VideoMediaYizhibo(BaseModel):
    avatar_img = TextField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    follower_num = IntegerField()
    grade = IntegerField(null=True)
    last_update_time = IntegerField()
    media = CharField(db_column='media_id')
    media_name = CharField()
    platform_money1 = CharField(null=True)
    put_up = IntegerField(null=True)
    remark = CharField(null=True)
    signature = CharField(null=True)
    status = IntegerField(null=True)
    total_vendor_uuid = TextField(null=True)
    url = TextField(null=True)
    uuid = CharField()
    verify_status = IntegerField(null=True)

    class Meta:
        db_table = 'video_media_yizhibo'

class VideoPlatformCommonInfo(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    account = CharField(db_column='account_id')
    account_name = CharField()
    audit_time = IntegerField()
    auth_type = IntegerField()
    avatar = CharField(null=True)
    avg_watch_num = IntegerField(null=True)
    create_time = IntegerField()
    fail_reason = TextField(null=True)
    follower_num = IntegerField()
    is_push = IntegerField()
    is_put = IntegerField()
    is_top = IntegerField()
    person_sign = CharField(null=True)
    platform_type = IntegerField()
    remark = TextField(null=True)
    status = IntegerField()
    update_time = IntegerField()
    url = CharField(null=True)
    uuid = CharField()
    video_uuid = CharField()

    class Meta:
        db_table = 'video_platform_common_info'

class VideoVendorBind(BaseModel):
    advantage = TextField(null=True)
    area = CharField()
    avatar_img = CharField(null=True)
    avg_watch_num = IntegerField()
    base_info_uuid = CharField(null=True)
    birth_date = IntegerField(null=True)
    case_desc = TextField(null=True)
    create_time = IntegerField()
    current_address = CharField(null=True)
    graduate_school = CharField(null=True)
    height = IntegerField(null=True)
    last_update_time = IntegerField()
    main_platform = IntegerField(null=True)
    main_platform_follower_num = IntegerField()
    measurements = CharField(null=True)
    mobile_phone = BigIntegerField(null=True)
    name_cn = CharField(null=True)
    name_en = CharField(null=True)
    platform_conf = TextField()
    put = IntegerField(null=True)
    qq = IntegerField(null=True)
    remark = TextField(null=True)
    sex = IntegerField(null=True)
    stage_name = CharField()
    status = IntegerField(null=True)
    style = CharField(null=True)
    tag = CharField()
    top = IntegerField()
    _type = IntegerField(null=True,db_column='type')
    urls = TextField(null=True)
    uuid = CharField(unique=True)
    vendor_uuid = CharField()
    weight = IntegerField(null=True)
    weixin = CharField(null=True)

    class Meta:
        db_table = 'video_vendor_bind'

class VideoVendorPrice(BaseModel):
    _id = PrimaryKeyField(db_column='Id')
    create_time = IntegerField()
    is_main_platform = IntegerField()
    platform_type = IntegerField()
    platform_uuid = CharField()
    price_config = TextField(null=True)
    price_execute_one = DecimalField()
    price_execute_two = DecimalField()
    price_orig_one = DecimalField()
    price_orig_two = DecimalField()
    price_retail_one = DecimalField()
    price_retail_two = DecimalField()
    update_time = IntegerField()
    uuid = CharField()
    vendor_bind_uuid = CharField()

    class Meta:
        db_table = 'video_vendor_price'
class MediaVendor(BaseModel):
    account_uuid = CharField(index=True)
    active_end_time = IntegerField(null=True)
    balance = DecimalField()
    bank_account = CharField(null=True)
    bank_account_1 = CharField(null=True)
    bank_account_2 = CharField(null=True)
    bank_name = CharField(null=True)
    comment = TextField(null=True)
    comp_address = CharField(null=True)
    comp_industry = CharField(null=True)
    comp_website = CharField(null=True)
    contact1 = CharField()
    contact2 = CharField(null=True)
    contact_info = TextField(null=True)
    contact_person = CharField(null=True)
    default_vendor = IntegerField(null=True)
    etl_status = IntegerField(null=True)
    is_assigned = IntegerField()
    last_update_time = IntegerField()
    location = CharField(null=True)
    name = CharField(null=True)
    nickname = CharField(null=True)
    pay_type = IntegerField(null=True)
    pay_user = CharField(null=True)
    qq = CharField(null=True)
    register_type = IntegerField()
    _type = IntegerField(null=True,db_column='type')
    uuid = CharField(unique=True)
    video_media_cnt = IntegerField()
    weibo_media_cnt = IntegerField()
    weixin = CharField(null=True)
    weixin_media_cnt = IntegerField()
    withdraw_amount = DecimalField(null=True)
    class Meta:
        db_table = 'media_vendor'

