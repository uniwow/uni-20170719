from tools.tool_mongo import MongoTool
from tools.tool_mysql import MysqlTool
from tools.tool_file import FileTool
from tools.settings import ERP_PROD_TUPLE, WOM_DTS_OPS_MONGO_TUPLE, ZOOKEEPER, KAFKA, BACKUP_INDEX_COMPUTE_PATH, BACKUP_MACHINE_LEARNING_PATH, WOM_DEV_MYSQL_TUPLE, TARGET_COLUMN_TUPLE, TARGET_COLUMN_LIST_ONE, BACKUP_CSV_PATH_BIG_DATA, WOM_PROD_1_TUPLE, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO, BACKUP_JSON_PATH, CHECK_CSV_PATH, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_225_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_32_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_249_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_70_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_102_MONGO_TUPLE, CHECK_SQL_PATH, UNI_MONGO_TUPLE, ERROR_FILE_ARGS, BACKUP_CSV_PATH, MONGO_SHARD_IP_LIST, MONGO_SHARD_IP_SUFFIX_LIST, MONGO_COLLECTIONS_LIST, BACKUP_JSON_PATH
import multiprocessing
import time
from libs.media_weixin_check import MediaWeiXinCheck
from libs.media_weixin_storage import MediaWeiXinStorage
from libs.media_weixin_backups import MediaWeixinBackups
from libs.media_weixin_kafka_transform import MediaWeiXinKafkaTransform
from libs.media_weixin_storage_orm import MediaWeiXinStorageORM
from tools.tasks import dts_media_weixin_influence_rank
from libs.video_weiboyi_storage import video_weiboyi_transfer
import os
import sys
import getopt
from tools.tool_time import current_time_to_stamp, str_to_time_stamp, current_stamp_to_time
import datetime


def usage():
    print('help me............')


def get_mysql_tool(host, db, user, pwd):
    mysql_tool = MysqlTool(host, db, user, pwd)
    return mysql_tool


def get_file_tool(file_path, file_name, open_status):
    file_tool = FileTool(file_path, file_name, open_status)
    return file_tool


def media_weixin_id_list_mongo(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, source_collection_args, target_file_args_list):
    '''指定条件的微信账号列表
                func_1
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.mongo_duplicate(
        source_mongo_args, source_collection_args, target_file_args_list)


def media_weixin_id_storage(source_file_args, target_mysql_args, target_file_args):
    '''
    修改账号id
    func_2
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_id_update(
        source_file_args, target_mysql_args, target_file_args)


def media_weixin_status_storage(source_mysql_args, target_mysql_args, source_table_args, target_file_args):
    '''修改账号status
                func_3
    '''

    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_status_update(
        source_mysql_args, target_mysql_args, source_table_args, target_file_args)


def media_weixin_storage():
    '''
        账号入库
        func_4
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_storage_mongo()
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'mongo_to_mysql_media_weixin'}, error_file_tool)


def media_weixin_backups(backup_path, ip, port, suffix, collection_name, source_file_args, query_list):
    '''根据账号备份程序
        func_5
    '''
    media_weixin_backups_obj = MediaWeixinBackups()
    media_weixin_backups_obj.media_weixin_backups(
        backup_path, ip, port, suffix, collection_name, source_file_args, query_list)


def origin_average_price(source_mysql_args, source_table_args, target_mysql_args, target_file_args):
    '''
    更新media_weixin表的avg_price_pv
    func_6
    '''
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_average_pv_update(
        source_mysql_args, source_table_args, target_mysql_args, target_file_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'orig_avg_price'}, (CHECK_SQL_PATH, 'error.sql', 'ab'))


def price_list_pub_config_storage(target_table_name):
    '''更新media_weixin pub_config信息
        func_7:media_weixin
        func_8:media_vendor_weixin_price_list
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.price_list_pub_type_update(target_table_name)


def get_media_weixin_id_list_mysql(source_mysql_args, source_table_args, target_file_args_list):
    '''获得满足条件的ID列表
        func_9:price_0


    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.mysql_duplicate(
        source_mysql_args, source_table_args, target_file_args_list)


def media_weixin_1_price_1_duplicate(source_mysql_args, target_file_args_list):
    '''有价格值的账号列表
        func_10
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_1_price_1_duplicate(
        source_mysql_args, target_file_args_list)


def media_weixin_1_price_1_storage(source_file_args, source_mysql_args, target_mysql_args, target_table_args):
    '''有price信息的数据更新到media_weixin表
        func_11
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_1_price_1_update(
        source_file_args, source_mysql_args, target_mysql_args, target_table_args)


def file_name_storage(target_file_args, file_path):
    '''路径导出到文件
        func_12
    '''
    target_file_tool = get_file_tool(*target_file_args)
    target_file_tool.walks(file_path)


def media_weixin_recovery(ip, port, user, pwd, db, collection_name, source_file_args):
    '''json文件入库程序
        func_13
    '''
    media_weixin_backups_obj = MediaWeixinBackups()
    media_weixin_backups_obj.media_weixin_recovery(
        ip, port, user, pwd, db, collection_name, source_file_args)


def unique_id_duplicate(source_mysql_args, source_table_args, target_file_args):
    '''查重方法
    func_14
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.unique_id_duplicate(
        source_mysql_args, source_table_args, target_file_args)


def unique_id_duplicate_further(source_mysql_args, source_file_args, target_file_args):
    '''进一步查重
        func_15
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.unique_id_duplicate_further(
        source_mysql_args, source_file_args, target_file_args)


def media_weixin_post_time_count(source_mongo_args, source_collection_args, target_file_args):
    '''
    微信文章发布时间区间统计
    func_16
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_post_time_range(
        source_mongo_args, source_collection_args, target_file_args)


def meida_weixin_post_time_count_from_id_file(source_mongo_args, source_file_args, target_file_args, target_mysql_args):
    '''
    微信文章发布时间区间统计
    func_17
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_post_time_range_from_id_file(
        source_mongo_args, source_file_args, target_file_args, target_mysql_args)


def media_weixin_task_check(source_mongo_args, source_collection_args, source_file_args, source_mysql_args, target_file_args):
    '''
    微信爬取任务检查
    func_18
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_task_check(
        source_mongo_args, source_collection_args, source_file_args, source_mysql_args, target_file_args)


def media_weixin_follower_area_update(source_mysql_args, target_mysql_args, error_file_args):
    '''
    粉丝覆盖地域更新操作
    func_19
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_follower_area_update(
        source_mysql_args, target_mysql_args, error_file_args)


def media_weixin_article_analysis_split(source_mongo_args, target_mongo_args):
    '''
    mapreduce计算结果添加进data_analysis列表
    func_20
    '''
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_article_analysis_split(
        source_mongo_args, target_mongo_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)

    task_mysql_tool.insert_one('data_task_time_axis', {
                               'start_time': start_time_str, 'end_time': end_time_str, 'cost_time': cost_time, 'task_name': 'split_into_mongo_media_weixin'}, error_file_tool)


def media_weixin_article_power_analysis(source_mongo_args, source_collection_args, target_mongo_args):
    '''
    微信文章影响力指数计算
    func_21
    '''
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_article_power_analysis(
        source_mongo_args, source_collection_args, target_mongo_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_weixin_article_power'}, error_file_tool)


def media_weixin_article_count(source_mongo_args, target_file_args):
    '''文章数目统计
        func_22
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_article_count(
        source_mongo_args, target_file_args)


def media_weixin_article_power_analysis_multi_process_control(source_mongo_args, source_collection_args, process_num, target_mongo_args, target_file_args):
    '''
    微信文章影响力指数计算 :multi process
    func_23
    '''
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    end_time = media_weixin_storage_obj.media_weixin_article_power_analysis_multi_process_control(
        source_mongo_args, source_collection_args, process_num, target_mongo_args, target_file_args)
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_weixin_article_power_multi_process'}, error_file_tool)


def media_vendor_bind_duplicate(WOM_DEV_MYSQL_TUPLE, source_mysql_args_2, target_mysql_args_1, target_mysql_args_2, error_file_args):
    '''media_vendor_bind 表冗余处理
        func_24
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_vendor_bind_duplicate(source_mysql_args_1, source_mysql_args_2,
                                                         target_mysql_args_1, target_mysql_args_2, error_file_args)


def media_vendor_bind_duplicate_again(source_mysql_args_1, source_mysql_args_2, target_mysql_args_1, target_mysql_args_2, error_file_args):
    '''
        media_vendor_bind 表冗余处理
        func_25
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_vendor_bind_duplicate_again(source_mysql_args_1, source_mysql_args_2,
                                                               target_mysql_args_1, target_mysql_args_2, error_file_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_weixin_article_power_multi_process'}, error_file_tool)


def media_weixin_contact_info_update(source_mysql_args, target_mysql_args, error_file_args):
    '''
        更新contact_info字段
        func_26
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_contact_info_update(
        source_mysql_args, target_mysql_args, error_file_args)


def delete_duplicate_date_from_transfer(source_mysql_args, target_mysql_args, error_file_args):
    '''清除迁移冗余数据
        func_27
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.delete_duplicate_date_from_transfer(
        source_mysql_args, target_mysql_args, error_file_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'delete_duplicate_date_from_transfer'}, error_file_tool)


def media_weixin_task_account_level_update(source_mysql_args, target_table_args, error_file_args):
    '''
    更改task表的account_level
    func_28
    '''

    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_task_account_level_update(
        source_mysql_args, target_table_args, error_file_args)


def media_weixin_data_backup(source_mysql_args, target_file_args, error_file_args):
    '''
    media_weixin表中数据导入到csv文件
    func_29
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_data_backup(
        source_mysql_args, target_file_args, error_file_args)


def media_weixin_pub_type_retail_min(source_mysql_args, target_file_args, error_file_args):
    '''
    线上数据库 media_weixin 里找出那些 pub_type = 0 (不接单)， 但是 相应位置 retail_price_xx_min ！= 0 的那些记录
    func_30
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_pub_type_retail_min(
        source_mysql_args, target_file_args, error_file_args)


def media_vendor_duplicate(source_mysql_args_1, source_mysql_args_2, target_mysql_args, error_file_args):
    '''
    media_vendor 表中的无效数据标记
    func_31
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_vendor_duplicate(
        source_mysql_args_1, source_mysql_args_2, target_mysql_args, error_file_args)


def media_weixn_media_vendor_bind(source_mysql_args_1, source_mysql_args_2):
    '''
    media_weixin and media_vendor 数据是否对应
    func_32
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixn_media_vendor_bind(
        source_mysql_args_1, source_mysql_args_2)


def t_media_duplicate(source_mysql_args, target_file_args_1, target_file_args_2):
    '''
    t_media 数据冗余查询
    func_33
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.t_media_duplicate(
        source_mysql_args, target_file_args_1, target_file_args_2)


def t_media_i_user_id_not_in_user_id(source_mysql_args_1, source_mysql_args_2, target_file_args):
    '''
    t_media_user_id 没有找到对应
    func_34
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.t_media_i_user_id_not_in_user_id(
        source_mysql_args_1, source_mysql_args_2, target_file_args)


def media_weixin_1_price_1_update_upgrade(source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args):
    '''
    1.0数据迁移
    只做insert操作
    func_35
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_1_price_1_update_upgrade(
        source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'data tranfer'}, error_file_tool)


def media_weixin_1_price_1_duplicate_data(source_file_args, source_mysql_args, target_file_args):
    '''
    1.0迁移重复数据
    func_36
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_1_price_1_duplicate_data(
        source_file_args, source_mysql_args, target_file_args)


def settled_yuyue_public_id(source_file_args_1, source_file_args_2, source_mysql_args, error_file_args):
    '''
    不重复的预约账号列表
    func_37
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.settled_yuyue_public_id(
        source_file_args_1, source_file_args_2, source_mysql_args, error_file_args)


def media_weixin_media_cate_update(source_mysql_args, target_mysql_args, error_file_args):
    '''
        media_cate 字段生成
        func_38
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_media_cate_update(
        source_mysql_args, target_mysql_args, error_file_args)


def media_weixin_public_id_update(source_mongo_args, source_collection_args, source_mysql_args, target_mongo_args, target_mysql_args, error_file_args):
    '''
    更新media_weixin real_public_id字段
    func_39
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_public_id_update(
        source_mongo_args, source_collection_args, source_mysql_args, target_mongo_args, target_mysql_args, error_file_args)


def media_weixin_price_errors(source_mysql_args, target_file_args):
    '''
    media_weixin中价格最小值大于价格最大值的情况
    func_40
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_price_errors(
        source_mysql_args, target_file_args)


def media_weixin_1_price_1_duplicate_update_upgrade(source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args, target_file_args_1, target_file_args_2, target_file_args_3):
    '''
    1.0中重复数据迁移
    func_41
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_1_price_1_duplicate_update_upgrade(
        source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args, target_file_args_1, target_file_args_2, target_file_args_3)


def ad_owner_transfer(source_mysql_args, target_mysql_args):
    '''
    广告主迁移程序
    func_42
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.ad_owner_transfer(
        source_mysql_args, target_mysql_args)


def media_vendor_transfer(source_mysql_args, target_mysql_args):
    '''
    媒体主迁移程序
    func_43
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_vendor_transfer(
        source_mysql_args, target_mysql_args)


def media_weixin_status_sync(source_mysql_args, target_mysql_args):
    '''
    media_weixin status 无效状态标记
    func_44
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_status_sync(
        source_mysql_args, target_mysql_args)


def media_weixin_pub_type_update(source_mysql_args, target_mysql_args):
    '''
    media_weixin price data update
    func_45
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_pub_type_update(
        source_mysql_args, target_mysql_args)


def media_weixin_article_post_time_law(source_mysql_args, source_mongo_args, target_mysql_args):
    '''
    微信公众号发布规律
    func_46
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_article_post_time_law(
        source_mysql_args, source_mongo_args, target_mysql_args)


def media_weixin_has_direct_or_origin(source_mysql_args, target_file_args):
    """
    has_***_pub字段异常
    func_47
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_has_direct_or_origin(
        source_mysql_args, target_file_args)


def media_weixin_article_csv_dat(backup_path, ip_list, port, suffix_list, collection_name):
    '''
    mapreduce计算程序所需csv数据导出
    func_48
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_backups_obj = MediaWeixinBackups()
    end_time = media_weixin_backups_obj.media_weixin_article_csv_multi_process_control(
        backup_path, ip_list, port, suffix_list, collection_name)
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'mapreduce csv export'}, error_file_tool)


def media_weixin_backups_multi_process_control(backup_path, ip_list, port, suffix_list, collection_name_list):
    '''
    mongo数据库数据备份程序
    func_49
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_backups_obj = MediaWeixinBackups()
    end_time = media_weixin_backups_obj.media_weixin_backups_multi_process_control(
        backup_path, ip_list, port, suffix_list, collection_name_list)
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'weixin related collection backups'}, error_file_tool)


def backups_csv(source_mongo_args, source_collection_args, target_collection_args, target_columns_and_files):
    '''
        csv文件拉取
        func_50
    '''
    media_weixin_backups_obj = MediaWeixinBackups()
    media_weixin_backups_obj.backups_csv(
        source_mongo_args, source_collection_args, target_collection_args, target_columns_and_files)


def backups_csv_multi_process_control(process_num, source_mongo_args, source_collection_args, target_collection_args, target_columns_and_files):
    '''
    csv文件拉取,多进程
        func_51
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_backups_obj = MediaWeixinBackups()
    end_time = media_weixin_backups_obj.backups_csv_multi_process_control(
        process_num, source_mongo_args, source_collection_args, target_collection_args, target_columns_and_files)
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'mapreduce csv export'}, error_file_tool)


def weibo_account_duplicate(source_mysql_args, target_file_args_1, target_file_args_2):
    '''
    微博重复数据检查
    func_52
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.weibo_account_duplicate(
        source_mysql_args, target_file_args_1, target_file_args_2)


def weibo_transfer(source_file_args, source_mysql_args, target_mysql_args, error_file_args):
    '''
    微博数据迁移
    func_53
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.weibo_transfer(
        source_file_args, source_mysql_args, target_mysql_args, error_file_args)


def new_weixin_account_storage(source_file_args, source_mysql_args, target_mysql_args, target_file_args_1, target_file_args_2, target_file_args_3, target_file_args_4, error_file_args):
    '''
    微信新账号入库
    func_54
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.new_weixin_account_storage(
        source_file_args, source_mysql_args, target_mysql_args, target_file_args_1, target_file_args_2, target_file_args_3, target_file_args_4, error_file_args)


def file_encode_exchange(source_file_args, target_file_args):
    '''文件格式转换
    func_55
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.file_encode_exchange(
        source_file_args, target_file_args)


def public_name_duplicate(source_mysql_args, target_file_args):
    '''
    public_name重复
    func_56
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.public_name_duplicate(
        source_mysql_args, target_file_args)


def media_vendor_account_count(source_mysql_args_1, source_mysql_args_2, target_mysql_args):
    """
    更新媒体主的资源数
    func_57
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_vendor_account_count(
        source_mysql_args_1, source_mysql_args_2, target_mysql_args)


def media_weixin_article_add_last_update_day(source_file_args, target_mongo_args):
    '''
    添加last_update_day字段
    func_58
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_article_add_last_update_day(
        source_file_args, target_mongo_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(start_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'add last_update_day'}, error_file_tool)


def media_weixin_article_csv(source_mongo_args, target_collection_args, target_columns_and_files):
    '''
    csv 数据拉取
    func_59
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_backups_obj = MediaWeixinBackups()
    article_count = media_weixin_backups_obj.media_weixin_article_csv(
        source_mongo_args, target_collection_args, target_columns_and_files)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(start_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'backups csv'}, error_file_tool)


def media_weixin_article_add_last_update_day_account_to_file(source_mongo_args, target_mongo_args):
    '''
        func_60
        账号分批存储
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_article_add_last_update_day_account_to_file(
        source_mongo_args, target_mongo_args)


def media_weixin_public_id_collect(source_mongo_args):
    '''
    func_61
    微信账号列表收集到文件
    '''
    error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    settled_count = media_weixin_storage_obj.media_weixin_public_id_collect(
        source_mongo_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(start_time)
    cost_time = int((end_time - start_time) / 60)
    task_mysql_tool.insert_one('data_task_time_axis', {
                               "item_count": settled_count, 'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'weixin public_id settled'}, error_file_tool)


def weixin_article_mongo_to_weixin_article_mysql(source_file_args, source_mongo_args, target_mysql_args):
    '''
    func_62
    mysql.weixin_article initial
    '''
    task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    document_count = media_weixin_storage_obj.weixin_article_mongo_to_weixin_article_mysql(
        source_file_args, source_mongo_args, target_mysql_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(start_time)
    cost_time = int((end_time - start_time) / 60)
    query = ("INSERT INTO `data_task_time_axis`(`item_count`,`cost_time`,`start_time`,`end_time`,`task_name`) VALUES(%d,%d,'%s','%s','%s')" % (
        document_count, cost_time, start_time, end_time, "weixin article initial"))
    print(query)
    task_mysql_tool.cursor.execute(query)
    task_mysql_tool.cnx.commit()
    task_mysql_tool.disconnection()


def weixin_article_kafka_mongo_control():
    """
    func_63
    kafka & mongodb 控制
    """
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_kafka_mongo_control()


def weixin_article_csv_consumer():
    """
    func_64
    weixin_article_csv_consumer
    """
    # media_weixin_kafka_transform=MediaWeiXinKafkaTransform()
    # media_weixin_kafka_transform.consumer_test((BACKUP_CSV_PATH_BIG_DATA,"kafka_test.csv","ab"),KAFKA)
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_csv_consumer(
        WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, KAFKA)


def erp_transfer(source_mysql_args, target_mysql_args):
    '''
    func_65
    erp transfer
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.erp_transfer(source_mysql_args, target_mysql_args)


def transer_data_to_hdfs():
    '''
    func_66
    csv增量数据上传到hdfs
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.transer_data_to_hdfs()


def basic_info_and_index_compute_data_to_mysql(source_mongo_args_1, source_mongo_args_2, source_mysql_args, target_mysql_args):
    '''
    func_67
    momngo.media_weixin基本信息 && 计算数据导入mysql.media_weixin
    '''
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    document_count = media_weixin_storage_obj.basic_info_and_index_compute_data_to_mysql(
        source_mongo_args_1, source_mongo_args_2, source_mysql_args, target_mysql_args)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(start_time)
    cost_time = int((end_time - start_time) / 60)
    query = ("INSERT INTO `data_task_time_axis`(`item_count`,`cost_time`,`start_time`,`end_time`,`task_name`) VALUES(%d,%d,'%s','%s','%s')" % (
        document_count, cost_time, start_time_str, end_time_str, "media_weixin_mysql update"))
    print(query)
    task_mysql_tool = get_mysql_tool(*WOM_DEV_MYSQL_TUPLE)
    task_mysql_tool.cursor.execute(query)
    task_mysql_tool.cnx.commit()
    task_mysql_tool.disconnection()


def spark_index_compute():
    """
    func_68
    spark 计算
    """
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.spark_index_compute()
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    query = ("INSERT INTO `data_task_time_axis`(`item_count`,`cost_time`,`start_time`,`end_time`,`task_name`) VALUES(%d,%d,'%s','%s','%s')" % (
        0, cost_time, start_time_str, end_time_str, "spark index compute"))
    print(query)
    task_mysql_tool = get_mysql_tool(*WOM_DEV_MYSQL_TUPLE)
    task_mysql_tool.cursor.execute(query)
    task_mysql_tool.cnx.commit()
    task_mysql_tool.disconnection()


def media_weixin_retail_price_update(source_mysql_args, target_mysql_args_1):
    """
    func_69
    media_weixin_retail_price_update
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weixin_retail_price_update(
        source_mysql_args, target_mysql_args_1)


def orm_mysql(host, db, user, password, table, file_path):
    """
    func_70
    """
    os.system("/usr/local/python35/bin/python3 -m pwiz -e mysql -H %s -p 3306 -u %s -P %s -t %s  %s > %s" %
              (host, user, password, table, db, file_path))


def dts_media_weixin_influence_rank_slave(source_mongo_args_1, source_mongo_args_2):
    """
    func_71
    导入排行榜数据
    """
    start_time = current_time_to_stamp()
    start_time_str = current_stamp_to_time(start_time)
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.dts_media_weixin_influence_rank_slave(
        source_mongo_args_1, source_mongo_args_2)
    end_time = current_time_to_stamp()
    end_time_str = current_stamp_to_time(end_time)
    cost_time = int((end_time - start_time) / 60)
    query = ("INSERT INTO `data_task_time_axis`(`item_count`,`cost_time`,`start_time`,`end_time`,`task_name`) VALUES(%d,%d,'%s','%s','%s')" % (
        0, cost_time, start_time_str, end_time_str, "dts_media_weixin_influence_rank"))
    task_mysql_tool = get_mysql_tool(*WOM_DEV_MYSQL_TUPLE)
    task_mysql_tool.cursor.execute(query)
    task_mysql_tool.cnx.commit()
    task_mysql_tool.disconnection()


def media_weixin_public_id_mysql_not_in_wom_info(source_mongo_args, source_mysql_args, target_file_args):
    """
    func_73
    检查榜单数据
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_public_id_mysql_not_in_wom_info(
        source_mongo_args, source_mysql_args, target_file_args)


def video_media_transfer_orm():
    """
    func_75
    视频迁移
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.video_media_transfer_orm()


def media_video_to_video_collection_info(source_mysql_args, target_mysql_args):
    """
    func_77
    视频account_id to collection dbs
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_video_to_video_collection_info(
        source_mysql_args, target_mysql_args)


def miaopai_account_to_collection_info(source_mysql_args, target_mysql_args):
    """
    func_78
    秒拍账号导入
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.miaopai_account_to_collection_info(
        source_mysql_args, target_mysql_args)


def transfer_public_id_to_file(source_mongo_args, target_file_args):
    '''
    func_79
    导出账号列表
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.transfer_public_id_to_file(
        source_mongo_args, target_file_args)


def media_video_price_adjust(source_mysql_args, target_mysql_args):
    '''
    func_80
    视频价格修改
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_video_price_adjust(
        source_mysql_args, target_mysql_args)


def media_weibo_price_adjust(source_mysql_args, target_mysql_args):
    '''
    func_81
    微博价格修改
    '''
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.media_weibo_price_adjust(
        source_mysql_args, target_mysql_args)


def delete_weixin_article_two_month_ago(target_mysql_args):
    '''
    func_82
    删除两个月以外的数据
    '''
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.delete_weixin_article_two_month_ago(
        target_mysql_args)


def media_weixin_head_view_and_like_select(source_mysql_args, source_file_args, target_file_args_1, target_file_args_2):
    """
    func_83
    查找制定账号所属分类，阅读数，点赞数
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_head_view_and_like_select(
        source_mysql_args, source_file_args, target_file_args_1, target_file_args_2)


def media_weixin_wom_avg_view_cnt_gte_6000(source_mongo_args, target_file_args_1,target_file_args_2,target_file_args_3,target_file_args_4,target_file_args_5,target_file_args_6,target_file_args_7,target_file_args_8,target_file_args_9,target_file_args_10):
    """
    func_84
    查找阅读平均数大于6000的账号
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_wom_avg_view_cnt_gte_6000(source_mongo_args, target_file_args_1,target_file_args_2,target_file_args_3,target_file_args_4,target_file_args_5,target_file_args_6,target_file_args_7,target_file_args_8,target_file_args_9,target_file_args_10)


def media_weixin_mongo_avg_view_cnt_gte_6000(source_mongo_args, source_file_args, target_file_args):
    """
    func_85
    查找阅读平均数大于6000的账号信息
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_mongo_avg_view_cnt_gte_6000(
        source_mongo_args, source_file_args, target_file_args)


def media_weixin_statistic_update(source_file_args, source_mongo_args):
    """
    func_86
    更新词云
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_statistic_update(
        source_file_args, source_mongo_args)


def media_weixin_public_id_to_kafka(source_file_args):
    """
    func_87
    更新词云，将将要更新的account 写入kafka
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_public_id_to_kafka(source_file_args)


def media_weixin_statistic_update_from_kafka(source_mongo_args):
    """
    func_88
    更新词云，从kafka获取任务
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_statistic_update_from_kafka(
        source_mongo_args)


def conventional_grow_trend_info_object_id_to_kafka(source_mongo_args, topic_numbers):
    """
    func_89
    多进程消费文章消息
    """
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.conventional_grow_trend_info_object_id_to_kafka(
        source_mongo_args, topic_numbers)


def weixin_article_consumer_from_grow_trend_info(kafka_args, source_mongo_args, target_mysql_args):
    """
    func_90
    多进程消费文章消息-mongo
    """
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_consumer_from_grow_trend_info(
        kafka_args, source_mongo_args, target_mysql_args)


def weixin_article_consumer_from_kafka(kafka_args, target_mysql_args):
    """
    func_91
    多进程消费文章消息-kafka
    """
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_consumer_from_kafka(
        kafka_args, target_mysql_args)


def media_weixin_account_tags(target_file_args_1,target_file_args_2):
    """
    func_94
    account tags 列表拉取
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_account_tags(target_file_args_1,target_file_args_2)

def media_weixin_vendor_bind_transfer():
    """
    func_101
    media_vendor_bind 数据迁移
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_vendor_bind_transfer()
def sogou_weixin_crawl_account_search(source_mongo_args):
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.sogou_weixin_crawl_account_search(source_mongo_args)

def media_prices_binds_alteration():
    """func_105
    定时更新价格数据
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_prices_binds_alteration()
def media_weixin_account_tags_top_100(target_file_args):
    """
    laqu wom tags top 100
    func_106

    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_account_tags_top_100(target_file_args)
def media_weixin_info_accounts_split_to_five_files(source_mongo_args,file_nums):
    """
    func_107
    拉取 influence 进程文件
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_info_accounts_split_to_five_files(source_mongo_args,file_nums)

def dts_media_weixin_influence_rank_slave_multi(source_mongo_args_1, source_mongo_args_2 , source_file_args, queue_tag):
    """
    func_108--func_112
    多进程跑入库任务
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.dts_media_weixin_influence_rank_slave_multi(source_mongo_args_1, source_mongo_args_2 , source_file_args, queue_tag)
def media_weixin_public_id_and_digest_to_mysql(source_mongo_args):
    """
    func_118
    分类所需数据导入mysql数据库
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_public_id_and_digest_to_mysql(source_mongo_args)
def dts_public_id_classify_by_classify_dict(source_file_args,target_file_args,classify):
    """
    func_119
    账号分类
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.dts_public_id_classify_by_classify_dict(source_file_args,target_file_args,classify)

def dts_account_recovery_to_others(source_file_args):
    """

    func_120
    账号分类恢复
    #26#

    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.dts_account_recovery_to_others(source_file_args)

def dts_account_tags_from_weixin_media_cate(target_file_args):
    """
    func_121
    账号分类列表拉出
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.dts_account_tags_from_weixin_media_cate(target_file_args)
def media_weixin_media_cate_to_mongo_media_weixin():
    """
    func_122
    wom_media_cate 填充
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_media_cate_to_mongo_media_weixin()
def mongo_media_weixin_to_mysql_media_weixin(source_mongo_args,source_file_args):
    """
    func_129
    迁移mongodb 基本信息
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.mongo_media_weixin_to_mysql_media_weixin(source_mongo_args,source_file_args)

def weixin_article_consumer_from_grow_trend_info_random(groups_num,group_id,source_mongo_args,target_mysql_args):
    """
    139 -148
    随机选取属于自己的分组，消费mongo消息
    """
    media_weixin_kafka_transform = MediaWeiXinKafkaTransform()
    media_weixin_kafka_transform.weixin_article_consumer_from_grow_trend_info_random(groups_num,group_id,source_mongo_args,target_mysql_args)
def wom_dev_media_tags(source_mysql_args_1,source_mysql_args_2,source_mysql_args_3,target_mysql_args):
    """
    func_149
    打地域标签
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.wom_dev_media_tags(source_mysql_args_1,source_mysql_args_2,source_mysql_args_3,target_mysql_args)

def mongo_media_weixin_real_public_id_updated(source_mysql_args,target_mongo_args):
    """
    func_153
    mongo media weixin 打遗失标记
    """
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.mongo_media_weixin_real_public_id_updated(source_mysql_args,target_mongo_args)
def mysql_media_weixin_basic_info_update(source_mongo_args,target_mysql_args):
    media_weixin_storage_obj = MediaWeiXinStorage()
    media_weixin_storage_obj.mysql_media_weixin_basic_info_update(source_mongo_args,target_mysql_args)
def mysql_media_weixin_basic_info_update(source_mongo_args):
    """
    func_155
    基本信息初始化
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.mysql_media_weixin_basic_info_update(source_mongo_args)
def wom_dev_weixin_cate_tags(source_file_args):
    """
    func_156
    打标签
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.wom_dev_weixin_cate_tags(source_file_args)
def wom_dev_cates_tags_to_dev_prod():
    """
    func_157
    分类，地域入库
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.wom_dev_cates_tags_to_dev_prod()

def brand_warehouse_qmg_csv(source_mongo_args,target_file_args_1,target_file_args_2):
    """
    func_158
    导出标签词典
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.brand_warehouse_qmg_csv(source_mongo_args,target_file_args_1,target_file_args_2)

def sogou_scel_dicts_csv(source_mongo_args,target_file_args):
    """
    func_158
    导出标签词典
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.sogou_scel_dicts_csv(source_mongo_args,target_file_args)
def brand_warehouse_qmg_by_level_csv(source_mongo_args,target_file_args_1,target_file_args_2):
    """
    按词长度导出标签字典
    func_159
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.brand_warehouse_qmg_by_level_csv(source_mongo_args,target_file_args_1,target_file_args_2)

def media_weixin_mongo_public_id_export(source_mongo_args,target_file_args):
    """
        media_weixin accounts exports
        func_161
    """
    media_weixin_check_obj = MediaWeiXinCheck()
    media_weixin_check_obj.media_weixin_mongo_public_id_export(source_mongo_args,target_file_args)

def media_weixin_statistic_brand_keyword_update(source_mongo_args):
    """
    func_162
    账号品牌词云入库
    """
    media_weixin_storage_orm = MediaWeiXinStorageORM()
    media_weixin_storage_orm.media_weixin_statistic_brand_keyword_update(source_mongo_args)

def main(argv):
    function_name = None
    try:
        opts, args = getopt.getopt(argv[1:], 'h', ['help=', 'func='])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ('-h', '--help'):
            sys.exit(1)
        elif o in ('-f', '--func'):
            function_name = a
    if function_name == "func_1":
        media_weixin_id_list()
    elif function_name == "func_2":
        pass
    elif function_name == "func_3":
        media_weixin_status_storage((DTS_OPT_MYSQL['host'], DTS_OPT_MYSQL['db'], DTS_OPT_MYSQL['user'], DTS_OPT_MYSQL['password']), (WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL['db'], WOM_PROD_MYSQL[
                                    'user'], WOM_PROD_MYSQL['password']), ('sogou_weixin_account_init_task', 'account_id', [['account_status', '=', 0]]), (CHECK_CSV_PATH, 'media_weixin_status_update_false.csv', 'ab'))
    elif function_name == "func_4":
        media_weixin_storage()
    elif function_name == "func_5":
        media_weixin_backups(BACKUP_JSON_PATH, WOM_DTS_DATAWAREHOSE_225_MONGO['host'],
                             27017, 225, 'media_weixin', (CHECK_CSV_PATH, 'media_weixin_id_has_article.csv', 'rb'), ["public_id"])
    elif function_name == "func_6":
        origin_average_price(WOM_PROD_MYSQL_TUPLE, ('media_weixin', 'id', ([
                             'status', '!=', 3],)), WOM_PROD_MYSQL_TUPLE, (CHECK_SQL_PATH, 'error.sql', 'ab'))
    elif function_name == "func_7":
        # media_weixin_pub_config_storage('media_weixin')

        error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
        start_time = current_time_to_stamp()
        start_time_str = current_stamp_to_time(start_time)
        price_list_pub_config_storage('media_weixin')
        # price_list_pub_config_storage('media_vendor_weixin_price_list')
        end_time = current_time_to_stamp()
        end_time_str = current_stamp_to_time(end_time)
        cost_time = int((end_time - start_time) / 60)
        task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
        task_mysql_tool.insert_one('data_task_time_axis', {
            'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_weixin_pub_config_update'}, error_file_tool)
        task_mysql_tool.disconnection()
    elif function_name == "func_8":

        error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
        start_time = current_time_to_stamp()
        start_time_str = current_stamp_to_time(start_time)
        # price_list_pub_config_storage('media_weixin')
        price_list_pub_config_storage('media_vendor_weixin_price_list')
        end_time = current_time_to_stamp()
        end_time_str = current_stamp_to_time(end_time)
        cost_time = int((end_time - start_time) / 60)
        task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
        task_mysql_tool.insert_one('data_task_time_axis', {
            'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_weixin_price_list_pub_config_update'}, error_file_tool)
        task_mysql_tool.disconnection()
    elif function_name == "func_9":
        get_media_weixin_id_list_mysql(WOM_PROD_1_TUPLE, ('t_media', 'sOpenName', [['iMediaType', '=', 1], ['iPrice1', '=', 0], ['iPrice2', '=', 0], ['iPrice3', '=', 0], ['iPrice4', '=', 0], [
                                       'iPut', '=', 1]]), ([1, CHECK_CSV_PATH, 'media_weixin_id_1_price_0.csv', 'ab'], [1, CHECK_CSV_PATH, 'media_weixin_id_1_price_0_duplicate.csv', 'ab'], [0, ]))
    elif function_name == "func_10":
        media_weixin_1_price_1_duplicate(WOM_PROD_1_TUPLE, ([1, CHECK_CSV_PATH, 'media_weixin_id_1_price_1.csv', 'ab'], [
                                         1, CHECK_CSV_PATH, 'media_weixin_id_1_price_1_duplicate.csv', 'ab'], [0, ]))
    elif function_name == "func_11":
        media_weixin_1_price_1_storage((CHECK_CSV_PATH, 'media_weixin_id_1_price_1.csv', 'rb'),
                                       WOM_PROD_1_TUPLE, WOM_PROD_MYSQL_TUPLE, ('media_weixin', 'public_id', [['public_id', '!=', '']]))
    elif function_name == "func_12":
        file_name_storage(
            (CHECK_CSV_PATH, 'backups_file_path.csv', 'ab'), '/home/admin/backups/json')
    elif function_name == "func_13":
        media_weixin_recovery(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO[
                              'user'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['password'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], 'media_weixin_temp', (CHECK_CSV_PATH, 'backups_file_path.csv', 'rb'))
    elif function_name == "func_14":
        unique_id_duplicate(WOM_PROD_MYSQL_TUPLE, ('media_weixin', 'public_id', [
        ]), (CHECK_CSV_PATH, 'media_weixin_public_id_duplicate_short.csv', 'ab'))
    elif function_name == "func_15":
        unique_id_duplicate_further(WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_public_id_duplicate_shorter.csv',
                                                           'rb'), (CHECK_CSV_PATH, 'media_weixin_public_id_duplicate_shorter_id.csv', 'ab'))
    elif function_name == "func_16":
        media_weixin_post_time_count(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin', {"post_time_law": {
                                     "$exists": True}}, ["public_id", ]), (CHECK_CSV_PATH, 'media_weixin_post_time_count.csv', 'ab'))
    elif function_name == "func_17":
        meida_weixin_post_time_count_from_id_file(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (CHECK_CSV_PATH, 'weixin_id_list.csv', 'rb'), (CHECK_CSV_PATH, 'media_weixin_post_time_count_from_id_file.csv', 'ab'), DTS_OPT_MYSQL_TUPLE)
    elif function_name == "func_18":
        time_stamp = str_to_time_stamp("2016-10-01 00:00:01")
        media_weixin_task_check(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin_article', {
                                "page_view_num": 0, "page_like_num": 0, "article_pos": 1, "post_date": {"$gte": time_stamp}}, ["weixin_id", ]), (CHECK_CSV_PATH, 'weixin_id_list.csv', 'rb'), DTS_OPT_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_article_task_server_host_name_in_hot_id_list.csv', 'ab'))
    elif function_name == "func_19":
        media_weixin_follower_area_update(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, (CHECK_SQL_PATH, 'error.sql', 'ab'))
    elif function_name == "func_20":
        media_weixin_article_analysis_split(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_21":
        media_weixin_article_power_analysis(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (
            'media_weixin', {}, ['public_id', ]), WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_22":
        media_weixin_article_count(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (
            CHECK_CSV_PATH, 'media_weixin_article_trend.csv', 'ab'))
    elif function_name == "func_23":
        media_weixin_article_power_analysis_multi_process_control(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin', {}, [
                                                                  'public_id', ]), 5, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (CHECK_CSV_PATH, 'media_weixin_article_power.csv', 'ab'))
    elif function_name == "func_24":
        media_vendor_bind_duplicate(WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE,
                                    WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_25":
        media_vendor_bind_duplicate_again(WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE,
                                          WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_26":
        error_file_tool = get_file_tool(*(CHECK_SQL_PATH, 'error.sql', 'ab'))
        task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
        start_time = current_time_to_stamp()
        start_time_str = current_stamp_to_time(start_time)
        media_weixin_contact_info_update(
            WOM_DEV_MYSQL_TUPLE, WOM_DEV_MYSQL_TUPLE, ERROR_FILE_ARGS)
        end_time = current_time_to_stamp()
        end_time_str = current_stamp_to_time(end_time)
        cost_time = int((end_time - start_time) / 60)
        task_mysql_tool.insert_one('data_task_time_axis', {
            'cost_time': cost_time, 'start_time': start_time_str, 'end_time': end_time_str, 'task_name': 'media_vendor_contact_info_update'}, error_file_tool)
    elif function_name == "func_27":
        delete_duplicate_date_from_transfer(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_28":
        media_weixin_task_account_level_update(
            WOM_PROD_MYSQL_TUPLE, DTS_OPT_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_29":
        media_weixin_data_backup(WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH,
                                                        'media_weixin_data_backups.csv', 'ab'), ERROR_FILE_ARGS)
    elif function_name == "func_30":
        media_weixin_pub_type_retail_min(
            WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_pub_type_retail_min.csv', 'ab'), ERROR_FILE_ARGS)
    elif function_name == "func_31":
        media_vendor_duplicate(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_32":
        media_weixn_media_vendor_bind(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_33":
        t_media_duplicate(WOM_PROD_1_TUPLE, (CHECK_CSV_PATH, 't_media_distinct.csv',
                                             'ab'), (CHECK_CSV_PATH, 't_media_duplicate.csv', 'ab'))
    elif function_name == "func_34":
        t_media_i_user_id_not_in_user_id(WOM_PROD_1_TUPLE, WOM_PROD_1_TUPLE, (
            CHECK_CSV_PATH, 't_media_user_id_not_in_t_user.csv', 'ab'))
    elif function_name == "func_35":
        media_weixin_1_price_1_update_upgrade((CHECK_CSV_PATH, 't_media_distinct.csv', 'rb'), (
            CHECK_CSV_PATH, 't_media_duplicate_distinct.csv', 'rb'), WOM_PROD_1_TUPLE, WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, '1_2_error.csv', 'ab'))
    elif function_name == "func_36":
        media_weixin_1_price_1_duplicate_data((CHECK_CSV_PATH, 't_media_duplicate_distinct.csv', 'rb'),
                                              WOM_PROD_1_TUPLE, (CHECK_CSV_PATH, 't_media_duplicate_records.csv', 'ab'))
    elif function_name == "func_37":
        settled_yuyue_public_id((CHECK_CSV_PATH, 't_media_distinct.csv', 'rb'),  (CHECK_CSV_PATH, 't_media_duplicate_distinct.csv',
                                                                                  'rb'), WOM_PROD_1_TUPLE, (CHECK_CSV_PATH, 't_media_yuyue_distinct_records.csv', 'ab'))
    elif function_name == "func_38":
        media_weixin_media_cate_update(
            WOM_DEV_MYSQL_TUPLE, WOM_DEV_MYSQL_TUPLE, (CHECK_SQL_PATH, 'error.sql', 'ab'))

    elif function_name == "func_39":
        media_weixin_public_id_update(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin', {'real_public_id_updated': {"$exists": False}}, [
                                      'public_id', ]), WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
    elif function_name == "func_40":
        media_weixin_price_errors(
            WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_price_errors.csv', 'ab'))
    elif function_name == "func_41":
        media_weixin_1_price_1_duplicate_update_upgrade((CHECK_CSV_PATH, 't_media_distinct.csv', 'rb'), (CHECK_CSV_PATH, 't_media_duplicate_distinct.csv', 'rb'), WOM_PROD_1_TUPLE, WOM_PROD_MYSQL_TUPLE, (
            CHECK_CSV_PATH, '1_2_error.csv', 'ab'), (CHECK_CSV_PATH, 't_media_duplicate_data_put_1_count_1.csv', 'ab'), (CHECK_CSV_PATH, 't_media_duplicate_data_put_0.csv', 'ab'), (CHECK_CSV_PATH, 't_media_duplicate_data_put_1_count_2.csv', 'ab'))
    elif function_name == "func_42":
        ad_owner_transfer(WOM_PROD_1_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_43":
        media_vendor_transfer(WOM_PROD_1_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_44":
        media_weixin_status_sync(DTS_OPT_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_45":
        media_weixin_pub_type_update(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_46":
        media_weixin_article_post_time_law(
            DTS_OPT_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, DTS_OPT_MYSQL_TUPLE)
    elif function_name == "func_47":
        media_weixin_has_direct_or_origin(
            WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_pub_xxx_pub_exception.csv', 'ab'))
    elif function_name == "func_48":
        media_weixin_article_csv(BACKUP_CSV_PATH, MONGO_SHARD_IP_LIST,
                                 27017, MONGO_SHARD_IP_SUFFIX_LIST, MONGO_COLLECTIONS_LIST[1])
    elif function_name == "func_49":
        media_weixin_backups_multi_process_control(
            BACKUP_JSON_PATH, MONGO_SHARD_IP_LIST, 27017, MONGO_SHARD_IP_SUFFIX_LIST, MONGO_COLLECTIONS_LIST)
    elif function_name == "func_50":
        backups_csv(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ("media_weixin", {}, ['public_id']), ('media_weixin_article', 'weixin_id', TARGET_COLUMN_LIST_ONE), (2, (TARGET_COLUMN_TUPLE[
                    1], (BACKUP_CSV_PATH_BIG_DATA, 'media_weixin_article_spark', 'ab'), "*|233|*|"), (TARGET_COLUMN_TUPLE[0], (BACKUP_CSV_PATH_BIG_DATA, 'media_weixin_article_hadoop', 'ab'), ",")))
    elif function_name == "func_51":
        backups_csv_multi_process_control(5, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ("media_weixin", {}, ['public_id']), ('media_weixin_article', 'weixin_id', TARGET_COLUMN_LIST_ONE), (1, (TARGET_COLUMN_TUPLE[0], (BACKUP_CSV_PATH_BIG_DATA, 'media_weixin_article_hadoop', 'ab'), ","), (TARGET_COLUMN_TUPLE[
            1], (BACKUP_CSV_PATH_BIG_DATA, 'media_weixin_article_spark', 'ab'), "*|233|*|")))
    elif function_name == "func_52":
        weibo_account_duplicate(WOM_PROD_1_TUPLE, (CHECK_CSV_PATH, 'weibo_url_unique.csv',
                                                   'ab'), (CHECK_CSV_PATH, 'weibo_url_duplicate_to_acui.csv', 'ab'))
    elif function_name == "func_53":
        weibo_transfer((CHECK_CSV_PATH, 'weibo_url_unique.csv', 'rb'), WOM_PROD_1_TUPLE,
                       WOM_PROD_MYSQL_TUPLE, (CHECK_SQL_PATH, 'error.sql', 'ab'))
    elif function_name == "func_54":
        new_weixin_account_storage((CHECK_CSV_PATH, 'weixin_11_17_4.csv', 'rb'), WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, '11_17_account_storaged.csv', 'ab'), (CHECK_CSV_PATH,
                                                                                                                                                                                    '11_17_account_existed.csv', 'ab'), (CHECK_CSV_PATH, '11_17_account_duplicate.csv', 'ab'), (CHECK_CSV_PATH, '11_17_account_format_error.csv', 'ab'), (CHECK_SQL_PATH, 'error.sql', 'ab'))
    elif function_name == "func_55":
        file_encode_exchange((CHECK_CSV_PATH, '11_17_account_format_error.csv', 'rb'),
                             (CHECK_CSV_PATH, '11_17_account_format_error_gbk.csv', 'ab'))
    elif function_name == "func_56":
        public_name_duplicate(
            WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'public_name_duplicate_11_09.csv', 'ab'))
    elif function_name == "func_57":
        media_vendor_account_count(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_58":
        media_weixin_article_add_last_update_day(
            (CHECK_CSV_PATH, 'weixin_id_to_add_last_update_day_1.csv', 'rb'), WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_59":
        media_weixin_article_csv(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin_article', {}, TARGET_COLUMN_LIST_ONE), (2, (TARGET_COLUMN_TUPLE[
            1], (BACKUP_MACHINE_LEARNING_PATH, 'article', 'ab'), "*|233|*|"), (TARGET_COLUMN_TUPLE[0], (BACKUP_INDEX_COMPUTE_PATH, 'article', 'ab'), ",")))
    elif function_name == "func_60":
        media_weixin_article_add_last_update_day_account_to_file(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_61":
        media_weixin_public_id_collect(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_62":
        weixin_article_mongo_to_weixin_article_mysql(
            ("/alidata1/dts/export_from_mongo/weixin_article", "public_id_2.csv", "rb"), WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_63":
        weixin_article_kafka_mongo_control()
    elif function_name == "func_64":
        weixin_article_csv_consumer()
    elif function_name == "func_65":
        erp_transfer(WOM_PROD_MYSQL_TUPLE, ERP_PROD_TUPLE)
    elif function_name == "func_66":
        start_time = current_time_to_stamp()
        start_time_str = current_stamp_to_time(start_time)
        transer_data_to_hdfs()
        end_time = current_time_to_stamp()
        end_time_str = current_stamp_to_time(start_time)
        cost_time = int((end_time - start_time) / 60)
        query = ("INSERT INTO `data_task_time_axis`(`item_count`,`cost_time`,`start_time`,`end_time`,`task_name`) VALUES(%d,%d,'%s','%s','%s')" % (
            0, cost_time, start_time_str, end_time_str, "transer_data_to_hdfs"))
        print(query)
        task_mysql_tool = get_mysql_tool(*DEV_WOM2_MYSQL_TUPLE)
        task_mysql_tool.cursor.execute(query)
        task_mysql_tool.cnx.commit()
        task_mysql_tool.disconnection()
    elif function_name == "func_67":
        basic_info_and_index_compute_data_to_mysql(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_68":
        spark_index_compute()
    elif function_name == "func_69":
        media_weixin_retail_price_update(
            WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_70":
        orm_mysql(*WOM_DEV_MYSQL_TUPLE, "dts_media_weixin_influence_rank,dts_media_weixin_influence_rank_slave,dts_influence_rank_table_tip,media_weixin_statistic", "/home/admin/ETL_PYTHON_3/models/influence_rank_model.py")
    elif function_name == "func_71":
        dts_media_weixin_influence_rank_slave(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_72":
        orm_mysql(*WOM_PROD_MYSQL_TUPLE, "media_weixin", "/home/admin/ETL_PYTHON_3/models/media_weixin_update_model.py")
    elif function_name == "func_73":
        media_weixin_public_id_mysql_not_in_wom_info(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, (
            CHECK_CSV_PATH, 'media_weixin_public_id_mysql_not_in_wom_info.csv', 'ab'))
    elif function_name == "func_74":
        orm_mysql(*WOM_DEV_MYSQL_TUPLE, "media_video,vendor_video_bind,video_platform_common_info,video_vendor_price,video_media_base_info,video_vendor_bind,video_media_douyu,video_media_hani,video_media_huajiao,video_media_meipai,video_media_miaopai,video_media_panda,video_media_taobao,video_media_yingke,video_media_yizhibo", "/home/admin/ETL_PYTHON_3/models/video_table_model.py")
    elif function_name == "func_75":
        video_media_transfer_orm()
    elif function_name == "func_76":
        orm_mysql(*WOM_DEV_MYSQL_TUPLE, "media_vendor", "/home/admin/ETL_PYTHON_3/models/media_vendor_model.py")
    elif function_name == "func_77":
        media_video_to_video_collection_info(
            WOM_PROD_MYSQL_TUPLE, DTS_OPT_MYSQL_TUPLE)
    elif function_name == "func_78":
        miaopai_account_to_collection_info(
            WOM_PROD_MYSQL_TUPLE, DTS_OPT_MYSQL_TUPLE)
    elif function_name == "func_79":
        transfer_public_id_to_file(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (
            CHECK_CSV_PATH, 'media_weixin_info_public_id_1.csv', 'ab'))
    elif function_name == "func_80":
        media_video_price_adjust(WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_81":
        media_weibo_price_adjust(WOM_PROD_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_82":
        delete_weixin_article_two_month_ago(WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_83":
        media_weixin_head_view_and_like_select(WOM_PROD_MYSQL_TUPLE, (CHECK_CSV_PATH, 'media_weixin_public_id.csv', 'rb'), (
            CHECK_CSV_PATH, 'media_weixin_has_head_view_like.csv', 'ab'), (CHECK_CSV_PATH, 'media_weixin_has_not_head_view_like.csv', 'ab'))
    elif function_name == "func_84":
        media_weixin_wom_avg_view_cnt_gte_6000(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_1.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_2.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_3.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_4.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_5.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_6.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_7.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_8.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_9.csv', 'ab'),
            (CHECK_CSV_PATH, 'media_weixin_mongo_public_id_10.csv', 'ab'),)
    elif function_name == "func_85":
        media_weixin_mongo_avg_view_cnt_gte_6000(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, (
            CHECK_CSV_PATH, 'media_weixin_info_public_id.csv', 'rb'), (CHECK_CSV_PATH, 'media_weixin_account_info.csv', 'ab'))
    elif function_name == "func_86":
        media_weixin_statistic_update(
            (CHECK_CSV_PATH, 'account_tags.csv', 'rb'), WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_87":
        media_weixin_public_id_to_kafka(
            (CHECK_CSV_PATH, 'media_weixin_info_public_id.csv', 'rb'))
    elif function_name == "func_88":
        media_weixin_statistic_update_from_kafka(
            WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_89":
        conventional_grow_trend_info_object_id_to_kafka(
            WOM_DTS_OPS_MONGO_TUPLE, 12)
    elif function_name == "func_90":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-1-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_91":
        weixin_article_consumer_from_kafka({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                           b"media_weixin_grow_trend_info", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_92":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-2-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_93":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-3-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_94":
        media_weixin_account_tags((CHECK_CSV_PATH, 'account_tags.csv', 'ab'),(CHECK_CSV_PATH, 'media_weixin_basic_data_export.csv', 'ab'))
    elif function_name == "func_95":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-4-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_96":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-5-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_97":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-6-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_98":
        orm_mysql(*WOM_PROD_MYSQL_TUPLE, "weiboyi_live_talent_data,media_video,video_platform_common_info,video_vendor_price,vendor_video_bind", "/home/admin/ETL_PYTHON_3/models/video_weiboyi_model.py")
    elif function_name == "func_99":
        video_weiboyi_transfer()
    elif function_name == "func_100":
        orm_mysql(*WOM_DEV_MYSQL_TUPLE, "weixin_article", "/home/admin/ETL_PYTHON_3/models/dts_media_weixin_kaikai.py")
    elif function_name == "func_101":
        media_weixin_vendor_bind_transfer()
    elif function_name == "func_102":
        orm_mysql(*WOM_PROD_MYSQL_TUPLE, "media_weixin", "/home/admin/ETL_PYTHON_3/models/media_weixin_new.py")
    elif function_name == "func_103":
        orm_mysql(*DTS_OPT_MYSQL_TUPLE, "sogou_weixin_account_latest_article_crawl_task,sogou_weixin_account_init_task", "/home/admin/ETL_PYTHON_3/models/sogou_weixin_article_crawl_model.py")
    elif function_name == "func_104":
        sogou_weixin_crawl_account_search(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_105":
        media_prices_binds_alteration()
    elif function_name == "func_106":
        media_weixin_account_tags_top_100((CHECK_CSV_PATH, 'qmg_classify_tags_top_100.csv', 'ab'))
    elif function_name == "func_107":
        media_weixin_info_accounts_split_to_five_files(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,6)
    elif function_name == "func_108":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_1.csv","rb"), 1)
    elif function_name == "func_109":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_2.csv","rb"),2)
    elif function_name == "func_110":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_3.csv","rb"), 3)
    elif function_name == "func_111":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_4.csv","rb"), 4)
    elif function_name == "func_112":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_5.csv","rb"), 5)
    elif function_name == "func_113":
        dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_6.csv","rb"), 6)
    # elif function_name == "func_114":
    #     dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_7.csv","rb"),7)
    # elif function_name == "func_115":
    #     dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_8.csv","rb"), 8)
    # elif function_name == "func_116":
    #     dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_9.csv","rb"), 9)
    # elif function_name == "func_117":
    #     dts_media_weixin_influence_rank_slave_multi(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE , (CHECK_CSV_PATH,"media_weixin_info_influence_public_id_10.csv","rb"), 10)
    elif function_name == "func_118":
        media_weixin_public_id_and_digest_to_mysql(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_119":
        dts_public_id_classify_by_classify_dict((CHECK_CSV_PATH,"classify_2.csv","rb"),(CHECK_CSV_PATH,"classify_2_1_classified_by_desc.csv","ab"),"#2#")
    elif function_name == "func_120":
        dts_account_recovery_to_others((CHECK_CSV_PATH,"classify_2_1_classified_by_desc.csv","rb"))
    elif function_name == "func_121":
        dts_account_tags_from_weixin_media_cate((CHECK_CSV_PATH, 'account_tags.csv', 'ab'))
    elif function_name == "func_122":
        media_weixin_media_cate_to_mongo_media_weixin()
    elif function_name == "func_123":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-7-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_124":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-8-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_125":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-9-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_126":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-10-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_127":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-11-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_128":
        weixin_article_consumer_from_grow_trend_info({"hosts": "server-32:9092, server-32:9093, server-32:9094", "topics": [
                                                     b"grow-trend-info-12-uniwow", b""], "zookeepers": "server-32:2001,server-32:2002,server-32:2003"}, WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_129":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_1.csv', 'rb'))
    elif function_name == "func_130":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_2.csv', 'rb'))
    elif function_name == "func_131":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_3.csv', 'rb'))
    elif function_name == "func_132":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_4.csv', 'rb'))
    elif function_name == "func_133":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_5.csv', 'rb'))
    elif function_name == "func_134":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_6.csv', 'rb'))
    elif function_name == "func_135":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_7.csv', 'rb'))
    elif function_name == "func_136":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_8.csv', 'rb'))
    elif function_name == "func_137":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_9.csv', 'rb'))
    elif function_name == "func_138":
        mongo_media_weixin_to_mysql_media_weixin(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_mongo_public_id_10.csv', 'rb'))
    elif function_name == "func_139":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,0,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_140":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,1,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_141":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,2,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_142":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,3,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_143":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,4,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_144":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,5,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_145":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,6,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_146":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,7,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_147":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(8,8,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_148":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(9,9,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_149":
        wom_dev_media_tags(WOM_DEV_MYSQL_TUPLE,WOM_DEV_MYSQL_TUPLE,WOM_DEV_MYSQL_TUPLE,WOM_DEV_MYSQL_TUPLE)
    elif function_name == "func_150":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(3,0,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_151":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(3,1,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_152":
        while True:
            weixin_article_consumer_from_grow_trend_info_random(3,2,WOM_DTS_OPS_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_153":
        mongo_media_weixin_real_public_id_updated(WOM_PROD_MYSQL_TUPLE,WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_154":
        mysql_media_weixin_basic_info_update(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,WOM_PROD_MYSQL_TUPLE)
    elif function_name == "func_155":
        mysql_media_weixin_basic_info_update(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    elif function_name == "func_156":
        wom_dev_weixin_cate_tags((CHECK_CSV_PATH, 'wom_dev_tags.csv', 'rb'))
    elif function_name == "func_157":
        wom_dev_cates_tags_to_dev_prod()
    elif function_name == "func_158":
        brand_warehouse_qmg_csv(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'brand_dict_utf_remained.csv', 'ab'),(CHECK_CSV_PATH, 'brand_dict_utf_removed.csv', 'ab'))
    elif function_name == "func_159":
        sogou_scel_dicts_csv(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'sogou_scel_dicts.txt', 'ab'))
    elif function_name == "func_160":
        brand_warehouse_qmg_by_level_csv(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'brand_dict_utf_by_name_length_remained.csv', 'ab'),(CHECK_CSV_PATH, 'brand_dict_utf_by_name_length_removed.csv', 'ab'))
    elif function_name == "func_161":
        media_weixin_mongo_public_id_export(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE,(CHECK_CSV_PATH, 'media_weixin_accounts_segment.csv', 'ab'))
    elif function_name == "func_162":
        media_weixin_statistic_brand_keyword_update(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)

if __name__ == '__main__':
    main(sys.argv)
