# 1.内存溢出  

内存溢出分两种情况，executer内存溢出或者driver内存溢出，分别调整--executor-memory 和 --driver-memory,问题解决。  

# 2.jps 4791 -- process information unavailable 问题  

进入tmp目录，cd /tmp ，删除该目录下名称为hsperfdata_{ username}的文件夹，问题解决。  

# 3.java.lang.NullPointerException 

对相应对象加上 `@transient` 非序列化注释注释

# 4.org.codehaus.commons.compiler.CompileException  

这个错误是类型不一致错误。spark dataset 只识别scala.collection.Map,将scala.collection.mutable.Map 以及 scala.collection.immutable.Map 转换为scala.collection.Map，问题解决。  

# 5.sparkContext 同时打开多个问题

val conf = new SparkConf().setMaster("spark://spider-host-001:7077").setAppName("NetworkWordCount")，conf.set("spark.driver.allowMultipleContexts","true")，问题解决。

# 6.RPC 连接错误  

设置spark.shuffle.service.port=17999,spark.shuffle.service.enabled=true两个参数，并启动shuffer服务，问题解决。

# shuffer 错误

设置spark.memory.useLegacyMode=true，增大spark.shuffle.memoryFraction=0.4。


