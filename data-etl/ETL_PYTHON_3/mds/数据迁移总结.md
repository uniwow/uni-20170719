# 数据迁移总结

## 流程

1. 列出target表所需字段
2. 列出source表所有字段
3. 根据target表在source表中标记对应关系
4. 删除source表中没有被用到的字段
5. 按顺序整理对应关系
6. 根据字段列表,写select&insert表操作语句
7. 根据对应关系赋值
8. 填充流程逻辑

## 注意事项

#### INSERT&UPDATE 语句填充

以下面INSERT语句为例

query = ("INSERT INTO `media_weibo`(`uuid`,`weibo_name`,`follower_num`,`follower_screenshot`,`weibo_url`,`avatar`,`qrcode`,`read_num`,`media_level`,`intro`,`status`,`is_put`,`is_top`,`active_end_time`,`fail_reason`,`put_down_reason`,`audit_remark`,`create_time`,`update_time`,`audit_time`,`enter_time`) VALUES('%s','%s',%d,'%s','%s','%s','%s',%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s',%d,%d,%d,%d)" % (media_weibo_uuid,weibo_name,follower_num,follower_screenshot,weibo_url,avatar,qrcode,read_num,media_level,intro,media_weibo_status, is_put,is_top,active_end_time,fail_reason,put_down_reason,audit_remark,create_time,update_time,audit_time,enter_time))

填充时，应用[media_weibo_uuid,weibo_name,follower_num,follower_screenshot,weibo_url,avatar,qrcode,read_num,media_level,intro,media_weibo_status, is_put,is_top,active_end_time,fail_reason,put_down_reason,audit_remark,create_time,update_time,audit_time,enter_time]分别填充三个位置的括号中充当占位符，再转化成需要的格式。

#### 所有字段都在赋值后使用。

不应该拿字段原始值做判断或者其他使用方式。原始字段经过处理后赋值给target表对应字段