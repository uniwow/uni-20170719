# 异常及其修正
1. 30k数据量时，不启动shuffle service报java.io.IOException: Connection reset by peer异常,启动后报java.lang.OutOfMemoryError: Java heap space。两种情况分别测试了两次。
2. 30k数据量时,通过控制spark.shuffle.memoryFraction、spark.storage.memoryFraction、spark.storage.unrollFraction可以观察到错误日志中打印的内存由大变小。详情观察excel. 通过观察4040页面storage，发现storagelevel 全为内存级别。详细请执行程序观察4040页面，整个执行过程总共16分钟左右。

# 参数说明  

1. spark.shuffle.service.enabled=true Enables the external shuffle service.设置后可通过 $SPARK_HOME/sbin/./start-shuffle-service.sh 开启服务  
2. spark.memory.useLegacyMode=true 设置为True,允许调节以下参数  
    * spark.shuffle.memoryFraction=0.3 shuffer 占用内存比例
    * spark.storage.memoryFraction=0.2 cache内存 占用比例   
    * spark.storage.unrollFraction=0.5 use for unrolling blocks in memory.  知乎中有一个人把cache内存设置为0.1。这个可能牵涉到源码中的持久化方案.   
3. spark.reducer.maxSizeInFlight=50m Maximum size of map outputs to fetch simultaneously from each reduce task. 这个参数可能比较重要。尚未试验 

# 编码优化  
1. map -> mapPartitions(foreachPartition)  
2. parallelize 生成并行集合  

# 记得查看4040页面查看执行过程中的一些参数，jobs，stages，storage，Executer属于必看项


