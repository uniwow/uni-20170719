# 视频热榜采集流程

## 花椒热门账号采集
1. 解析分类链接
  [花椒热榜页](http://www.huajiao.com/category/1000)
2. 将采集到的新账号放入dev_wom2.video_collection_info表中

## 美拍热门账号采集
1. 解析分类链接
[美拍热榜链接](http://www.meipai.com/home/hot_timeline?page=1&count=100&single_column=1)
2. 将采集到的新账号放入dts_opt.video_collection_info表中 

# 热门网红信息采集流程图
* 详见热门网红信息采集流程图