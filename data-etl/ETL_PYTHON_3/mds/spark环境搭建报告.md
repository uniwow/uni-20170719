# spark环境搭建报告

## 配置java环境

* 安装路径为:/usr/local/java

## 配置hadoop环境

1. 把hadoop-2.7.2解压到/usr/local/hadoop27目录下

2. cd /usr/local/hadoop27 && mkdir tmp hdfs &&cd hdfs && mkdir data name

3. cd /usr/local/hadoop27/etc/hadoop

 * vim core-site.xml 添加
        ```
        <property>
            <name>fs.defaultFS</name>
            <value>hdfs://server-32:9000</value>
        </property>
        <property>
            <name>hadoop.tmp.dir</name>
            <value>/usr/local/hadoop27/tmp</value>
        </property>
        <property>
            <name>io.file.buffer.size</name>
            <value>131702</value>
        </property>
        ```

 * vim hdfs-site.xml 添加
        ```
        <property>
            <name>dfs.namenode.name.dir</name>
            <value>file:///usr/local/hadoop27/hdfs/name</value>
        </property>
        <property>
            <name>dfs.datanode.data.dir</name>
            <value>file:///usr/local/hadoop27/hdfs/data</value>
        </property>
        <property>
            <name>dfs.replication</name>
            <value>2</value>
        </property>
        <property>
            <name>dfs.namenode.secondary.http-address</name>
            <value>server-32:9001</value>
        </property>
        <property>
            <name>dfs.webhdfs.enabled</name>
            <value>true</value>
        </property>
        ```
 * vim mapred-site.xml 添加
        ```
        <property>
            <name>mapreduce.framework.name</name>
            <value>yarn</value>
        </property>
        <property>
            <name>mapreduce.jobhistory.address</name>
            <value>server-32:10020</value>
        </property>
        <property>
            <name>mapreduce.jobhistory.webapp.address</name>
            <value>server-32:19888</value>
        </property>
        <property>
            <name>mapreduce.map.memory.mb</name>
            <value>1536</value>
        </property>
        <property>
　　         <name>mapreduce.map.java.opts</name>
　　         <value>-Xmx1024M</value>
        </property>
        <property>
　　          <name>mapreduce.reduce.memory.mb</name>
　　          <value>3072</value>
        </property>
        <property>
　　          <name>mapreduce.reduce.java.opts</name>
　　          <value>-Xmx1024M</value>
        </property>
        ```
 * vim yarn-site.xml
        ```
        <property>
            <name>yarn.resourcemanager.hostname</name>
            <value>server-32</value>
        </property>
        <property> 
            <name>yarn.nodemanager.aux-services</name>
            <value>mapreduce_shuffle</value>
        </property>
        <property> 
            <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
            <value>org.apache.hadoop.mapred.ShuffleHandler</value>
        </property>
        ```

    * vim hadoop-env.sh 添加

        export JAVA_HOME=/usr/local/java/jdk1.8.0_74

    * vim slaves 添加
    
        server-32

        server-102

4. scp -r /usr/local/hadoop27 192.168.0.183:/usr/local/

5. cd /usr/local/hadoop27/bin && ./hdfs namenode -format

6. ../sbin/start-all.sh

7. jps 命令验证

    * server-32
    
        4102 SecondaryNameNode

        4262 ResourceManager

        7004 Jps

        3804 NameNode

        4366 NodeManager

        3934 DataNode

    * server-102
    
        15952 DataNode

        16068 NodeManager

        18255 Jps

## 配置scala环境

    * 解压scala-2.11.8.tar.gz 到 /usr/local/scala

## 配置spark环境
    
1. 解压 spark-2.0.0-bin-hadoop2.7.tgz 到 /usr/local/spark

2. cd /usr/local/spark/conf && vim spark-env.sh 添加

    export SCALA_HOME=/usr/local/scala

    export JAVA_HOME=/usr/local/java/jdk1.8.0_74

    export HADOOP_HOME=/usr/local/hadoop27

    export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop

    SPARK_MASTER_HOST=server-32

    export SPARK_LOCAL_DIRS=/usr/local/spark

    SPARK_DRIVER_MEMORY=1G

3. cd /usr/local/spark && sbin/start-all.sh && jps

    * server-32
    
        6706 Worker

        4102 SecondaryNameNode

        4262 ResourceManager

        6601 Master

        7004 Jps

        3804 NameNode

        4366 NodeManager

        3934 DataNode

    * server-102
    
        15952 DataNode

        18049 Worker

        16068 NodeManager

        18255 Jps

### spark 环境搭建完成
