# coding=utf-8
from pymongo import MongoClient
import os
import sys
from .tool_log import Logger
mongo_connection = Logger(logname='./logs/mongo_connection.log',
                          loglevel=1, logger="database_connection").getlog()


class MongoTool(object):

    def __init__(self, ip, port, db_name, db_user, db_pwd, *args, **kvargs):
        try:
            self.client = MongoClient(ip, port)
            self.client[db_name].authenticate(db_user, db_pwd)
            self.database = self.client[db_name]
        except Exception as e:
            mongo_connection.error("mongo connection error")
            raise e

    def __del__(self):
        self.client.close()

    def disconnection(self, *args, **kvargs):
        self.client.close()
    '''
    首先获得id列表，再循环id列表逐一取出结果集，在进行后续操作
    '''
    def cursor_has_next(self,cursor):
        if cursor and cursor.alive:
            return True
        else:
            return False

    def get_unique_id_list(self, source_collection, source_condition_dict, source_result_key_list, *args, **kvargs):
        '''
        获得将要查询的表的主键列表
        '''
        source_result_item_dict = {}
        '''
        拼接结果集字典
        '''
        for source_result_item in source_result_key_list:
            source_result_item_dict[source_result_item] = 1
        document_list = self.database[source_collection].find(
            source_condition_dict, source_result_item_dict)
        document_list.batch_size(500)
        source_unique_id_list = []
        count = 0
        while self.cursor_has_next(document_list):
            document=document_list.next()
            source_unique_id_list.append(
                document[source_result_key_list[0]])
            count += 1
            print("unique id selected " +
                  str(document[source_result_key_list[0]]))
        print("totally %d items was found" % count)
        # input('.................')
        return source_unique_id_list

    def find_one(self, source_collection, source_condition_dict, source_result_key_list, *args, **kvargs):
        '''
        取出结果集
        '''
        source_result_item_dict = {}
        '''
        拼接结果集字典
        '''
        for source_result_item in source_result_key_list:
            source_result_item_dict[source_result_item] = 1
        # print(str(source_result_item_dict))
        return self.database[source_collection].find_one(source_condition_dict, source_result_item_dict)

    def update_one(self, source_collection, source_condition_dict, source_update_dict):
        '''
                更新结果集
        '''
        self.database[source_collection].update(
            source_condition_dict, source_update_dict, True)

    def delete_one(self, source_collection, source_condition_dict):
        self.database[source_collection].remove(source_condition_dict)

    def unordered_bulk_options(self, collection_name):
        bulk = collection_name.initialize_ordered_bulk_op()
        return bulk

    def ordered_bulk_options(self, collection_name):
        bulk = collection_name.initialize_ordered_bulk_op()
        return bulk
if __name__ == '__main__':
    mongo_tool = MongoTool("139.196.52.148", 27017, "uni", "uni", "uni")
    mongo_tool.get_unique_id_list("uniwow_analysis", {}, ["_id", ])
