def sd_generator(value_list, prob_list, value_avg):
    """
    方差生成器
    """
    new_value_list = [(value - value_avg)**2 for value in value_list]
    value_avg = 0.0
    for value, prob in zip(new_value_list, prob_list):
        value_avg = value_avg + (value * prob)
    return round(value_avg, 2)


def avg_generator(value_list, prob_list):
    """
    平均数生成器
    """
    value_avg = 0.0
    for value, prob in zip(value_list, prob_list):
        value_avg = value_avg + (value * prob)
    return round(value_avg, 2)
