import subprocess
import sys
args = sys.argv


def get_pid(process):
    cmd = 'ps aux|grep ' + process
    p = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    pid_list = []
    while True:
        buff = p.stdout.readline()
        if buff == b'':
            break
        infos = buff.split()
        print(buff)
        pid_list.append(str(infos[1]))
    # print(str(pid_list))
    # input("............")

    return pid_list
