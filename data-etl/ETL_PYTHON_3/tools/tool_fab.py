# coding=utf-8
from __future__ import with_statement
from fabric.api import *
from fabric.contrib.console import confirm
env.user = 'admin'
# env.hosts = [,'spider-host-001','spider-host-002']
env.roledefs = {
    'ETL': ['server-32',],
    'SPARK': ['spider-host-001','spider-host-002']
}

@roles('web') 
def get_version():
    run('cat /etc/issue')

@hosts('host1', 'host2')
def mytask():
    run('ls /var/www')
    
@roles('dts')
def get_host_name():
    run('hostname')

@parallel(pool_size=5)
def heavy_task():
    print("hello parallel")
def hello(name="world"):
    print("Hello %s!" % name)

def deploy():
    code_dir = '/srv/django/myproject'
    with cd(code_dir):
        run("git pull")
        run("touch app.wsgi")