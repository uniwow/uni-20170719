# coding=utf-8
from pykafka import KafkaClient, SslConfig
from .settings import KAFKA


class KafkaTool(object):

    def __init__(self, kafka_host, kafka_topic):
        client = KafkaClient(hosts=kafka_host)
        topics = client.topics
        self.target_topic = topics[kafka_topic.encode('utf-8', 'ignore')]

    def get_topic(self):
        return self.target_topic

    def get_simple_consumer(self, consumer_group=None, **kvargs):
        return self.target_topic.get_simple_consumer(consumer_group, use_rdkafka=False, **kvargs)

    def get_balanced_consumer(self, consumer_group=None, **kvargs):
        return self.target_topic.get_balanced_consumer(consumer_group, managed=False, **kvargs)

    def get_producer(use_rdkafka=False, sync=False, **kvargs):
        return self.target_topic.get_producer(use_rdkafka, sync, **kvargs)

    def get_latest_available_offsets(self):
        return self.target_topic.get_latest_available_offsets()

    def fetch_offset_limits(offsets_before, max_offsets=5):
        return self.target_topic.fetch_offset_limits(offsets_before, max_offsets)

    def commit_offset(self, target_consumer):
        target_consumer.commit_offsets()
