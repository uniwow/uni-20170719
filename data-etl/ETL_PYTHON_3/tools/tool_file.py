# coding=utf-8
line_count = 0
import os
from os.path import join, getsize


class FileTool(object):

    def __init__(self, file_path, file_name, open_type):
        full_file_name = file_path + "/" + file_name
        if not os.path.exists(full_file_name):
            os.system("touch %s" % full_file_name)
        self.f = open(full_file_name, open_type)

    def __del__(self):
        self.f.close()

    def get_file(self):
        '''获取文件'''
        return self.f

    def close_file(self):
        self.f.close()


    def line_strip_decode_ignore(self, line):
        '''文件转码'''
        return line.strip().decode("utf-8", 'ignore')

    def write_file_line_to_file(self, line):
        '''把文件行写入文件'''
        self.f.write(line + "\n".encode("utf-8", 'ignore'))

    def write_database_line_to_file(self, line):
        '''把数据库行写入文件'''
        self.f.write(line.encode("utf-8", 'ignore'))
        self.f.flush()

    def line_split_decode_ignore(self, line, target_column_in_file_list):
        '''解析csv文件，返回列表'''

        pass

    def csv_generator(self, li):
        item_line = ""
        for item in li[:-1]:
            item_line = item_line + item + "\t"
        item_line = item_line + li[-1] + '\n'
        return item_line

    def countline(self, f):
        global line_count
        if f.endswith(('.dat')):
            print(f)
            fo = open(f, 'rb')
            lines = len(fo.read().split('\n'))
            fo.close()
            line_count += lines
            fo.close()
            return lines - 1

    def walks(self, path):
        for root, dirs, files in os.walk(path):
            for fo in files:
                fo = os.path.join(path, fo)
                self.write_database_line_to_file(fo)

    def remove_file(self, path):
        if os.path.exists(path):
            os.remove(file_absolute_path)

    def getdirsize(self, dir):
        size = 0
        for root, dirs, files in os.walk(dir):
            size += sum([getsize(join(root, name)) for name in files])
        return size

    def get_files_size(self, files):
        size = 0
        for file in files:
            size += getsize(file) / (1024 * 1024)
        return size

    def write_mongo_csv_to_file(self, column_list, separator, document):
        target_line = ''
        for column in column_list:
            try:
                if column in ['post_time', 'article_type', 'article_pos', 'page_view_num','page_like_num', 'comment_num','update_time']:
                    target_line = target_line + str(int(document[column])) + separator
                else:
                    target_line = target_line + str(document[column]) + separator
            except Exception as e:
                target_line = target_line + '' + separator

        target_line = target_line[:-len(separator)].replace("\t", '').replace("\r", '').replace("\n", "")
        return target_line


if __name__ == '__main__':
    file_helper = FileTool('/home/admin', 'xxxxx.txt', 'ab')
