def my_strip(value):
    return value.strip()


def my_lower(value):
    return value.lower()


def my_upper(value):
    return value.upper()


def my_encode(value):
    return value.encode("utf-8", 'ignore')


def my_decode(value):
    return value.decode("utf-8", 'ignore')


def my_replace(value, str_from="'", str_to='"'):
    return value.replace(str_from, str_to)


def strip_and_replace(value, str_from="'", str_to='"'):
    return my_replace(my_strip(value), str_from, str_to)


def strip_and_replace_all(value):
    '''去除空格+制表符+换行'''
    return my_replace(my_replace(my_replace(my_replace(my_replace(my_strip(value), "\n", ""),"\r","")," ",""),"\\",""),"'",'"')
