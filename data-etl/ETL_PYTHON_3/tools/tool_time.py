# coding=utf-8
import datetime
import time
import random


def str_to_time_stamp(time_str):
    time_array = time.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    time_stamp = int(time.mktime(time_array))
    return time_stamp


def datetime_to_str(dt):
    if str(type(dt)).find("datetime") > -1:
        d3 = dt.strftime('%Y%m%d')
        return d3
def datetime_to_str_upgrade(dt,strft):
    if str(type(dt)).find("datetime") > -1:
        d3 = dt.strftime(strft)
        return d3
def datetime_yesterday_to_str():
    today=datetime.date.today() 
    oneday=datetime.timedelta(days=1) 
    yesterday=today-oneday
    return datetime_to_str(yesterday)
def datetime_yesterday_to_str_upgrade():
    today=datetime.date.today() 
    oneday=datetime.timedelta(days=1) 
    yesterday=today-oneday
    return datetime_to_str_upgrade(yesterday,'%Y-%m-%d')

def datetime_one_month_ago():
    today=datetime.date.today() 
    oneday=datetime.timedelta(days=31*5) 
    yesterday=today-oneday
    return datetime_to_str(yesterday)
def current_time_to_stamp():
    stamp_time = int(time.time())
    return stamp_time


def current_datetime_to_str():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    

def current_stamp_to_time(time_stamp):
    dateArray = datetime.datetime.fromtimestamp(time_stamp)
    otherStyleTime = dateArray.strftime("%Y-%m-%d %H:%M:%S")
    return otherStyleTime


def current_stamp_to_datetime(time_stamp):
    dateArray = datetime.datetime.fromtimestamp(time_stamp)
    return dateArray


def uuid_generator():
    random_list = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-'
    time_stamp = current_time_to_stamp()
    random_suffix = ''
    for i in range(1, 9):
        random_suffix = random_suffix + random.choice(random_list)
    uuid = str(time_stamp) + random_suffix
    # print(uuid)
    return uuid


def uuid_generator_wom_account():
    random_list = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_'
    time_stamp = current_time_to_stamp()
    random_suffix = ''
    for i in range(1, 9):
        random_suffix = random_suffix + random.choice(random_list)
    uuid = str(time_stamp) + random_suffix
    # print(uuid)
    return uuid

if __name__ == '__main__':
    uuid_generator()
