# coding=utf-8
import json
def list_to_dict(source_list):
    source_dict = {}
    '''
    拼接结果集字典
    '''
    for source_item in source_list:
        source_dict[source_item] = 1
    return source_dict
def str_to_dict(source_str):
	"""
	字符串转字典结构
	"""
	return json.loads(source_str)

def dict2list(dic:dict):
    ''' 将字典转化为列表 '''
    keys = dic.keys()
    vals = dic.values()
    lst = [(key, val) for key, val in zip(keys, vals)]
    return lst
