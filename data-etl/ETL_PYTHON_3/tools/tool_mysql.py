# coding=utf-8
import mysql.connector
import os
import sys
from .tool_log import Logger
logger_mysql_conn = Logger(logname='./logs/mysql_connectin.txt',
                           loglevel=1, logger="mysql_connectin").getlog()


class MysqlTool(object):

    def __init__(self, host, database, user, password):
        '''
        构造器中初始化数据库的连接
        :return:
        '''
        # 连接Mysql数据库,并把数据存储
        self.user = user
        self.password = password
        self.host = host
        self.database = database
        try:
            self.cnx = mysql.connector.connect(user=self.user,
                                               password=self.password,
                                               host=self.host,
                                               database=self.database,
                                               charset='utf8')
            self.cursor = self.cnx.cursor(buffered=True)

        except Exception as e:
            logger_mysql_conn.error(
                "mysql connector error" + self.host + ":" + self.database)
            raise e

    def disconnection(self):
        self.cnx.close()

    def get_unique_id_list_by_sql(self, query):
        unique_id_list = []
        count = 0
        self.cursor.execute(query)
        for (unique_id,) in self.cursor:
            print("unique_id selected " + str(unique_id))
            unique_id_list.append(unique_id)
            count += 1
        print("totally %d items was found" % count)
        return unique_id_list

    def get_unique_id_list(self, source_table_name, source_unique_column_name, source_condition_list, *args, **kvargs):
        '''
        找到唯一id列表，其中source_condition_list格式为[["a","=","3"],[]]的结构
        注：来自外部输入的字符串判断不需要 u''
        '''
        query = "SELECT `%s` FROM `%s`" % (
            source_unique_column_name, source_table_name)
        query_where = ""
        condition_count = 0
        for source_condition in source_condition_list:
            condition_count = condition_count + 1
            if condition_count == 1:
                query_where = " WHERE "
                if type(source_condition[2]) == type(""):
                    if source_condition[1] in ["is", "IS"]:
                        query_where = (
                            query_where + " `%s` %s %s ") % tuple(source_condition)
                    else:
                        query_where = (
                            query_where + " `%s` %s '%s' ") % tuple(source_condition)
                if type(source_condition[2]) == type(1):
                    query_where = (
                        query_where + " `%s` %s %d ") % tuple(source_condition)
                if type(source_condition[2]) == type(0.0):
                    query_where = (
                        query_where + "`%s` %s %.2f") % tuple(source_condition)

            else:
                if type(source_condition[2]) == type(""):
                    if source_condition[1] in ["is", "IS"]:
                        query_where = (
                            query_where + " AND `%s` %s %s ") % tuple(source_condition)
                    else:
                        query_where = (
                            query_where + " AND `%s` %s '%s' ") % tuple(source_condition)
                if type(source_condition[2]) == type(1):
                    query_where = (
                        query_where + " AND `%s` %s %d ") % tuple(source_condition)
                if type(source_condition[2]) == type(0.0):
                    query_where = (
                        query_where + " AND `%s` %.2f %d ") % tuple(source_condition)
        query = query + query_where
        print(query)
        # input('..................')
        self.cursor.execute(query)
        public_id_list = []
        count = 0
        for (public_id,) in self.cursor:
            print("public_id selected " + str(public_id))
            public_id_list.append(public_id)
            count += 1
        print("totally %d items was found" % count)
        return public_id_list

    def find_one_by_sql(self, query, target_file_tool):
        print(query)
        try:
            self.cursor.execute(query)
            row = self.cursor.fetchone()
            return row
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def update_by_sql(self, query, target_file_tool):
        print(query)
        # input("...........")
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def insert_by_sql(self, query, target_file_tool):
        print(query)
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def delete_by_sql(self, query, target_file_tool):
        print(query)
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def find_one(self, source_table_name, target_column_list, source_unique_column_name, source_unique_column_value, target_file_tool, *args, **kvargs):
        '''target_column_list：查询列表，source_unique_column_name，source_unique_column_value:查询条件键值对，返回一行，字典格式'''
        query = "SELECT "
        query_body = ""
        query_where = ""
        for result_column in target_column_list:
            query_body = (query_body + '`%s`,') % result_column
        query_body = query_body[:-1] + (" FROM `%s` " % source_table_name)
        if type(source_unique_column_value) == type(1):
            query_where = " WHERE `%s` = %d" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(""):
            query_where = " WHERE `%s`= '%s'" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(0.0):
            query_where = " WHERE `%s`= %.2f" % (
                source_unique_column_name, source_unique_column_value)
        query = query + query_body + query_where
        print(query)
        #input('help me..............')
        try:
            self.cursor.execute(query)
            row = self.cursor.fetchone()
            return row
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def update_one(self, target_table_name, source_column_dict, source_unique_column_name, source_unique_column_value, target_file_tool, *args, **kvargs):
        '''source_column_dict:字典格式的键值对'''
        query = "UPDATE `%s` " % target_table_name
        query_set = " SET "
        source_column_value_list = []
        for k, v in source_column_dict.items():
            if type(v) == type(1):
                query_set = query_set + "`" + k + "`=%d,"
                source_column_value_list.append(v)
            elif type(v) == type(0.0):
                query_set = query_set + "`" + k + "`=%.2f,"
                source_column_value_list.append(v)
            elif type(v) == type(""):
                query_set = query_set + "`" + k + "`='%s',"
                source_column_value_list.append(v.replace("'", '"'))
        query_set = query_set[:-1]
        query_where = ""
        if type(source_unique_column_value) == type(1):
            query_where = " WHERE `%s`=%d" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(u""):
            query_where = " WHERE `%s`='%s'" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(0.0):
            query_where = " WHERE `%s`=%.2f" % (
                source_unique_column_name, source_unique_column_value)
        query = (query + query_set +
                 query_where) % tuple(source_column_value_list)
        print(query)
        #input('help me..............')
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def insert_one(self, target_table_name, source_column_dict, target_file_tool, *args, **kvargs):
        query = "INSERT INTO `%s`" % target_table_name
        query_column = "("
        query_values = " VALUES("
        value_list = []
        for k, v in source_column_dict.items():
            query_column = query_column + "`" + k + "`,"
            #print("key:"+k + "value:"+str(type(v)))
            if type(v) == type(1):
                query_values = query_values + "%d,"
                value_list.append(v)
            elif type(v) == type(""):
                #print("key:"+k + "value:"+v)
                query_values = query_values + "'%s',"
                value_list.append(v.replace("'", '"'))
            elif type(v) == type(0.0):
                #print("key:"+k + "value:"+v)
                query_values = query_values + "%.2f,"
                value_list.append(v)

        query_column = query_column[:-1] + ")"
        query_values = query_values[:-1] + ")"
        query = (query + query_column + query_values) % tuple(value_list)
        print(query)
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)

    def delete_one(self, target_table_name, source_unique_column_name, source_unique_column_value, target_file_tool, *args, **kvargs):
        query = "DELETE FROM `%s`" % target_table_name
        query_where = ""
        if type(source_unique_column_value) == type(1):
            query_where = " WHERE `%s` = %d" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(""):
            query_where = " WHERE `%s` = '%s'" % (
                source_unique_column_name, source_unique_column_value)
        elif type(source_unique_column_value) == type(0.0):
            query_where = " WHERE `%s` = %.2f" % (
                source_unique_column_name, source_unique_column_value)
        query = query + query_where
        print(query)
        try:
            self.cursor.execute(query)
            self.cnx.commit()
        except Exception as e:
            target_file_tool.write_database_line_to_file(query)
if __name__ == '__main__':
    mysql_helper = MysqlTool(
        'wom', 'wom51hdQm2015mqou', 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com', 'wom_prod')

    '''
    unique_id_list = mysql_helper.get_unique_id_list(
        "video_media_huajiao", "media_id", [["grade", ">", 3], ["follower_num", "is", "not null"]])
    for unique_id in unique_id_list[:1]:
        mysql_helper.find_one("video_media_huajiao", [
                              "media_id", "verify_status", "follower_num"], "media_id", unique_id)
    '''
