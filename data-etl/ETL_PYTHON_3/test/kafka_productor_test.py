from pykafka import KafkaClient
import time
client = KafkaClient(
    hosts="server-70:9091, server-70:9092, server-70:9093")  # 可接受多个Client这是重点
topic = client.topics[b'demo-topic']



def kafka_producer():
    with topic.get_sync_producer() as producer:
        while True:
            for i in range(1, 100):
                for j in range(1, 100):
                    kafka_str=('test message ' + str(i) + ":" + str(j))
                    print(kafka_str)
                    producer.produce(kafka_str.encode("utf-8",'ignore'))
if __name__ == '__main__':
    kafka_producer()
