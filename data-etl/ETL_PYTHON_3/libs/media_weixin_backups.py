# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.settings import BACKUP_CSV_PATH_BIG_DATA, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_1_TUPLE, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, CHECK_CSV_PATH, BACKUP_CSV_PATH, MONGO_SHARD_IP_LIST, MONGO_SHARD_IP_SUFFIX_LIST
import os
from tools.tool_time import datetime_to_str, current_time_to_stamp
from tools.tool_dict import list_to_dict
import datetime
import multiprocessing


class MediaWeixinBackups(object):

    def __init__(self):
        pass

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def media_weixin_backups(self, backup_path, ip, port, suffix, collection_name, source_file_args, query_list):
        '''备份数据：先删除四天前备份的数据，再进行备份
            根据给定账号备份数据
        '''
        # os.system("find %s/* -ctime +4 |xargs rm -rf" % backup_path)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        count = 0
        for line in source_file:
            count += 1
            if count == 2001:
                break
            mongo_export_command_pre = 'mongoexport -h%s:%d -uwom-dts-datawarehouse-admin -pwom-dts-datawarehouse-admin -d wom-dts-datawarehouse' % (
                ip, port)
            time_stamp = datetime_to_str(datetime.datetime.now())
            query_dict = {}
            for query in query_list:
                query_dict[
                    query] = source_file_tool.line_strip_decode_ignore(line)

            mongo_export_command = mongo_export_command_pre + (' -c %s -q \'%s\' ' % (collection_name, str(query_dict).replace("'", '"'))) + '-o {0}/{2}_{1}_{4}_{3}.dat'.format(
                backup_path, time_stamp, collection_name, str(suffix), source_file_tool.line_strip_decode_ignore(line))
            print("mongo_export_command:" + mongo_export_command)
            # input("............................")
            os.system(mongo_export_command)

    def media_weixin_recovery(self, ip, port, user, pwd, db, collection_name, source_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()

        for line in source_file:
            mongo_import_command = "mongoimport -h%s:%d -u%s -p%s -d %s -c %s %s" % (
                ip, port, user, pwd, db, collection_name, source_file_tool.line_strip_decode_ignore(line))
            print(mongo_import_command)
            # input("........................")
            os.system(mongo_import_command)

    # def media_weixin_article_csv(self, backup_path, ip, port, suffix, collection_name):
    #     # os.system("touch %s/xyf.txt" % backup_path)

    #     mongo_export_command_pre = 'mongoexport -h%s:%d -uwom-dts-datawarehouse-admin -pwom-dts-datawarehouse-admin -d wom-dts-datawarehouse' % (
    #         ip, port)
    #     time_stamp = datetime_to_str(datetime.datetime.now())
    #     file_name = collection_name + "_" + \
    #         time_stamp + "_" + str(suffix) + ".csv"
    #     mongo_export_command = mongo_export_command_pre + \
    #         ' -c {0} --type=csv -f weixin_id,post_date,article_type,article_pos,page_view_num,page_like_num,comment_num -o {1}/{2}'.format(
    #             collection_name, backup_path, file_name)
    #     os.system(mongo_export_command)

    def media_weixin_article_csv_multi_process_control(self, backup_path, ip_list, port, suffix_list, collection_name):
        try:
            os.system("/usr/bin/rm -rf %s/*" % backup_path)
        except Exception as e:
            pass
        process_list = []
        ip_pos = -1
        for ip in ip_list:
            ip_pos += 1
            p = multiprocessing.Process(target=self.media_weixin_article_csv, args=(
                backup_path, ip, port, suffix_list[ip_pos], collection_name))
            p.daemon = True
            process_list.append(p)
        for process in process_list:
            process.start()
        for process in process_list:
            process.join()
        end_time = current_time_to_stamp()
        return end_time

    def media_weixin_backups_multi_process_control(self, backup_path, ip_list, port, suffix_list, collection_name_list):
        try:
            os.system("find %s/* -ctime +4 |xargs rm -rf" % backup_path)
        except Exception as e:
            pass

        process_list = []
        ip_pos = -1
        for ip in ip_list:
            ip_pos += 1
            p = multiprocessing.Process(target=self.media_weixin_backups, args=(
                backup_path, ip, port, suffix_list[ip_pos], collection_name_list))
            p.daemon = True
            process_list.append(p)
        for process in process_list:
            process.start()
        for process in process_list:
            process.join()
        end_time = current_time_to_stamp()
        return end_time

    def media_weixin_backups(self, backup_path, ip, port, suffix, collection_name_list):
        '''备份数据：先删除四天前备份的数据，再进行备份'''

        for collection_name in collection_name_list:
            mongo_export_command_pre = 'mongoexport -h%s:%d -uwom-dts-datawarehouse-admin -pwom-dts-datawarehouse-admin -d wom-dts-datawarehouse' % (
                ip, port)
            time_stamp = datetime_to_str(datetime.datetime.now())
            backup_file = collection_name + "_" + \
                time_stamp + "_" + str(suffix) + ".dat"
            # time_stamp + "_" + str(thread_number) + ".dat"
            mongo_export_command = mongo_export_command_pre + ' -c {2} -o {0}/{2}_{1}_{3}.dat'.format(
                backup_path, time_stamp, collection_name, str(suffix))
            print("mongo_export_command:" + mongo_export_command)
            # start_time = datetime.datetime.now()
            # start to backup......
            os.system(mongo_export_command)
            # lines = self.file_helper.countline(
            #    backup_path + '/' + backup_file)
            # end_time = current_time_to_stamp()
            # size = os.path.getsize(
            #    backup_path + '/' + collection_name + '_' + time_stamp + '_' + str(suffix) + '.dat') / (1024 * 1024)
            # self.backup_document_size(
            #    "data_viewer", collection_name, backup_file, start_time, end_time, lines, size)

    def backups_csv(self, process_tag, source_mongo_args, unique_id_list, target_collection_args, target_columns_and_files):
        time_stamp=current_time_to_stamp()
        source_mongo_tool = MongoTool(*source_mongo_args)
        target_file_tool_1 = None
        target_file_tool_2 = None
        target_file_tool_3 = None
        i = 0
        for i in range(target_columns_and_files[0]):
            target_file_args = (target_columns_and_files[i + 1][1][0], target_columns_and_files[
                                i + 1][1][1] + ('_%d.csv' % process_tag), target_columns_and_files[i + 1][1][2])
            if i == 0:
                target_file_tool_1 = FileTool(
                    *target_file_args)
            elif i == 1:
                target_file_tool_2 = FileTool(
                    *target_file_args)
            elif i == 2:
                target_file_tool_3 = FileTool(
                    *target_file_args)
            i += 1

        target_columns_dict = {}
        for target_column in target_collection_args[2]:
            target_columns_dict[target_column] = 1
        account_count = 0
        target_file_1_line_joint=''
        target_file_2_line_joint=''
        target_file_3_line_joint=''
        target_file_1_line_count=0
        target_file_2_line_count=0
        target_file_3_line_count=0
        try:
            for unique_id in unique_id_list:
                account_count += 1
                documents = source_mongo_tool.database[target_collection_args[0]].find(
                    {target_collection_args[1]: unique_id,"post_date":{"$gte":(time_stamp-30*24*3600)}}, target_columns_dict)
                documents_count=documents.count()
                article_count = 0
                # article_id_list = []
                for document in documents:
                    # print(str(document['_id']))
                    # if str(document['_id']) not in article_id_list:
                    #    article_id_list.append(article_id_list)
                    article_count += 1
                    if target_file_tool_1 is not None:
                        target_file_1_line_count+=1
                        target_file_1_line_joint+=(target_file_tool_1.write_mongo_csv_to_file(
                            target_columns_and_files[1][0], target_columns_and_files[1][2], document)) + '\n'
                        if (target_file_1_line_count % 1000) ==0:
                            target_file_tool_1.write_database_line_to_file(target_file_1_line_joint)
                            target_file_1_line_joint=''
                        elif account_count==len(unique_id_list)  and article_count== documents.count():
                            target_file_tool_1.write_database_line_to_file(target_file_1_line_joint)
                            

                    if target_file_tool_2 is not None:
                        target_file_2_line_count+=1
                        target_file_2_line_joint+=target_file_tool_2.write_mongo_csv_to_file(
                            target_columns_and_files[2][0], target_columns_and_files[2][2], document)+'\n'
                        if (target_file_2_line_count % 1000) ==0:
                            target_file_tool_2.write_database_line_to_file(target_file_2_line_joint)
                            target_file_2_line_joint=''
                        elif account_count==len(unique_id_list)  and article_count== documents.count():
                            target_file_tool_2.write_database_line_to_file(target_file_2_line_joint)

                    if target_file_tool_3 is not None:
                        target_file_3_line_count+=1
                        target_file_3_line_joint+=target_file_tool_3.write_mongo_csv_to_file(
                            target_columns_and_files[3][0], target_columns_and_files[3][2], document)+'\n'
                        if (target_file_3_line_count % 1000) ==0:
                            target_file_tool_3.write_database_line_to_file(target_file_3_line_joint)
                            target_file_3_line_joint=''
                        elif account_count==len(unique_id_list)  and article_count== documents.count():
                            target_file_tool_3.write_database_line_to_file(target_file_3_line_joint)

                    # print('atricle count %d' % article_count)
                print("process tag %d account id %s settled %d articles: total tasks:%d;settled tasks:%d;settled rate:%.2f" %
                      (process_tag, unique_id, article_count, len(unique_id_list), account_count, (account_count / len(unique_id_list))))
                # print("articles count is real :%s" %
                #      str(len(article_id_list) == article_count))

        finally:
            source_mongo_tool.disconnection()
            if target_file_tool_1 is not None:
                target_file_tool_1.close_file()
            if target_file_tool_2 is not None:
                target_file_tool_2.close_file()
            if target_file_tool_3 is not None:
                target_file_tool_3.close_file()

    def backups_csv_multi_process_control(self, process_num, source_mongo_args, source_collection_args, target_collection_args, target_columns_and_files):
        try:
            os.system("/usr/bin/rm -rf %s/*" % BACKUP_CSV_PATH_BIG_DATA)
        except Exception as e:
            pass
        source_mongo_tool = MongoTool(*source_mongo_args)
        unique_id_list = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        # unique_id_list=unique_id_list[:100]
        process_list = []
        split_num = int(len(unique_id_list) / process_num)
        try:
            for i in range(process_num):
                if i != (process_num - 1):
                    p = multiprocessing.Process(target=self.backups_csv, args=(
                        (i + 1), source_mongo_args, unique_id_list[(i * split_num):((i + 1) * split_num)], target_collection_args, target_columns_and_files))
                    p.daemon = True
                    process_list.append(p)
                else:
                    p = multiprocessing.Process(target=self.backups_csv, args=(
                        (i + 1), source_mongo_args, unique_id_list[(i * split_num):], target_collection_args, target_columns_and_files))
                    p.daemon = True
                    process_list.append(p)

            for process in process_list:
                process.start()
            for process in process_list:
                process.join()

        finally:
            source_mongo_tool.disconnection()
        end_time = current_time_to_stamp()
        return end_time


    # def media_weixin_article_csv(self,source_mongo_args,source_collection_args,target_file_list):
    #     source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
    #     target_list=target_list_1+target_list_2
    #     target_dict=list_to_dict(target_list)
    #     documents_cursor=source_mongo_tool.database[source_collection_name].find(where_condition,target_dict)
    #     for document in documents_cursor:
    #         for x in xrange(1,10):
    #             pass

    def media_weixin_article_csv(self, source_mongo_args, target_collection_args, target_columns_and_files):
        # try:
        #     os.system("/usr/bin/rm -rf %s/*" % BACKUP_CSV_PATH_BIG_DATA)
        # except Exception as e:
        #     pass
        update_day=datetime_to_str(datetime.datetime.now())
        time_stamp=current_time_to_stamp()
        source_mongo_tool = MongoTool(*source_mongo_args)
        target_file_tool_1 = None
        target_file_tool_2 = None
        target_file_tool_3 = None
        print("....................1")
        for i in range(target_columns_and_files[0]):
            target_file_args = (target_columns_and_files[i + 1][1][0], target_columns_and_files[
                                i + 1][1][1] + '_%s_1.csv' % update_day , target_columns_and_files[i + 1][1][2])
            if i == 0:
                target_file_tool_1 = FileTool(
                    *target_file_args)
            elif i == 1:
                target_file_tool_2 = FileTool(
                    *target_file_args)
            elif i == 2:
                target_file_tool_3 = FileTool(
                    *target_file_args)
        print("....................2")
        target_columns_dict = {}
        for target_column in target_collection_args[2]:
            target_columns_dict[target_column] = 1
        target_file_1_line_joint=''
        target_file_2_line_joint=''
        target_file_3_line_joint=''
        target_file_1_line_count=0
        target_file_2_line_count=0
        target_file_3_line_count=0
        article_count = 0
        print("....................3")
        try:
            print(target_collection_args[0])
            documents_cursor=source_mongo_tool.database[target_collection_args[0]].find(target_collection_args[1],target_columns_dict) #{'last_update_day':{"$exists":True}}
            documents_count=documents_cursor.count()
            print(str(documents_count)+'.............')
            documents_cursor.batch_size(500)

            # article_id_list = []
            file_flag=1

            while source_mongo_tool.cursor_has_next(documents_cursor):
                
                document=documents_cursor.next()
                # print(str(document['_id']))
                # if str(document['_id']) not in article_id_list:
                #    article_id_list.append(article_id_list)
                article_count += 1
                # if article_count<= 7860000:
                #     print('skip ......%d' % article_count)
                #     continue
                
                if target_file_tool_1 is not None:
                    target_file_1_line_count+=1
                    target_file_1_line_joint+=(target_file_tool_1.write_mongo_csv_to_file(
                        target_columns_and_files[1][0], target_columns_and_files[1][2], document)) + '\n'
                    if (target_file_1_line_count % 500) ==0:
                        target_file_tool_1.write_database_line_to_file(target_file_1_line_joint)
                        target_file_1_line_joint='' 
                    elif article_count== documents_count:
                    # elif article_count== 258:
                        target_file_tool_1.write_database_line_to_file(target_file_1_line_joint)
                        
                if target_file_tool_2 is not None:
                    target_file_2_line_count+=1
                    target_file_2_line_joint+=target_file_tool_2.write_mongo_csv_to_file(
                        target_columns_and_files[2][0], target_columns_and_files[2][2], document)+'\n'
                    if (target_file_2_line_count % 500) ==0:
                        target_file_tool_2.write_database_line_to_file(target_file_2_line_joint)
                        target_file_2_line_joint=''
                    elif article_count== documents_count:
                    # elif article_count==258:
                        target_file_tool_2.write_database_line_to_file(target_file_2_line_joint)

                if target_file_tool_3 is not None:
                    target_file_3_line_count+=1
                    target_file_3_line_joint+=target_file_tool_3.write_mongo_csv_to_file(
                        target_columns_and_files[3][0], target_columns_and_files[3][2], document)+'\n'
                    if (target_file_3_line_count % 500) ==0:
                        target_file_tool_3.write_database_line_to_file(target_file_3_line_joint)
                        target_file_3_line_joint=''
                    elif  article_count== documents_count:
                    # elif article_count==258:
                        target_file_tool_3.write_database_line_to_file(target_file_3_line_joint)
                print('atricle count %d settled rate %.2f' % (article_count,article_count/documents_count))
                # if article_count==100000:
                #     break
                if article_count % 500000 == 0:
                    file_flag+=1
                    if target_file_tool_1 is not None:
                        target_file_tool_1.close_file()
                    if target_file_tool_2 is not None:
                        target_file_tool_2.close_file()
                    if target_file_tool_3 is not None:
                        target_file_tool_3.close_file()
                    for i in range(target_columns_and_files[0]):
                        target_file_args = (target_columns_and_files[i + 1][1][0], target_columns_and_files[
                                            i + 1][1][1] + '_%s_%d.csv' % (update_day,file_flag) , target_columns_and_files[i + 1][1][2])
                        if i == 0:
                            target_file_tool_1 = FileTool(
                                *target_file_args)
                        elif i == 1:
                            target_file_tool_2 = FileTool(
                                *target_file_args)
                        elif i == 2:
                            target_file_tool_3 = FileTool(
                                *target_file_args)
                
                
                # print("process tag %d  settled %d articles: total tasks:%d;settled tasks:%d;settled rate:%.2f" %
                #       (process_tag, unique_id, article_count, len(unique_id_list), (account_count / len(unique_id_list))))
                # print("articles count is real :%s" %
                #      str(len(article_id_list) == article_count))

        finally:
            source_mongo_tool.disconnection()
            if target_file_tool_1 is not None:
                target_file_tool_1.close_file()
            if target_file_tool_2 is not None:
                target_file_tool_2.close_file()
            if target_file_tool_3 is not None:
                target_file_tool_3.close_file()
        return article_count


            

