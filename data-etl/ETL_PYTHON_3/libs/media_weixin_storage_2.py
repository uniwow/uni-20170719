# coding=utf-8
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.tool_mongo import MongoTool
from tools.tool_time import uuid_generator, current_time_to_stamp, current_stamp_to_time, current_stamp_to_datetime
from bson import ObjectId
from tools.settings import WEIBO_VERIFY_STATUS, CATEGORY_SPLIT, CATEGORY, CITY, WOM_PROD_1_TUPLE, ERROR_FILE_ARGS, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO, BACKUP_JSON_PATH, CHECK_CSV_PATH, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_225_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_32_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_249_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_70_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_102_MONGO_TUPLE, CHECK_SQL_PATH
import math
import multiprocessing
from tools.tool_math import avg_generator, sd_generator
from pymongo.errors import BulkWriteError


class MediaWeiXinStorage(object):

    def __init__(self):
        pass

    def get_mysql_tool(self, host, db, user, pwd):
        mysql_tool = MysqlTool(host, db, user, pwd)
        return mysql_tool

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def file_mysql(self, source_file_args, target_mysql_args, target_table_args, target_column_in_file_list, target_column_user_added_dict):
        '''
        更新或者插入数据，既有文件中的字段数据，也有自己添加的字段数据
        target_column_in_file_list=[['column_1','字段类型'],...]
        字段类型：i:int,d:double,s:string.后续增加
        '''
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        unique_id_list_mysql = target_mysql_tool.get_unique_id_list(
            *target_table_args)
        for line in source_file:
            if source_file_tool.line_strip_decode_ignore(line) is not u'':

                if source_file_tool.line_strip_decode_ignore(line) in unique_id_list_mysql:

                    '''数据存在，更新数据'''
                else:
                    '''数据不存在，插入数据'''

    def mysql_mysql(self, source_mysql_args, source_table_args, target_mysql_args, target_table_args, source_column_name_list, target_column_name_list, target_column_user_added_dict, target_file_args):
        '''mysql到mysql，查询出一个列表，列表的第一项同target表为主外键关系'''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        for unique_id_mysql in unique_id_list_mysql:

            row = source_mysql_tool.find_one(source_table_args[
                                             0], source_column_name_list, source_table_args[1], unique_id_mysql.target_file_tool)
            target_column_dict = {}
            count_target_column_name_list = 0
            for target_column_name in target_column_name_list:
                count_target_column_name_list += 1
                target_column_dict[target_column_name] = row[
                    count_target_column_name_list]
            for k, v in target_column_user_added_dict.items():
                target_column_dict[k] = v

            target_mongo_tool.update_one(
                target_table_args[0], target_column_dict, target_table_args[1], row[0], target_file_tool)

    def media_weixin_1_price_1_update_upgrade(self, source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args):
        '''
        把1.0的信息迁移到2.0
        只做INSERT操作
        '''
        count_correct = 0
        count_error = 0
        source_file_tool_1 = self.get_file_tool(*source_file_args_1)
        source_file_tool_2 = self.get_file_tool(*source_file_args_2)
        error_file_tool = self.get_file_tool(*error_file_args)
        '''
        更新操作的2.0数据库连接
        '''
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        '''
        1.0数据库连接
        '''
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        '''
            distinct账号列表
        '''
        source_file_1 = source_file_tool_1.get_file()
        '''
            duplicate 账号列表
        '''
        source_file_2 = source_file_tool_2.get_file()
        source_file_2_line_list = []
        for line in source_file_2:
            source_file_2_line_list.append(
                source_file_tool_2.line_strip_decode_ignore(line))
        wom_account_login_account_list = []
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        count = 0
        for line in source_file_1:
            '''
            对于distinct账号列表中的数据，判断是否出现在duplicate列表中，如果不存在，则做迁移
            '''
            if source_file_tool_1.line_strip_decode_ignore(line) not in source_file_2_line_list:
                count += 1
                # if source_file_tool_1.line_strip_decode_ignore(line) not in ['mrbrand888','qiluwanbao002','heimayingxiao1','hitonghua','miss_shopping_li']:
                #    continue
                # if count >= 50:
                #    continue
                # continue
                '''
                迁移t_media表
                '''
                i_status = None
                i_media_id = None
                i_user_id = None
                s_media_name = None
                i_follower_num = None
                i_price_1 = None
                i_price_2 = None
                i_price_3 = None
                i_price_4 = None
                i_price_5 = None
                i_price_6 = None
                i_price_7 = None
                i_price_8 = None
                i_price_11 = None
                i_price_12 = None
                i_price_13 = None
                i_price_14 = None
                d_price_1_l = None
                d_price_1_h = None
                d_price_2_l = None
                d_price_2_h = None
                d_price_3_l = None
                d_price_3_h = None
                d_price_4_l = None
                d_price_4_h = None
                d_price_5_l = None
                d_price_5_h = None
                d_price_6_l = None
                d_price_6_h = None
                d_price_7_l = None
                d_price_7_h = None
                d_price_8_l = None
                d_price_8_h = None
                d_price_11 = None
                d_price_12 = None
                d_price_13 = None
                d_price_14 = None

                i_read_num = None
                i_create_type = None
                i_publish_type = None
                i_effective_time = None
                i_wom_sect_top = None
                i_beizhu = None
                i_instroduction = None
                query = ("SELECT `iStatus`,`iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14`,`dPrice1L`,`dPrice1H`,`dPrice2L`,`dPrice2H`,`dPrice3L`,`dPrice3H`,`dPrice4L`,`dPrice4H`,`dPrice5L`,`dPrice5H`,`dPrice6L`,`dPrice6H`,`dPrice7L`,`dPrice7H`,`dPrice8L`,`dPrice8H`,`dPrice11`,`dPrice12`,`dPrice13`,`dPrice14`,`iReadNum`,`iCreateType`,`iPublishType`,`iEffectiveTime`,`iWomSectTop`,`iBeizhu`,`sIntroduction` FROM `t_media` WHERE LOWER(`sOpenName`)= '%s' " %
                         source_file_tool_1.line_strip_decode_ignore(line))
                row = None
                print(query)
                try:
                    source_mysql_tool_1.cursor.execute(query)
                    row = source_mysql_tool_1.cursor.fetchone()
                except Exception as e:
                    '''查询错误，记录并执行下一条'''
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '在t_media中查找失败')
                    continue
                if row is not None:
                    i_status, i_media_id, i_user_id, s_media_name, i_follower_num, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14, d_price_1_l, d_price_1_h, d_price_2_l, d_price_2_h, d_price_3_l, d_price_3_h, d_price_4_l, d_price_4_h, d_price_5_l, d_price_5_h, d_price_6_l, d_price_6_h, d_price_7_l, d_price_7_h, d_price_8_l, d_price_8_h, d_price_11, d_price_12, d_price_13, d_price_14, i_read_num, i_create_type, i_publish_type, i_effective_time, i_wom_sect_top, i_beizhu, i_instroduction = row

                    if i_effective_time is None:
                        i_effective_time = -1
                    if i_beizhu is None:
                        i_beizhu = ''
                    if i_instroduction is None:
                        i_instroduction = ''
                else:
                    continue
                orig_price_s_min = None
                orig_price_m_1_min = None
                orig_price_m_2_min = None
                orig_price_m_3_min = None
                retail_price_s_min = None
                retail_price_m_1_min = None
                retail_price_m_2_min = None
                retail_price_m_3_min = None
                execute_price_s = None
                execute_price_m_1 = None
                execute_price_m_2 = None
                execute_price_m_3 = None
                orig_price_s_max = None
                orig_price_m_1_max = None
                orig_price_m_2_max = None
                orig_price_m_3_max = None
                retail_price_s_max = None
                retail_price_m_1_max = None
                retail_price_m_2_max = None
                retail_price_m_3_max = None

                i_s_pub_type_media_weixin = None
                i_m_1_pub_type_media_weixin = None
                i_m_2_pub_type_media_weixin = None
                i_m_3_pub_type_media_weixin = None

                i_has_origin_pub = None
                i_has_direct_pub = None

                if i_create_type == 0:
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                        i_price_1), float(i_price_2), float(i_price_3), float(i_price_4), float(i_price_1), float(i_price_2), float(i_price_3), float(i_price_4)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                        i_price_5), float(i_price_6), float(i_price_7), float(i_price_8), float(i_price_5), float(i_price_6), float(i_price_7), float(i_price_8)
                    execute_price_s = float(i_price_11)
                    execute_price_m_1 = float(i_price_12)
                    execute_price_m_2 = float(i_price_13)
                    execute_price_m_3 = float(i_price_14)

                    if orig_price_s_min == 0:
                        i_s_pub_type_media_weixin = 0
                    else:
                        i_s_pub_type_media_weixin = 1

                    if orig_price_m_1_min == 0:
                        i_m_1_pub_type_media_weixin = 0
                    else:
                        i_m_1_pub_type_media_weixin = 1

                    if orig_price_m_2_min == 0:
                        i_m_2_pub_type_media_weixin = 0
                    else:
                        i_m_2_pub_type_media_weixin = 1
                    if orig_price_m_3_min == 0:
                        i_m_3_pub_type_media_weixin = 0
                    else:
                        i_m_3_pub_type_media_weixin = 1
                    if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                        i_has_direct_pub = 0
                    else:
                        i_has_direct_pub = 1
                    i_has_origin_pub = 0
                elif i_create_type == 1:
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                        d_price_1_l), float(d_price_2_l), float(d_price_3_l), float(d_price_4_l), float(d_price_1_h), float(d_price_2_h), float(d_price_3_h), float(d_price_4_h)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                        d_price_5_l), float(d_price_6_l), float(d_price_7_l), float(d_price_8_l), float(d_price_5_h), float(d_price_6_h), float(d_price_7_h), float(d_price_8_h)
                    execute_price_s = float(d_price_11)
                    execute_price_m_1 = float(d_price_12)
                    execute_price_m_2 = float(d_price_13)
                    execute_price_m_3 = float(d_price_14)

                    if i_publish_type == 0:
                        if orig_price_s_min == 0:
                            i_s_pub_type_media_weixin = 0
                        else:
                            i_s_pub_type_media_weixin = 1

                        if orig_price_m_1_min == 0:
                            i_m_1_pub_type_media_weixin = 0
                        else:
                            i_m_1_pub_type_media_weixin = 1

                        if orig_price_m_2_min == 0:
                            i_m_2_pub_type_media_weixin = 0
                        else:
                            i_m_2_pub_type_media_weixin = 1

                        if orig_price_m_3_min == 0:
                            i_m_3_pub_type_media_weixin = 0
                        else:
                            i_m_3_pub_type_media_weixin = 1

                        if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                            i_has_direct_pub = 0
                        else:
                            i_has_direct_pub = 1
                        i_has_origin_pub = 0
                    elif i_publish_type == 1:
                        if orig_price_s_min == 0:
                            i_s_pub_type_media_weixin = 0
                        else:
                            i_s_pub_type_media_weixin = 2
                        if orig_price_m_1_min == 0:
                            i_m_1_pub_type_media_weixin = 0
                        else:
                            i_m_1_pub_type_media_weixin = 2
                        if orig_price_m_2_min == 0:
                            i_m_2_pub_type_media_weixin = 0
                        else:
                            i_m_2_pub_type_media_weixin = 2
                        if orig_price_m_3_min == 0:
                            i_m_3_pub_type_media_weixin = 0
                        else:
                            i_m_3_pub_type_media_weixin = 2
                        if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                            i_has_origin_pub = 0
                        else:
                            i_has_origin_pub = 1
                        i_has_direct_pub = 0
                #print("%.2f %.2f %.2f %.2f" % (orig_price_s_min,orig_price_m_1_min,orig_price_m_2_min,orig_price_m_3_min))
                #print("%d %d %d %d" % (i_s_pub_type_media_weixin,i_m_1_pub_type_media_weixin,i_m_2_pub_type_media_weixin,i_m_3_pub_type_media_weixin))
                #input('help me......')
                '''t_user'''
                s_email = None
                s_mobile = None
                s_real_name = None
                s_co_name = None
                s_weixin = None
                s_qq = None
                query = (
                    "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iType`=2 AND `iUserID`=%d" % i_user_id)
                row = None
                print(query)
                try:
                    source_mysql_tool_1.cursor.execute(query)
                    row = source_mysql_tool_1.cursor.fetchone()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '在t_user中查找失败')
                    continue
                i_media_vendor_uuid = None
                if row is not None:
                    s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                    if s_mobile is None:
                        s_mobile = ''
                    if s_real_name is None:
                        s_real_name = ''
                    if s_co_name is '':
                        s_co_name = s_real_name.strip()
                    if s_weixin is None:
                        s_weixin = ''
                    if s_qq is None:
                        s_qq = ''
                else:
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                    (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                        query, error_file_tool)
                    s_email = '1160154200@qq.com'
                if s_email is None:
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '供应商的email不存在')
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                    (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                        query, error_file_tool)
                    s_email = '1160154200@qq.com'

                '''
                wom_account/media_vendor表所需信息
                '''
                i_wom_account_uuid = uuid_generator()
                i_wom_account_login_account = s_email
                '''to do'''
                i_wom_account_login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                '''自媒体主'''
                i_wom_account_type = 2
                i_wom_account_status = 1
                i_wom_account_create_time = current_time_to_stamp()
                '''
                    如果媒体主信息不存在，则建立,并把email添加进email list;存在，则查询media_vendor_uuid
                '''
                if i_wom_account_login_account not in wom_account_login_account_list:
                    i_wom_account_uuid = uuid_generator()
                    wom_account_login_account_list.append(
                        i_wom_account_login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        i_wom_account_uuid, i_wom_account_login_account.strip(), i_wom_account_login_password.strip(), i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        error_file_tool.write_database_line_to_file(
                            i_wom_account_login_account + 'wom_account 插入操作失败')
                    i_media_vendor_uuid = uuid_generator()
                    query = (
                        "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name.strip(), s_mobile.strip(), s_weixin.strip(), s_qq, s_co_name.strip()))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        error_file_tool.write_database_line_to_file(
                            i_wom_account_login_account + 'media_vendor 插入操作失败')
                else:
                    query = (
                        "SELECT `uuid` FROM `wom_account` WHERE `type`=2 AND `login_account`='%s'" % s_email)
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (i_wom_account_uuid,) = row
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % i_wom_account_uuid)
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (i_media_vendor_uuid,) = row
                uuid_media_weixin = uuid_generator()
                i_create_time_media_weixin = current_time_to_stamp()
                i_last_update_time_media_weixin = i_create_time_media_weixin
                i_last_put_up_time_media_weixin = i_create_time_media_weixin
                i_last_verify_time_media_weixin = i_create_time_media_weixin
                i_status_media_weixin = None
                if i_status == 0:
                    i_status_media_weixin = 3
                elif i_status == 1:
                    i_status_media_weixin = 1
                elif i_status == 2:
                    i_status_media_weixin = 0
                elif i_status == 3:
                    i_status_media_weixin = 2
                i_put_up_media_weixin = None
                if i_effective_time > current_time_to_stamp():
                    i_put_up_media_weixin = 1
                else:
                    i_put_up_media_weixin = 0

                i_has_pref_vendor = 1
                i_is_activated = 1
                i_vendor_cnt = 1
                i_to_verify_vendor_cnt = 0
                query = (
                    "INSERT INTO `media_weixin`(`uuid`,`public_name`,`public_id`,`follower_num`,`put_up`,`status`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`has_pref_vendor`,`pref_vendor_uuid`,`cust_sort`,`create_time`,`is_activated`,`has_origin_pub`,`has_direct_pub`,`vendor_cnt`,`active_end_time`,`to_verify_vendor_cnt`,`last_update_time`,`last_put_up_time`,`comment`,`t_media_intro`,`last_verify_time`) VALUES('%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,'%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s','%s',%d)" % (uuid_media_weixin, str(s_media_name).strip(), source_file_tool_1.line_strip_decode_ignore(line), i_follower_num, i_put_up_media_weixin, i_status_media_weixin, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_has_pref_vendor, i_media_vendor_uuid, i_wom_sect_top, i_create_time_media_weixin, i_is_activated, i_has_origin_pub, i_has_direct_pub, i_vendor_cnt, i_effective_time, i_to_verify_vendor_cnt, i_last_update_time_media_weixin, i_last_put_up_time_media_weixin, i_beizhu.replace("'", '"'), i_instroduction.replace("'", '"'), i_last_verify_time_media_weixin))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        uuid_media_weixin + 'media_weixin 插入操作失败')

                uuid_media_vendor_bind_other = uuid_generator()
                i_media_type = 1
                i_is_activated = 1
                i_status = 1
                i_is_pref_vendor = 1
                query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,%d,%d,%d,%d)" % (
                    uuid_media_vendor_bind_other, i_media_type, uuid_media_weixin, i_media_vendor_uuid, i_is_activated, i_status, i_is_pref_vendor, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time_media_weixin))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    print('media_vendor_bind %s' %
                          uuid_media_vendor_bind_other)
                uuid_media_vendor_weixin_price_list = uuid_generator()
                i_deposit_percent_config = '{"pos_s":0.5,"pos_m_1":0.5,"pos_m_2":0.5,"pos_m_3":0.5}'
                i_serve_percent_config = '{"pos_s":0.4,"pos_m_1":0.4,"pos_m_2":0.4,"pos_m_3":0.4}'
                query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`active_end_time`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d)" % (
                    uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_other, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"'), orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_effective_time))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    print('media_vendor_weixin_price_list %s ' %
                          uuid_media_vendor_weixin_price_list)
        print(("%d " % count))

    def weibo_transfer(self, source_file_args, source_mysql_args, target_mysql_args, error_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        error_file_tool = self.get_file_tool(*error_file_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        wom_account_login_account_list = []
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        try:
            for line in source_file:
                '''
                media_weibo字段列表
                '''
                media_weibo_uuid = None  # 1
                weibo_name = None  # 2
                follower_num = None  # 3
                follower_screenshot = None  # 4
                weibo_url = None  # 5
                avatar = None  # 6
                qrcode = None  # 7
                read_num = None  # 8
                media_level = None  # 9
                intro = None  # 10
                media_weibo_status = None  # 11
                is_put = None  # 12
                is_top = None  # 13
                active_end_time = None  # 14
                fail_reason = None  # 15
                put_down_reason = None  # 16
                audit_remark = None  # 17
                create_time = None  # 18
                update_time = None  # 19
                audit_time = None  # 20
                enter_time = None  # 21

                '''
	            wom_account 字段列表
	            '''
                wom_account_uuid = None
                login_account = None
                login_password = None
                wom_account_type = None
                wom_account_status = None
                '''
	            media_vendor 字段列表
	            '''
                media_vendor_uuid = None
                contact_person = None
                contact1 = None
                weixin = None
                qq = None
                name = None

                '''
	            weibo_vendor_bind 字段列表
	            '''
                weibo_vendor_bind_uuid = None
                soft_direct_price_orig = None
                soft_transfer_price_orig = None
                soft_direct_price_retail = None
                soft_transfer_price_retail = None
                soft_direct_price_execute = None
                soft_transfer_price_execute = None
                micro_direct_price_execute = None
                micro_transfer_price_execute = None
                micro_direct_price_retail = None
                micro_transfer_price_retail = None
                micro_direct_price_orig = None
                micro_transfer_price_orig = None

                '''
	            t_media 字段列表
	            '''
                i_media_id = None
                i_user_id = None
                s_media_name = None  # weibo_name
                i_follower_num = None  # follower_num
                s_follower_img = None  # follower_screenshot

                s_url = None  # weibo_url
                s_avater = None  # avatar
                s_qrcode = None  # qrcode
                i_read_num = None  # read_num
                s_bottom_put_explain = None  # put_down_reason

                i_beizhu = None  # audit_remark
                i_verify_status = None  # media_level
                s_introduction = None  # s_introduction
                i_status = None  # media_weibo_status
                i_put = None  # is_put

                i_explain = None  # fail_reason
                i_wom_sect_top = None  # is_top
                i_effective_time = None  # active_end_time

                i_price_1 = None
                i_price_2 = None
                i_price_3 = None
                i_price_4 = None
                i_price_5 = None
                i_price_6 = None
                i_price_7 = None
                i_price_8 = None
                i_price_11 = None
                i_price_12 = None
                i_price_13 = None
                i_price_14 = None

                query = (
                    "SELECT `iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`sFollowerImg`,`sUrl`,`sAvatar`,`sQRCode`,`iReadNum`,`sBottomPutExplain`,`iBeizhu`,`iVerifyState`,`sIntroduction`,`iStatus`,`iPut`,`iExplain`,`iWomSectTop`,`iEffectiveTime`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14` FROM `t_media` WHERE `sUrl`='%s'" % source_file_tool.line_strip_decode_ignore(line))
                source_mysql_tool.cursor.execute(query)
                row = source_mongo_tool.cursor.fetchone()
                if row is not None:
                    i_media_id, i_user_id, s_media_name, i_follower_num, s_follower_img, s_url, s_avater, s_qrcode, i_read_num, s_bottom_put_explain, i_beizhu, i_verify_status, s_introduction, i_status, i_put, i_explain, i_wom_sect_top, i_effective_time, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14 = row
                else:
                    continue

                if s_media_name is None:
                    s_media_name = ''

                if i_follower_num is None:
                    i_follower_num = 0

                if s_follower_img is None:
                    s_follower_img = ''

                if s_url is None:
                    s_url = ''

                if s_avater is None:
                    s_avater = ''

                if s_qrcode is None:
                    s_qrcode = ''

                if i_read_num is None:
                    i_read_num = 0

                if s_bottom_put_explain is None:
                    s_bottom_put_explain = ''

                if i_beizhu is None:
                    i_beizhu = ''

                if i_verify_status is None:
                    i_verify_status = 0

                if s_introduction is None:
                    s_introduction = ''

                if i_status is None:
                    i_status = 0

                if i_put is None:
                    i_put = 0

                if i_explain is None:
                    i_explain = ''

                if i_wom_sect_top is None:
                    i_wom_sect_top = 0

                if i_effective_time is None:
                    i_effective_time = -1

                '''
	            填充 media_weibo字段
	            '''
                media_weibo_uuid = uuid_generator()
                weibo_name = s_media_name.strip().replace("'", '"')
                follower_num = i_follower_num
                follower_screenshot = s_follower_img.strip()
                weibo_url = s_url.strip().replace("'", '"')
                avatar = s_avater.strip().replace("'", '"')
                qrcode = s_qrcode.strip().replace("'", '"')
                read_num = i_read_num
                media_level = WEIBO_VERIFY_STATUS[i_verify_status]
                intro = s_introduction.strip().replace("'", '"')
                media_weibo_status = i_status
                is_put = i_put
                is_top = i_wom_sect_top
                active_end_time = i_effective_time
                fail_reason = i_explain.strip().replace("'", '"')
                put_down_reason = s_bottom_put_explain.strip().replace("'", '"')
                audit_remark = i_beizhu.strip().replace("'", '"')
                create_time = current_time_to_stamp()
                update_time = current_time_to_stamp()
                audit_time = current_time_to_stamp()
                enter_time = current_time_to_stamp()

                '''t_user'''
                s_email = None
                s_mobile = None
                s_real_name = None
                s_co_name = None
                s_weixin = None
                s_qq = None
                row = None
                query = (
                    "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iType`=2 AND `iUserID`=%d" % i_user_id)
                source_mysql_tool.cursor.execute()
                row = source_mysql_tool.fetchone()
                if row is not None:
                    s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                    if s_mobile is None:
                        s_mobile = ''
                    else:
                        s_mobile = s_mobile.strip().replace("'", '"')
                    if s_real_name is None:
                        s_real_name = ''
                    else:
                        s_real_name = s_real_name.strip().replace("'", '"')
                    if s_co_name is '':
                        s_co_name = s_real_name.strip()
                    else:
                        s_co_name = s_co_name.strip().replace("'", '"')
                    if s_weixin is None:
                        s_weixin = ''
                    else:
                        s_weixin = s_weixin.strip().replace("'", '"')
                    if s_qq is None:
                        s_qq = ''
                    else:
                        s_qq = s_qq.strip().replace("'", '"')
                else:
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                    (media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                        query, error_file_tool)
                    s_email = '1160154200@qq.com'

                '''
	            填充wom_account字段
	            '''
                wom_account_uuid = uuid_generator()
                login_account = s_email
                login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                wom_account_type = 2
                wom_account_status = 1

                if login_account not in wom_account_login_account_list:
                    wom_account_login_account_list.append(login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        wom_account_uuid, login_account, login_password, wom_account_type, wom_account_status, create_time))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                    media_vendor_uuid = uuid_generator()
                    contact_person = s_real_name
                    contact1 = s_mobile
                    weixin = s_weixin
                    qq = s_qq
                    name = s_co_name
                    query = (
                        "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (media_vendor_uuid, wom_account_uuid, contact_person, contact1, weixin, qq, name))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                else:
                    query = (
                        "SELECT `uuid` FROM `wom_account` WHERE `type`=2 AND `login_account`='%s'" % s_email)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (wom_account_uuid,) = row
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % wom_account_uuid)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (media_vendor_uuid,) = row
                try:
                    query = ("INSERT INTO `media_weibo`(`uuid`,`weibo_name`,`follower_num`,`follower_screenshot`,`weibo_url`,`avatar`,`qrcode`,`read_num`,`media_level`,`intro`,`media_weibo_status`,`is_put`,`is_top`,`active_end_time`,`fail_reason`,`put_down_reason`,`audit_remark`,`create_time`,`update_time`,`audit_time`,`enter_time`) VALUES('%s','%s',%d,'%s','%s','%s','%s',%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s',%d,%d,%d,%d)" % (
                        media_weibo_uuid, weibo_name, follower_num, follower_screenshot, weibo_url, avatar, qrcode, read_num, media_level, intro, media_weibo_status, is_put, is_top, active_end_time, fail_reason, put_down_reason, audit_remark, create_time, update_time, audit_time, enter_time))
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        media_weibo_uuid + 'media_weixin 插入操作失败')
                    break
                try:

                    query = ("INSERT INTO `weibo_vendor_bind`(`uuid`,`weibo_uuid`,`vendor_uuid`,`soft_direct_price_orig`,`soft_transfer_price_orig`,`micro_direct_price_orig`,`micro_transfer_price_orig`,`soft_direct_price_retail`,`soft_transfer_price_retail`,`micro_direct_price_retail`,`micro_transfer_price_retail`,`soft_direct_price_execute`,`soft_transfer_price_execute`,`micro_direct_price_execute`,`micro_transfer_price_execute`,`create_time`,`update_time`) VALUES ('%s','%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d)" % (
                        weibo_vendor_bind_uuid, media_weibo_uuid, media_vendor_bind, soft_direct_price_orig, soft_transfer_price_orig, soft_direct_price_retail, soft_transfer_price_retail, soft_direct_price_execute, soft_transfer_price_execute, micro_direct_price_execute, micro_transfer_price_execute, micro_direct_price_retail, micro_transfer_price_retail, micro_direct_price_orig, micro_transfer_price_orig, create_time, update_time))
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        weibo_vendor_bind_uuid + 'weibo_vendor_bind 插入操作失败')
                    break
        finally:
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()
            source_file_tool.close_file()
            error_file_tool.close_file()


if __name__ == '__main__':
    media_weixin_storage = MediaWeiXinStorage()
    # media_weixin_storage.media_weixin_storage_csv()
    media_weixin_storage.media_weixin_storage_mongo()
    # media_weixin_storage.mysql_to_mysql()
