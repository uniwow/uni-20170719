from models.video_weiboyi_model import MediaVideo, VideoPlatformCommonInfo, VideoVendorPrice, WeiboyiLiveTalentData, VendorVideoBind, database
from tools.tool_time import uuid_generator, current_time_to_stamp


def video_weiboyi_transfer():
    count = 0
    for weiboyi_live_talent_data in WeiboyiLiveTalentData.select().where(WeiboyiLiveTalentData.platform == "哔哩哔哩"):
        media_video_dict = {}
        media_video_dict["uuid"] = uuid_generator()
        if weiboyi_live_talent_data.name is not None:
            media_video_dict["nickname"] = weiboyi_live_talent_data.name
        else:
            media_video_dict["nickname"] = ""
        media_video_dict["sex"] = weiboyi_live_talent_data.sex
        media_video_dict["main_platform"] = 12
        if weiboyi_live_talent_data.area is not None:
            media_video_dict["address"] = weiboyi_live_talent_data.area
        else:
            media_video_dict["address"] = ""
        media_video_dict["create_time"] = current_time_to_stamp()
        media_video_dict["update_time"] = current_time_to_stamp()
        MediaVideo.create(**media_video_dict)

        video_platform_common_info_dict = {}
        video_platform_common_info_dict["uuid"] = uuid_generator()
        video_platform_common_info_dict[
            "video_uuid"] = media_video_dict["uuid"]
        video_platform_common_info_dict["platform_type"] = 12
        video_platform_common_info_dict[
            "account_name"] = media_video_dict["nickname"]
        video_platform_common_info_dict[
            "account"] = weiboyi_live_talent_data.platform_uuid
        video_platform_common_info_dict[
            "follower_num"] = weiboyi_live_talent_data.follower_num * 10000
        video_platform_common_info_dict[
            "avatar"] = weiboyi_live_talent_data.avatar
        if weiboyi_live_talent_data.url is not None:
            video_platform_common_info_dict[
                "url"] = weiboyi_live_talent_data.url
        else:
            video_platform_common_info_dict["url"] = ""
        if weiboyi_live_talent_data.url is not None:
            video_platform_common_info_dict[
                "person_sign"] = weiboyi_live_talent_data.person_intro
        else:
            video_platform_common_info_dict["person_sign"] = ""
        if weiboyi_live_talent_data.is_auth == 1:
            video_platform_common_info_dict[
                "auth_type"] = weiboyi_live_talent_data.is_auth
        else:
            video_platform_common_info_dict["auth_type"] = 2
        video_platform_common_info_dict["status"] = 0
        video_platform_common_info_dict[
            "create_time"] = current_time_to_stamp()
        video_platform_common_info_dict[
            "update_time"] = current_time_to_stamp()
        VideoPlatformCommonInfo.create(**video_platform_common_info_dict)
        vendor_video_bind_dict = {}
        vendor_video_bind_dict["uuid"] = uuid_generator()
        vendor_video_bind_dict["video_uuid"] = media_video_dict["uuid"]
        vendor_video_bind_dict["vendor_uuid"] = "1471856236aY2Is"
        vendor_video_bind_dict["status"] = 0
        vendor_video_bind_dict["is_pref_vendor"] = 1
        vendor_video_bind_dict["create_time"] = current_time_to_stamp()
        vendor_video_bind_dict["update_time"] = current_time_to_stamp()
        vendor_video_bind_dict["active_end_time"] = current_time_to_stamp()
        VendorVideoBind.create(**vendor_video_bind_dict)
        video_vendor_price_dict = {}
        video_vendor_price_dict["uuid"] = uuid_generator()
        video_vendor_price_dict[
            "platform_uuid"] = weiboyi_live_talent_data.platform_uuid
        video_vendor_price_dict[
            "vendor_bind_uuid"] = vendor_video_bind_dict["uuid"]
        video_vendor_price_dict["platform_type"] = 12
        video_vendor_price_dict["is_main_platform"] = 1
        video_vendor_price_dict["price_orig_one"] = float(
            weiboyi_live_talent_data.advert_price)
        video_vendor_price_dict["price_orig_two"] = float(
            weiboyi_live_talent_data.live_price)
        video_vendor_price_dict["create_time"] = current_time_to_stamp()
        video_vendor_price_dict["update_time"] = current_time_to_stamp()
        VideoVendorPrice.create(**video_vendor_price_dict)
        count += 1
        print("settled count:%d,weiboyi_uuid:%s,media_video_uuid:%s,video_platform_common_info_uuid:%s,vendor_video_bind_uuid:%s,video_vendor_price_uuid:%s" % (
            count, weiboyi_live_talent_data.platform_uuid, media_video_dict["uuid"], video_platform_common_info_dict["uuid"], vendor_video_bind_dict["uuid"], video_vendor_price_dict["uuid"]))
        # input(".................................")
