
def dts_media_weixin_influence_rank_slave(self, source_mongo_args_1, source_mongo_args_2 , source_file_args, queue_tag):
        _calculate_timestamp = current_time_to_stamp()
        source_mongo_tool_1 = self.get_mongo_tool(*source_mongo_args_1)
        source_mongo_tool_2 = self.get_mongo_tool(*source_mongo_args_2)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        current_date_time = datetime.datetime.now()
        current_date_time_to_str = datetime_to_str(current_date_time)
        month_cycle_type_tuple = ("month", "oneMonth", "twoMonth")
        week_cycle_type_tuple = ("week", "oneWeek", "twoWeek", "threeWeek")
        cycle_type_tuple = ("month", "oneMonth", "twoMonth",
                            "week", "oneWeek", "twoWeek", "threeWeek")
        classify_rank_tuple = (('rank_1000', 'rank_1000_inc', 0), ('rank_1001', 'rank_1001_inc', 1), ('rank_1002', 'rank_1002_inc', 2), ('rank_1003', 'rank_1003_inc', 3), ('rank_1004', 'rank_1004_inc', 4), ('rank_1005', 'rank_1005_inc', 5), ('rank_1006', 'rank_1006_inc', 6), ('rank_1007', 'rank_1007_inc', 7), ('rank_1008', 'rank_1008_inc', 8), ('rank_1009', 'rank_1009_inc', 9), ('rank_1010', 'rank_1010_inc', 10), ('rank_1011', 'rank_1011_inc', 11), ('rank_1012', 'rank_1012_inc', 12), (
            'rank_1013', 'rank_1013_inc', 13), ('rank_1014', 'rank_1014_inc', 14), ('rank_1015', 'rank_1015_inc', 15), ('rank_1016', 'rank_1016_inc', 16), ('rank_1017', 'rank_1017_inc', 17), ('rank_1018', 'rank_1018_inc', 18), ('rank_1019', 'rank_1019_inc', 19), ('rank_1020', 'rank_1020_inc', 20), ('rank_1021', 'rank_1021_inc', 21), ('rank_1022', 'rank_1022_inc', 22), ('rank_1023', 'rank_1023_inc', 23), ('rank_1024', 'rank_1024_inc', 24), ('rank_1025', 'rank_1025_inc', 25), ('rank_1026', 'rank_1026_inc', 26))
        auth_info_type_tuple = (
            "account_cert", "tencent_account_cert", "sina_account_cert",)
        # database.connect()
        write_to = DtsInfluenceRankTableTip.get(
            DtsInfluenceRankTableTip.id == 1).read_from
        read_from = write_to
        # database.close()
        write_to_orm = None
        delete_from_orm = None
        if write_to == "dts_media_weixin_influence_rank":
            write_to = "dts_media_weixin_influence_rank_slave"
            write_to_orm = DtsMediaWeixinInfluenceRankSlave
            delete_from_orm = DtsMediaWeixinInfluenceRank
        elif write_to == "dts_media_weixin_influence_rank_slave":
            write_to = "dts_media_weixin_influence_rank"
            write_to_orm = DtsMediaWeixinInfluenceRank
            delete_from_orm = DtsMediaWeixinInfluenceRankSlave
        if queue_tag == 1:
            write_to_orm.delete().execute()
        else:
            print("sleeping 5min.................")
            time.sleep(60*5)
        settled_count = 0
        total_count = document_list.count()
        hour_of_day_list = ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23")
        day_of_week_list = ("1", "2", "3", "4", "5", "6", "7")
        re_head_view_avg_cnt = "(\d+)_head_view_avg_cnt"
        re_head_like_avg_cnt = "(\d+)_head_like_avg_cnt"

        re_many_second_view_avg_cnt = "(\d+)_many_second_view_avg_cnt"
        re_many_second_like_avg_cnt = "(\d+)_many_second_like_avg_cnt"

        re_day_of_month_article_cnt = "(\d+)_day_of_month_article_cnt"
        re_day_of_week_article_avg_cnt = "(\d+)_day_of_week_article_avg_cnt"
        re_hour_of_day_article_cnt = "(\d+)_hour_of_day_article_cnt"
        re_rank = "rank_(\d+)"

        dts_media_weixin_influence_rank_data_source_list = []
        dts_media_weixin_influence_rank_public_id_list = []
        dts_media_weixin_influence_rank_data_source_count = 0
        for line in source_file:
            document = source_mongo_tool_1.database[
                "test-test"].find_one({"public_id":source_file_tool.line_strip_decode_ignore(line)})
            print(document["public_id"])
            dts_media_weixin_influence_rank_public_id_list.append(document[
                                                                  "public_id"])
            settled_count += 1
            total_count -= 1
            if "cycle" not in document or current_date_time_to_str not in document['cycle']:
                continue
            """
            更新详情页数据
            """
            _account_keyword = {}
            _article_effect = {}
            _influence_data = {}
            _operation_status = {}
            _public = document["public_id"]
            _update_time = current_datetime_to_str()

            month_article_effect = {}
            operational_status = {}
            month_article_keyword = {}

            month_head_article_trend = {}
            month_next_article_trend = {}

            month_post_trend = {}
            month_post_time_hour_distribute = {}
            month_post_time_week_distribute = {}
            month_rank_trend = {}

            for k, v in document['cycle'][current_date_time_to_str]["month"].items():
                m = re.search(re_head_view_avg_cnt, k)
                if m is not None:
                    month_head_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_head_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_many_second_view_avg_cnt, k)
                if m is not None:
                    month_next_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_many_second_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_day_of_month_article_cnt, k)
                if m is not None:
                    month_post_trend[str(m.group(1))] = document['cycle'][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_hour_of_day_article_cnt, k)
                if m is not None:
                    month_post_time_hour_distribute[str(m.group(1))] = document["cycle"][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_day_of_week_article_avg_cnt, k)
                if m is not None:
                    day_of_week = int(m.group(1))
                    if day_of_week == 0:
                        day_of_week = 7
                    month_post_time_week_distribute[str(day_of_week)] = document["cycle"][
                        current_date_time_to_str]["month"][k]

            month_statistics = {}
            month_statistics["head_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["head_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["head_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["head_10w_article_total_cnt"]
            }
            month_statistics["next_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_10w_article_total_cnt"]
            }
            month_article_effect[
                "month_head_article_trend"] = month_head_article_trend
            month_article_effect[
                "month_next_article_trend"] = month_next_article_trend
            month_article_effect["month_statistics"] = month_statistics
            """
            文章效果
            """
            _article_effect["month_article_effect"] = month_article_effect
            """
            运营状况
            """
            operational_status["month_post_articles_count"] = document["cycle"][
                current_date_time_to_str]["month"]["article_total_cnt"]
            operational_status["month_post_count"] = document["cycle"][
                current_date_time_to_str]["month"]["pub_total_cnt"]
            operational_status["month_article_per_post"] = document["cycle"][
                current_date_time_to_str]["month"]["article_avg_pub_cnt"]
            operational_status["month_post_trend"] = month_post_trend
            operational_status[
                "month_post_time_hour_distribute"] = month_post_time_hour_distribute
            operational_status[
                "month_post_time_week_distribute"] = month_post_time_week_distribute
            operational_status["month_single_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["single_avg_view_cnt"]
            operational_status["month_multi_pos_1_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos1_avg_view_cnt"]
            operational_status["month_multi_pos_2_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos2_avg_view_cnt"]
            operational_status["month_multi_pos_3_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos3_avg_view_cnt"]

            _operation_status["operational_status"] = operational_status
            """
            账户关键词
            """

            if "text_info" in document and current_date_time_to_str in document["text_info"] and "account_wordle" in document['text_info'][current_date_time_to_str]:
                _account_keyword["month_article_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle"]
                _account_keyword["month_top_five_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle_top_5"]
            else:
                _account_keyword["month_article_keyword"] = []
                _account_keyword["month_top_five_keyword"] = []

            """
            影响力数据
            """
            for k, v in document["rank_trend"].items():
                m = re.search(re_rank, k)
                if m is not None:
                    # print(type(m.group(1)))
                    rank_trend_list = []
                    for rank_trend in document["rank_trend"][k][-30:]:
                        rank_trend_dict = {}
                        rank_trend_dict[rank_trend[
                            "name"]] = rank_trend["value"]
                        rank_trend_list.append(rank_trend_dict)
                    month_rank_trend[
                        str(int(m.group(1)) - 1000)] = rank_trend_list

            _influence_data["month_rank_trend"] = month_rank_trend
            month_wom_index_trend_list = []
            for month_wom_index_rank in document["month_wom_index_rank"][-30:]:
                month_wom_index_rank_dict = {}
                month_wom_index_rank_dict[month_wom_index_rank[
                    "name"]] = month_wom_index_rank["value"]
                month_wom_index_trend_list.append(month_wom_index_rank_dict)
            _influence_data[
                "month_wom_index_trend"] = month_wom_index_trend_list
            # print("account_keyword :%s" % str(_account_keyword))
            # print("article_effect :%s" % str(_article_effect))
            # print("influence_data :%s" % str(_influence_data))
            # print("operation_status :%s" % str(_operation_status))
            if MediaWeixinStatistic.select().where(MediaWeixinStatistic.public == document["public_id"]).count() == 1:
                # account_keyword=json.dumps(_account_keyword, ensure_ascii=False),
                MediaWeixinStatistic.update(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    update_time=current_datetime_to_str()).where(MediaWeixinStatistic.public == document["public_id"]).execute()
                # print("1")

            else:
                # print("2")
                MediaWeixinStatistic.insert(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    public=document["public_id"],
                    update_time=current_datetime_to_str()).execute()

            # input(",,,,,,,,,,,,,,,,,,,,,")
            # print(str(document))
            _account_short_desc = ''
            _auth_info = ''
            _auth_type = -1
            _nickname = ''
            _follower_num = -1
            _retail_avg_price = 0

            document_media_weixin = source_mongo_tool_2.database[
                "media_weixin"].find_one({"public_id": document["public_id"]})
            if MediaWeixin.select().where(MediaWeixin.real_public == document["public_id"]).count() == 1:

                media_weixin_record = MediaWeixin.get(
                    MediaWeixin.real_public == document["public_id"])
                _follower_num = media_weixin_record.follower_num
                if(media_weixin_record.retail_price_m_1_min == 0):
                    if(media_weixin_record.retail_price_m_2_min == 0):
                        if(media_weixin_record.retail_price_m_3_min == 0):
                            _retail_avg_price = 0
                        else:
                            _retail_avg_price = float(
                                media_weixin_record.retail_price_m_3_min)
                    else:
                        _retail_avg_price = float(
                            media_weixin_record.retail_price_m_2_min)
                else:
                    _retail_avg_price = float(
                        media_weixin_record.retail_price_m_1_min)

            _m_avg_price_pv = 0
            if document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != -1 and document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != 0:
                _m_avg_price_pv = _retail_avg_price / \
                    document['cycle'][current_date_time_to_str][
                        "month"]["head_avg_view_cnt"]

            if document_media_weixin is not None:

                """
                更新榜单数据
                """

                if "account_short_desc" in document_media_weixin:
                    _account_short_desc = document_media_weixin[
                        "account_short_desc"]
                else:
                    _account_short_desc = ''
                for auth_info_type in auth_info_type_tuple:
                    if auth_info_type in document_media_weixin:
                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "account_cert":
                            _auth_type = 0
                            if "account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "tencent_account_cert":
                            _auth_type = 1
                            if "tencent_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "tencent_account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "sina_account_cert":
                            _auth_type = 2
                            if "sina_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "sina_account_cert_info"]

                if "public_name" in document_media_weixin:
                    _nickname = document_media_weixin["public_name"]
            week_or_month = None

            '''
            如果头条平均阅读数<1000或者沃米指数<=2.0，则跳过
            '''
            # if (document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["oneMonth"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["twoMonth"]["head_avg_view_cnt"] < 1000) or (document['cycle'][current_date_time_to_str]["month"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["oneMonth"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["twoMonth"]["wom_index"] <= 2):
            #     continue
            for cycle_type in cycle_type_tuple:
                if cycle_type in month_cycle_type_tuple:
                    week_or_month = 1
                else:
                    week_or_month = 0

                for classify_rank in classify_rank_tuple:
                    # print(current_date_time_to_str)
                    # print(document[current_date_time_to_str])

                    if classify_rank[0] in document['cycle'][current_date_time_to_str][cycle_type]:
                        # print("....................")
                        # database.connect()
                        # print(str(document[current_date_time_to_str][
                        #         cycle_type]["view_median"]))

                        # input("............................")

                        dts_media_weixin_influence_rank_data_source_count += 1
                        dts_media_weixin_influence_rank_data_source_item = {}
                        dts_media_weixin_influence_rank_data_source_item["_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["10w_article_total_cnt"]

                        dts_media_weixin_influence_rank_data_source_item["act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["article_post_end_date"] = document['cycle'][
                            current_date_time_to_str][cycle_type]["time"]
                        dts_media_weixin_influence_rank_data_source_item["article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_pub_count"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_avg_pub_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_median"]
                        dts_media_weixin_influence_rank_data_source_item["like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["max_post_time"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["max_post_time"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["pub_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["pub_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "public"] = document["public_id"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "classify"] = classify_rank[2]
                        dts_media_weixin_influence_rank_data_source_item["rank"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[0]]
                        dts_media_weixin_influence_rank_data_source_item["rank_incr"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[1]]
                        dts_media_weixin_influence_rank_data_source_item["single_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_median"]
                        dts_media_weixin_influence_rank_data_source_item["view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["wom_index"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["wom_index"]
                        '''
                        潜力指标计算
                        '''
                        dts_media_weixin_influence_rank_data_source_item[
                            "potential_index"] = 0
                        if document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] > 0:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][
                                current_date_time_to_str][cycle_type]["wom_index"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] + 1) * 0.4
                            dts_media_weixin_influence_rank_data_source_item[
                                "potential_index"] = math.pow(potential_index, 2)
                        else:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][
                                cycle_type]["wom_index"] + 1) * 0.3 - math.log((document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] * (-1)) + 1) * 0.4
                            if potential_index > 0:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = math.pow(potential_index, 2)
                            else:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = (math.pow((potential_index * (-1)), 2)) * (-1)

                        dts_media_weixin_influence_rank_data_source_item[
                            "cycle_type"] = week_or_month
                        dts_media_weixin_influence_rank_data_source_item[
                            "account_short_desc"] = _account_short_desc
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_type"] = _auth_type
                        dts_media_weixin_influence_rank_data_source_item[
                            "nickname"] = _nickname
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_info"] = _auth_info
                        dts_media_weixin_influence_rank_data_source_item["highest_read_num"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["highest_view_num"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "cost_per_read"] = _m_avg_price_pv
                        dts_media_weixin_influence_rank_data_source_item[
                            "fans_number"] = _follower_num

                        dts_media_weixin_influence_rank_data_source_list.append(
                            dts_media_weixin_influence_rank_data_source_item)
            try:
                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []
            except Exception as e:
                print("delete duplicated items.....................")
                influence_data_prod_database.connect()
                print(str(dts_media_weixin_influence_rank_public_id_list))
                for public_id in dts_media_weixin_influence_rank_public_id_list:
                    print("delete %s" % public_id)
                    write_to_orm.delete().where(write_to_orm.public == public_id).execute()

                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []

            print("settled %d items;%d items lefted" %
                  (settled_count, total_count))
            # input("...........................")
        '''
        导入最后不足100条数据
        '''
        try:
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()
        except Exception as e:
            print("delete duplicated items.....................")
            influence_data_prod_database.connect()
            for public_id in dts_media_weixin_influence_rank_public_id_list:
                write_to_orm.delete().where(write_to_orm.public == public_id).execute()
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()

        _read_from = write_to
        _update_time = current_datetime_to_str()
        # database.connect()
        '''
        如果写入数据超过100K,则切换表
        以免导致没有数据
        '''
        if write_to_orm.select().count() >= 100000:
            DtsInfluenceRankTableTip.update(calculate_timestamp=_calculate_timestamp,
                                            update_time=_update_time,
                                            read_from=_read_from).where(DtsInfluenceRankTableTip.id == 1).execute()
            delete_from_orm.delete().execute()
            delete_from_orm.raw("TRUNCATE TABLE `%s`" % read_from)
        influence_data_prod_database.close()
        source_mongo_tool_1.disconnection()
        source_mongo_tool_2.disconnection()
        # influence_data_prod_database.close()
        while True:
            result=requests.get("http://api.womdata.com/site/after-refresh-rank?access-token=b05a329eb619eb80763a03d50a81c233")
            if int(result.text) == 1:
                print("success!!!")
                break
            else:
                print("fail!!!")