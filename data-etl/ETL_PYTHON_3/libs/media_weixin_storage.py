# coding=utf-8
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.tool_mongo import MongoTool
from tools.tool_time import uuid_generator, current_time_to_stamp, current_stamp_to_time, current_stamp_to_datetime, datetime_to_str
from bson import ObjectId
from tools.settings import WEIBO_VERIFY_STATUS, CATEGORY_SPLIT, CATEGORY, CITY, WOM_PROD_1_TUPLE, ERROR_FILE_ARGS, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO, BACKUP_JSON_PATH, CHECK_CSV_PATH, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_225_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_32_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_249_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_70_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_102_MONGO_TUPLE, CHECK_SQL_PATH
import math
import multiprocessing
from tools.tool_math import avg_generator, sd_generator
from pymongo.errors import BulkWriteError
from tools.tool_string import strip_and_replace_all, strip_and_replace
import json
import random
import os
import datetime
import pymongo


class MediaWeiXinStorage(object):

    def __init__(self):
        pass

    def get_mysql_tool(self, host, db, user, pwd):
        mysql_tool = MysqlTool(host, db, user, pwd)
        return mysql_tool

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def file_mysql(self, source_file_args, target_mysql_args, target_table_args, target_column_in_file_list, target_column_user_added_dict):
        '''
        更新或者插入数据，既有文件中的字段数据，也有自己添加的字段数据
        target_column_in_file_list=[['column_1','字段类型'],...]
        字段类型：i:int,d:double,s:string.后续增加
        '''
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        unique_id_list_mysql = target_mysql_tool.get_unique_id_list(
            *target_table_args)
        for line in source_file:
            if source_file_tool.line_strip_decode_ignore(line) is not u'':

                if source_file_tool.line_strip_decode_ignore(line) in unique_id_list_mysql:

                    '''数据存在，更新数据'''
                else:
                    '''数据不存在，插入数据'''

    def mysql_mysql(self, source_mysql_args, source_table_args, target_mysql_args, target_table_args, source_column_name_list, target_column_name_list, target_column_user_added_dict, target_file_args):
        '''mysql到mysql，查询出一个列表，列表的第一项同target表为主外键关系'''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        for unique_id_mysql in unique_id_list_mysql:

            row = source_mysql_tool.find_one(source_table_args[
                                             0], source_column_name_list, source_table_args[1], unique_id_mysql.target_file_tool)
            target_column_dict = {}
            count_target_column_name_list = 0
            for target_column_name in target_column_name_list:
                count_target_column_name_list += 1
                target_column_dict[target_column_name] = row[
                    count_target_column_name_list]
            for k, v in target_column_user_added_dict.items():
                target_column_dict[k] = v

            target_mongo_tool.update_one(
                target_table_args[0], target_column_dict, target_table_args[1], row[0], target_file_tool)

    def price_list_pub_type_update(self, target_table_name):
        target_file_tool = self.get_file_tool(*ERROR_FILE_ARGS)
        source_mysql_tool = self.get_mysql_tool(*WOM_PROD_MYSQL_TUPLE)
        target_mysql_tool = self.get_mysql_tool(*WOM_PROD_MYSQL_TUPLE)
        source_unique_id_list = source_mysql_tool.get_unique_id_list(
            target_table_name, 'uuid', [])
        count = 0
        # print(str(len(source_unique_id_list)))
        # input('.....................')
        try:
            for source_unique_id in source_unique_id_list:
                count += 1
                row = source_mysql_tool.find_one(target_table_name, ["s_pub_type", "orig_price_s_min", "orig_price_s_max", "retail_price_s_min", "retail_price_s_max", "execute_price_s", "m_1_pub_type", "orig_price_m_1_min", "orig_price_m_1_max", "retail_price_m_1_min", "retail_price_m_1_max", "execute_price_m_1",
                                                                     "m_2_pub_type", "orig_price_m_2_min", "orig_price_m_2_max", "retail_price_m_2_min", "retail_price_m_2_max", "execute_price_m_2", "m_3_pub_type", "orig_price_m_3_min", "orig_price_m_3_max", "retail_price_m_3_min", "retail_price_m_3_max", "execute_price_m_3", "active_end_time"], "uuid", source_unique_id, target_file_tool)
                s_pub_type, orig_price_s_min, orig_price_s_max, retail_price_s_min, retail_price_s_max, execute_price_s, m_1_pub_type, orig_price_m_1_min, orig_price_m_1_max, retail_price_m_1_min, retail_price_m_1_max, execute_price_m_1, m_2_pub_type, orig_price_m_2_min, orig_price_m_2_max, retail_price_m_2_min, retail_price_m_2_max, execute_price_m_2, m_3_pub_type, orig_price_m_3_min, orig_price_m_3_max, retail_price_m_3_min, retail_price_m_3_max, execute_price_m_3, active_end_time = row
                if active_end_time is None:
                    active_end_time = -1
                pos_s = {"active_end_time": active_end_time, "pub_type": s_pub_type, "orig_price_min": float(round(orig_price_s_min, 2)), "orig_price_max": float(round(orig_price_s_max, 2)),
                         "retail_price_min": float(round(retail_price_s_min, 2)), "retail_price_max": float(round(retail_price_s_max, 2)), "execute_price": float(round(execute_price_s, 2))}
                pos_m_1 = {"active_end_time": active_end_time, "pub_type": m_1_pub_type, "orig_price_min": float(round(orig_price_m_1_min, 2)), "orig_price_max": float(round(orig_price_m_1_max, 2)),
                           "retail_price_min": float(round(retail_price_m_1_min, 2)), "retail_price_max": float(round(retail_price_m_1_max, 2)), "execute_price": float(round(execute_price_m_1, 2))}
                pos_m_2 = {"active_end_time": active_end_time, "pub_type": m_2_pub_type, "orig_price_min": float(round(orig_price_m_2_min, 2)), "orig_price_max": float(round(orig_price_m_2_max, 2)),
                           "retail_price_min": float(round(retail_price_m_2_min, 2)), "retail_price_max": float(round(retail_price_m_2_max, 2)), "execute_price": float(round(execute_price_m_2, 2))}
                pos_m_3 = {"active_end_time": active_end_time, "pub_type": m_3_pub_type, "orig_price_min": float(round(orig_price_m_3_min, 2)), "orig_price_max": float(round(orig_price_m_3_max, 2)),
                           "retail_price_min": float(round(retail_price_m_3_min, 2)), "retail_price_max": float(round(retail_price_m_3_max, 2)), "execute_price": float(round(execute_price_m_3, 2))}
                pub_type = {}
                pub_type['pos_s'] = pos_s
                pub_type['pos_m_1'] = pos_m_1
                pub_type['pos_m_2'] = pos_m_2
                pub_type['pos_m_3'] = pos_m_3
                target_mysql_tool.update_one(target_table_name, {'pub_config': str(
                    pub_type).replace("'", '"')}, "uuid", source_unique_id, target_file_tool)
                print(('%d ---' % count))
                # input('............................')
        finally:
            target_file_tool.close_file()
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()

    def media_weixin_pub_type_update(self, source_mysql_args, target_mysql_args):
        '''to do'''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `uuid`,`pref_vendor_uuid` FROM `media_weixin`")
        source_mysql_tool.cursor.execute(query)
        unique_id_list = []
        for row in source_mysql_tool.cursor:
            unique_id_list.append(row)
        count = 0
        try:
            for row in unique_id_list:
                count += 1
                (uuid_media_weixin, uuid_pref_vendor) = row
                query = ("SELECT `uuid` FROM `media_vendor_bind` WHERE `media_uuid`='%s' AND `vendor_uuid`='%s' " % (
                    uuid_media_weixin, uuid_pref_vendor))
                source_mysql_tool.cursor.execute(query)
                (uuid_media_vendor_bind,) = source_mysql_tool.cursor.fetchone()
                query = ("SELECT `s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_s_max`,`orig_price_m_1_min`,`orig_price_m_1_max`,`orig_price_m_2_min`,`orig_price_m_2_max`,`orig_price_m_3_min`,`orig_price_m_3_max`,`retail_price_s_min`,`retail_price_s_max`,`retail_price_m_1_min`,`retail_price_m_1_max`,`retail_price_m_2_min`,`retail_price_m_2_max`,`retail_price_m_3_min`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`active_end_time` FROM `media_vendor_weixin_price_list` WHERE `bind_uuid`='%s'" % uuid_media_vendor_bind)
                source_mysql_tool.cursor.execute(query)
                (s_pub_type, m_1_pub_type, m_2_pub_type, m_3_pub_type, orig_price_s_min, orig_price_s_max, orig_price_m_1_min, orig_price_m_1_max, orig_price_m_2_min, orig_price_m_2_max, orig_price_m_3_min, orig_price_m_3_max, retail_price_s_min, retail_price_s_max,
                 retail_price_m_1_min, retail_price_m_1_max, retail_price_m_2_min, retail_price_m_2_max, retail_price_m_3_min, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, active_end_time) = source_mysql_tool.cursor.fetchone()
                if active_end_time is None:
                    active_end_time = -1
                pos_s = {"active_end_time": active_end_time, "pub_type": s_pub_type, "orig_price_min": float(round(orig_price_s_min, 2)), "orig_price_max": float(round(orig_price_s_max, 2)),
                         "retail_price_min": float(round(retail_price_s_min, 2)), "retail_price_max": float(round(retail_price_s_max, 2)), "execute_price": float(round(execute_price_s, 2))}
                pos_m_1 = {"active_end_time": active_end_time, "pub_type": m_1_pub_type, "orig_price_min": float(round(orig_price_m_1_min, 2)), "orig_price_max": float(round(orig_price_m_1_max, 2)),
                           "retail_price_min": float(round(retail_price_m_1_min, 2)), "retail_price_max": float(round(retail_price_m_1_max, 2)), "execute_price": float(round(execute_price_m_1, 2))}
                pos_m_2 = {"active_end_time": active_end_time, "pub_type": m_2_pub_type, "orig_price_min": float(round(orig_price_m_2_min, 2)), "orig_price_max": float(round(orig_price_m_2_max, 2)),
                           "retail_price_min": float(round(retail_price_m_2_min, 2)), "retail_price_max": float(round(retail_price_m_2_max, 2)), "execute_price": float(round(execute_price_m_2, 2))}
                pos_m_3 = {"active_end_time": active_end_time, "pub_type": m_3_pub_type, "orig_price_min": float(round(orig_price_m_3_min, 2)), "orig_price_max": float(round(orig_price_m_3_max, 2)),
                           "retail_price_min": float(round(retail_price_m_3_min, 2)), "retail_price_max": float(round(retail_price_m_3_max, 2)), "execute_price": float(round(execute_price_m_3, 2))}
                pub_type = {}
                pub_type['pos_s'] = pos_s
                pub_type['pos_m_1'] = pos_m_1
                pub_type['pos_m_2'] = pos_m_2
                pub_type['pos_m_3'] = pos_m_3
                query = ("UPDATE `media_weixin` SET `s_pub_type`=%d,`m_1_pub_type`=%d,`m_2_pub_type`=%d,`m_3_pub_type`=%d,`orig_price_s_min`=%.2f,`orig_price_s_max`=%.2f,`orig_price_m_1_min`=%.2f,`orig_price_m_1_max`=%.2f,`orig_price_m_2_min`=%.2f,`orig_price_m_2_max`=%.2f,`orig_price_m_3_min`=%.2f,`orig_price_m_3_max`=%.2f,`retail_price_s_min`=%.2f,`retail_price_s_max`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_1_max`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_2_max`=%.2f,`retail_price_m_3_min`=%.2f,`retail_price_m_3_max`=%.2f,`execute_price_s`=%.2f,`execute_price_m_1`=%.2f,`execute_price_m_2`=%.2f,`execute_price_m_3`=%.2f,`pub_config`='%s',`active_end_time`=%d WHERE `uuid`='%s'" % (
                    s_pub_type, m_1_pub_type, m_2_pub_type, m_3_pub_type, orig_price_s_min, orig_price_s_max, orig_price_m_1_min, orig_price_m_1_max, orig_price_m_2_min, orig_price_m_2_max, orig_price_m_3_min, orig_price_m_3_max, retail_price_s_min, retail_price_s_max, retail_price_m_1_min, retail_price_m_1_max, retail_price_m_2_min, retail_price_m_2_max, retail_price_m_3_min, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, str(pub_type).replace("'", '"'), active_end_time, uuid_media_weixin))
                print(query)
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
                # input('.....................')
                print(('%d ---' % count))
        finally:
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()

    def media_weixin_article_analysis_split(self, source_mongo_args, target_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        target_mysql_tool = self.get_mysql_tool(*DTS_OPT_MYSQL_TUPLE)
        target_coll = target_mongo_tool.database['media_weixin']
        bulk = target_mongo_tool.unordered_bulk_options(target_coll)
        item_count = 0
        try:
            document_list = source_mongo_tool.database[
                "media_weixin_article_analysis"].find({})
            for document_input in document_list:
                if type(document_input) == type({}):

                    user_id = document_input["_id"]
                    analysis_joint = document_input["value"].split(":")
                    '''
                     *
                     *年沃米指数：年平均阅读：       点赞：                互动：                 年头条平均阅读：            点赞：                      互动：                        年单图文平均阅读：             点赞：                       互动：                             年多图文第一条平均阅读：        点赞：                             互动：                             年多图文第二条平均阅读：            点赞：                             互动：                             年多图文三~八条平均阅读：           点赞：                         互动：月平均阅读：        点赞：             互动：                   月头条平均阅读：            点赞：                        互动：                           月单图文平均阅读：       点赞：                    互动：                         月多图文第一条平均阅读：      点赞：                         互动：                            月多图文第二条平均阅读：         点赞：                          互动：                              月多图文三~八条平均阅读：     点赞：                         互动：                           周平均阅读：        点赞：             互动：                周头条平均阅读：             点赞：                      互动：                          周单图文平均阅读：       点赞：                   互动：                       周多图文第一条平均阅读：     点赞：                       互动：                          周多图文第二条平均阅读：        点赞：                         互动：                             周3~8条平均阅读：            点赞：                      互动：                         年文章总数：月文章总数：周文章总数：年单图文数：    月单图文数：        周单图文数：     年多图文数：     月多图文数：     周多图文数：    年群发数：  月群发数：        年十万+：              月十万+：                      周十万+：                   月沃米指数：     周沃米指数：
                     *
                    '''
                    y_view_cnt, y_like_cnt, m_view_cnt, m_like_cnt, w_view_cnt, w_like_cnt, p_value_count, min_post_time, max_post_time, y_wom_index, year_view_average, year_like_average, year_interact_average, year_headline_view_average, year_headline_like_average, year_headline_interact_average, year_single_view_average, year_single_like_average, year_single_interact_average, year_many_first_view_average, year_many_first_like_average, year_many_first_interact_average, year_many_second_view_average, year_many_second_like_average, year_many_second_interact_average, year_many_third_view_average, year_many_third_like_average, year_many_third_interact_average, month_view_average, month_like_average, month_interact_average, month_headline_view_average, month_headline_like_average, month_headline_interact_average, month_single_view_average, month_single_like_average, month_single_interact_average, month_many_first_view_average, month_many_first_like_average, month_many_first_interact_average, month_many_second_view_average, month_many_second_like_average, month_many_second_interact_average, month_many_third_view_average, month_many_third_like_average, month_many_third_interact_average, week_view_average, week_like_average, week_interact_average, week_headline_view_average, week_headline_like_average, week_headline_interact_average, week_single_view_average, week_single_like_average, week_single_interact_average, week_many_first_view_average, week_many_first_like_average, week_many_first_interact_average, week_many_second_view_average, week_many_second_like_average, week_many_second_interact_average, week_many_third_view_average, week_many_third_like_average, w_multil_pos3_avg_act_cnt, count, month_count, week_count, year_single_count, month_single_count, week_single_count, year_many_count, month_many_count, week_many_count, mass_count, month_mass_count, hundred_thousand_count, month_hundred_thousand_count, week_hundred_thousand_count, month_wom_index, week_wom_index = analysis_joint
                    # y_view_cnt, y_like_cnt, m_view_cnt, m_like_cnt, w_view_cnt, w_like_cnt, article_total_cnt, min_post_time, max_post_time, y_wom_index, y_avg_view_cnt, y_avg_like_cnt, y_avg_act_cnt, y_head_avg_view_cnt, y_head_avg_like_cnt, y_head_avg_act_cnt, y_single_avg_view_cnt, y_single_avg_like_cnt, y_single_avg_act_cnt, y_multil_pos1_avg_view_cnt, y_multil_pos1_avg_like_cnt, year_many_first_interact_average, y_multil_pos1_avg_act_cnt, y_multil_pos2_avg_like_cnt, y_multil_pos2_avg_act_cnt, y_multil_pos3_avg_view_cnt, y_multil_pos3_avg_like_cnt, y_multil_pos3_avg_act_cnt, m_avg_view_cnt, m_avg_like_cnt, m_avg_act_cnt, m_head_avg_view_cnt, m_head_avg_like_cnt, m_head_avg_act_cnt, m_single_avg_view_cnt, m_single_avg_like_cnt, m_single_avg_act_cnt, m_multil_pos1_avg_view_cnt, m_multil_pos1_avg_like_cnt, m_multil_pos1_avg_like_cnt, m_multil_pos2_avg_view_cnt, m_multil_pos2_avg_like_cnt, m_multil_pos2_avg_act_cnt, m_multil_pos3_avg_view_cnt, m_multil_pos3_avg_like_cnt, m_multil_pos3_avg_act_cnt, w_avg_view_cnt, w_avg_like_cnt, w_avg_act_cnt, w_head_avg_view_cnt, w_head_avg_like_cnt, w_head_avg_act_cnt, w_single_avg_view_cnt, w_single_avg_like_cnt, w_single_avg_act_cnt, w_multil_pos1_avg_view_cnt, w_multil_pos1_avg_like_cnt, w_multil_pos1_avg_act_cnt, w_multil_pos2_avg_view_cnt, w_multil_pos2_avg_like_cnt, w_multil_pos2_avg_act_cnt, w_multil_pos3_avg_view_cnt, w_multil_pos3_avg_like_cnt, w_multil_pos3_avg_act_cnt, y_article_total_cnt, m_article_total_cnt, w_article_total_cnt, y_single_article_total_cnt, m_single_article_total_cnt, w_single_article_total_cnt, y_multil_article_total_cnt, m_multil_article_total_cnt, w_multil_article_total_cnt, y_pub_total_cnt, m_pub_total_cnt, y_10w_article_total_cnt, m_10w_article_total_cnt, w_10w_article_total_cnt, m_wom_index, w_wom_index = analysis_joint

                    data_analysis = {}
                    data_analysis['y_view_cnt'] = int(y_view_cnt)
                    data_analysis['y_like_cnt'] = int(y_like_cnt)
                    data_analysis['m_view_cnt'] = int(m_view_cnt)
                    data_analysis['m_like_cnt'] = int(m_like_cnt)
                    data_analysis['w_view_cnt'] = int(w_view_cnt)
                    data_analysis['w_like_cnt'] = int(w_like_cnt)
                    data_analysis["min_post_time"] = int(min_post_time)
                    data_analysis["max_post_time"] = int(max_post_time)
                    data_analysis["y_wom_index"] = float(y_wom_index)
                    data_analysis["y_avg_view_cnt"] = int(year_view_average)
                    data_analysis["y_avg_like_cnt"] = int(year_like_average)
                    data_analysis["y_avg_act_cnt"] = int(
                        year_interact_average)

                    data_analysis["y_head_avg_view_cnt"] = int(
                        year_headline_view_average)
                    data_analysis["y_head_avg_like_cnt"] = int(
                        year_headline_like_average)
                    data_analysis["y_head_avg_act_cnt"] = int(
                        year_headline_interact_average)

                    data_analysis["y_single_avg_view_cnt"] = int(
                        year_single_view_average)
                    data_analysis["y_single_avg_like_cnt"] = int(
                        year_single_like_average)
                    data_analysis["y_single_avg_act_cnt"] = int(
                        year_single_interact_average)

                    data_analysis["y_multil_pos1_avg_view_cnt"] = int(
                        year_many_first_view_average)
                    data_analysis["y_multil_pos1_avg_like_cnt"] = int(
                        year_many_first_like_average)
                    data_analysis["y_multil_pos1_avg_act_cnt"] = int(
                        year_many_first_interact_average)

                    data_analysis["y_multil_pos2_avg_view_cnt"] = int(
                        year_many_second_view_average)
                    data_analysis["y_multil_pos2_avg_like_cnt"] = int(
                        year_many_second_like_average)
                    data_analysis["y_multil_pos2_avg_act_cnt"] = int(
                        year_many_second_interact_average)

                    data_analysis["y_multil_pos3_avg_view_cnt"] = int(
                        year_many_third_view_average)
                    data_analysis["y_multil_pos3_avg_like_cnt"] = int(
                        year_many_third_like_average)
                    data_analysis["y_multil_pos3_avg_act_cnt"] = int(
                        year_many_third_interact_average)

                    data_analysis["m_avg_view_cnt"] = int(month_view_average)
                    data_analysis["m_avg_like_cnt"] = int(month_like_average)
                    data_analysis["m_avg_act_cnt"] = int(
                        month_interact_average)

                    data_analysis["m_head_avg_view_cnt"] = int(
                        month_headline_view_average)
                    data_analysis["m_head_avg_like_cnt"] = int(
                        month_headline_like_average)
                    data_analysis["m_head_avg_act_cnt"] = int(
                        month_headline_interact_average)

                    data_analysis["m_single_avg_view_cnt"] = int(
                        month_single_view_average)
                    data_analysis["m_single_avg_like_cnt"] = int(
                        month_single_like_average)
                    data_analysis["m_single_avg_act_cnt"] = int(
                        month_single_interact_average)

                    data_analysis["m_multil_pos1_avg_view_cnt"] = int(
                        month_many_first_view_average)
                    data_analysis["m_multil_pos1_avg_like_cnt"] = int(
                        month_many_first_like_average)
                    data_analysis["m_multil_pos1_avg_act_cnt"] = int(
                        month_many_first_interact_average)

                    data_analysis["m_multil_pos2_avg_view_cnt"] = int(
                        month_many_second_view_average)
                    data_analysis["m_multil_pos2_avg_like_cnt"] = int(
                        month_many_second_like_average)
                    data_analysis["m_multil_pos2_avg_act_cnt"] = int(
                        month_many_second_interact_average)

                    data_analysis["m_multil_pos3_avg_view_cnt"] = int(
                        month_many_third_view_average)
                    data_analysis["m_multil_pos3_avg_like_cnt"] = int(
                        month_many_third_like_average)
                    data_analysis["m_multil_pos3_avg_act_cnt"] = int(
                        month_many_third_interact_average)

                    data_analysis["w_avg_view_cnt"] = int(week_view_average)
                    data_analysis["w_avg_like_cnt"] = int(week_like_average)
                    data_analysis["w_avg_act_cnt"] = int(week_interact_average)

                    data_analysis["w_head_avg_view_cnt"] = int(
                        week_headline_view_average)
                    data_analysis["w_head_avg_like_cnt"] = int(
                        week_headline_like_average)
                    data_analysis["w_head_avg_act_cnt"] = int(
                        week_headline_interact_average)

                    data_analysis["w_single_avg_view_cnt"] = int(
                        week_single_view_average)
                    data_analysis["w_single_avg_like_cnt"] = int(
                        week_single_like_average)
                    data_analysis["w_single_avg_act_cnt"] = int(
                        week_single_interact_average)

                    data_analysis["w_multil_pos1_avg_view_cnt"] = int(
                        week_many_first_view_average)
                    data_analysis["w_multil_pos1_avg_like_cnt"] = int(
                        week_many_first_like_average)
                    data_analysis["w_multil_pos1_avg_act_cnt"] = int(
                        week_many_first_interact_average)

                    data_analysis["w_multil_pos2_avg_view_cnt"] = int(
                        week_many_second_view_average)
                    data_analysis["w_multil_pos2_avg_like_cnt"] = int(
                        week_many_second_like_average)
                    data_analysis["w_multil_pos2_avg_act_cnt"] = int(
                        week_many_second_interact_average)

                    data_analysis["w_multil_pos3_avg_view_cnt"] = int(
                        week_many_third_view_average)
                    data_analysis["w_multil_pos3_avg_like_cnt"] = int(
                        week_many_third_like_average)
                    data_analysis["w_multil_pos3_avg_act_cnt"] = int(
                        w_multil_pos3_avg_act_cnt)

                    data_analysis["article_total_cnt"] = int(p_value_count)
                    data_analysis["y_article_total_cnt"] = int(count)
                    data_analysis["m_article_total_cnt"] = int(month_count)
                    data_analysis["w_article_total_cnt"] = int(week_count)

                    data_analysis["y_single_article_total_cnt"] = int(
                        year_single_count)
                    data_analysis["m_single_article_total_cnt"] = int(
                        month_single_count)
                    data_analysis["w_single_article_total_cnt"] = int(
                        week_single_count)

                    data_analysis["y_multil_article_total_cnt"] = int(
                        year_many_count)
                    data_analysis["m_multil_article_total_cnt"] = int(
                        month_many_count)
                    data_analysis["w_multil_article_total_cnt"] = int(
                        week_many_count)

                    data_analysis["y_pub_total_cnt"] = int(mass_count)
                    data_analysis["m_pub_total_cnt"] = int(month_mass_count)

                    data_analysis["y_10w_article_total_cnt"] = int(
                        hundred_thousand_count)
                    data_analysis["m_10w_article_total_cnt"] = int(
                        month_hundred_thousand_count)
                    data_analysis["w_10w_article_total_cnt"] = int(
                        week_hundred_thousand_count)
                    data_analysis['m_wom_index'] = float(month_wom_index)
                    data_analysis['w_wom_index'] = float(week_wom_index)
                    data_analysis["time"] = current_time_to_stamp()
                    bulk.find({'public_id': user_id}).update_one({"$set": {"stat_article_cnt": int(
                        p_value_count)}, "$addToSet": {"data_analysis": data_analysis}})
                    item_count += 1
                    if (item_count % 100) == 0:
                        bulk.execute()
                        bulk = target_mongo_tool.unordered_bulk_options(
                            target_coll)
                        print('......................................')
                    elif (source_mongo_tool.database["media_weixin_article_analysis"].count() == 0):
                        try:
                            bulk.execute()
                            print('......................................')
                        except Exception as e:
                            pass
                    '''
                    query = ("UPDATE `sogou_weixin_account_init_task` SET `article_num`=%d WHERE LOWER(`account_id`)='%s'" % (
                        int(p_value_count), user_id.lower()))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                    '''
                    print("data_analysis " + str(item_count) + " : " +
                          user_id + "@ " + str(analysis_joint))
                else:
                    break
            source_mongo_tool.database[
                "media_weixin_article_analysis"].remove({})
        finally:
            source_mongo_tool.disconnection()
            target_mongo_tool.disconnection()
            target_mysql_tool.disconnection()

    def media_weixin_article_power_analysis(self, source_mongo_args, source_collection_args, target_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        source_unique_id_list = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        max_page_power = 0
        min_page_power = 9999999
        count = 0
        for source_unique_id in source_unique_id_list:
            print(str(source_unique_id))
            documents = source_mongo_tool.database['media_weixin_article'].find(
                {"weixin_id": source_unique_id}, {"_id": 1})
            article_id_list = []
            for document in documents:
                article_id_list.append(document['_id'])
            target_coll = target_mongo_tool.database["media_weixin_article"]
            bulk = target_mongo_tool.unordered_bulk_options(target_coll)
            article_count = 0
            for article_id in article_id_list:
                article_count += 1
                document = source_mongo_tool.find_one("media_weixin_article", {"_id": article_id}, [
                    "page_view_num", "page_like_num", "comment_num", "comment", "weixin_id"])
                page_view_num = document["page_view_num"]
                page_like_num = document["page_like_num"]
                page_comment_num = document["comment_num"]
                page_comment_like_num = 0
                if int(page_comment_num) is not 0:
                    for comment in document["comment"]:
                        page_comment_like_num += comment["like_num"]
                # print("page_comment_like_num : %d" % page_comment_like_num)

                page_power = (math.pow(math.log(page_view_num + 1), 2) * 0.7 + math.pow((math.log(page_like_num + 1) * 0.6 + (
                    math.log(100 * page_comment_num + 1) * 0.6 + math.log(100 * page_comment_like_num + 1) * 0.4) * 0.4), 2) * 0.3) * 10
                # page_power = math.pow(math.log(page_view_num + 1) * 0.7 +
                # (math.log(page_like_num + 1) * 0.6 + (math.log(100 *
                # page_comment_num + 1) * 0.6 + math.log(100 *
                # page_comment_like_num + 1) * 0.4) * 0.4) * 0.3,2)
                update_time = current_time_to_stamp()
                data_analysis = {}
                data_analysis["page_power"] = round(page_power, 2)
                data_analysis["time"] = update_time
                bulk.find({'_id': article_id}).update_one({
                    "$set": {"article_power_current": round(page_power, 2)}, "$addToSet": {"article_power": data_analysis}})

                count += 1
                print("page_power %d: %.2f" % (count, page_power))
                if page_power < min_page_power:
                    min_page_power = page_power
                if page_power > max_page_power:
                    max_page_power = page_power
            print('article_count %d' % article_count)
            if article_count == 0:
                continue
            bulk.execute()
        source_mongo_tool.disconnection()
        target_mongo_tool.disconnection()
        print("参与分析的文章数为 ： %d;max_page_power %.2f;min_page_power %.2f" %
              (len(source_unique_id_list), max_page_power, min_page_power))

    def media_weixin_article_power_analysis_multi_process_control(self, source_mongo_args, source_collection_args, process_num, target_mongo_args, target_file_args):

        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_unique_id_list = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        source_unique_id_list = source_unique_id_list[:100]
        process_list = []
        try:
            for i in range(process_num):
                if i == (process_num - 1):
                    # input(".......... %d" %
                    #      (int((len(source_unique_id_list) / process_num)) * i))
                    source_unique_id_list_process = source_unique_id_list[
                        (int((len(source_unique_id_list) / process_num)) * i):(int(len(source_unique_id_list)))]
                    p = multiprocessing.Process(target=self.media_weixin_article_power_analysis_multi_process, args=(
                        i, source_mongo_args, source_unique_id_list_process, target_mongo_args, target_file_args))
                    p.daemon = True
                    process_list.append(p)
                else:
                    # print(str(type(len(source_unique_id_list) /
                    # process_num)))
                    source_unique_id_list_process = source_unique_id_list[
                        ((int(len(source_unique_id_list) / process_num)) * i):(int((len(source_unique_id_list) / process_num)) * (i + 1))]
                    p = multiprocessing.Process(target=self.media_weixin_article_power_analysis_multi_process, args=(
                        i, source_mongo_args, source_unique_id_list_process, target_mongo_args, target_file_args))
                    p.daemon = True
                    process_list.append(p)
            for process in process_list:
                process.start()
            for process in process_list:
                process.join()
        finally:
            source_mongo_tool.disconnection()
        end_time = current_time_to_stamp()
        return end_time

    def media_weixin_article_power_analysis_multi_process(self, process_flag, source_mongo_args, source_unique_id_list, target_mongo_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        max_page_power = 0
        min_page_power = 9999999
        count = 0
        try:
            for source_unique_id in source_unique_id_list:
                # print(str(type(source_unique_id)))
                documents = source_mongo_tool.database['media_weixin_article'].find(
                    {"weixin_id": source_unique_id}, {"_id": 1})
                article_id_list = []
                for document in documents:
                    article_id_list.append(document['_id'])
                target_coll = target_mongo_tool.database[
                    "media_weixin_article"]
                bulk = target_mongo_tool.unordered_bulk_options(target_coll)
                article_count = 0
                for article_id in article_id_list:
                    article_count += 1
                    document = source_mongo_tool.find_one("media_weixin_article", {"_id": article_id}, [
                        "page_view_num", "page_like_num", "comment_num", "comment", "weixin_id"])
                    page_view_num = document["page_view_num"]
                    page_like_num = document["page_like_num"]
                    page_comment_num = document["comment_num"]
                    page_comment_like_num = 0
                    if int(page_comment_num) is not 0:
                        for comment in document["comment"]:
                            page_comment_like_num += comment["like_num"]
                    # print("page_comment_like_num : %d" %
                    # page_comment_like_num)

                    page_power = (math.pow(math.log(page_view_num + 1), 2) * 0.7 + math.pow((math.log(page_like_num + 1) * 0.6 + (
                        math.log(100 * page_comment_num + 1) * 0.6 + math.log(100 * page_comment_like_num + 1) * 0.4) * 0.4), 2) * 0.3) * 10
                    # page_power = math.pow(math.log(page_view_num + 1) * 0.7 +
                    # (math.log(page_like_num + 1) * 0.6 + (math.log(100 *
                    # page_comment_num + 1) * 0.6 + math.log(100 *
                    # page_comment_like_num + 1) * 0.4) * 0.4) * 0.3,2)
                    update_time = current_time_to_stamp()
                    data_analysis = {}
                    data_analysis["page_power"] = round(page_power, 2)
                    data_analysis["time"] = update_time
                    bulk.find({'_id': article_id}).update_one({
                        "$set": {"article_power_current": round(page_power, 2)}, "$addToSet": {"article_power": data_analysis}})

                    count += 1
                    print("page_power %d: %.2f" % (count, page_power))
                    if page_power < min_page_power:
                        min_page_power = page_power
                    if page_power > max_page_power:
                        max_page_power = page_power
                print('process tag %d article_count %d' %
                      (process_flag, article_count))
                if article_count == 0:
                    continue
                bulk.execute()
            target_file_tool.write_database_line_to_file(str(process_flag) + "," + str(len(
                source_unique_id_list)) + "," + str(round(max_page_power, 2)) + "," + str(round(min_page_power, 2)))
            print("参与分析的账号数为 ： %d;max_page_power %.2f;min_page_power %.2f" %
                  (len(source_unique_id_list), max_page_power, min_page_power))
        finally:
            source_mongo_tool.disconnection()
            target_mongo_tool.disconnection()
            target_file_tool.close_file()

    def media_weixin_status_update(self, source_mysql_args, target_mysql_args, source_table_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        target_file_tool_1 = self.get_file_tool(
            *(CHECK_CSV_PATH, 'media_weixin_status_invalid_marked.csv', 'ab'))
        target_file_tool_2 = self.get_file_tool(
            *(CHECK_CSV_PATH, 'media_weixin_status_invalid_not_marked.csv', 'ab'))
        target_file_tool_3 = self.get_file_tool(
            *(CHECK_CSV_PATH, 'media_weixin_status_invalid_marked_yet.csv', 'ab'))
        source_unique_id_list = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        query = ("SELECT `public_id` FROM `media_weixin`")
        source_unique_id_list_media_weixin = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        source_unique_id_list_media_weixin_lower = []
        for source_unique_id in source_unique_id_list_media_weixin:
            source_unique_id_list_media_weixin_lower.append(
                source_unique_id.lower())
        for source_unique_id in source_unique_id_list:
            query = ("UPDATE `media_weixin` SET `status`=3 WHERE LOWER(`public_id`)='%s'" %
                     source_unique_id.lower())
            print(query)
            # try:

            #     # target_mysql_tool.cursor.execute(query)
            #     # target_mysql_tool.cnx.commit()
            #     # target_file_tool_1.write_database_line_to_file(
            #     #     source_unique_id + '\n')
            #     pass

            # except Exception as e:
            #     # target_file_tool.write_database_line_to_file(source_unique_id)
            #     target_file_tool_3.write_database_line_to_file(
            #         source_unique_id + '\n')
        for source_unique_id in source_unique_id_list:
            if source_unique_id.lower() not in source_unique_id_list_media_weixin_lower:
                target_file_tool_2.write_database_line_to_file(
                    source_unique_id + '\n')
            else:
                target_file_tool_1.write_database_line_to_file(
                    source_unique_id + '\n')

        source_mysql_tool.disconnection()
        target_mysql_tool.disconnection()
        target_file_tool.close_file()
        target_file_tool_1.close_file()
        target_file_tool_2.close_file()

    def media_weixin_id_update(self, source_file_args, target_mysql_args, target_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        t_count = 0
        f_count = 0
        for line in source_file:
            weixin_id = source_file_tool.line_strip_decode_ignore(
                line)
            query = ("UPDATE `media_weixin` SET `public_id`='%s' WHERE LOWER(`public_id`)='%s'" % (
                weixin_id, weixin_id.lower()))
            try:
                print(query)
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
                t_count += 1
            except Exception as e:
                f_count += 1
                target_file_tool.write_file_line_to_file(line)
        print('correct count %d' % t_count)
        print('false count %d ' % f_count)

    def media_weixin_price_fix(self):
        source_mysql_tool = self.get_mysql_tool(WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL[
            'db'], WOM_PROD_MYSQL['user'], WOM_PROD_MYSQL['password'])
        query = ("SELECT `uuid`,`orig_price_s_min` FROM `media_vendor_weixin_price_list` WHERE `s_pub_type` !=0 AND `orig_price_s_min` != 0 AND `retail_price_s_min` = 0")
        source_mysql_tool.cursor.execute(query)
        row_list = []
        for row in source_mysql_tool.cursor:
            row_list.append(row)

        for row in row_list:
            media_weixin_uuid, orig_price_s_min = row
            query = (
                "UPDATE `media_vendor_weixin_price_list` SET `retail_price_s_min`=%.2f WHERE `uuid`='%s'" % (orig_price_s_min, media_weixin_uuid))
            print(query)
            source_mysql_tool.cursor.execute(query)
            source_mysql_tool.cnx.commit()
        print('count %d ' % len(row_list))

    def media_weixin_price_update(self):
        source_mysql_tool = self.get_mysql_tool(WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL[
            'db'], WOM_PROD_MYSQL['user'], WOM_PROD_MYSQL['password'])
        query = ("SELECT `uuid`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min` FROM `media_weixin` WHERE `retail_price_s_min` =0 AND `retail_price_m_1_min` =0 AND `retail_price_m_2_min` =0 AND `retail_price_m_3_min` =0 AND (`orig_price_s_min`!=0 OR `orig_price_m_1_min`!=0 OR `orig_price_m_2_min` !=0 OR `orig_price_m_3_min` != 0)")
        source_mysql_tool.cursor.execute(query)
        row_list = []
        for row in source_mysql_tool.cursor:
            row_list.append(row)

        for row in row_list:
            media_weixin_uuid, orig_price_s_min,  orig_price_m_1_min, orig_price_m_2_min,  orig_price_m_3_min = row
            query = (
                "UPDATE `media_weixin` SET `retail_price_s_min`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_3_min`=%.2f WHERE `uuid`='%s'" % (orig_price_s_min,  orig_price_m_1_min,  orig_price_m_2_min,  orig_price_m_3_min, media_weixin_uuid))
            print(query)
            source_mysql_tool.cursor.execute(query)
            source_mysql_tool.cnx.commit()
        print('count %d ' % len(row_list))

    def media_vendor_weixin_price_list_price_update(self):
        source_mysql_tool = self.get_mysql_tool(WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL[
            'db'], WOM_PROD_MYSQL['user'], WOM_PROD_MYSQL['password'])
        query = ("SELECT `uuid`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min` FROM `media_vendor_weixin_price_list` WHERE `retail_price_s_min` =0 AND `retail_price_m_1_min` =0 AND `retail_price_m_2_min` =0 AND `retail_price_m_3_min` =0 AND (`orig_price_s_min`!=0 OR `orig_price_m_1_min`!=0 OR `orig_price_m_2_min` !=0 OR `orig_price_m_3_min` != 0)")
        source_mysql_tool.cursor.execute(query)
        row_list = []
        for row in source_mysql_tool.cursor:
            row_list.append(row)
        for row in row_list:
            uuid, orig_price_s_min,  orig_price_m_1_min, orig_price_m_2_min,  orig_price_m_3_min = row

            query = (
                "UPDATE `media_vendor_weixin_price_list` SET `retail_price_s_min`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_3_min`=%.2f WHERE `uuid`='%s'" % (orig_price_s_min,  orig_price_m_1_min,  orig_price_m_2_min,  orig_price_m_3_min, uuid))
            print(query)
            source_mysql_tool.cursor.execute(query)
            source_mysql_tool.cnx.commit()
        print('count %d ' % len(row_list))

    def media_weixin_storage_csv(self):
        '''csv文件中账号数据录入mysql'''
        source_file = self.source_file_tool.get_file()
        for line in source_file:
            uuid = uuid_generator()
            vendor_cnt = 1
            to_verify_vendor_cnt = 0
            put_up = 0
            status = 0
            public_id = self.source_file_tool.line_strip_decode_ignore(line)
            query = ("INSERT into `media_weixin`(`uuid`,`create_time`,`last_update_time`,`public_id`,`vendor_cnt`,`to_verify_vendor_cnt`,`put_up`,`status`) VALUES('%s',%d,%d,'%s',%d,%d,%d,%d)" % (
                uuid, current_time_to_stamp(), current_time_to_stamp(), public_id, vendor_cnt, to_verify_vendor_cnt, put_up, status))
            # query=("UPDATE `media_weixin` SET `uuid`='%s' WHERE
            # `public_id`='%s'" % (uuid,public_id))
            print(query)
            self.source_mysql_tool.cursor.execute(query)
            self.source_mysql_tool.cnx.commit()
            query = ("INSERT INTO `media_vendor_bind` SET `uuid`='%s',`media_type`=%d,`media_uuid`='%s',`vendor_uuid`='%s',`create_time`=%d,`status`=%d" % (
                uuid_generator(), 1, uuid, '1472531107emnSK', current_time_to_stamp(), 0))
            self.source_mysql_tool.cursor.execute(query)
            self.source_mysql_tool.cnx.commit()

    def basic_info_and_index_compute_data_to_mysql(self, source_mongo_args_1, source_mongo_args_2, source_mysql_args, target_mysql_args):
        self.media_weixin_public_id_update(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, ('media_weixin', {'real_public_id_updated': {"$exists": False}}, [
            'public_id', ]), WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_PROD_MYSQL_TUPLE, ERROR_FILE_ARGS)
        # source_mongo_tool_1 = self.get_mongo_tool(*source_mongo_args_1)
        # source_mongo_tool_2 = self.get_mongo_tool(*source_mongo_args_2)

        # '''
        # 分成两类，一类real_public_id已经更新,一类没有更新
        # 首先找real_public_id 更新的
        # '''
        # count_1 = 0
        # count_2 = 0
        # count_3 = 0
        # count_4 = 0
        # count_5 = 0
        # count_6 = 0
        # count_7 = 0

        # query = (
        #     "SELECT `real_public_id` FROM `media_weixin` WHERE `real_public_id` != ''")
        # source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        # source_mysql_tool.cursor.execute(query)
        # real_public_id_list = []
        # for row in source_mysql_tool.cursor:
        #     (real_public_id,) = row
        #     real_public_id_list.append(real_public_id)

        # source_mysql_tool.disconnection()
        # source_mongo_tool_2.database["media_weixin_info"].create_index(
        #     [("public_id", pymongo.HASHED)], background=True)
        # for real_public_id in real_public_id_list:
        #     count_7 += 1
        #     document = source_mongo_tool_1.database[
        #         'media_weixin'].find_one({"public_id": real_public_id})
        #     if document is not None:
        #         count_5 += 1
        #         print("settled %d items has real public id" % count_5)
        #         if "public_name" in document:
        #             public_name = document["public_name"].replace("'", '"')
        #         else:
        #             public_name = ""

        #         if "avatar_big_img" in document:
        #             avatar_big_img = document[
        #                 "avatar_big_img"]
        #         else:
        #             avatar_big_img = ""

        #         if "avatar_small_img" in document:
        #             avatar_small_img = document[
        #                 "avatar_small_img"]
        #         else:
        #             avatar_small_img = ""

        #         if "qrcode_img" in document:
        #             qrcode_img = document["qrcode_img"]
        #         else:
        #             qrcode_img = ""

        #         if "account_cert" in document:
        #             account_cert = document["account_cert"]
        #         else:
        #             account_cert = 0

        #         if "account_cert_info" in document:
        #             account_cert_info = document[
        #                 "account_cert_info"].replace("'", '"')
        #         else:
        #             account_cert_info = ""

        #         if "account_short_desc" in document:
        #             account_short_desc = document[
        #                 "account_short_desc"].replace("'", '"')
        #         else:
        #             account_short_desc = ""

        #         document = source_mongo_tool_2.database[
        #             'media_weixin_info'].find_one({"public_id": real_public_id})
        #         target_mysql_tool = self.get_mysql_tool(*target_mysql_args)

        #         if document is not None:
        #             if ("cycle" not in document) or (current_date_time_to_str not in document["cycle"]):
        #                 continue
        #             # else:
        #             #     document = document[current_date_time_to_str]
        #             # if "month" not in document:
        #             #     continue
        #             count_1 += 1
        #             m_head_avg_view_cnt = document["cycle"][current_date_time_to_str][
        #                 "month"]["head_avg_view_cnt"]
        #             m_head_avg_like_cnt = document["cycle"][current_date_time_to_str][
        #                 "month"]["head_avg_like_cnt"]
        #             m_avg_view_cnt = document["cycle"][
        #                 current_date_time_to_str]["month"]["avg_view_cnt"]
        #             m_avg_like_cnt = document["cycle"][
        #                 current_date_time_to_str]["month"]["avg_like_cnt"]
        #             m_total_article_cnt = document["cycle"][
        #                 current_date_time_to_str]["month"]["article_total_cnt"]
        #             m_release_cnt = document["cycle"][
        #                 current_date_time_to_str]["month"]["pub_total_cnt"]
        #             head_read_median = document["cycle"][
        #                 current_date_time_to_str]["month"]["head_view_median"]
        #             head_like_median = document["cycle"][
        #                 current_date_time_to_str]["month"]["head_like_median"]
        #             m_10w_article_total_cnt = document["cycle"][current_date_time_to_str]["month"][
        #                 "10w_article_total_cnt"]
        #             m_wmi = document["cycle"][
        #                 current_date_time_to_str]["month"]["wom_index"]
        #             latest_article_post_date = document["cycle"][current_date_time_to_str][
        #                 "month"]["max_post_time"]
        #             promote_index = 0.7 * math.log(document["cycle"][current_date_time_to_str]["month"]["head_view_median"] + 1) + 0.3 * math.log(
        #                 document["cycle"][current_date_time_to_str]["month"]["head_like_median"] + 1)
        #             sales_index = 0.5 * math.log(document["cycle"][current_date_time_to_str]["month"]["head_view_median"] + 1) + 0.5 * math.log(
        #                 document["cycle"][current_date_time_to_str]["month"]["head_like_median"] + 1)
        #             query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s',`account_short_desc`='%s',`m_avg_view_cnt`=%d,`m_avg_like_cnt`=%d,`m_head_avg_view_cnt`=%d,`m_head_avg_like_cnt`=%d,`m_10w_article_total_cnt`=%d,`m_wmi`=%.2f,`latest_article_post_date`=%d,`promote_index`=%.2f,`sales_index`=%.2f,`total_article_cnt`=%d,`m_release_cnt`=%d,`head_read_median`=%d,`head_like_median`=%d WHERE `real_public_id`='%s'" % (
        #                 public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, m_avg_view_cnt, m_avg_like_cnt, m_head_avg_view_cnt, m_head_avg_like_cnt, m_10w_article_total_cnt, m_wmi, latest_article_post_date, promote_index, sales_index, m_total_article_cnt, m_release_cnt, head_read_median, head_like_median, real_public_id))
        #             print(query)
        #             print("count_1 : %d" % count_1)
        #             target_mysql_tool.cursor.execute(query)
        #             target_mysql_tool.cnx.commit()
        #         else:
        #             count_2 += 1
        #             query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s' ,`account_short_desc`='%s' WHERE `real_public_id`='%s'" % (
        #                 public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, real_public_id))
        #             print(query)
        #             print("count_2 : %d" % count_2)

        #             target_mysql_tool.cursor.execute(query)
        #             target_mysql_tool.cnx.commit()
        #         target_mysql_tool.disconnection()
        # for public_id in public_id_list[:1000]:
        #     document = source_mongo_tool_1.database['media_weixin'].find_one(
        #         {"public_id": {"$regex": public_id, "$options": 'i'}})
        #     if document is not None:
        #         count_6 += 1
        #         print("settled %d items has not real public id" % count_6)
        #         real_public_id = document["month"]["public_id"]
        #         if "public_name" in document:
        #             public_name = document["public_name"].replace("'", '"')
        #         else:
        #             public_name = ""

        #         if "avatar_big_img" in document:
        #             avatar_big_img = document[
        #                 "avatar_big_img"]
        #         else:
        #             avatar_big_img = ""

        #         if "avatar_small_img" in document:
        #             avatar_small_img = document[
        #                 "avatar_small_img"]
        #         else:
        #             avatar_small_img = ""

        #         if "qrcode_img" in document:
        #             qrcode_img = document["qrcode_img"]
        #         else:
        #             qrcode_img = ""

        #         if "account_cert" in document:
        #             account_cert = document["account_cert"]
        #         else:
        #             account_cert = 0

        #         if "account_cert_info" in document:
        #             account_cert_info = document[
        #                 "account_cert_info"].replace("'", '"')
        #         else:
        #             account_cert_info = ""

        #         if "account_short_desc" in document:
        #             account_short_desc = document[
        #                 "account_short_desc"].replace("'", '"')
        #         else:
        #             account_short_desc = ""

        #         document = source_mongo_tool_2.database['index_compute'].find_one(
        #             {"weixinId": {"$regex": public_id, "$options": 'i'}})
        #         target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        #         if document is not None:
        #             count_3 += 1
        #             m_head_avg_view_cnt = document["m_head_avg_view_cnt"]
        #             m_head_avg_like_cnt = document["m_head_avg_like_cnt"]
        #             m_avg_view_cnt = document["m_avg_view_cnt"]
        #             m_avg_like_cnt = document["m_avg_like_cnt"]
        #             m_10w_article_total_cnt = document[
        #                 "m_10w_article_total_cnt"]
        #             m_wmi = document["m_wom_index"]
        #             w_wmi = document["w_wom_index"]
        #             latest_article_post_date = document["max_post_time"]
        #             query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s',`account_short_desc`='%s',`m_avg_view_cnt`=%d,`m_avg_like_cnt`=%d,`m_head_avg_view_cnt`=%d,`m_head_avg_like_cnt`=%d,`m_10w_article_total_cnt`=%d,`m_wmi`=%.2f,`w_wmi`=%.2f,`latest_article_post_date`=%d,`real_public_id`='%s' WHERE `public_id`='%s'" % (
        #                 public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, m_avg_view_cnt, m_avg_like_cnt, m_head_avg_view_cnt, m_head_avg_like_cnt, m_10w_article_total_cnt, m_wmi, w_wmi, latest_article_post_date, real_public_id, public_id))
        #             print(query)
        #             print("count_3 : %d" % count_3)
        #             target_mysql_tool.cursor.execute(query)
        #             target_mysql_tool.cnx.commit()
        #         else:
        #             count_4 += 1
        #             query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s' ,`account_short_desc`='%s',`real_public_id`='%s' WHERE `public_id`='%s'" % (
        #                 public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, real_public_id, public_id))
        #             print(query)
        #             print("count_4 : %d" % count_4)
        #             target_mysql_tool.cursor.execute(query)
        #             target_mysql_tool.cnx.commit()
        #         target_mysql_tool.disconnection()
        # print("count_1:%d,count_2:%d,count_3:%d,count_4:%d" %
        #       (count_1, count_2, count_3, count_4))
        return -1

    def media_weixin_storage_mongo(self):
        start_time = current_time_to_stamp()
        '''media_weixin mongo表数据更新到media_weixin mysql表'''
        source_mongo_tool = self.get_mongo_tool(WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['host'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO[
            'port'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['db'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['user'], WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO['password'])
        public_id_list_mongo = source_mongo_tool.get_unique_id_list(
            "media_weixin", {}, ["public_id", ])
        target_mysql_tool = self.get_mysql_tool(WOM_PROD_MYSQL['host'], WOM_PROD_MYSQL[
            'db'], WOM_PROD_MYSQL['user'], WOM_PROD_MYSQL['password'])
        public_id_list_mysql = target_mysql_tool.get_unique_id_list(
            "media_weixin", "public_id", [])

        public_id_list_mysql_lower = []
        count = 0
        insert_count = 0
        error_count = 0
        error_weixin_id_list = []
        try:
            for public_id_mysql in public_id_list_mysql:
                public_id_list_mysql_lower.append(public_id_mysql.lower())
            for public_id_mongo in public_id_list_mongo:
                print(public_id_mongo)
                document = source_mongo_tool.database[
                    "media_weixin"].find_one({"public_id": public_id_mongo})

                if public_id_mongo.lower() in public_id_list_mysql_lower:
                    """
                    判断mongo 数据是否在mysql中
                    """
                    # public_id_update = ""
                    # for public_id_mysql in public_id_list_mysql:
                    #     if public_id_mysql.lower() == public_id_mongo.lower():
                    #         public_id_update = public_id_mysql

                    if "public_name" in document:
                        public_name = document["public_name"].replace("'", '"')

                    else:
                        public_name = ""

                    if "avatar_big_img" in document:
                        avatar_big_img = document[
                            "avatar_big_img"]

                    else:
                        avatar_big_img = ""

                    if "avatar_small_img" in document:
                        avatar_small_img = document[
                            "avatar_small_img"]

                    else:
                        avatar_small_img = ""

                    if "qrcode_img" in document:
                        qrcode_img = document["qrcode_img"]

                    else:
                        qrcode_img = ""

                    if "account_cert" in document:
                        account_cert = document["account_cert"]

                    else:
                        account_cert = 0

                    if "account_cert_info" in document:
                        account_cert_info = document[
                            "account_cert_info"].replace("'", '"')

                    else:
                        account_cert_info = ""

                    if "account_short_desc" in document:
                        account_short_desc = document[
                            "account_short_desc"].replace("'", '"')

                    else:
                        account_short_desc = ""

                    if "data_analysis" in document:
                        m_head_avg_view_cnt = document[
                            "data_analysis"][-1]["m_head_avg_view_cnt"]

                        m_head_avg_like_cnt = document[
                            "data_analysis"][-1]["m_head_avg_like_cnt"]

                        m_avg_view_cnt = document[
                            "data_analysis"][-1]["m_avg_view_cnt"]

                        m_avg_like_cnt = document[
                            "data_analysis"][-1]["m_avg_like_cnt"]

                        m_10w_article_total_cnt = document[
                            "data_analysis"][-1]["m_10w_article_total_cnt"]

                        m_wmi = document["data_analysis"][-1]["m_wom_index"]

                        w_wmi = document["data_analysis"][-1]["w_wom_index"]

                        # y_wmi = document["data_analysis"][-1]["y_wom_index"]

                        # y_head_avg_view_cnt = document[
                        #     "data_analysis"][-1]["y_head_avg_view_cnt"]

                        # y_head_avg_like_cnt = document[
                        #     "data_analysis"][-1]["y_head_avg_like_cnt"]
                        # #source_column_dict[
                        #     "y_head_avg_like_cnt"] = y_head_avg_like_cnt
                        # y_avg_view_cnt = document[
                        #     "data_analysis"][-1]["y_avg_view_cnt"]
                        #source_column_dict["y_avg_view_cnt"] = y_avg_view_cnt
                        # y_avg_like_cnt = document[
                        #     "data_analysis"][-1]["y_avg_like_cnt"]
                        #source_column_dict["y_avg_like_cnt"] = y_avg_like_cnt
                        # y_10w_article_total_cnt = document[
                        #     "data_analysis"][-1]["y_10w_article_total_cnt"]
                        # source_column_dict[
                        # "y_10w_article_total_cnt"] = y_10w_article_total_cnt
                        # total_article_cnt = document[
                        #     "data_analysis"][-1]["article_total_cnt"]

                        latest_article_post_date = document[
                            "data_analysis"][-1]["max_post_time"]
                        # source_column_dict[
                        # "latest_article_post_date"] = latest_article_post_date
                        # oldest_article_post_date = document[
                        #     "data_analysis"][-1]["min_post_time"]
                        # source_column_dict[
                        #    "oldest_article_post_date"] = oldest_article_post_date
                        # self.source_mysql_tool.update_one("media_weixin",#source_column_dict,"LOWER()")
                        query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s',`account_short_desc`='%s',`m_avg_view_cnt`=%d,`m_avg_like_cnt`=%d,`m_head_avg_view_cnt`=%d,`m_head_avg_like_cnt`=%d,`m_10w_article_total_cnt`=%d,`m_wmi`=%.2f,`w_wmi`=%.2f,`latest_article_post_date`=%d,`real_public_id`='%s' WHERE LOWER(`public_id`)='%s'" % (
                            public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, m_avg_view_cnt, m_avg_like_cnt, m_head_avg_view_cnt, m_head_avg_like_cnt, m_10w_article_total_cnt, m_wmi, w_wmi, latest_article_post_date, public_id_mongo, public_id_mongo.lower()))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                            count += 1

                        except Exception as e:
                            error_count += 1
                            error_weixin_id_list.append(public_id_mongo)
                            continue

                    else:
                        query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s' ,`account_short_desc`='%s',`real_public_id`='%s' WHERE LOWER(`public_id`)='%s'" % (
                            public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, public_id_mongo, public_id_mongo.lower()))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                            count += 1
                        except Exception as e:
                            error_count += 1
                            print("error_count:" + str(error_count))
                            error_weixin_id_list.append(public_id_mongo)
                            continue
                    print("completed " + str(float('%.2f' %
                                                   (float(count) / len(public_id_list_mongo)))))
        finally:
            source_mongo_tool.disconnection()
            target_mysql_tool.disconnection()
        print("correct number:" + str(count))
        print("insert completed " + str(insert_count))
        print("error_number:" + str(error_count))

    def media_weixin_article_duplicate_delete(self):
        source_file = self.source_file_tool.get_file()
        count = 0
        for line in source_file:
            weixin_id = self.source_file_tool.line_strip_decode_ignore(line)
            self.source_mongo_tool.database["media_weixin_article"].remove(
                {"weixin_id": {"$regex": weixin_id, "$options": 'i'}})
            count += 1
            print("completed " + str(count))

    def public_id_mysql_update(self):
        public_id_list_mongo = self.source_mongo_tool.get_unique_id_list(
            "media_weixin", {}, ["public_id", ])
        item_count = 0
        for public_id_mongo in public_id_list_mongo:
            # print(public_id_mongo.lower())
            # input("...........")
            public_id_mongo_lower = public_id_mongo.lower()
            query = ("UPDATE `media_weixin` SET `public_id`='%s' WHERE LOWER(`public_id`)='%s'" % (
                public_id_mongo, public_id_mongo_lower))
            print(query)
            self.source_mysql_tool.cursor.execute(query)
            self.source_mysql_tool.cnx.commit()
            item_count += 1
            print("completed %.2f" %
                  (float(item_count) / len(public_id_list_mongo)))

    def public_id_mongo_update(self):
        public_id_list_mongo = self.source_mongo_tool.get_unique_id_list(
            "media_weixin", {}, ["public_id", ])
        # document = self.source_mongo_tool.database["media_weixin_article"].find_one({"_id": ObjectId("57d8b44b0c5d551beb82bf8d")})
        # print(str(type(document)))
        # input("...............")
        count = 0
        for public_id_mongo in public_id_list_mongo:
            # print(public_id_mongo)
            document_list = self.source_mongo_tool.database["media_weixin_article"].find(
                {"weixin_id": {"$regex": public_id_mongo, "$options": 'i'}}, {"_id": 1})
            object_id_list = []
            for document in document_list:
                object_id_list.append(document["_id"])
                # print(str(type(document["_id"])))
                # count = count + 1
                # print(str(count))

            for object_id in object_id_list:
                print("update media_weixin_article where _id is " + str(object_id))
                # input(".........")
                self.source_mongo_tool.database["media_weixin_article"].update(
                    {"_id": object_id}, {"$set": {"weixin_id": public_id_mongo}})
            count = count + 1
            print("completed %.2f" % (float(count) / len(public_id_mongo)))

    def media_weixin_average_pv_update(self, source_mysql_args, source_table_args, target_mysql_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_unique_id_list = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        count = 0
        try:
            for source_unique_id in source_unique_id_list:
                row = source_mysql_tool.find_one('media_weixin', ["retail_price_m_1_min", "retail_price_m_2_min",
                                                                  "retail_price_m_3_min", "m_head_avg_view_cnt", "y_head_avg_view_cnt"], "id", source_unique_id, target_file_tool)
                retail_price_m_1_min,  retail_price_m_2_min,  retail_price_m_3_min, m_head_avg_view_cnt, y_head_avg_view_cnt = row
                if(retail_price_m_1_min == 0):
                    if(retail_price_m_2_min == 0):
                        if(retail_price_m_3_min == 0):
                            retail_avg_price = 0
                        else:
                            retail_avg_price = float(retail_price_m_3_min)
                    else:
                        retail_avg_price = float(retail_price_m_2_min)
                else:
                    retail_avg_price = float(retail_price_m_1_min)
                m_avg_price_pv = 0
                y_avg_price_pv = 0
                if m_head_avg_view_cnt != -1 and m_head_avg_view_cnt != 0:
                    m_avg_price_pv = retail_avg_price / m_head_avg_view_cnt
                if y_head_avg_view_cnt != -1 and y_head_avg_view_cnt != 0:
                    y_avg_price_pv = retail_avg_price / y_head_avg_view_cnt
                # print(str(avg_price_pv))
                # print(str(type(avg_price_pv)))
                # input("...................")
                source_column_dict = {}
                source_column_dict["m_avg_price_pv"] = m_avg_price_pv
                source_column_dict["y_avg_price_pv"] = y_avg_price_pv
                target_mysql_tool.update_one(
                    'media_weixin', source_column_dict, "id", source_unique_id, target_file_tool)
                count += 1
                print("completed %.2f" %
                      (float(count) / len(source_unique_id_list)))
        finally:
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()
            target_file_tool.close_file()

    def media_weixin_1_price_1_update(self, source_file_args, source_mysql_args, target_mysql_args, target_table_args):
        '''
        更新有price数据的账号信息到2.0
        '''
        target_file_tool_1 = self.get_file_tool(
            *(CHECK_CSV_PATH, 't_media_to_update.csv', 'ab'))
        target_file_tool_2 = self.get_file_tool(
            *(CHECK_CSV_PATH, 't_media_to_insert.csv', 'ab'))
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        '''media_weixin账号列表'''
        unique_id_list_mysql = target_mysql_tool.get_unique_id_list(
            *target_table_args)
        unique_id_list_mysql_lower = []

        for unique_id_mysql in unique_id_list_mysql:
            unique_id_list_mysql_lower.append(unique_id_mysql.lower())
        count = 0
        for line in source_file:
            if source_file_tool.line_strip_decode_ignore(line) is not u'':
                if source_file_tool.line_strip_decode_ignore(line).lower() in unique_id_list_mysql_lower:
                    target_file_tool_1.write_database_line_to_file(
                        source_file_tool.line_strip_decode_ignore(line))
                    count += 1
                    continue

                    query = ("SELECT `iEffectiveTime`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4` FROM `t_media` WHERE `sOpenName` like '%s'" %
                             source_file_tool.line_strip_decode_ignore(line))
                    source_mysql_tool.cursor.execute(query)
                    row = source_mysql_tool.cursor.fetchone()
                    print(str(row))
                    i_effective_time, i_price_1, i_price_2, i_price_3, i_price_4 = row
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min = float(
                        i_price_1), float(i_price_2), float(i_price_3), float(i_price_4)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min = orig_price_s_min * \
                        1.3, orig_price_m_1_min * 1.3, orig_price_m_2_min * 1.3, orig_price_m_3_min * 1.3

                    if i_effective_time is None:
                        i_effective_time = -1
                    query = ("UPDATE `media_weixin` SET `orig_price_s_min`=%.2f,`orig_price_m_1_min`=%.2f,`orig_price_m_2_min`=%.2f,`orig_price_m_3_min`=%.2f,`retail_price_s_min`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_3_min`=%.2f,`active_end_time`=%d WHERE LOWER(`public_id`)='%s'" % (
                        orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_effective_time, source_file_tool.line_strip_decode_ignore(line).lower()))
                    print(("%d " % count) + query)
                    # input("........................")
                    target_mysql_tool.cursor.execute(query)
                else:
                    target_file_tool_2.write_database_line_to_file(
                        source_file_tool.line_strip_decode_ignore(line))
                    count += 1
                    continue
                    '''t_media'''

                    query = ("SELECT `iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iReadNum`,`iCreateType`,`iPublishType`,`iEffectiveTime`,`iWomSectTop` FROM `t_media` WHERE `sOpenName` like '%{0}%'".format(
                        source_file_tool.line_strip_decode_ignore(line)))
                    print(query)
                    source_mysql_tool.cursor.execute(query)
                    row = source_mysql_tool.cursor.fetchone()
                    i_media_id, i_user_id, s_media_name, i_follower_num, i_price_1, i_price_2, i_price_3, i_price_4, i_read_num, i_create_type, i_publish_type, i_effective_time, i_wom_sect_top = row
                    if i_user_id == 0:
                        continue
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min = float(
                        i_price_1), float(i_price_2), float(i_price_3), float(i_price_4)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min = orig_price_s_min * \
                        1.3, orig_price_m_1_min * 1.3, orig_price_m_2_min * 1.3, orig_price_m_3_min * 1.3
                    if i_effective_time is None:
                        i_effective_time = -1
                    '''t_user'''
                    query = (
                        "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iUserID`=%d" % i_user_id)
                    source_mysql_tool.cursor.execute(query)
                    row = source_mysql_tool.cursor.fetchone()
                    s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                    if s_email is None:
                        continue
                    if s_mobile is None:
                        s_mobile = ""
                    if s_real_name is None:
                        s_real_name = ""
                    if s_co_name is None:
                        s_co_name = ""
                    if s_weixin is None:
                        s_weixin = ""
                    if s_qq is None:
                        s_qq = ""

                    '''wom_account'''
                    email_list_wom_account = []
                    query = (
                        "SELECT `login_account` FROM `wom_account` WHERE `type`=2")
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    uuid_wom_account = None
                    uuid_media_vendor = None
                    for row in target_mysql_tool.cursor:
                        (login_account,) = row
                        email_list_wom_account.append(login_account.strip())

                    if s_email in email_list_wom_account:
                        # input('s_email in email_list_wom_account')
                        query = (
                            "SELECT `uuid` FROM `wom_account` WHERE `login_account`='%s'" % s_email)
                        print(query)
                        target_mysql_tool.cursor.execute(query)
                        row = target_mysql_tool.cursor.fetchone()
                        (uuid_wom_account,) = row
                        query = (
                            "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % uuid_wom_account)
                        print(query)
                        target_mysql_tool.cursor.execute(query)
                        row = target_mysql_tool.cursor.fetchone()
                        (uuid_media_vendor,) = row
                    else:
                        input('s_email not in email_list_wom_account')
                        i_wom_account_uuid = uuid_generator()
                        i_wom_account_login_account = s_email
                        i_wom_account_login_password = "1234556"
                        i_wom_account_type = 2
                        i_wom_account_status = 1
                        i_wom_account_create_time = current_time_to_stamp()
                        query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                            i_wom_account_uuid, i_wom_account_login_account, i_wom_account_login_password, i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                        print(query)
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                        i_media_vendor_uuid = uuid_generator()
                        uuid_media_vendor = i_media_vendor_uuid
                        query = (
                            "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name, s_mobile, s_weixin, s_qq, s_co_name))
                        print(query)
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()

                    uuid_media_weixin = uuid_generator()
                    i_create_time_media_weixin = current_time_to_stamp()
                    i_put_up_media_weixin = 1
                    i_status_media_weixin = 1
                    i_s_pub_type_media_weixin = None
                    i_m_1_pub_type_media_weixin = None
                    i_m_2_pub_type_media_weixin = None
                    i_m_3_pub_type_media_weixin = None
                    if orig_price_s_min == 0:
                        i_s_pub_type_media_weixin = 0
                    else:
                        i_s_pub_type_media_weixin = i_create_type + 1
                    if orig_price_m_1_min == 0:
                        i_m_1_pub_type_media_weixin = 0
                    else:
                        i_m_1_pub_type_media_weixin = i_create_type + 1
                    if orig_price_s_min == 0:
                        i_m_2_pub_type_media_weixin = 0
                    else:
                        i_m_2_pub_type_media_weixin = i_create_type + 1
                    if orig_price_s_min == 0:
                        i_m_3_pub_type_media_weixin = 0
                    else:
                        i_m_3_pub_type_media_weixin = i_create_type + 1
                    query = (
                        "SELECT `iCityID` FROM `t_media_city` WHERE `iMediaID`=%d" % i_media_id)
                    print(query)
                    source_mysql_tool.cursor.execute(query)
                    i_city_id_list = []
                    for row in source_mysql_tool.cursor:
                        (i_city_id,) = row
                        if i_city_id not in i_city_id_list:
                            i_city_id_list.append(i_city_id)

                    i_city_name_list = []
                    for i_city_id in i_city_id_list:
                        query = (
                            "SELECT `sCityName` FROM `t_city` WHERE `iCityID`=%d" % i_city_id)
                        print(query)
                        source_mysql_tool.cursor.execute(query)
                        row = source_mysql_tool.cursor.fetchone()
                        (s_city_name,) = row
                        i_city_name_list.append(s_city_name)

                    i_city_id_joint = ""
                    for i_city_name in i_city_name_list:
                        if i_city_name in CITY:
                            i_city_id_joint = i_city_id_joint + \
                                '#' + str(CITY[i_city_name])
                        else:
                            i_city_id_joint = i_city_id_joint + '#' + str(36)
                    i_city_id_joint = i_city_id_joint + "#"
                    query = (
                        "SELECT `iCategoryID` FROM `t_media_category` WHERE `iMediaID`=%d" % i_media_id)
                    print(query)
                    source_mysql_tool.cursor.execute(query)
                    i_category_id_list = []
                    for row in source_mysql_tool.cursor:
                        (i_category_id,) = row
                        if i_category_id not in i_category_id_list:
                            i_category_id_list.append(i_category_id)
                    i_s_name_joint = ""
                    for i_category_id in i_category_id_list:
                        query = (
                            "SELECT `sName` FROM `t_domain` WHERE `iAutoID`=%d" % i_category_id)
                        print(query)
                        source_mysql_tool.cursor.execute(query)
                        row = source_mysql_tool.cursor.fetchone()
                        (s_name,) = row
                        if s_name in CATEGORY:
                            i_s_name_joint = i_s_name_joint + "#" + str(s_name)
                        else:
                            i_s_name_joint = i_s_name_joint + "#" + str(26)

                    i_s_name_joint = i_s_name_joint + "#"
                    i_has_pref_vendor = 1
                    i_is_activated = 1
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % uuid_wom_account)
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (i_pref_vendor_uuid,) = row
                    i_has_origin_pub = None
                    if i_create_type == 1:
                        i_has_origin_pub = 1
                    else:
                        i_has_origin_pub = 0
                    i_has_direct_pub = None
                    if i_create_type == 0:
                        i_has_direct_pub = 1
                    else:
                        i_has_direct_pub = 0
                    i_vendor_cnt = 2
                    query = (
                        "INSERT INTO `media_weixin`(`uuid`,`public_name`,`public_id`,`follower_num`,`put_up`,`status`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`has_pref_vendor`,`pref_vendor_uuid`,`in_wom_rank`,`create_time`,`is_activated`,`has_origin_pub`,`has_direct_pub`,`vendor_cnt`,`active_end_time`) VALUES('%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,'%s',%d,%d,%d,%d,%d,%d,%d)" % (uuid_media_weixin, s_media_name, source_file_tool.line_strip_decode_ignore(line), i_follower_num, i_put_up_media_weixin, i_status_media_weixin, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_has_pref_vendor, i_pref_vendor_uuid, i_wom_sect_top, i_create_time_media_weixin, i_is_activated, i_has_origin_pub, i_has_direct_pub, i_vendor_cnt, i_effective_time))
                    print(query)

                    try:
                        # target_mysql_tool.cursor.execute(query)
                        # target_mysql_tool.cnx.commit()
                        uuid_media_vendor_bind_other = uuid_generator()
                        i_media_type = 1
                        i_is_activated = 1
                        i_status = 1
                        i_is_pref_vendor = 0
                        query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`media_cate`,`follower_area`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,'%s','%s',%d,%d,%d,%d)" % (
                            uuid_media_vendor_bind_other, i_media_type, uuid_media_weixin, uuid_media_vendor, i_is_activated, i_status, i_is_pref_vendor, i_s_name_joint, i_city_id_joint, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time_media_weixin))
                        print(query)
                        # target_mysql_tool.cursor.execute(query)
                        # target_mysql_tool.cnx.commit()
                        uuid_media_vendor_bind_qianma = uuid_generator()
                        i_media_type = 1
                        i_is_activated = 1
                        i_status = 1
                        i_is_pref_vendor = 1
                        default_vendor = None
                        query = (
                            "SELECT `uuid` FROM `media_vendor` WHERe `default_vendor` = 1")
                        target_mysql_tool.cursor.execute(query)
                        (default_vendor,) = target_mysql_tool.cursor.fetchone()
                        query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`media_cate`,`follower_area`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,'%s','%s',%d,%d,%d,%d)" % (
                            uuid_media_vendor_bind_qianma, i_media_type, uuid_media_weixin, default_vendor, i_is_activated, i_status, i_is_pref_vendor, i_s_name_joint, i_city_id_joint, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time_media_weixin))
                        print(query)
                        # target_mysql_tool.cursor.execute(query)
                        # target_mysql_tool.cnx.commit()
                        uuid_media_vendor_weixin_price_list = uuid_generator()
                        i_deposit_percent_config = '{"pos_s":0.5,"pos_m_1":0.5,"pos_m_2":0.5,"pos_m_3":0.5}'
                        i_serve_percent_config = '{"pos_s":0.4,"pos_m_1":0.4,"pos_m_2":0.4,"pos_m_3":0.4}'
                        query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s')" % (
                            uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_other, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"')))
                        print(query)
                        # target_mysql_tool.cursor.execute(query)
                        # target_mysql_tool.cnx.commit()
                        query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s')" % (
                            uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_qianma, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"')))
                        print(query)
                        # target_mysql_tool.cursor.execute(query)
                        # target_mysql_tool.cnx.commit()
                        # input("help me...........")
                    except Exception as e:
                        print('%s has inserted yet' %
                              source_file_tool.line_strip_decode_ignore(line))
        print(("%d " % count))

    def media_vendor_bind_duplicate(self, source_mysql_args_1, source_mysql_args_2, target_mysql_args_1, target_mysql_args_2, error_file_args):
        '''
        media_vendor_bind 表冗余处理
        '''
        error_file_tool = self.get_file_tool(*error_file_args)
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        target_mysql_tool_1 = self.get_mysql_tool(*target_mysql_args_1)
        target_mysql_tool_2 = self.get_mysql_tool(*target_mysql_args_2)
        query = ("SELECT `uuid` FROM `media_weixin`")
        unique_id_list_mysql_1 = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        query = ("SELECT `media_uuid` FROM `media_vendor_bind`")
        unique_id_list_mysql_2 = source_mysql_tool_2.get_unique_id_list_by_sql(
            query)
        count_unique_id_list_1_not_2 = 0

        '''在media_weixin 但不在media_vendor_bind'''
        for unique_id in unique_id_list_mysql_1:
            if unique_id not in unique_id_list_mysql_2:
                count_unique_id_list_1_not_2 += 1
                query = (
                    "SELECT `media_cate`,`follower_area`,`follower_num`,`has_origin_pub`,`has_direct_pub`, `s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`FROM `media_weixin` WHERE `uuid`='%s'" % unique_id)
                i_s_name_joint, i_city_id_joint, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = source_mysql_tool_1.find_one_by_sql(
                    query, error_file_tool)
                uuid_media_vendor_bind_qianma = uuid_generator()
                i_media_type = 1
                i_is_activated = 1
                i_status = 1
                i_is_pref_vendor = 1
                i_create_time = current_time_to_stamp()
                query = (
                    "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor` = 1")
                (i_media_vendor_uuid,) = source_mysql_tool_1.find_one_by_sql(
                    query, error_file_args)
                query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`media_cate`,`follower_area`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,'%s','%s',%d,%d,%d,%d)" % (
                    uuid_media_vendor_bind_qianma, i_media_type, unique_id, i_media_vendor_uuid, i_is_activated, i_status, i_is_pref_vendor, i_s_name_joint, i_city_id_joint, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time))
                # print(query)
                target_mysql_tool_1.insert_by_sql(query, error_file_tool)
                uuid_media_vendor_weixin_price_list = uuid_generator()
                i_deposit_percent_config = '{"pos_s":0.5,"pos_m_1":0.5,"pos_m_2":0.5,"pos_m_3":0.5}'
                i_serve_percent_config = '{"pos_s":0.4,"pos_m_1":0.4,"pos_m_2":0.4,"pos_m_3":0.4}'
                query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f)" % (
                    uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_qianma, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"'), orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max))
                target_mysql_tool_2.insert_by_sql(query, error_file_tool)
                # print(query)
                # input("help me..........")
        print("count_unique_id_list_1_not_2 %d" % count_unique_id_list_1_not_2)

    def media_vendor_bind_duplicate_again(self, source_mysql_args_1, source_mysql_args_2, target_mysql_args_1, target_mysql_args_2, error_file_args):
        '''
            media_vendor_bind 表冗余处理
        '''
        error_file_tool = self.get_file_tool(*error_file_args)
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        target_mysql_tool_1 = self.get_mysql_tool(*target_mysql_args_1)
        target_mysql_tool_2 = self.get_mysql_tool(*target_mysql_args_2)
        query = ("SELECT `uuid` FROM `media_weixin`")
        unique_id_list_mysql_1 = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        query = ("SELECT `media_uuid` FROM `media_vendor_bind`")
        unique_id_list_mysql_2 = source_mysql_tool_2.get_unique_id_list_by_sql(
            query)
        count_unique_id_list_2_not_1 = 0
        unique_id_list_2_not_1_list = []
        '''在media_vendor_bind 但不在media_weixin'''
        for unique_id in unique_id_list_mysql_2:
            if unique_id not in unique_id_list_mysql_1:
                unique_id_list_2_not_1_list.append(unique_id)
        for unique_id in unique_id_list_2_not_1_list:
            count_unique_id_list_2_not_1 += 1
            print('unique_id_list_2_not_1 %s' % unique_id)
            query = (
                "SELECT `uuid` FROM `media_vendor_bind` WHERE `media_uuid`='%s'" % unique_id)
            media_vendor_bind_uuid_list = source_mysql_tool_2.get_unique_id_list_by_sql(
                query)
            for media_vendor_bind_uuid in media_vendor_bind_uuid_list:
                query = (
                    "DELETE FROM `media_vendor_weixin_price_list` WHERE `bind_uuid`='%s'" % media_vendor_bind_uuid)
                target_mysql_tool_1.delete_by_sql(query, error_file_tool)
            query = (
                "DELETE FROM `media_vendor_bind` WHERE `media_uuid` = '%s'" % unique_id)
            target_mysql_tool_2.delete_by_sql(query, error_file_tool)
            print("count %d;completed %.2f" % (len(unique_id_list_2_not_1_list),
                                               (count_unique_id_list_2_not_1 / len(unique_id_list_2_not_1_list))))
        print("count_unique_id_list_2_not_1 %d" % count_unique_id_list_2_not_1)

    def media_weixin_follower_area_update(self, source_mysql_args, target_mysql_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        # query = ("SELECT `public_id` FROM `media_weixin` WHERE
        # `public_name`!=''")
        query = ("SELECT `id` FROM `media_weibo` WHERE `weibo_name`!=''")
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        count = 0
        for unique_id_mysql in unique_id_list_mysql:
            # query = (
            #    "SELECT `public_name` FROM `media_weixin` WHERE `public_id`='%s'" % unique_id_mysql)
            query = (
                "SELECT `weibo_name` FROM `media_weibo` WHERE `id`=%d" % unique_id_mysql)
            (public_name,) = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)
            print(str(type(public_name)))
            follower_area = ""
            media_cate_list = []
            for k, v in CITY.items():
                # print(str(type(k)))
                # input("............")
                if k in public_name:
                    if CITY[k] not in media_cate_list:
                        media_cate_list.append(CITY[k])
            for v in media_cate_list:
                follower_area = follower_area + "#" + str(v)
            follower_area = follower_area + "#"
            if follower_area is not "#":
                count += 1
                # query = ("UPDATE `media_weixin` SET `follower_area`='%s' WHERE `public_id`='%s'" % (
                #    follower_area, unique_id_mysql))
                query = ("UPDATE `media_weibo` SET `follower_area`='%s' WHERE `id`=%d" % (
                    follower_area, unique_id_mysql))
                target_mysql_tool.update_by_sql(query, error_file_tool)
            else:
                follower_area = "#0#"
                count += 1
                # query = (("UPDATE `media_weixin` SET `follower_area`='%s' WHERE `public_id`='%s'" % (
                #    follower_area, unique_id_mysql)))
                query = (("UPDATE `media_weibo` SET `follower_area`='%s' WHERE `id`=%d" % (
                    follower_area, unique_id_mysql)))
                target_mysql_tool.update_by_sql(query, error_file_tool)
        print("UPDATE %d items" % count)

    def media_weixin_contact_info_update(self, source_mysql_args, target_mysql_args, error_file_args):
        '''
            更新media_weixin.contact_info字段
        '''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `uuid` FROM `media_vendor`")
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for unique_id in unique_id_list_mysql:
            update_time = current_time_to_stamp()
            query = (
                "SELECT `contact_person`,`contact1`,`weixin`,`qq` FROM `media_vendor` WHERE `uuid`='%s'" % unique_id)
            contact_person, contact1, weixin, qq = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)
            contact_info = {}
            contact_info['contact_person'] = contact_person
            contact_info['contact_phone'] = contact1
            contact_info['weixin'] = weixin
            contact_info['qq'] = qq
            contact_info['add_time'] = update_time
            contact_info_list = []
            contact_info_list.append(contact_info)
            query = ("UPDATE `media_vendor` SET `contact_info`='%s' WHERE `uuid`='%s'" % (
                str(contact_info_list).replace("'", '"'), unique_id))
            target_mysql_tool.update_by_sql(query, error_file_tool)

    def delete_duplicate_date_from_transfer(self, source_mysql_args, target_mysql_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        '''找到插入的uuid_wom_account'''
        count_delete_media_vendor_weixin_price_list = 0
        count_delete_media_vendor_bind = 0
        count_delete_media_weixin = 0
        count_delete_media_vendor = 0
        count_delete_wom_account = 0
        error_count = 0
        query = ("SELECT `uuid` FROM `wom_account` WHERE `login_password`='1234556'")
        uuid_wom_account_list = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for uuid_wom_account in uuid_wom_account_list:
            '''根据uuid_wom_account找到插入的uuid_media_vendor'''
            query = (
                "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid` = '%s'" % uuid_wom_account)

            (uuid_media_vendor,) = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)
            '''根据uuid_media_vendor找到插入的uuid_media_weixin'''
            query = (
                "SELECT `media_uuid` FROM `media_vendor_bind` WHERE `vendor_uuid`='%s'" % uuid_media_vendor)
            try:
                '''
                (uuid_media_weixin,) = source_mysql_tool.find_one_by_sql(
                    query, error_file_tool)
                '''
                '''根据uuid_media_weixin找到插入的uuid_media_vendor_bind'''
                '''
                query = (
                    "SELECT `uuid` FROM `media_vendor_bind` WHERE `media_uuid`='%s'" % uuid_media_weixin)
                uuid_media_vendor_bind_list = source_mysql_tool.get_unique_id_list_by_sql(
                    query)

                for uuid_media_vendor_bind in uuid_media_vendor_bind_list:

                    count_delete_media_vendor_weixin_price_list += 1
                    query = ("DELETE FROM `media_vendor_weixin_price_list` WHERE `bind_uuid`='%s'" %
                             uuid_media_vendor_bind)
                    target_mysql_tool.delete_by_sql(query,error_file_tool)
                count_delete_media_vendor_bind += 2
                query = (
                    "DELETE FROM `media_vendor_bind` WHERE `media_uuid`='%s'" % uuid_media_weixin)

                target_mysql_tool.delete_by_sql(query,error_file_tool)
                count_delete_media_weixin += 1
                query = ("DELETE FROM `media_weixin` WHERE `uuid`='%s'" %
                         uuid_media_weixin)
                target_mysql_tool.delete_by_sql(query,error_file_tool)
                count_delete_media_vendor += 1
                '''
                query = ("DELETE FROM `media_vendor` WHERE `uuid`= '%s'" %
                         uuid_media_vendor)
                # target_mysql_tool.delete_by_sql(query,error_file_tool)
                count_delete_wom_account += 1
                query = ("DELETE FROM `wom_account` WHERE `uuid`='%s'" %
                         uuid_wom_account)
                target_mysql_tool.delete_by_sql(query, error_file_tool)
            except Exception as e:
                error_count += 1
        print("error_count %d;count %d" %
              (error_count, len(uuid_wom_account_list)))

        print("%d-%d-%d-%d-%d" % (count_delete_media_vendor_weixin_price_list, count_delete_media_vendor_bind,
                                  count_delete_media_weixin, count_delete_media_vendor, count_delete_wom_account))

    def media_weixin_task_account_level_update(self, source_mysql_args, target_mysql_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `public_id` FROM `media_weixin` WHERE `y_wmi`> 1000")
        source_unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        count = 0
        for unique_id in source_unique_id_list:
            count += 1
            query = (
                "UPDATE `sogou_weixin_account_init_task` SET `account_level`=1 WHERE LOWER(`account_id`)='%s'" % unique_id.lower())
            target_mysql_tool.update_by_sql(query, error_file_tool)
            query = (
                "UPDATE `sogou_weixin_article_grow_trend_task` SET `enable_task`=1 WHERE LOWER(`account_id`)='%s'" % unique_id.lower())
            target_mysql_tool.update_by_sql(query, error_file_tool)
            print("completed %.2f" % (count / len(source_unique_id_list)))
        print("y_wom > 1000 的数目为 %d" % count)

    def media_vendor_duplicate(self, source_mysql_args_1, source_mysql_args_2, target_mysql_args, error_file_args):
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        query = ("SELECT `uuid` FROM `media_vendor`")
        uuid_list_media_vendor = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        query = ("SELECT `vendor_uuid` FROM `media_vendor_bind`")
        vendor_uuid_list_media_vendor_bind = source_mysql_tool_2.get_unique_id_list_by_sql(
            query)
        count = 0
        for uuid_media_vendor in vendor_uuid_list_media_vendor_bind:
            if uuid_media_vendor not in uuid_list_media_vendor:
                count += 1
                '''
                query = (
                    "UPDATE `media_vendor` SET `etl_status`=0 WHERE `uuid`='%s'" % uuid_media_vendor)
                target_mysql_tool.update_by_sql(query,error_file_tool)
                '''
                print("%s" % uuid_media_vendor)
        print("共处理 %d 条数据" % count)

    def media_weixin_media_cate_update(self, source_mysql_args, target_mysql_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `id` FROM `weixin_article` WHERE `public_id`!=''")
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        count = 0
        for unique_id_mysql in unique_id_list_mysql:
            query = (
                "SELECT `public_name` FROM `weixin_article` WHERE `id`=%d" % unique_id_mysql)
            (public_name,) = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)
            print(str(type(public_name)))

            follower_area = ""
            media_cate_list = []
            for k, v in CATEGORY_SPLIT.items():
                # print(str(type(k)))
                # input("............")
                if k in public_name:
                    if CATEGORY_SPLIT[k] not in media_cate_list:
                        media_cate_list.append(CATEGORY_SPLIT[k])
            for v in media_cate_list:
                follower_area = follower_area + "#" + str(v)
            follower_area = follower_area + "#"
            if follower_area is not "#":
                count += 1
                query = ("UPDATE `weixin_article` SET `media_cate`='%s' WHERE `id`=%d" % (
                    follower_area, unique_id_mysql))
                target_mysql_tool.update_by_sql(query, error_file_tool)
            else:
                follower_area = "#26#"
                count += 1
                query = (("UPDATE `weixin_article` SET `media_cate`='%s' WHERE `id`=%d" % (
                    follower_area, unique_id_mysql)))
                target_mysql_tool.update_by_sql(query, error_file_tool)
        print("UPDATE %d items" % count)

    def media_weixin_1_price_1_update_upgrade(self, source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args):
        '''
        把1.0的信息迁移到2.0
        只做INSERT操作
        '''
        count_correct = 0
        count_error = 0
        source_file_tool_1 = self.get_file_tool(*source_file_args_1)
        source_file_tool_2 = self.get_file_tool(*source_file_args_2)
        error_file_tool = self.get_file_tool(*error_file_args)
        '''
        更新操作的2.0数据库连接
        '''
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        '''
        1.0数据库连接
        '''
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        '''
            distinct账号列表
        '''
        source_file_1 = source_file_tool_1.get_file()
        '''
            duplicate 账号列表
        '''
        source_file_2 = source_file_tool_2.get_file()
        source_file_2_line_list = []
        for line in source_file_2:
            source_file_2_line_list.append(
                source_file_tool_2.line_strip_decode_ignore(line))
        wom_account_login_account_list = []
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        count = 0
        for line in source_file_1:
            '''
            对于distinct账号列表中的数据，判断是否出现在duplicate列表中，如果不存在，则做迁移
            '''
            if source_file_tool_1.line_strip_decode_ignore(line) not in source_file_2_line_list:
                count += 1
                # if source_file_tool_1.line_strip_decode_ignore(line) not in ['mrbrand888','qiluwanbao002','heimayingxiao1','hitonghua','miss_shopping_li']:
                #    continue
                # if count >= 50:
                #    continue
                # continue
                '''
                迁移t_media表
                '''
                i_status = None
                i_media_id = None
                i_user_id = None
                s_media_name = None
                i_follower_num = None
                i_price_1 = None
                i_price_2 = None
                i_price_3 = None
                i_price_4 = None
                i_price_5 = None
                i_price_6 = None
                i_price_7 = None
                i_price_8 = None
                i_price_11 = None
                i_price_12 = None
                i_price_13 = None
                i_price_14 = None
                d_price_1_l = None
                d_price_1_h = None
                d_price_2_l = None
                d_price_2_h = None
                d_price_3_l = None
                d_price_3_h = None
                d_price_4_l = None
                d_price_4_h = None
                d_price_5_l = None
                d_price_5_h = None
                d_price_6_l = None
                d_price_6_h = None
                d_price_7_l = None
                d_price_7_h = None
                d_price_8_l = None
                d_price_8_h = None
                d_price_11 = None
                d_price_12 = None
                d_price_13 = None
                d_price_14 = None

                i_read_num = None
                i_create_type = None
                i_publish_type = None
                i_effective_time = None
                i_wom_sect_top = None
                i_beizhu = None
                i_instroduction = None
                query = ("SELECT `iStatus`,`iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14`,`dPrice1L`,`dPrice1H`,`dPrice2L`,`dPrice2H`,`dPrice3L`,`dPrice3H`,`dPrice4L`,`dPrice4H`,`dPrice5L`,`dPrice5H`,`dPrice6L`,`dPrice6H`,`dPrice7L`,`dPrice7H`,`dPrice8L`,`dPrice8H`,`dPrice11`,`dPrice12`,`dPrice13`,`dPrice14`,`iReadNum`,`iCreateType`,`iPublishType`,`iEffectiveTime`,`iWomSectTop`,`iBeizhu`,`sIntroduction` FROM `t_media` WHERE LOWER(`sOpenName`)= '%s' " %
                         source_file_tool_1.line_strip_decode_ignore(line))
                row = None
                print(query)
                try:
                    source_mysql_tool_1.cursor.execute(query)
                    row = source_mysql_tool_1.cursor.fetchone()
                except Exception as e:
                    '''查询错误，记录并执行下一条'''
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '在t_media中查找失败')
                    continue
                if row is not None:
                    i_status, i_media_id, i_user_id, s_media_name, i_follower_num, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14, d_price_1_l, d_price_1_h, d_price_2_l, d_price_2_h, d_price_3_l, d_price_3_h, d_price_4_l, d_price_4_h, d_price_5_l, d_price_5_h, d_price_6_l, d_price_6_h, d_price_7_l, d_price_7_h, d_price_8_l, d_price_8_h, d_price_11, d_price_12, d_price_13, d_price_14, i_read_num, i_create_type, i_publish_type, i_effective_time, i_wom_sect_top, i_beizhu, i_instroduction = row

                    if i_effective_time is None:
                        i_effective_time = -1
                    if i_beizhu is None:
                        i_beizhu = ''
                    if i_instroduction is None:
                        i_instroduction = ''
                else:
                    continue
                orig_price_s_min = None
                orig_price_m_1_min = None
                orig_price_m_2_min = None
                orig_price_m_3_min = None
                retail_price_s_min = None
                retail_price_m_1_min = None
                retail_price_m_2_min = None
                retail_price_m_3_min = None
                execute_price_s = None
                execute_price_m_1 = None
                execute_price_m_2 = None
                execute_price_m_3 = None
                orig_price_s_max = None
                orig_price_m_1_max = None
                orig_price_m_2_max = None
                orig_price_m_3_max = None
                retail_price_s_max = None
                retail_price_m_1_max = None
                retail_price_m_2_max = None
                retail_price_m_3_max = None

                i_s_pub_type_media_weixin = None
                i_m_1_pub_type_media_weixin = None
                i_m_2_pub_type_media_weixin = None
                i_m_3_pub_type_media_weixin = None

                i_has_origin_pub = None
                i_has_direct_pub = None

                if i_create_type == 0:
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                        i_price_1), float(i_price_2), float(i_price_3), float(i_price_4), float(i_price_1), float(i_price_2), float(i_price_3), float(i_price_4)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                        i_price_5), float(i_price_6), float(i_price_7), float(i_price_8), float(i_price_5), float(i_price_6), float(i_price_7), float(i_price_8)
                    execute_price_s = float(i_price_11)
                    execute_price_m_1 = float(i_price_12)
                    execute_price_m_2 = float(i_price_13)
                    execute_price_m_3 = float(i_price_14)

                    if orig_price_s_min == 0:
                        i_s_pub_type_media_weixin = 0
                    else:
                        i_s_pub_type_media_weixin = 1

                    if orig_price_m_1_min == 0:
                        i_m_1_pub_type_media_weixin = 0
                    else:
                        i_m_1_pub_type_media_weixin = 1

                    if orig_price_m_2_min == 0:
                        i_m_2_pub_type_media_weixin = 0
                    else:
                        i_m_2_pub_type_media_weixin = 1
                    if orig_price_m_3_min == 0:
                        i_m_3_pub_type_media_weixin = 0
                    else:
                        i_m_3_pub_type_media_weixin = 1
                    if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                        i_has_direct_pub = 0
                    else:
                        i_has_direct_pub = 1
                    i_has_origin_pub = 0
                elif i_create_type == 1:
                    orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                        d_price_1_l), float(d_price_2_l), float(d_price_3_l), float(d_price_4_l), float(d_price_1_h), float(d_price_2_h), float(d_price_3_h), float(d_price_4_h)
                    retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                        d_price_5_l), float(d_price_6_l), float(d_price_7_l), float(d_price_8_l), float(d_price_5_h), float(d_price_6_h), float(d_price_7_h), float(d_price_8_h)
                    execute_price_s = float(d_price_11)
                    execute_price_m_1 = float(d_price_12)
                    execute_price_m_2 = float(d_price_13)
                    execute_price_m_3 = float(d_price_14)

                    if i_publish_type == 0:
                        if orig_price_s_min == 0:
                            i_s_pub_type_media_weixin = 0
                        else:
                            i_s_pub_type_media_weixin = 1

                        if orig_price_m_1_min == 0:
                            i_m_1_pub_type_media_weixin = 0
                        else:
                            i_m_1_pub_type_media_weixin = 1

                        if orig_price_m_2_min == 0:
                            i_m_2_pub_type_media_weixin = 0
                        else:
                            i_m_2_pub_type_media_weixin = 1

                        if orig_price_m_3_min == 0:
                            i_m_3_pub_type_media_weixin = 0
                        else:
                            i_m_3_pub_type_media_weixin = 1

                        if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                            i_has_direct_pub = 0
                        else:
                            i_has_direct_pub = 1
                        i_has_origin_pub = 0
                    elif i_publish_type == 1:
                        if orig_price_s_min == 0:
                            i_s_pub_type_media_weixin = 0
                        else:
                            i_s_pub_type_media_weixin = 2
                        if orig_price_m_1_min == 0:
                            i_m_1_pub_type_media_weixin = 0
                        else:
                            i_m_1_pub_type_media_weixin = 2
                        if orig_price_m_2_min == 0:
                            i_m_2_pub_type_media_weixin = 0
                        else:
                            i_m_2_pub_type_media_weixin = 2
                        if orig_price_m_3_min == 0:
                            i_m_3_pub_type_media_weixin = 0
                        else:
                            i_m_3_pub_type_media_weixin = 2
                        if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                            i_has_origin_pub = 0
                        else:
                            i_has_origin_pub = 1
                        i_has_direct_pub = 0
                # print("%.2f %.2f %.2f %.2f" % (orig_price_s_min,orig_price_m_1_min,orig_price_m_2_min,orig_price_m_3_min))
                # print("%d %d %d %d" % (i_s_pub_type_media_weixin,i_m_1_pub_type_media_weixin,i_m_2_pub_type_media_weixin,i_m_3_pub_type_media_weixin))
                # input('help me......')
                '''t_user'''
                s_email = None
                s_mobile = None
                s_real_name = None
                s_co_name = None
                s_weixin = None
                s_qq = None
                query = (
                    "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iType`=2 AND `iUserID`=%d" % i_user_id)
                row = None
                print(query)
                try:
                    source_mysql_tool_1.cursor.execute(query)
                    row = source_mysql_tool_1.cursor.fetchone()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '在t_user中查找失败')
                    continue
                i_media_vendor_uuid = None
                if row is not None:
                    s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                    if s_mobile is None:
                        s_mobile = ''
                    if s_real_name is None:
                        s_real_name = ''
                    if s_co_name is '':
                        s_co_name = s_real_name.strip()
                    if s_weixin is None:
                        s_weixin = ''
                    if s_qq is None:
                        s_qq = ''
                else:
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                    (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                        query, error_file_tool)
                    s_email = '1160154200@qq.com'
                if s_email is None:
                    error_file_tool.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line) + '供应商的email不存在')
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                    (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                        query, error_file_tool)
                    s_email = '1160154200@qq.com'

                '''
                wom_account/media_vendor表所需信息
                '''
                i_wom_account_uuid = uuid_generator()
                i_wom_account_login_account = s_email
                '''to do'''
                i_wom_account_login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                '''自媒体主'''
                i_wom_account_type = 2
                i_wom_account_status = 1
                i_wom_account_create_time = current_time_to_stamp()
                '''
                    如果媒体主信息不存在，则建立,并把email添加进email list;存在，则查询media_vendor_uuid
                '''
                if i_wom_account_login_account not in wom_account_login_account_list:
                    i_wom_account_uuid = uuid_generator()
                    wom_account_login_account_list.append(
                        i_wom_account_login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        i_wom_account_uuid, i_wom_account_login_account.strip(), i_wom_account_login_password.strip(), i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        error_file_tool.write_database_line_to_file(
                            i_wom_account_login_account + 'wom_account 插入操作失败')
                    i_media_vendor_uuid = uuid_generator()
                    query = (
                        "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name.strip(), s_mobile.strip(), s_weixin.strip(), s_qq, s_co_name.strip()))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        error_file_tool.write_database_line_to_file(
                            i_wom_account_login_account + 'media_vendor 插入操作失败')
                else:
                    query = (
                        "SELECT `uuid` FROM `wom_account` WHERE `type`=2 AND `login_account`='%s'" % s_email)
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (i_wom_account_uuid,) = row
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % i_wom_account_uuid)
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (i_media_vendor_uuid,) = row
                uuid_media_weixin = uuid_generator()
                i_create_time_media_weixin = current_time_to_stamp()
                i_last_update_time_media_weixin = i_create_time_media_weixin
                i_last_put_up_time_media_weixin = i_create_time_media_weixin
                i_last_verify_time_media_weixin = i_create_time_media_weixin
                i_status_media_weixin = None
                if i_status == 0:
                    i_status_media_weixin = 3
                elif i_status == 1:
                    i_status_media_weixin = 1
                elif i_status == 2:
                    i_status_media_weixin = 0
                elif i_status == 3:
                    i_status_media_weixin = 2
                i_put_up_media_weixin = None
                if i_effective_time > current_time_to_stamp():
                    i_put_up_media_weixin = 1
                else:
                    i_put_up_media_weixin = 0

                i_has_pref_vendor = 1
                i_is_activated = 1
                i_vendor_cnt = 1
                i_to_verify_vendor_cnt = 0
                query = (
                    "INSERT INTO `media_weixin`(`uuid`,`public_name`,`public_id`,`follower_num`,`put_up`,`status`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`has_pref_vendor`,`pref_vendor_uuid`,`cust_sort`,`create_time`,`is_activated`,`has_origin_pub`,`has_direct_pub`,`vendor_cnt`,`active_end_time`,`to_verify_vendor_cnt`,`last_update_time`,`last_put_up_time`,`comment`,`t_media_intro`,`last_verify_time`) VALUES('%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,'%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s','%s',%d)" % (uuid_media_weixin, str(s_media_name).strip(), source_file_tool_1.line_strip_decode_ignore(line), i_follower_num, i_put_up_media_weixin, i_status_media_weixin, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_has_pref_vendor, i_media_vendor_uuid, i_wom_sect_top, i_create_time_media_weixin, i_is_activated, i_has_origin_pub, i_has_direct_pub, i_vendor_cnt, i_effective_time, i_to_verify_vendor_cnt, i_last_update_time_media_weixin, i_last_put_up_time_media_weixin, i_beizhu.replace("'", '"'), i_instroduction.replace("'", '"'), i_last_verify_time_media_weixin))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        uuid_media_weixin + 'media_weixin 插入操作失败')

                uuid_media_vendor_bind_other = uuid_generator()
                i_media_type = 1
                i_is_activated = 1
                i_status = 1
                i_is_pref_vendor = 1
                query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,%d,%d,%d,%d)" % (
                    uuid_media_vendor_bind_other, i_media_type, uuid_media_weixin, i_media_vendor_uuid, i_is_activated, i_status, i_is_pref_vendor, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time_media_weixin))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    print('media_vendor_bind %s' %
                          uuid_media_vendor_bind_other)
                uuid_media_vendor_weixin_price_list = uuid_generator()
                i_deposit_percent_config = '{"pos_s":0.5,"pos_m_1":0.5,"pos_m_2":0.5,"pos_m_3":0.5}'
                i_serve_percent_config = '{"pos_s":0.4,"pos_m_1":0.4,"pos_m_2":0.4,"pos_m_3":0.4}'
                query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`active_end_time`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d)" % (
                    uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_other, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"'), orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_effective_time))
                print(query)
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    print('media_vendor_weixin_price_list %s ' %
                          uuid_media_vendor_weixin_price_list)
        print(("%d " % count))

    def media_weixin_public_id_update(self, source_mongo_args, source_collection_args, source_mysql_args, target_mongo_args, target_mysql_args, error_file_args):
        error_file_tool = self.get_file_tool(*error_file_args)
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        query = ("SELECT `public_id` FROM `media_weixin` WHERE `real_public_id` = ''")
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_mysql_tool.cursor.execute(query)
        unique_id_list_mysql = []
        for item in source_mysql_tool.cursor:
            (public_id,) = item
            unique_id_list_mysql.append(public_id)
        source_mysql_tool.disconnection()
        # unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
        #     query)
        unique_id_list_mysql_lower = []
        for unique_id_mysql in unique_id_list_mysql:
            unique_id_list_mysql_lower.append(unique_id_mysql.lower())
        count = 0
        try:
            for unique_id_mongo in unique_id_list_mongo:
                if unique_id_mongo.lower() in unique_id_list_mysql_lower:
                    count += 1
                    document = source_mongo_tool.database["media_weixin"].find_one({"public_id":unique_id_mongo})
                    if document is not None:
                        if "public_name" in document:
                            public_name = document["public_name"].replace("'", '"')
                        else:
                            public_name = ""

                        if "avatar_big_img" in document:
                            avatar_big_img = document[
                                "avatar_big_img"]
                        else:
                            avatar_big_img = ""

                        if "avatar_small_img" in document:
                            avatar_small_img = document[
                                "avatar_small_img"]
                        else:
                            avatar_small_img = ""

                        if "qrcode_img" in document:
                            qrcode_img = document["qrcode_img"]
                        else:
                            qrcode_img = ""

                        if "account_cert" in document:
                            account_cert = document["account_cert"]
                        else:
                            account_cert = 0

                        if "account_cert_info" in document:
                            account_cert_info = document[
                                "account_cert_info"].replace("'", '"')
                        else:
                            account_cert_info = ""

                        if "account_short_desc" in document:
                            account_short_desc = document[
                                "account_short_desc"].replace("'", '"')
                        else:
                            account_short_desc = ""

                    query = ("UPDATE `media_weixin` SET `real_public_id` ='%s',`public_name`='%s', `avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s',`account_short_desc`='%s' WHERE LOWER(`public_id`)='%s'" % (
                        unique_id_mongo, public_name, avatar_big_img,avatar_small_img,qrcode_img,account_cert,account_cert_info,account_short_desc,unique_id_mongo.lower()))
                    target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
                    target_mysql_tool.update_by_sql(query, error_file_tool)
                    target_mysql_tool.disconnection()
                    target_mongo_tool.database['media_weixin'].update(
                        {'public_id': unique_id_mongo}, {'$set': {'real_public_id_updated': 1}})
            print('settled %d records' % count)
        finally:
            error_file_tool.close_file()
            source_mongo_tool.disconnection()
            target_mongo_tool.disconnection()

    def media_weixin_1_price_1_duplicate_update_upgrade(self, source_file_args_1, source_file_args_2, source_mysql_args_1, target_mysql_args, error_file_args, target_file_args_1, target_file_args_2, target_file_args_3):
        '''
        把1.0的信息迁移到2.0
        只做INSERT操作
        处理重复数据
        '''
        count_correct = 0
        count_error = 0
        source_file_tool_1 = self.get_file_tool(*source_file_args_1)
        source_file_tool_2 = self.get_file_tool(*source_file_args_2)
        error_file_tool = self.get_file_tool(*error_file_args)
        '''
        更新操作的2.0数据库连接
        '''
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        '''
        1.0数据库连接
        '''
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        '''
            distinct账号列表
        '''
        source_file_1 = source_file_tool_1.get_file()
        '''
            duplicate 账号列表
        '''
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        target_file_tool_3 = self.get_file_tool(*target_file_args_3)
        source_file_2 = source_file_tool_2.get_file()
        source_file_2_line_list = []
        for line in source_file_2:
            source_file_2_line_list.append(
                source_file_tool_2.line_strip_decode_ignore(line))
        wom_account_login_account_list = []
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        count_1 = 0
        count_2 = 0
        count_3 = 0
        unique_id_list_put_up_1_list = []
        query = (
            "SELECT DISTINCT(`sOpenName`) FROM `t_media` WHERE `iPut`=1 AND `iMediaType`=1")

        unique_id_list_put_up_1_list = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        unique_id_list_put_up_1_list_lower = []
        for unique_id in unique_id_list_put_up_1_list:
            if unique_id.lower() not in unique_id_list_put_up_1_list_lower:
                unique_id_list_put_up_1_list_lower.append(unique_id.lower())
        for line in source_file_1:
            '''
            对于distinct账号列表中的数据，判断是否出现在duplicate列表中，如果不存在，则做迁移
            '''
            if source_file_tool_1.line_strip_decode_ignore(line) in source_file_2_line_list:
                '''
                既在重复列表中又在上架列表中
                '''
                if source_file_tool_1.line_strip_decode_ignore(line) in unique_id_list_put_up_1_list_lower:
                    query = ("SELECT COUNT(*) FROM `t_media` WHERE LOWER(`sOpenName`)='%s' AND `iPut`=1" %
                             source_file_tool_1.line_strip_decode_ignore(line))
                    source_mysql_tool_1.cursor.execute(query)
                    (item_count,) = source_mysql_tool_1.cursor.fetchone()
                    print(str(type(item_count)) + str(item_count))
                    if item_count == 1:
                        count_1 += 1
                        # if source_file_tool_1.line_strip_decode_ignore(line) not in ['mrbrand888','qiluwanbao002','heimayingxiao1','hitonghua','miss_shopping_li']:
                        #    continue
                        # if count >= 50:
                        #    continue
                        # continue
                        '''
                        迁移t_media表
                        '''
                        i_status = None
                        i_media_id = None
                        i_user_id = None
                        s_media_name = None
                        i_follower_num = None
                        i_price_1 = None
                        i_price_2 = None
                        i_price_3 = None
                        i_price_4 = None
                        i_price_5 = None
                        i_price_6 = None
                        i_price_7 = None
                        i_price_8 = None
                        i_price_11 = None
                        i_price_12 = None
                        i_price_13 = None
                        i_price_14 = None
                        d_price_1_l = None
                        d_price_1_h = None
                        d_price_2_l = None
                        d_price_2_h = None
                        d_price_3_l = None
                        d_price_3_h = None
                        d_price_4_l = None
                        d_price_4_h = None
                        d_price_5_l = None
                        d_price_5_h = None
                        d_price_6_l = None
                        d_price_6_h = None
                        d_price_7_l = None
                        d_price_7_h = None
                        d_price_8_l = None
                        d_price_8_h = None
                        d_price_11 = None
                        d_price_12 = None
                        d_price_13 = None
                        d_price_14 = None

                        i_read_num = None
                        i_create_type = None
                        i_publish_type = None
                        i_effective_time = None
                        i_wom_sect_top = None
                        i_beizhu = None
                        i_instroduction = None
                        query = ("SELECT `iStatus`,`iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14`,`dPrice1L`,`dPrice1H`,`dPrice2L`,`dPrice2H`,`dPrice3L`,`dPrice3H`,`dPrice4L`,`dPrice4H`,`dPrice5L`,`dPrice5H`,`dPrice6L`,`dPrice6H`,`dPrice7L`,`dPrice7H`,`dPrice8L`,`dPrice8H`,`dPrice11`,`dPrice12`,`dPrice13`,`dPrice14`,`iReadNum`,`iCreateType`,`iPublishType`,`iEffectiveTime`,`iWomSectTop`,`iBeizhu`,`sIntroduction` FROM `t_media` WHERE LOWER(`sOpenName`)= '%s' AND `iPut`=1" %
                                 source_file_tool_1.line_strip_decode_ignore(line))
                        row = None
                        print(query)
                        try:
                            source_mysql_tool_1.cursor.execute(query)
                            row = source_mysql_tool_1.cursor.fetchone()
                        except Exception as e:
                            '''查询错误，记录并执行下一条'''
                            error_file_tool.write_database_line_to_file(
                                source_file_tool_1.line_strip_decode_ignore(line) + '在t_media中查找失败')
                            continue
                        if row is not None:
                            i_status, i_media_id, i_user_id, s_media_name, i_follower_num, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14, d_price_1_l, d_price_1_h, d_price_2_l, d_price_2_h, d_price_3_l, d_price_3_h, d_price_4_l, d_price_4_h, d_price_5_l, d_price_5_h, d_price_6_l, d_price_6_h, d_price_7_l, d_price_7_h, d_price_8_l, d_price_8_h, d_price_11, d_price_12, d_price_13, d_price_14, i_read_num, i_create_type, i_publish_type, i_effective_time, i_wom_sect_top, i_beizhu, i_instroduction = row

                            if i_effective_time is None:
                                i_effective_time = -1
                            if i_beizhu is None:
                                i_beizhu = ''
                            if i_instroduction is None:
                                i_instroduction = ''
                        else:
                            continue
                        orig_price_s_min = None
                        orig_price_m_1_min = None
                        orig_price_m_2_min = None
                        orig_price_m_3_min = None
                        retail_price_s_min = None
                        retail_price_m_1_min = None
                        retail_price_m_2_min = None
                        retail_price_m_3_min = None
                        execute_price_s = None
                        execute_price_m_1 = None
                        execute_price_m_2 = None
                        execute_price_m_3 = None
                        orig_price_s_max = None
                        orig_price_m_1_max = None
                        orig_price_m_2_max = None
                        orig_price_m_3_max = None
                        retail_price_s_max = None
                        retail_price_m_1_max = None
                        retail_price_m_2_max = None
                        retail_price_m_3_max = None

                        i_s_pub_type_media_weixin = None
                        i_m_1_pub_type_media_weixin = None
                        i_m_2_pub_type_media_weixin = None
                        i_m_3_pub_type_media_weixin = None

                        i_has_origin_pub = None
                        i_has_direct_pub = None

                        if i_create_type == 0:
                            orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                                i_price_1), float(i_price_2), float(i_price_3), float(i_price_4), float(i_price_1), float(i_price_2), float(i_price_3), float(i_price_4)
                            retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                                i_price_5), float(i_price_6), float(i_price_7), float(i_price_8), float(i_price_5), float(i_price_6), float(i_price_7), float(i_price_8)
                            execute_price_s = float(i_price_11)
                            execute_price_m_1 = float(i_price_12)
                            execute_price_m_2 = float(i_price_13)
                            execute_price_m_3 = float(i_price_14)

                            if orig_price_s_min == 0:
                                i_s_pub_type_media_weixin = 0
                            else:
                                i_s_pub_type_media_weixin = 1

                            if orig_price_m_1_min == 0:
                                i_m_1_pub_type_media_weixin = 0
                            else:
                                i_m_1_pub_type_media_weixin = 1

                            if orig_price_m_2_min == 0:
                                i_m_2_pub_type_media_weixin = 0
                            else:
                                i_m_2_pub_type_media_weixin = 1
                            if orig_price_m_3_min == 0:
                                i_m_3_pub_type_media_weixin = 0
                            else:
                                i_m_3_pub_type_media_weixin = 1
                            if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                                i_has_direct_pub = 0
                            else:
                                i_has_direct_pub = 1
                            i_has_origin_pub = 0
                        elif i_create_type == 1:
                            orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max = float(
                                d_price_1_l), float(d_price_2_l), float(d_price_3_l), float(d_price_4_l), float(d_price_1_h), float(d_price_2_h), float(d_price_3_h), float(d_price_4_h)
                            retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max = float(
                                d_price_5_l), float(d_price_6_l), float(d_price_7_l), float(d_price_8_l), float(d_price_5_h), float(d_price_6_h), float(d_price_7_h), float(d_price_8_h)
                            execute_price_s = float(d_price_11)
                            execute_price_m_1 = float(d_price_12)
                            execute_price_m_2 = float(d_price_13)
                            execute_price_m_3 = float(d_price_14)

                            if i_publish_type == 0:
                                if orig_price_s_min == 0:
                                    i_s_pub_type_media_weixin = 0
                                else:
                                    i_s_pub_type_media_weixin = 1

                                if orig_price_m_1_min == 0:
                                    i_m_1_pub_type_media_weixin = 0
                                else:
                                    i_m_1_pub_type_media_weixin = 1

                                if orig_price_m_2_min == 0:
                                    i_m_2_pub_type_media_weixin = 0
                                else:
                                    i_m_2_pub_type_media_weixin = 1

                                if orig_price_m_3_min == 0:
                                    i_m_3_pub_type_media_weixin = 0
                                else:
                                    i_m_3_pub_type_media_weixin = 1

                                if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                                    i_has_direct_pub = 0
                                else:
                                    i_has_direct_pub = 1
                                i_has_origin_pub = 0
                            elif i_publish_type == 1:
                                if orig_price_s_min == 0:
                                    i_s_pub_type_media_weixin = 0
                                else:
                                    i_s_pub_type_media_weixin = 2
                                if orig_price_m_1_min == 0:
                                    i_m_1_pub_type_media_weixin = 0
                                else:
                                    i_m_1_pub_type_media_weixin = 2
                                if orig_price_m_2_min == 0:
                                    i_m_2_pub_type_media_weixin = 0
                                else:
                                    i_m_2_pub_type_media_weixin = 2
                                if orig_price_m_3_min == 0:
                                    i_m_3_pub_type_media_weixin = 0
                                else:
                                    i_m_3_pub_type_media_weixin = 2
                                if orig_price_s_min == 0 and orig_price_m_1_min == 0 and orig_price_m_2_min == 0 and orig_price_m_3_min == 0:
                                    i_has_origin_pub = 0
                                else:
                                    i_has_origin_pub = 1
                                i_has_direct_pub = 0
                        # print("%.2f %.2f %.2f %.2f" % (orig_price_s_min,orig_price_m_1_min,orig_price_m_2_min,orig_price_m_3_min))
                        # print("%d %d %d %d" % (i_s_pub_type_media_weixin,i_m_1_pub_type_media_weixin,i_m_2_pub_type_media_weixin,i_m_3_pub_type_media_weixin))
                        # input('help me......')
                        '''t_user'''
                        s_email = None
                        s_mobile = None
                        s_real_name = None
                        s_co_name = None
                        s_weixin = None
                        s_qq = None
                        query = (
                            "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iType`=2 AND `iUserID`=%d" % i_user_id)
                        row = None
                        print(query)
                        try:
                            source_mysql_tool_1.cursor.execute(query)
                            row = source_mysql_tool_1.cursor.fetchone()
                        except Exception as e:
                            error_file_tool.write_database_line_to_file(
                                source_file_tool_1.line_strip_decode_ignore(line) + '在t_user中查找失败')
                            continue
                        i_media_vendor_uuid = None
                        if row is not None:
                            s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                            if s_mobile is None:
                                s_mobile = ''
                            if s_real_name is None:
                                s_real_name = ''
                            if s_co_name is '':
                                s_co_name = s_real_name.strip()
                            if s_weixin is None:
                                s_weixin = ''
                            if s_qq is None:
                                s_qq = ''
                        else:
                            query = (
                                "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                            (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                                query, error_file_tool)
                            s_email = '1160154200@qq.com'
                        if s_email is None:
                            error_file_tool.write_database_line_to_file(
                                source_file_tool_1.line_strip_decode_ignore(line) + '供应商的email不存在')
                            query = (
                                "SELECT `uuid` FROM `media_vendor` WHERE `default_vendor`=1")
                            (i_media_vendor_uuid,) = target_mysql_tool.find_one_by_sql(
                                query, error_file_tool)
                            s_email = '1160154200@qq.com'

                        '''
                        wom_account/media_vendor表所需信息
                        '''
                        i_wom_account_uuid = uuid_generator()
                        i_wom_account_login_account = s_email
                        '''to do'''
                        i_wom_account_login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                        '''自媒体主'''
                        i_wom_account_type = 2
                        i_wom_account_status = 1
                        i_wom_account_create_time = current_time_to_stamp()
                        '''
                            如果媒体主信息不存在，则建立,并把email添加进email list;存在，则查询media_vendor_uuid
                        '''
                        if i_wom_account_login_account not in wom_account_login_account_list:
                            i_wom_account_uuid = uuid_generator()
                            wom_account_login_account_list.append(
                                i_wom_account_login_account)
                            query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                                i_wom_account_uuid, i_wom_account_login_account.strip(), i_wom_account_login_password.strip(), i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                            print(query)
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                error_file_tool.write_database_line_to_file(
                                    i_wom_account_login_account + 'wom_account 插入操作失败')
                            i_media_vendor_uuid = uuid_generator()
                            query = (
                                "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name.strip(), s_mobile.strip(), s_weixin.strip(), s_qq, s_co_name.strip()))
                            print(query)
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                error_file_tool.write_database_line_to_file(
                                    i_wom_account_login_account + 'media_vendor 插入操作失败')
                        else:
                            query = (
                                "SELECT `uuid` FROM `wom_account` WHERE `type`=2 AND `login_account`='%s'" % s_email)
                            print(query)
                            target_mysql_tool.cursor.execute(query)
                            row = target_mysql_tool.cursor.fetchone()
                            (i_wom_account_uuid,) = row
                            query = (
                                "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % i_wom_account_uuid)
                            print(query)
                            target_mysql_tool.cursor.execute(query)
                            row = target_mysql_tool.cursor.fetchone()
                            (i_media_vendor_uuid,) = row
                        uuid_media_weixin = uuid_generator()
                        i_create_time_media_weixin = current_time_to_stamp()
                        i_last_update_time_media_weixin = i_create_time_media_weixin
                        i_last_put_up_time_media_weixin = i_create_time_media_weixin
                        i_last_verify_time_media_weixin = i_create_time_media_weixin
                        i_status_media_weixin = None
                        if i_status == 0:
                            i_status_media_weixin = 3
                        elif i_status == 1:
                            i_status_media_weixin = 1
                        elif i_status == 2:
                            i_status_media_weixin = 0
                        elif i_status == 3:
                            i_status_media_weixin = 2
                        i_put_up_media_weixin = None
                        if i_effective_time > current_time_to_stamp():
                            i_put_up_media_weixin = 1
                        else:
                            i_put_up_media_weixin = 0

                        i_has_pref_vendor = 1
                        i_is_activated = 1
                        i_vendor_cnt = 1
                        i_to_verify_vendor_cnt = 0
                        query = (
                            "INSERT INTO `media_weixin`(`uuid`,`public_name`,`public_id`,`follower_num`,`put_up`,`status`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`has_pref_vendor`,`pref_vendor_uuid`,`cust_sort`,`create_time`,`is_activated`,`has_origin_pub`,`has_direct_pub`,`vendor_cnt`,`active_end_time`,`to_verify_vendor_cnt`,`last_update_time`,`last_put_up_time`,`comment`,`t_media_intro`,`last_verify_time`) VALUES('%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,'%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s','%s',%d)" % (uuid_media_weixin, str(s_media_name).strip(), source_file_tool_1.line_strip_decode_ignore(line), i_follower_num, i_put_up_media_weixin, i_status_media_weixin, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_has_pref_vendor, i_media_vendor_uuid, i_wom_sect_top, i_create_time_media_weixin, i_is_activated, i_has_origin_pub, i_has_direct_pub, i_vendor_cnt, i_effective_time, i_to_verify_vendor_cnt, i_last_update_time_media_weixin, i_last_put_up_time_media_weixin, i_beizhu.replace("'", '"'), i_instroduction.replace("'", '"'), i_last_verify_time_media_weixin))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                        except Exception as e:
                            error_file_tool.write_database_line_to_file(
                                uuid_media_weixin + 'media_weixin 插入操作失败')

                        uuid_media_vendor_bind_other = uuid_generator()
                        i_media_type = 1
                        i_is_activated = 1
                        i_status = 1
                        i_is_pref_vendor = 1
                        query = ("INSERT INTO `media_vendor_bind`(`uuid`,`media_type`,`media_uuid`,`vendor_uuid`,`is_activated`,`status`,`is_pref_vendor`,`follower_num`,`has_origin_pub`,`has_direct_pub`,`create_time`) VALUES('%s',%d,'%s','%s',%d,%d,%d,%d,%d,%d,%d)" % (
                            uuid_media_vendor_bind_other, i_media_type, uuid_media_weixin, i_media_vendor_uuid, i_is_activated, i_status, i_is_pref_vendor, i_follower_num, i_has_origin_pub, i_has_direct_pub, i_create_time_media_weixin))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                        except Exception as e:
                            print('media_vendor_bind %s' %
                                  uuid_media_vendor_bind_other)
                        uuid_media_vendor_weixin_price_list = uuid_generator()
                        i_deposit_percent_config = '{"pos_s":0.5,"pos_m_1":0.5,"pos_m_2":0.5,"pos_m_3":0.5}'
                        i_serve_percent_config = '{"pos_s":0.4,"pos_m_1":0.4,"pos_m_2":0.4,"pos_m_3":0.4}'
                        query = ("INSERT INTO `media_vendor_weixin_price_list`(`uuid`,`bind_uuid`,`s_pub_type`,`m_1_pub_type`,`m_2_pub_type`,`m_3_pub_type`,`orig_price_s_min`,`orig_price_m_1_min`,`orig_price_m_2_min`,`orig_price_m_3_min`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min`,`deposit_percent_config`,`serve_percent_config`,`orig_price_s_max`,`orig_price_m_1_max`,`orig_price_m_2_max`,`orig_price_m_3_max`,`retail_price_s_max`,`retail_price_m_1_max`,`retail_price_m_2_max`,`retail_price_m_3_max`,`execute_price_s`,`execute_price_m_1`,`execute_price_m_2`,`execute_price_m_3`,`active_end_time`) VALUES('%s','%s',%d,%d,%d,%d,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,'%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d)" % (
                            uuid_media_vendor_weixin_price_list, uuid_media_vendor_bind_other, i_s_pub_type_media_weixin, i_m_1_pub_type_media_weixin, i_m_2_pub_type_media_weixin, i_m_3_pub_type_media_weixin, orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min, i_deposit_percent_config.replace("'", '"'), i_serve_percent_config.replace("'", '"'), orig_price_s_max, orig_price_m_1_max, orig_price_m_2_max, orig_price_m_3_max, retail_price_s_max, retail_price_m_1_max, retail_price_m_2_max, retail_price_m_3_max, execute_price_s, execute_price_m_1, execute_price_m_2, execute_price_m_3, i_effective_time))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                        except Exception as e:
                            print('media_vendor_weixin_price_list %s ' %
                                  uuid_media_vendor_weixin_price_list)
                        target_file_tool_1.write_database_line_to_file(
                            source_file_tool_1.line_strip_decode_ignore(line))
                    else:
                        count_3 += 1
                        target_file_tool_3.write_database_line_to_file(
                            source_file_tool_1.line_strip_decode_ignore(line))
                else:
                    count_2 += 1
                    target_file_tool_2.write_database_line_to_file(
                        source_file_tool_1.line_strip_decode_ignore(line))
        print("put_up_1_duplicate %d records;%d records left;put_1_duplicate_2 %d" % (
            count_1, count_2, count_3))

    def ad_owner_transfer(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=1")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        s_email = None
        s_mobile = None
        s_real_name = None
        s_co_name = None
        s_weixin = None
        s_qq = None

        query = ("SELECT `iUserID` FROM `t_user` WHERE `iType`=1")

        unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        for unique_id in unique_id_list:

            query = (
                "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iUserID`=%d" % unique_id)
            row = None
            print(query)
            try:
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
            except Exception as e:
                continue
            i_media_vendor_uuid = None
            if row is not None:
                s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                if s_mobile is None:
                    s_mobile = ''
                if s_real_name is None:
                    s_real_name = ''
                if s_co_name is '':
                    s_co_name = s_real_name.strip()
                if s_weixin is None:
                    s_weixin = ''
                if s_qq is None:
                    s_qq = ''

                '''
                wom_account/ad_owner表所需信息
                '''
                i_wom_account_uuid = uuid_generator()
                i_wom_account_login_account = s_email
                '''to do'''
                i_wom_account_login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                '''广告主'''
                i_wom_account_type = 1
                i_wom_account_status = 1
                i_wom_account_create_time = current_time_to_stamp()
                '''
                    如果媒体主信息不存在，则建立,并把email添加进email list;存在，则查询media_vendor_uuid
                '''
                if i_wom_account_login_account not in wom_account_login_account_list:
                    i_wom_account_uuid = uuid_generator()
                    wom_account_login_account_list.append(
                        i_wom_account_login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        i_wom_account_uuid, i_wom_account_login_account.strip(), i_wom_account_login_password.strip(), i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        pass
                    i_media_vendor_uuid = uuid_generator()
                    query = (
                        "INSERT INTO `ad_owner`(`uuid`,`account_uuid`,`contact_name`,`contact_1`,`weixin`,`qq`,`comp_name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name.strip(), s_mobile.strip(), s_weixin.strip(), s_qq, s_co_name.strip()))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        pass

    def media_vendor_transfer(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        s_email = None
        s_mobile = None
        s_real_name = None
        s_co_name = None
        s_weixin = None
        s_qq = None

        query = ("SELECT `iUserID` FROM `t_user` WHERE `iType`=2")

        unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        for unique_id in unique_id_list:

            query = (
                "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iUserID`=%d" % unique_id)
            row = None
            print(query)
            try:
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
            except Exception as e:
                continue
            i_media_vendor_uuid = None
            if row is not None:
                s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                if s_mobile is None:
                    s_mobile = ''
                if s_real_name is None:
                    s_real_name = ''
                if s_co_name is '':
                    s_co_name = s_real_name.strip()
                if s_weixin is None:
                    s_weixin = ''
                if s_qq is None:
                    s_qq = ''

                '''
                wom_account/ad_owner表所需信息
                '''
                i_wom_account_uuid = uuid_generator()
                i_wom_account_login_account = s_email
                '''to do'''
                i_wom_account_login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                '''媒体主'''
                i_wom_account_type = 2
                i_wom_account_status = 1
                i_wom_account_create_time = current_time_to_stamp()
                '''
                    如果媒体主信息不存在，则建立,并把email添加进email list;存在，则查询media_vendor_uuid
                '''
                if i_wom_account_login_account not in wom_account_login_account_list:
                    i_wom_account_uuid = uuid_generator()
                    wom_account_login_account_list.append(
                        i_wom_account_login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        i_wom_account_uuid, i_wom_account_login_account.strip(), i_wom_account_login_password.strip(), i_wom_account_type, i_wom_account_status, i_wom_account_create_time))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        pass
                    i_media_vendor_uuid = uuid_generator()
                    query = (
                        "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (i_media_vendor_uuid, i_wom_account_uuid, s_real_name.strip(), s_mobile.strip(), s_weixin.strip(), s_qq, s_co_name.strip()))
                    print(query)
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        pass

    def media_weixin_status_sync(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = (
            "SELECT `account_id` FROM `sogou_weixin_account_init_task` WHERE `account_status`=0")
        source_unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        query = ("SELECT `public_id` FROM `media_weixin` WHERE `status`!=3")
        target_unique_id_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        target_unique_id_list_lower = []
        for unique_id in target_unique_id_list:
            target_unique_id_list_lower.append(unique_id.lower())
        count = 0
        for unique_id in source_unique_id_list:
            if unique_id.lower() in target_unique_id_list_lower:
                count += 1
                query = (
                    "UPDATE `media_weixin` SET `status`=3 WHERE LOWER(`public_id`)='%s'" % unique_id.lower())
                print(query)
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
        print("settled %d records" % count)

    def media_weixin_article_post_time_law(self, source_mysql_args, source_mongo_args, target_mysql_args):
        '''
        文章发布规律统计分析
        1.计算平均数&概率分布&方差

        '''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = (
            "SELECT `account_id` FROM `sogou_weixin_account_init_task` WHERE `account_status`=1 AND `account_level`=1 AND `task_status`=3")
        account_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        public_id_list_mongo_lower = []
        document_list = source_mongo_tool.database[
            'media_weixin'].find({}, {'public_id': 1})
        for document in document_list:
            public_id_list_mongo_lower.append(document['public_id'].lower())
        for account_id in account_id_list_mysql:
            if account_id.lower() in public_id_list_mongo_lower:
                print('account id %s' % account_id)
                document = source_mongo_tool.database['media_weixin'].find_one(
                    {"public_id": {"$regex": account_id, "$options": 'i'}}, {"post_time_law": 1})
                post_time_list = document["post_time_law"]
                '''区间离散值'''
                post_time_bar_list = [3600 * 1, 3600 * 2, 3600 * 3, 3600 * 4, 3600 * 5, 3600 * 6, 3600 * 7, 3600 * 8, 3600 * 9, 3600 * 10, 3600 * 11,
                                      3600 * 12, 3600 * 13, 3600 * 14, 3600 * 15, 3600 * 16, 3600 * 17, 3600 * 18, 3600 * 19, 3600 * 20, 3600 * 21, 3600 * 22, 3600 * 23]
                '''区间频次'''
                post_time_count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                '''区间概率分布'''
                post_time_prob_dist = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                min_post_time = 86400
                max_post_time = 0
                post_time_avg = 0.0
                post_time_sd = 0.0
                if len(post_time_list) >= 15:
                    for post_time in post_time_list:
                        # print(str(type(post_time)))
                        date_time = current_stamp_to_datetime(int(post_time))
                        date_time_hour = date_time.hour * 3600
                        date_time_minute = date_time.minute * 60
                        date_time_second = date_time.second
                        date_time_to_second = date_time_hour + date_time_minute + date_time_second
                        if date_time_to_second < min_post_time:
                            min_post_time = date_time_to_second
                        if date_time_to_second > max_post_time:
                            max_post_time = date_time_to_second
                        if date_time_to_second <= post_time_bar_list[0]:
                            post_time_count_list[0] += 1
                        elif date_time_to_second > post_time_bar_list[0] and date_time_to_second <= post_time_bar_list[1]:
                            post_time_count_list[1] += 1
                        elif date_time_to_second > post_time_bar_list[1] and date_time_to_second <= post_time_bar_list[2]:
                            post_time_count_list[2] += 1
                        elif date_time_to_second > post_time_bar_list[2] and date_time_to_second <= post_time_bar_list[3]:
                            post_time_count_list[3] += 1
                        elif date_time_to_second > post_time_bar_list[3] and date_time_to_second <= post_time_bar_list[4]:
                            post_time_count_list[4] += 1
                        elif date_time_to_second > post_time_bar_list[4] and date_time_to_second <= post_time_bar_list[5]:
                            post_time_count_list[5] += 1
                        elif date_time_to_second > post_time_bar_list[5] and date_time_to_second <= post_time_bar_list[6]:
                            post_time_count_list[6] += 1
                        elif date_time_to_second > post_time_bar_list[6] and date_time_to_second <= post_time_bar_list[7]:
                            post_time_count_list[7] += 1
                        elif date_time_to_second > post_time_bar_list[7] and date_time_to_second <= post_time_bar_list[8]:
                            post_time_count_list[8] += 1
                        elif date_time_to_second > post_time_bar_list[8] and date_time_to_second <= post_time_bar_list[9]:
                            post_time_count_list[9] += 1
                        elif date_time_to_second > post_time_bar_list[9] and date_time_to_second <= post_time_bar_list[10]:
                            post_time_count_list[10] += 1
                        elif date_time_to_second > post_time_bar_list[10] and date_time_to_second <= post_time_bar_list[11]:
                            post_time_count_list[11] += 1
                        elif date_time_to_second > post_time_bar_list[11] and date_time_to_second <= post_time_bar_list[12]:
                            post_time_count_list[12] += 1
                        elif date_time_to_second > post_time_bar_list[12] and date_time_to_second <= post_time_bar_list[13]:
                            post_time_count_list[13] += 1
                        elif date_time_to_second > post_time_bar_list[13] and date_time_to_second <= post_time_bar_list[14]:
                            post_time_count_list[14] += 1
                        elif date_time_to_second > post_time_bar_list[14] and date_time_to_second <= post_time_bar_list[15]:
                            post_time_count_list[15] += 1
                        elif date_time_to_second > post_time_bar_list[15] and date_time_to_second <= post_time_bar_list[16]:
                            post_time_count_list[16] += 1
                        elif date_time_to_second > post_time_bar_list[16] and date_time_to_second <= post_time_bar_list[17]:
                            post_time_count_list[17] += 1
                        elif date_time_to_second > post_time_bar_list[17] and date_time_to_second <= post_time_bar_list[18]:
                            post_time_count_list[18] += 1
                        elif date_time_to_second > post_time_bar_list[18] and date_time_to_second <= post_time_bar_list[19]:
                            post_time_count_list[19] += 1
                        elif date_time_to_second > post_time_bar_list[19] and date_time_to_second <= post_time_bar_list[20]:
                            post_time_count_list[20] += 1
                        elif date_time_to_second > post_time_bar_list[20] and date_time_to_second <= post_time_bar_list[21]:
                            post_time_count_list[21] += 1
                        elif date_time_to_second > post_time_bar_list[21] and date_time_to_second <= post_time_bar_list[22]:
                            post_time_count_list[22] += 1
                        elif date_time_to_second > post_time_bar_list[22]:
                            post_time_count_list[23] += 1
                    pos = -1

                    for post_time_count in post_time_count_list:
                        pos += 1
                        post_time_prob_dist[pos] = (
                            post_time_count / sum(post_time_count_list))
                    post_time_avg = avg_generator(
                        post_time_bar_list, post_time_prob_dist)
                    post_time_sd = round(math.sqrt(sd_generator(
                        post_time_bar_list, post_time_prob_dist, post_time_avg)), 2)
                    # print(str(post_time_prob_dist))
                    prob_post_time = int(
                        ((0.7995 * post_time_sd) + post_time_avg))
                    if len(post_time_list) >= 15:
                        prob_post_time_hour = int(prob_post_time / 3600)
                        prob_post_time_minute = int(
                            (prob_post_time - (prob_post_time_hour * 3600)) / 60)
                        prob_post_time_second = int(
                            (prob_post_time - (prob_post_time_hour * 3600) - (prob_post_time_minute * 60)))
                        prob_post_time_hour_convert = None
                        prob_post_time_minute_convert = None
                        prob_post_time_second_convert = None
                        prob_post_time_hour += 1
                        if len(str(prob_post_time_hour)) == 1:
                            prob_post_time_hour_convert = '0' + \
                                str(prob_post_time_hour)
                        elif prob_post_time_hour == 24:
                            prob_post_time_hour_convert = "00"
                        else:
                            prob_post_time_hour_convert = str(
                                prob_post_time_hour)
                        if len(str(prob_post_time_minute)) == 1:
                            prob_post_time_minute_convert = "0" + \
                                str(prob_post_time_minute)
                        else:
                            prob_post_time_minute_convert = str(
                                prob_post_time_minute)
                        if len(str(prob_post_time_second)) == 1:
                            prob_post_time_second_convert = "0" + \
                                str(prob_post_time_second)
                        else:
                            prob_post_time_second_convert = str(
                                prob_post_time_second)
                        prob_post_time_convert = prob_post_time_hour_convert + ":" + \
                            prob_post_time_minute_convert + ":" + prob_post_time_second_convert
                        query = ("UPDATE `sogou_weixin_account_init_task` SET `post_start_time`='%s' WHERE LOWER(`account_id`)='%s'" % (
                            prob_post_time_convert, account_id.lower()))
                        print(query)
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()

    def weibo_transfer(self, source_file_args, source_mysql_args, target_mysql_args, error_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        error_file_tool = self.get_file_tool(*error_file_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        wom_account_login_account_list = []
        query = ("SELECT `login_account` FROM `wom_account` WHERE `type`=2")
        wom_account_login_account_list = target_mysql_tool.get_unique_id_list_by_sql(
            query)
        item_count = 0
        try:
            for line in source_file:
                item_count += 1

                '''
                media_weibo字段列表
                '''
                media_weibo_uuid = None  # 1
                weibo_name = None  # 2
                follower_num = None  # 3
                follower_screenshot = None  # 4
                weibo_url = None  # 5
                avatar = None  # 6
                qrcode = None  # 7
                read_num = None  # 8
                media_level = None  # 9
                intro = None  # 10
                media_weibo_status = None  # 11
                is_put = None  # 12
                is_top = None  # 13
                active_end_time = None  # 14
                fail_reason = None  # 15
                put_down_reason = None  # 16
                audit_remark = None  # 17
                create_time = None  # 18
                update_time = None  # 19
                audit_time = None  # 20
                enter_time = None  # 21

                '''
                wom_account 字段列表
                '''
                wom_account_uuid = None
                login_account = None
                login_password = None
                wom_account_type = None
                wom_account_status = None
                '''
                media_vendor 字段列表
                '''
                media_vendor_uuid = None
                contact_person = None
                contact1 = None
                weixin = None
                qq = None
                name = None

                '''
                weibo_vendor_bind 字段列表
                '''
                weibo_vendor_bind_uuid = None
                soft_direct_price_orig = None
                soft_transfer_price_orig = None
                soft_direct_price_retail = None
                soft_transfer_price_retail = None
                soft_direct_price_execute = None
                soft_transfer_price_execute = None
                micro_direct_price_execute = None
                micro_transfer_price_execute = None
                micro_direct_price_retail = None
                micro_transfer_price_retail = None
                micro_direct_price_orig = None
                micro_transfer_price_orig = None

                '''
                t_media 字段列表
                '''
                i_media_id = None
                i_user_id = None
                s_media_name = None  # weibo_name
                i_follower_num = None  # follower_num
                s_follower_img = None  # follower_screenshot

                s_url = None  # weibo_url
                s_avater = None  # avatar
                s_qrcode = None  # qrcode
                i_read_num = None  # read_num
                s_bottom_put_explain = None  # put_down_reason

                i_beizhu = None  # audit_remark
                i_verify_status = None  # media_level
                s_introduction = None  # s_introduction
                i_status = None  # media_weibo_status
                i_put = None  # is_put

                i_explain = None  # fail_reason
                i_wom_sect_top = None  # is_top
                i_effective_time = None  # active_end_time

                i_price_1 = None
                i_price_2 = None
                i_price_3 = None
                i_price_4 = None
                i_price_5 = None
                i_price_6 = None
                i_price_7 = None
                i_price_8 = None
                i_price_11 = None
                i_price_12 = None
                i_price_13 = None
                i_price_14 = None

                query = (
                    "SELECT `iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`sFollowerImg`,`sUrl`,`sAvatar`,`sQRCode`,`iReadNum`,`sBottomPutExplain`,`iBeizhu`,`iVerifyState`,`sIntroduction`,`iStatus`,`iPut`,`iExplain`,`iWomSectTop`,`iEffectiveTime`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14` FROM `t_media` WHERE `sUrl`='%s'" % source_file_tool.line_strip_decode_ignore(line))
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
                if row is not None:
                    i_media_id, i_user_id, s_media_name, i_follower_num, s_follower_img, s_url, s_avater, s_qrcode, i_read_num, s_bottom_put_explain, i_beizhu, i_verify_status, s_introduction, i_status, i_put, i_explain, i_wom_sect_top, i_effective_time, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14 = row
                else:
                    continue

                if s_media_name is None:
                    s_media_name = ''

                if i_follower_num is None:
                    i_follower_num = 0

                if s_follower_img is None:
                    s_follower_img = ''

                if s_url is None:
                    s_url = ''

                if s_avater is None:
                    s_avater = ''

                if s_qrcode is None:
                    s_qrcode = ''

                if i_read_num is None:
                    i_read_num = 0

                if s_bottom_put_explain is None:
                    s_bottom_put_explain = ''

                if i_beizhu is None:
                    i_beizhu = ''

                if i_verify_status is None:
                    i_verify_status = 0

                if s_introduction is None:
                    s_introduction = ''

                if i_status is None:
                    i_status = 0

                if i_put is None:
                    i_put = 0

                if i_explain is None:
                    i_explain = ''

                if i_wom_sect_top is None:
                    i_wom_sect_top = 0

                if i_effective_time is None:
                    i_effective_time = -1

                '''
                填充 media_weibo字段
                '''
                media_weibo_uuid = uuid_generator()
                weibo_name = s_media_name.strip().replace("'", '"')
                follower_num = i_follower_num
                follower_screenshot = s_follower_img.strip()
                weibo_url = s_url.strip().replace("'", '"')
                avatar = s_avater.strip().replace("'", '"')
                qrcode = s_qrcode.strip().replace("'", '"')
                read_num = i_read_num
                media_level = WEIBO_VERIFY_STATUS[i_verify_status]
                intro = s_introduction.strip().replace("'", '"')
                media_weibo_status = i_status
                is_put = i_put
                is_top = i_wom_sect_top
                active_end_time = i_effective_time
                fail_reason = i_explain.strip().replace("'", '"')
                put_down_reason = s_bottom_put_explain.strip().replace("'", '"')
                audit_remark = i_beizhu.strip().replace("'", '"')
                create_time = current_time_to_stamp()
                update_time = current_time_to_stamp()
                audit_time = current_time_to_stamp()
                enter_time = current_time_to_stamp()

                '''填充weibo_vendor_bind'''
                weibo_vendor_bind_uuid = uuid_generator()
                soft_direct_price_orig = float(i_price_1)
                soft_transfer_price_orig = float(i_price_2)
                micro_direct_price_orig = float(i_price_3)
                micro_transfer_price_orig = float(i_price_4)
                soft_direct_price_retail = float(i_price_5)
                soft_transfer_price_retail = float(i_price_6)
                micro_direct_price_retail = float(i_price_7)
                micro_transfer_price_retail = float(i_price_8)
                soft_direct_price_execute = float(i_price_11)
                soft_transfer_price_execute = float(i_price_12)
                micro_direct_price_execute = float(i_price_13)
                micro_transfer_price_execute = float(i_price_14)

                '''t_user'''
                s_email = None
                s_mobile = None
                s_real_name = None
                s_co_name = None
                s_weixin = None
                s_qq = None
                row = None
                query = (
                    "SELECT `sEmail`,`sMobile`,`sRealName`,`sCoName`,`sWeixin`,`sQQ` FROM `t_user` WHERE `iType`=2 AND `iUserID`=%d" % i_user_id)
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
                if row is not None:
                    s_email, s_mobile, s_real_name, s_co_name, s_weixin, s_qq = row
                    if s_mobile is None:
                        s_mobile = ''
                    else:
                        s_mobile = s_mobile.strip().replace("'", '"')
                    if s_real_name is None:
                        s_real_name = ''
                    else:
                        s_real_name = s_real_name.strip().replace("'", '"')
                    if s_co_name is '':
                        s_co_name = s_real_name.strip()
                    else:
                        s_co_name = s_co_name.strip().replace("'", '"')
                    if s_weixin is None:
                        s_weixin = ''
                    else:
                        s_weixin = s_weixin.strip().replace("'", '"')
                    if s_qq is None:
                        s_qq = ''
                    else:
                        s_qq = s_qq.strip().replace("'", '"')
                else:
                    s_email = '1160154200@qq.com'

                '''
                填充wom_account字段
                '''
                wom_account_uuid = uuid_generator()
                login_account = s_email
                login_password = "$2y$13$PgMCcSnJrfTHSnlaOnMkoej0eUBcBNVmfxaLo6ghlFwghQFq13ksW"
                wom_account_type = 2
                wom_account_status = 1

                if login_account not in wom_account_login_account_list:
                    wom_account_login_account_list.append(login_account)
                    query = ("INSERT INTO `wom_account`(`uuid`,`login_account`,`login_password`,`type`,`status`,`create_time`) VALUES('%s','%s','%s',%d,%d,%d)" % (
                        wom_account_uuid, login_account, login_password, wom_account_type, wom_account_status, create_time))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                    media_vendor_uuid = uuid_generator()
                    contact_person = s_real_name
                    contact1 = s_mobile
                    weixin = s_weixin
                    qq = s_qq
                    name = s_co_name
                    query = (
                        "INSERT INTO `media_vendor`(`uuid`,`account_uuid`,`contact_person`,`contact1`,`weixin`,`qq`,`name`) VALUES('%s','%s','%s','%s','%s','%s','%s')" % (media_vendor_uuid, wom_account_uuid, contact_person, contact1, weixin, qq, name))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                else:
                    query = (
                        "SELECT `uuid` FROM `wom_account` WHERE `type`=2 AND `login_account`='%s'" % s_email)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (wom_account_uuid,) = row
                    query = (
                        "SELECT `uuid` FROM `media_vendor` WHERE `account_uuid`='%s'" % wom_account_uuid)
                    target_mysql_tool.cursor.execute(query)
                    row = target_mysql_tool.cursor.fetchone()
                    (media_vendor_uuid,) = row
                try:
                    query = ("INSERT INTO `media_weibo`(`uuid`,`weibo_name`,`follower_num`,`follower_screenshot`,`weibo_url`,`avatar`,`qrcode`,`read_num`,`media_level`,`intro`,`status`,`is_put`,`is_top`,`active_end_time`,`fail_reason`,`put_down_reason`,`audit_remark`,`create_time`,`update_time`,`audit_time`,`enter_time`) VALUES('%s','%s',%d,'%s','%s','%s','%s',%d,%d,'%s',%d,%d,%d,%d,'%s','%s','%s',%d,%d,%d,%d)" % (
                        media_weibo_uuid, weibo_name, follower_num, follower_screenshot, weibo_url, avatar, qrcode, read_num, media_level, intro, media_weibo_status, is_put, is_top, active_end_time, fail_reason, put_down_reason, audit_remark, create_time, update_time, audit_time, enter_time))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                    print('media_weibo settled %d' % item_count)
                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        media_weibo_uuid + 'media_weixin 插入操作失败')
                    break
                try:

                    query = ("INSERT INTO `weibo_vendor_bind`(`uuid`,`weibo_uuid`,`vendor_uuid`,`soft_direct_price_orig`,`soft_transfer_price_orig`,`micro_direct_price_orig`,`micro_transfer_price_orig`,`soft_direct_price_retail`,`soft_transfer_price_retail`,`micro_direct_price_retail`,`micro_transfer_price_retail`,`soft_direct_price_execute`,`soft_transfer_price_execute`,`micro_direct_price_execute`,`micro_transfer_price_execute`,`create_time`,`update_time`) VALUES ('%s','%s','%s',%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d)" % (
                        weibo_vendor_bind_uuid, media_weibo_uuid, media_vendor_uuid, soft_direct_price_orig, soft_transfer_price_orig, micro_direct_price_orig, micro_transfer_price_orig, soft_direct_price_retail, soft_transfer_price_retail, micro_direct_price_retail, micro_transfer_price_retail, soft_direct_price_execute, soft_transfer_price_execute, micro_direct_price_execute, micro_transfer_price_execute, create_time, update_time))
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                    print('weibo_vendor_bind settled %d' % item_count)
                    print('------------------------------------------')

                except Exception as e:
                    error_file_tool.write_database_line_to_file(
                        weibo_vendor_bind_uuid + 'weibo_vendor_bind 插入操作失败')
                    break
        finally:
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()
            source_file_tool.close_file()
            error_file_tool.close_file()

    def new_weixin_account_storage(self, source_file_args, source_mysql_args, target_mysql_args, target_file_args_1, target_file_args_2, target_file_args_3, target_file_args_4, error_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        target_file_tool_3 = self.get_file_tool(*target_file_args_3)
        target_file_tool_4 = self.get_file_tool(*target_file_args_4)
        error_file_tool = self.get_file_tool(*error_file_args)
        source_file = source_file_tool.get_file()
        item_count = 0
        query = ("SELECT `public_id` FROM `media_weixin`")
        public_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        public_id_list_lower = []
        for public_id in public_id_list:
            if public_id.lower() not in public_id_list_lower:
                public_id_list_lower.append(public_id.lower())
            else:
                print(public_id)
                input('..................')
        # input('..................')
        file_line = []
        file_line_unique = []
        file_line_duplicate = []
        file_line_duplicate_unique = []
        file_1_count = 0
        file_2_count = 0
        file_3_count = 0
        file_4_count = 0
        file_line_count = 2
        error_flag = 0

        for line in source_file:
            file_line_count += 1
            print(line.decode(
                'gbk', 'ignore').strip() + str(len(line.decode('gbk', 'ignore').strip().split(','))))
            if len(line.decode('gbk', 'ignore').strip().split(',')) == 1:
                target_file_tool_4.write_database_line_to_file(line.decode(
                    'gbk', 'ignore').strip())
                error_flag = 1
                continue
            if error_flag == 1:
                file_4_count += 1
                target_file_tool_4.write_database_line_to_file(line.decode(
                    'gbk', 'ignore').strip())
                error_flag = 0
                print('file 4 count %d' % file_4_count)
                continue

            (public_name, public_id) = line.decode(
                'gbk', 'ignore').strip().split(',')
            public_name, public_id = public_name.strip(), public_id.strip()
            if public_id.lower() not in file_line:
                file_line.append(public_id.lower())
            else:
                file_3_count += 1
                file_line_duplicate.append(public_id.lower())
                target_file_tool_3.f.write(
                    (line + '\n').encode("utf-8", 'ignore'))
                target_file_tool_3.write_database_line_to_file(
                    str(file_line_count) + ',' + public_id)
                print('file_3_count %d' % file_3_count)
        for public_id in file_line:
            if public_id not in file_line_duplicate:
                file_line_unique.append(public_id)
            else:
                file_line_duplicate_unique.append(public_id)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        for line in source_file:
            if len(line.decode('gbk', 'ignore').strip().split(',')) == 1:
                error_flag = 1
                continue
            if error_flag == 1:
                error_flag = 0
                continue
            (public_name, public_id) = line.decode(
                'gbk', 'ignore').strip().split(',')
            public_name, public_id = public_name.strip(), public_id.strip()
            if public_id.lower() in file_line_unique:
                # print(public_id.lower()+'--->'+str(type(public_id.lower()))+';'+public_id_list_lower[0]+'--->'+str(type(public_id_list_lower[0])))
                # input('..................')
                if public_id.lower() not in public_id_list_lower:
                    file_1_count += 1
                    public_id_list_lower.append(public_id.lower())
                    target_file_tool_1.write_database_line_to_file(
                        public_id + ',' + public_name)
                    uuid = uuid_generator()
                    query = ("INSERT INTO `media_weixin`(`uuid`,`public_id`,`public_name`) VALUES('%s','%s','%s')" % (
                        uuid, public_id, public_name))
                    # target_mysql_tool.cursor.execute(query)
                    # target_mysql_tool.cnx.commit()
                    print('file 1 count %d' % file_1_count)

                else:
                    file_2_count += 1
                    target_file_tool_2.write_database_line_to_file(
                        public_id.strip() + ',' + public_name.strip())
                    print('file 2 count %d' % file_2_count)
        print("1--->%d;2--->%d;3--->%d;4--->%d" %
              (file_1_count, file_2_count, file_3_count, file_4_count))
    # def media_weixin_invalid_mark(self,source_mysql_args,target_mysql_args,target_file_args_1,target_file_args_2,target_file_args_3):
    #     source_mysql_tool=self.get_mysql_tool(*source_mysql_args)
    #     target_mysql_tool=self.get_mysql_tool(*target_mysql_args)
    #     target_file_tool_1=self.get_file_tool(*target_file_args_1)
    #     target_file_tool_2=self.get_file_tool(*target_file_args_2)
    #     target_file_tool_3=self.get_file_tool(*target_file_args_3)
    #     query=("SELECT `account_id` FROM `sogou_weixin_account_init_task` WHERE `account_status`=0")
    #     source_account_id_list=source_mysql_tool.get_unique_id_list_by_sql(query)

    def media_vendor_account_count(self, source_mysql_args_1, source_mysql_args_2, target_mysql_args):
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `uuid` FROM `media_vendor`")
        uuid_media_vendor_list = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        uuid_count = 0
        try:
            for uuid_media_vendor in uuid_media_vendor_list:
                query = (
                    "SELECT COUNT(`uuid`) FROM `weibo_vendor_bind` WHERE `vendor_uuid`='%s'" % uuid_media_vendor)
                source_mysql_tool_2.cursor.execute(query)
                (count,) = source_mysql_tool_2.cursor.fetchone()
                query = (
                    "UPDATE `media_vendor` SET `weibo_media_cnt`=%d WHERE `uuid`='%s'" % (count, uuid_media_vendor))
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
                uuid_count += 1
                print("settled %d items;count %d" % (uuid_count, count))
        finally:
            source_mysql_tool_1.disconnection()
            source_mysql_tool_2.disconnection()
            target_mysql_tool.disconnection()

    def media_weixin_article_add_last_update_day_account_to_file(self, source_mongo_args, target_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        target_file_tool = self.get_file_tool(
            *(CHECK_CSV_PATH, 'weixin_id_to_add_last_update_day_1.csv', 'ab'))
        source_cursor = source_mongo_tool.database[
            'media_weixin'].find({}, {'public_id': 1})
        source_cursor.batch_size(500)
        unique_id_list = []
        item_count = 0
        try:
            while source_mongo_tool.cursor_has_next(source_cursor):
                document = source_cursor.next()
                item_count += 1
                print('settled %d ' % item_count)
                unique_id_list.append(document['public_id'])
            target_collection = target_mongo_tool.database[
                'media_weixin_article']

            item_count = 0
            i_count = 1
            for unique_id in unique_id_list:
                item_count += 1
                if item_count % 20000 == 0:
                    i_count += 1
                    target_file_tool = self.get_file_tool(
                        *(CHECK_CSV_PATH, 'weixin_id_to_add_last_update_day_%d.csv' % i_count, 'ab'))

                target_file_tool.write_database_line_to_file(unique_id + '\n')

                # target_collection.update_many({"weixin_id":unique_id},{'$set': {'last_update_day': '20161121'}})
                print('settled %d account id %s' % (item_count, unique_id))
        finally:
            source_mongo_tool.disconnection()
            target_mongo_tool.disconnection()

    def media_weixin_article_add_last_update_day(self, source_file_args, target_mongo_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        target_mongo_tool = self.get_mongo_tool(*target_mongo_args)
        source_file = source_file_tool.get_file()
        item_count = 0
        try:

            for line in source_file:
                item_count += 1
                unique_id = source_file_tool.line_strip_decode_ignore(line)
                target_mongo_tool.database['media_weixin_article'].update_many(
                    {"weixin_id": unique_id}, {'$set': {'last_update_day': '20161121'}})
                print('settled %d account id %s' % (item_count, unique_id))
        finally:
            source_file_tool.close_file()
            target_mongo_tool.disconnection()

    def media_weixin_top_k(self, source_mongo_args, target_mongo_args):
        pass

    def media_weixin_public_id_collect(self, source_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_args = [
            "/alidata1/dts/export_from_mongo/weixin_article", "public_id_1.csv", "ab"]
        target_file_tool = self.get_file_tool(*tuple(target_file_args))
        source_cursor = source_mongo_tool.database[
            'media_weixin'].find({}, {"public_id": 1})
        source_cursor.batch_size(500)
        settled_count = 0
        file_id = 1
        while source_mongo_tool.cursor_has_next(source_cursor):
            settled_count += 1
            document = source_cursor.next()
            target_file_tool.write_database_line_to_file(
                document['public_id'] + '\n')
            print("settled %d items" % settled_count)
            if settled_count % 30000 == 0:
                file_id += 1
                target_file_tool.close_file()
                target_file_tool = None
                target_file_args[1] = "public_id_%d.csv" % file_id
                target_file_tool = self.get_file_tool(*tuple(target_file_args))
        return settled_count

    def weixin_article_mongo_to_weixin_article_mysql(self, source_file_args, source_mongo_args, target_mysql_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        line_count = 0
        document_count = 0
        object_id = None
        try:
            for line in source_file:
                line_count += 1
                line_document_count = 0
                account_id = source_file_tool.line_strip_decode_ignore(line)
                print(account_id + " account id")
                source_cursor = source_mongo_tool.database["media_weixin_article"].find(
                    {"weixin_id": account_id})
                source_cursor.batch_size(500)
                if source_cursor.count() != 0:
                    while source_mongo_tool.cursor_has_next(source_cursor):
                        document_count += 1
                        line_document_count += 1
                        print()
                        document = source_cursor.next()
                        if document['last_update_day'] == '20161121':
                            object_id = strip_and_replace_all(
                                str(document["_id"]))
                            mongo_obj_id = object_id
                            weixin_id = strip_and_replace_all(
                                str(document["weixin_id"]))
                            title = ''
                            if "title" in document:
                                title = strip_and_replace_all(
                                    strip_and_replace(str(document["title"])))
                            article_url = ''
                            if "fixed_url" in document:
                                article_url = strip_and_replace_all(
                                    strip_and_replace(str(document["fixed_url"])))
                            short_desc = ''
                            if "digest" in document:
                                short_desc = strip_and_replace_all(
                                    strip_and_replace(str(document["digest"])))
                            article_type = -1
                            if "article_type" in document:
                                article_type = int(document["article_type"])
                            article_pos = -1
                            if "article_pos" in document:
                                article_pos = int(document["article_pos"])
                            read_num = 0
                            if "page_view_num" in document:
                                read_num = int(document["page_view_num"])
                            like_num = 0
                            if "page_like_num" in document:
                                like_num = int(document["page_like_num"])
                            post_time = -1
                            if "post_time" in document:
                                post_time = int(document["post_time"])
                            query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
                                mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                            print(query)
                            print("settled %d account %d document" %
                                  (line_count, document_count))
                            print(
                                str(source_mongo_tool.cursor_has_next(source_cursor)))
                            print(str(source_cursor.count()))
                        if line_document_count == source_cursor.count():
                            break

                # input('.............')
        except Exception as e:
            print("error..............")
            error_file_tool = self.get_file_tool(
                *("/alidata1/dts/export_from_mongo/weixin_article", "errors.txt", "ab"))
            error_file_tool.write_database_line_to_file(
                "%s failed\n" % object_id)
            error_file_tool.close_file()
        finally:
            source_file_tool.close_file()
            source_mongo_tool.disconnection()
            target_mysql_tool.disconnection()
        return document_count

    def erp_transfer(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        # target_file_tool=self.get_file_tool(*(CHECK_CSV_PATH,"media_vendor_has_matter.csv","ab"))
        """
        crm_supplier 表所需字段
        """
        uuid_crm_supplier = None
        name_crm_supplier = None
        remarks_crm_supplier = None
        created_uuid_crm_supplier = "wom_data"
        """
        crm_contact 表所需字段
        """
        uuid_crm_contact = None
        name_crm_contact = None
        qq_crm_contact = None
        weichat_crm_contact = None
        phone_crm_contact = None
        created_uuid_crm_contact = "wom_data"
        """
        crm_supplier_contact_map 表所需字段
        """
        contact_uuid_crm_supplier_contact_map = None
        supplier_uuid_crm_supplier_contact_map = None
        created_uuid_crm_supplier_contact_map = "wom_data"

        """
        source：media_vendor
        """
        uuid_media_vendor = None
        name_media_vendor = None
        comment_media_vendor = None
        contact_info_media_vendor = None
        query = ("SELECT `uuid`,`name`,`comment`,`contact_info` FROM `media_vendor`")
        source_mysql_tool.cursor.execute(query)
        matter_count = 0
        query = (
            "SELECT `config` FROM `com_config` WHERE `uuid`='08e37c70d876a09e13bcdd5df4d5b22a'")
        target_mysql_tool.cursor.execute(query)
        (config_com_config,) = target_mysql_tool.cursor.fetchone()
        config_com_config_dict = json.loads(config_com_config)
        # print(str(config_com_config_dict))
        # config_com_config_dict["supplier_code"]=160200
        # input("..................")
        settled_count = 0
        for row in source_mysql_tool.cursor:
            uuid_media_vendor, name_media_vendor, comment_media_vendor, contact_info_media_vendor = row
            query = (
                "SELECT COUNT(*) FROM `crm_supplier` WHERE `uuid`='%s'" % uuid_media_vendor)
            target_mysql_tool.cursor.execute(query)
            (count_crm_supplier,) = target_mysql_tool.cursor.fetchone()
            if count_crm_supplier != 0:
                break
            settled_count += 1
            # print(str(count_crm_supplier))
            # input("..................")
            if name_media_vendor is None:
                name_media_vendor = ''
            if comment_media_vendor is None:
                comment_media_vendor = ''
            if (contact_info_media_vendor is None) or (contact_info_media_vendor is ''):
                contact_info_media_vendor = []
            """
            填充 crm_supplier表字段
            """
            uuid_crm_supplier = uuid_media_vendor
            name_crm_supplier = name_media_vendor
            remarks_crm_supplier = comment_media_vendor

            query = ("INSERT INTO `crm_supplier`(`uuid`,`name`,`remarks`,`created_uuid`,`code`) VALUES('%s','%s','%s','%s','%s')" % (
                uuid_crm_supplier, name_crm_supplier, remarks_crm_supplier, created_uuid_crm_supplier, str(config_com_config_dict["supplier_code"])))
            target_mysql_tool.cursor.execute(query)
            target_mysql_tool.cnx.commit()
            config_com_config_dict["supplier_code"] += 1
            contact_info_list = eval(str(contact_info_media_vendor))
            for contact_info in contact_info_list:

                contact_person_media_vendor = contact_info[
                    'contact_person'].replace(" ", "")
                if contact_person_media_vendor is None:
                    contact_person_media_vendor = ''

                weixin_media_vendor = contact_info['weixin'].replace(" ", "")
                if weixin_media_vendor is None:
                    weixin_media_vendor = ''

                # qq_media_vendor = contact_info['qq']
                # if qq_media_vendor is None:
                #     qq = 0

                contact_phone_media_vendor = contact_info[
                    'contact_phone'].replace(" ", "")
                if contact_phone_media_vendor is None:
                    contact_phone_media_vendor = "0"

                qq_media_vendor = contact_info['qq'].replace(" ", "")
                if qq_media_vendor.isdigit():
                    qq_media_vendor = int(qq_media_vendor)
                else:
                    if qq_media_vendor is '':
                        qq_media_vendor = 0
                # contact_phone_media_vendor = contact_info['contact_phone'].replace(" ","")
                # if contact_phone_media_vendor.isdigit():
                #     pass
                # else:
                #     if contact_phone_media_vendor is not '':
                #         target_file_tool.write_database_line_to_file(uuid_media_vendor+"\n")
                #         print("matter account has %d items " % matter_count)
                #         matter_count+=1
                #         break

                '''
                填充 crm_contact 表
                '''
                uuid_crm_contact = uuid_generator()
                name_crm_contact = contact_person_media_vendor
                qq_crm_contact = qq_media_vendor
                weichat_crm_contact = weixin_media_vendor
                office_phone_crm_contact = contact_phone_media_vendor
                query = ("INSERT INTO `crm_contact`(`uuid`,`name`,`qq`,`weichat`,`office_phone`,`created_uuid`) VALUES('%s','%s',%d,'%s','%s','%s')" % (
                    uuid_crm_contact, name_crm_contact, qq_crm_contact, weichat_crm_contact, office_phone_crm_contact, created_uuid_crm_contact))
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()

                '''
                填充 crm_supplier_contact_map 表
                '''

                contact_uuid_crm_supplier_contact_map = uuid_crm_contact
                supplier_uuid_crm_supplier_contact_map = uuid_crm_supplier
                query = ("INSERT INTO `crm_supplier_contact_map`(`contact_uuid`,`supplier_uuid`,`created_uuid`) VALUES('%s','%s','%s')" % (
                    contact_uuid_crm_supplier_contact_map, supplier_uuid_crm_supplier_contact_map, created_uuid_crm_supplier_contact_map))
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
            print("settled %d accounts" % settled_count)
        query = ("UPDATE `com_config` SET `config`='%s' WHERE `uuid`='08e37c70d876a09e13bcdd5df4d5b22a'" % (
            str(config_com_config_dict).replace("'", '"')))
        target_mysql_tool.cursor.execute(query)
        target_mysql_tool.cnx.commit()

        target_mysql_tool.disconnection()
        source_mysql_tool.disconnection()

    def weixin_article_hot_index_compute(self, source_mysql_args, target_mysql_args):
        """to do"""
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        """
        文章新鲜度:文章新鲜度其实是文章老旧程度,指的是 “当前时间-发布时间” 转换为小时
        """
        """当前时间"""
        current_time = current_time_to_stamp()
        """发布时间"""
        post_time = None
        """发布时长"""
        post_duration = int((current_time - post_time) / (60 * 60))
        """文章阅读数"""
        read_num = None
        """点赞数"""
        like_num = None
        """沃米指数（月）"""
        wom_index = None
        """文章热度公式
        保留两位小数
        """
        weixin_article_hot_index = math.pow((0.5 * math.log(wom_index + 1) + 0.3 * math.log(
            read_num + 1) + 0.2 * math.log(like_num + 1)), 2) - math.pow(math.log(post_duration), 2)

    def intelligent_recommend_account_index_compute(self,):
        """to do"""

        user_budget = None
        """用户预算"""
        account_wanted_count = None
        """需求账号数"""

        mainly_account_count = math.ceil((account_wanted_count / 9))
        """主推账号数"""
        secondary_account_count = math.ceil((account_wanted_count / 7))
        """次推账号数"""
        quota_account_count = math.ceil((account_wanted_count / 5))
        """搭配账号数"""
        corrective_account_count = account_wanted_count - \
            mainly_account_count - secondary_account_count - quota_account_count
        """修正账号数"""
        """
        如果修正账号数为-1，则修正账号数和搭配账号数均设置为0
        如果修正账号数为-2，则修正账号数、搭配账号数和次推账号数均设置为0
        """
        if corrective_account_count == -1:
            quota_account_count = 0
            corrective_account_count = 0
        elif corrective_account_count == -2:
            quota_account_count = 0
            corrective_account_count = 0
            secondary_account_count = 0

        mainly_account_wanted_count_divide_by_2 = int(account_wanted_count / 2)
        """主推账号价格"""
        mainly_account_random_choice = random.choice(
            ((account_wanted_count_divide_by_2 - 1), account_wanted_count_divide_by_2, account_wanted_count_divide_by_2 + 1))
        mainly_account_price = (
            user_budget / account_wanted_count) * mainly_account_random_choice

        """次推账号价格"""
        secondary_account_random_choice = random.choice()

    def spark_index_compute(self):
        os.system("/usr/local/spark/bin/spark-submit --class compute.WeiXinAccountIndexCompute  --master spark://spider-host-003:7077  --executor-memory 12G --executor-cores 6    /usr/local/spark/spark_test/compute-1.0-SNAPSHOT.jar")

    def media_weixin_retail_price_update(self, source_mysql_args, target_mysql_args_1):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool_1 = self.get_mysql_tool(*target_mysql_args_1)
        target_file_tool = self.get_file_tool(
            *(CHECK_CSV_PATH, 'media_vendor_bind_uuid_not_in_price_list.csv', 'ab'))
        target_file = target_file_tool.get_file()
        query = ("SELECT `uuid` FROM `media_vendor_bind`")
        media_vendor_bind_uuid_list = []
        source_mysql_tool.cursor.execute(query)
        for row in source_mysql_tool.cursor:
            (uuid_media_vendor_bind,) = row
            media_vendor_bind_uuid_list.append(uuid_media_vendor_bind)
        item_count = 0
        for uuid_media_vendor_bind in media_vendor_bind_uuid_list:
            item_count += 1
            print("settled %d items" % item_count)
            query = ("SELECT `media_vendor_weixin_price_list`.`orig_price_s_min`, `media_vendor_weixin_price_list`.`orig_price_m_1_min`,`media_vendor_weixin_price_list`.`orig_price_m_2_min`,`media_vendor_weixin_price_list`.`orig_price_m_3_min`, `media_vendor_weixin_price_list`.`pub_config`, `media_vendor_bind`.`media_uuid`,`media_vendor_bind`.`is_pref_vendor` FROM  `media_vendor_bind` LEFT JOIN `media_vendor_weixin_price_list` ON `media_vendor_weixin_price_list`.`bind_uuid` = `media_vendor_bind`.`uuid` WHERE `media_vendor_bind`.`uuid` = '%s'" % uuid_media_vendor_bind)
            print(query)
            source_mysql_tool.cursor.execute(query)
            (orig_price_s_min, orig_price_m_1_min, orig_price_m_2_min, orig_price_m_3_min, pub_config, media_uuid,
             is_pref_vendor) = source_mysql_tool.cursor.fetchone()
            if (orig_price_s_min is None) or (orig_price_m_1_min is None) or (orig_price_m_2_min is None) or (orig_price_m_3_min is None) or ((pub_config is '') or (pub_config is None)):
                target_file_tool.write_database_line_to_file(
                    uuid_media_vendor_bind + '\n')
                continue
            orig_price_s_min = float(orig_price_s_min)
            orig_price_m_1_min = float(orig_price_m_1_min)
            orig_price_m_2_min = float(orig_price_m_2_min)
            orig_price_m_3_min = float(orig_price_m_3_min)
            pub_config_dict = eval(pub_config.replace("null", "0"))
            retail_price_s = 0.0
            retail_price_m_1 = 0.0
            retail_price_m_2 = 0.0
            retail_price_m_3 = 0.0
            if orig_price_s_min >= 200000:
                retail_price_s = orig_price_s_min * 1.15
            elif orig_price_s_min >= 100000 and orig_price_s_min < 200000:
                retail_price_s = orig_price_s_min * 1.2
            elif orig_price_s_min >= 50000 and orig_price_s_min < 100000:
                retail_price_s = orig_price_s_min * 1.25
            elif orig_price_s_min >= 10000 and orig_price_s_min < 50000:
                retail_price_s = orig_price_s_min * 1.3
            elif orig_price_s_min >= 3000 and orig_price_s_min < 10000:
                retail_price_s = orig_price_s_min * 1.4
            elif orig_price_s_min >= 1000 and orig_price_s_min < 3000:
                retail_price_s = orig_price_s_min * 1.6
            elif orig_price_s_min < 1000:
                retail_price_s = orig_price_s_min * 3

            if orig_price_m_1_min >= 200000:
                retail_price_m_1 = orig_price_m_1_min * 1.15
            elif orig_price_m_1_min >= 100000 and orig_price_m_1_min < 200000:
                retail_price_m_1 = orig_price_m_1_min * 1.2
            elif orig_price_m_1_min >= 50000 and orig_price_m_1_min < 100000:
                retail_price_m_1 = orig_price_m_1_min * 1.25
            elif orig_price_m_1_min >= 10000 and orig_price_m_1_min < 50000:
                retail_price_m_1 = orig_price_m_1_min * 1.3
            elif orig_price_m_1_min >= 3000 and orig_price_m_1_min < 10000:
                retail_price_m_1 = orig_price_m_1_min * 1.4
            elif orig_price_m_1_min >= 1000 and orig_price_m_1_min < 3000:
                retail_price_m_1 = orig_price_m_1_min * 1.6
            elif orig_price_m_1_min < 1000:
                retail_price_m_1 = orig_price_m_1_min * 3

            if orig_price_m_2_min >= 200000:
                retail_price_m_2 = orig_price_m_2_min * 1.15
            elif orig_price_m_2_min >= 100000 and orig_price_m_2_min < 200000:
                retail_price_m_2 = orig_price_m_2_min * 1.2
            elif orig_price_m_2_min >= 50000 and orig_price_m_2_min < 100000:
                retail_price_m_2 = orig_price_m_2_min * 1.25
            elif orig_price_m_2_min >= 10000 and orig_price_m_2_min < 50000:
                retail_price_m_2 = orig_price_m_2_min * 1.3
            elif orig_price_m_2_min >= 3000 and orig_price_m_2_min < 10000:
                retail_price_m_2 = orig_price_m_2_min * 1.4
            elif orig_price_m_2_min >= 1000 and orig_price_m_2_min < 3000:
                retail_price_m_2 = orig_price_m_2_min * 1.6
            elif orig_price_m_2_min < 1000:
                retail_price_m_2 = orig_price_m_2_min * 3

            if orig_price_m_3_min >= 200000:
                retail_price_m_3 = orig_price_m_3_min * 1.15
            elif orig_price_m_3_min >= 100000 and orig_price_m_3_min < 200000:
                retail_price_m_3 = orig_price_m_3_min * 1.2
            elif orig_price_m_3_min >= 50000 and orig_price_m_3_min < 100000:
                retail_price_m_3 = orig_price_m_3_min * 1.25
            elif orig_price_m_3_min >= 10000 and orig_price_m_3_min < 50000:
                retail_price_m_3 = orig_price_m_3_min * 1.3
            elif orig_price_m_3_min >= 3000 and orig_price_m_3_min < 10000:
                retail_price_m_3 = orig_price_m_3_min * 1.4
            elif orig_price_m_3_min >= 1000 and orig_price_m_3_min < 3000:
                retail_price_m_3 = orig_price_m_3_min * 1.6
            elif orig_price_m_3_min < 1000:
                retail_price_m_3 = orig_price_m_3_min * 3

            pub_config_dict["pos_s"]["retail_price_min"] = retail_price_s
            pub_config_dict["pos_s"]["retail_price_max"] = retail_price_s
            pub_config_dict['pos_m_1']['retail_price_min'] = retail_price_m_1
            pub_config_dict['pos_m_1']['retail_price_max'] = retail_price_m_1
            pub_config_dict['pos_m_2']['retail_price_min'] = retail_price_m_2
            pub_config_dict['pos_m_2']['retail_price_max'] = retail_price_m_2
            pub_config_dict['pos_m_3']['retail_price_min'] = retail_price_m_3
            pub_config_dict['pos_m_3']['retail_price_max'] = retail_price_m_3
            pub_config_media_vendor_weixin_price_list = str(
                pub_config_dict).replace("'", '"')
            query = ("UPDATE `media_vendor_weixin_price_list` SET `retail_price_s_min`=%.2f,`retail_price_s_max`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_1_max`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_2_max`=%.2f,`retail_price_m_3_min`=%.2f,`retail_price_m_3_max`=%.2f,`pub_config`='%s' WHERE `bind_uuid`='%s'" % (
                retail_price_s, retail_price_s, retail_price_m_1, retail_price_m_1, retail_price_m_2, retail_price_m_2, retail_price_m_3, retail_price_m_3, pub_config_media_vendor_weixin_price_list, uuid_media_vendor_bind))
            print(query)
            target_mysql_tool_1.cursor.execute(query)
            target_mysql_tool_1.cnx.commit()
            if is_pref_vendor == 1:
                query = ("UPDATE `media_weixin` SET `retail_price_s_min`=%.2f,`retail_price_s_max`=%.2f,`retail_price_m_1_min`=%.2f,`retail_price_m_1_max`=%.2f,`retail_price_m_2_min`=%.2f,`retail_price_m_2_max`=%.2f,`retail_price_m_3_min`=%.2f,`retail_price_m_3_max`=%.2f,`pub_config`='%s' WHERE `uuid`='%s'" % (
                    retail_price_s, retail_price_s, retail_price_m_1, retail_price_m_1, retail_price_m_2, retail_price_m_2, retail_price_m_3, retail_price_m_3, pub_config_media_vendor_weixin_price_list, media_uuid))
                print(query)
                target_mysql_tool_1.cursor.execute(query)
                target_mysql_tool_1.cnx.commit()
            # input("..............................")

    def media_video_to_video_collection_info(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = (
            "SELECT `account_id`,`platform_type` FROM `video_platform_common_info`")
        source_mysql_tool.cursor.execute(query)

        platform_dict = {1: "huajiao", 2: "meipai", 5: "miaopai", 6: "douyu"}

        for row in source_mysql_tool.cursor:
            (account_id, platform) = row
            if account_id is None or account_id.strip() == "":
                continue
            print("account_id :%s ,type %d" % (str(account_id), platform))
            if platform in (1, 2, 5, 6):
                insert_platform = None
                uuid = uuid_generator()
                if platform == 1:
                    insert_platform = 1
                elif platform == 2:
                    insert_platform = 2
                elif platform == 5:
                    insert_platform = 3
                elif platform == 6:
                    insert_platform = 4

                query = ("INSERT INTO `video_collection_info`(`uuid`,`user_id`,`platform`) VALUES('%s','%s',%d)" % (
                    uuid, account_id, insert_platform))
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()

    def miaopai_account_to_collection_info(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `media_id` FROM `video_media_miaopai`")
        source_mysql_tool.cursor.execute(query)
        for row in source_mysql_tool.cursor:
            (account_id,) = row
            if account_id is None or account_id.strip() is '':
                continue
            print(account_id)
            uuid = uuid_generator()
            insert_platform = 3
            query = ("INSERT INTO `video_collection_info`(`uuid`,`user_id`,`platform`) VALUES('%s','%s',%d)" % (
                uuid, account_id, insert_platform))
            target_mysql_tool.cursor.execute(query)
            target_mysql_tool.cnx.commit()

    def media_video_price_adjust(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `uuid` FROM `video_vendor_price`")
        uuid_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        for uuid in uuid_list:
            print("uuid-> %s" % uuid)
            query = (
                "SELECT `price_orig_one`,`price_orig_two`,`price_config` FROM `video_vendor_price` WHERE `uuid`='%s'" % uuid)
            source_mysql_tool.cursor.execute(query)
            row = source_mysql_tool.cursor.fetchone()
            (price_orig_one, price_orig_two, price_config) = row
            '''
            5万及以上的1.5倍（大于等于）
            2万-5万按1.7倍
            2万及以下按2倍
            '''
            if float(price_orig_one) >= 50000:
                price_retail_one = float(price_orig_one) * 1.5
            elif float(price_orig_one) >= 20000 and float(price_orig_one) < 50000:
                price_retail_one = float(price_orig_one) * 1.7
            elif float(price_orig_one) < 20000:
                price_retail_one = float(price_orig_one) * 2
            price_execute_one = float(price_orig_one)
            if float(price_orig_two) >= 50000:
                price_retail_two = float(price_orig_two) * 1.5
            elif float(price_orig_two) >= 20000 and float(price_orig_two) < 50000:
                price_retail_two = float(price_orig_two) * 1.7
            elif float(price_orig_two) < 20000:
                price_retail_two = float(price_orig_two) * 2
            price_execute_two = float(price_orig_two)
            price_config_dict = json.loads(price_config)
            price_config_dict["retail_one"] = price_retail_one
            price_config_dict["retail_two"] = price_retail_two
            price_config_dict["execute_one"] = price_retail_one
            price_config_dict["execute_two"] = price_retail_two
            query = ("UPDATE `video_vendor_price` SET `price_retail_one`=%.2f,`price_retail_two`=%.2f,`price_execute_one`=%.2f,`price_execute_two`=%.2f,`price_config`='%s' WHERE `uuid`='%s'" % (
                price_retail_one, price_retail_two, price_execute_one, price_execute_two, json.dumps(price_config_dict), uuid))
            target_mysql_tool.cursor.execute(query)
            target_mysql_tool.cnx.commit()
            # input('-----------------------------')
        source_mysql_tool.disconnection()
        target_mysql_tool.disconnection()

    def media_weibo_price_adjust(self, source_mysql_args, target_mysql_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        query = ("SELECT `uuid` FROM `weibo_vendor_bind`")
        uuid_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        for uuid in uuid_list:
            print("uuid-> %s" % uuid)
            query = ("SELECT `soft_direct_price_orig`,`soft_transfer_price_orig`,`micro_direct_price_orig`,`micro_transfer_price_orig` FROM `weibo_vendor_bind` WHERE `uuid`='%s'" % uuid)
            source_mysql_tool.cursor.execute(query)
            row = source_mysql_tool.cursor.fetchone()
            soft_direct_price_orig, soft_transfer_price_orig, micro_direct_price_orig, micro_transfer_price_orig = row
            '''
            微博定价
            200元及以下4倍
            200元-1000元含以下2.5倍
            1000元-3000元含1.8倍
            3000元-8000元含1.5倍
            8000以上1.3倍
            特殊账号媒介直接操作零售价格
            '''
            if float(soft_direct_price_orig) <= 200:
                soft_direct_price_retail = float(soft_direct_price_orig) * 4
            elif float(soft_direct_price_orig) <= 1000 and float(soft_direct_price_orig) > 200:
                soft_direct_price_retail = float(soft_direct_price_orig) * 2.5
            elif float(soft_direct_price_orig) <= 3000 and float(soft_direct_price_orig) > 1000:
                soft_direct_price_retail = float(soft_direct_price_orig) * 1.8
            elif float(soft_direct_price_orig) <= 8000 and float(soft_direct_price_orig) > 3000:
                soft_direct_price_retail = float(soft_direct_price_orig) * 1.5
            elif float(soft_direct_price_orig) > 8000:
                soft_direct_price_retail = float(soft_direct_price_orig) * 1.3
            soft_direct_price_execute = float(soft_direct_price_orig)

            if float(soft_transfer_price_orig) <= 200:
                soft_transfer_price_retail = float(
                    soft_transfer_price_orig) * 4
            elif float(soft_transfer_price_orig) <= 1000 and float(soft_transfer_price_orig) > 200:
                soft_transfer_price_retail = float(
                    soft_transfer_price_orig) * 2.5
            elif float(soft_transfer_price_orig) <= 3000 and float(soft_transfer_price_orig) > 1000:
                soft_transfer_price_retail = float(
                    soft_transfer_price_orig) * 1.8
            elif float(soft_transfer_price_orig) <= 8000 and float(soft_transfer_price_orig) > 3000:
                soft_transfer_price_retail = float(
                    soft_transfer_price_orig) * 1.5
            elif float(soft_transfer_price_orig) > 8000:
                soft_transfer_price_retail = float(
                    soft_transfer_price_orig) * 1.3
            soft_transfer_price_execute = float(soft_transfer_price_orig)

            if float(micro_direct_price_orig) <= 200:
                micro_direct_price_retail = float(micro_direct_price_orig) * 4
            elif float(micro_direct_price_orig) <= 1000 and float(micro_direct_price_orig) > 200:
                micro_direct_price_retail = float(
                    micro_direct_price_orig) * 2.5
            elif float(micro_direct_price_orig) <= 3000 and float(micro_direct_price_orig) > 1000:
                micro_direct_price_retail = float(
                    micro_direct_price_orig) * 1.8
            elif float(micro_direct_price_orig) <= 8000 and float(micro_direct_price_orig) > 3000:
                micro_direct_price_retail = float(
                    micro_direct_price_orig) * 1.5
            elif float(micro_direct_price_orig) > 8000:
                micro_direct_price_retail = float(
                    micro_direct_price_orig) * 1.3
            micro_direct_price_execute = float(micro_direct_price_orig)

            if float(micro_transfer_price_orig) <= 200:
                micro_transfer_price_retail = float(
                    micro_transfer_price_orig) * 4
            elif float(micro_transfer_price_orig) <= 1000 and float(micro_transfer_price_orig) > 200:
                micro_transfer_price_retail = float(
                    micro_transfer_price_orig) * 2.5
            elif float(micro_transfer_price_orig) <= 3000 and float(micro_transfer_price_orig) > 1000:
                micro_transfer_price_retail = float(
                    micro_transfer_price_orig) * 1.8
            elif float(micro_transfer_price_orig) <= 8000 and float(micro_transfer_price_orig) > 3000:
                micro_transfer_price_retail = float(
                    micro_transfer_price_orig) * 1.5
            elif float(micro_transfer_price_orig) > 8000:
                micro_transfer_price_retail = float(
                    micro_transfer_price_orig) * 1.3
            micro_transfer_price_execute = float(micro_transfer_price_orig)
            query = ("UPDATE `weibo_vendor_bind` SET `soft_direct_price_retail`=%.2f,`soft_transfer_price_retail`=%.2f,`micro_direct_price_retail`=%.2f,`micro_transfer_price_retail`=%.2f,`soft_direct_price_execute`=%.2f,`soft_transfer_price_execute`=%.2f,`micro_direct_price_execute`=%.2f,`micro_transfer_price_execute`=%.2f WHERE `uuid`='%s'" % (
                soft_direct_price_retail, soft_transfer_price_retail, micro_direct_price_retail, micro_transfer_price_retail, soft_direct_price_execute, soft_transfer_price_execute, micro_direct_price_execute, micro_transfer_price_execute, uuid))
            target_mysql_tool.cursor.execute(query)
            target_mysql_tool.cnx.commit()
            # input("-----------------------------")

        source_mysql_tool.disconnection()
        target_mysql_tool.disconnection()
    def wom_dev_media_tags(self,source_mysql_args_1,source_mysql_args_2,source_mysql_args_3,target_mysql_args):
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        source_mysql_tool_3 = self.get_mysql_tool(*source_mysql_args_3)

        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        settled_count = 0

        sql = "SELECT `id`,`public_name`,`account_short_desc` from `weixin_cate`"
        source_mysql_tool_1.cursor.execute(sql)
        for row_weixin_cate in source_mysql_tool_1.cursor:
            print("----------start-----------")
            (id_weixin_cate,public_name,account_short_desc) = row_weixin_cate
            merge_str = public_name + account_short_desc 
            # sql = "SELECT `id`,`name` FROM `wom_district` WHERE `level` = 3"
            # source_mysql_tool_2.cursor.execute(sql)
            media_tags_list = []
            tags_get_flag = 0
            # count_first = 0
            # for row_wom_district in source_mysql_tool_2.cursor:
            #     count_first +=1
            #     (id_wom_district,name) = row_wom_district
            #     if len(name[:-1]) <2:
            #         continue
            #     if name[:-1] in merge_str:
            #         print("current id is %d" % id_wom_district)
            #         sql = """SELECT c.a_id AS `c_a_id`,c.a_name AS `c_a_name`,c.a_level AS `c_a_level`,c.a_upid AS `c_a_upid`,c.b_id AS `c_b_id`,c.b_name AS `c_b_name`,c.b_level AS `c_b_level`,c.b_upid AS `c_b_upid`,d.id AS `d_id`,d.name AS `d_name`,d.level AS `d_level`,d.upid AS `d_upid` FROM (SELECT a.id AS `a_id`,a.name AS `a_name`,a.level AS `a_level`,a.upid AS `a_upid`,b.id AS `b_id`,b.name AS `b_name`,b.level AS `b_level`,b.upid AS `b_upid` FROM `wom_district`  a RIGHT JOIN `wom_district` b ON a.upid = b.id WHERE a.id = %d) c RIGHT JOIN `wom_district` d ON c.b_upid = d.id WHERE c.a_id = %d""" % (id_wom_district,id_wom_district)
            #         source_mysql_tool_3.cursor.execute(sql)
            #         row_target = source_mysql_tool_3.cursor.fetchone()
            #         if row_target is not None:
            #             (c_a_id,c_a_name,c_a_level,c_a_upid,c_b_id,c_b_name,c_b_level,c_b_upid,d_id,d_name,d_level,d_upid) = row_target
            #             media_cate_item = {1:{"id" : -1,
            #                                   "name" : ""},
            #                                2:{"id" : -1,
            #                                   "name" : ""},
            #                                3:{"id" : -1,
            #                                   "name" : ""}}
            #             # print(c_a_name+"---------------------------",count_first)
            #             media_cate_item[3]["id"] = c_a_id
            #             media_cate_item[3]["name"] = c_a_name
            #             media_cate_item[2]["id"] = c_b_id
            #             media_cate_item[2]["name"] = c_b_name
            #             media_cate_item[1]["id"] = d_id
            #             media_cate_item[1]["name"] = d_name
            #             media_tags_list.append(media_cate_item)
            #             tags_get_flag = 1
            print("-------------start 2-------------------")
            sql = "SELECT `id`,`name` FROM `wom_district` WHERE `level` = 2"
            source_mysql_tool_2.cursor.execute(sql)
            count_sec = 0
            for row_wom_district in source_mysql_tool_2.cursor:
                count_sec +=1
                # print("count_sec：",count_sec)
                (id_wom_district,name) = row_wom_district
                if len(name[:-1]) <2:
                    continue
                if name[:-1] in merge_str:
                    print("current id is %d" % id_wom_district ,"count_sec: ",count_sec)
                    sql = """SELECT a.id AS `a_id`,a.name AS `a_name`,a.level AS `a_level`,a.upid AS `a_upid`,b.id AS `b_id`,b.name AS `b_name`,b.level AS `b_level`,b.upid AS `b_upid` FROM `wom_district`  a RIGHT JOIN `wom_district` b ON a.upid = b.id WHERE a.id = %d""" % id_wom_district
                    source_mysql_tool_3.cursor.execute(sql)
                    row_target = source_mysql_tool_3.cursor.fetchone()
                    if row_target is not None:
                        (a_id,a_name,a_level,a_upid,b_id,b_name,b_level,b_upid) = row_target
                        media_cate_item = {1:{"id" : -1,
                                          "name" : ""},
                                       2:{"id" : -1,
                                          "name" : ""}
                                       }
                        if len(media_tags_list) >= 1:
                            # media_tags_copy = media_tags_list[:]
                            item_2_id_list = []
                            for item in media_tags_list:
                                item_2_id_list.append(item[2]["id"])
                            if a_id not in item_2_id_list:
                                item_2_id_list.append(a_id)
                                media_cate_item[2]["id"] = a_id
                                media_cate_item[2]["name"] = a_name
                                media_cate_item[1]["id"] = b_id
                                media_cate_item[1]["name"] = b_name
                                media_tags_list.append(media_cate_item)
                                tags_get_flag = 1
                            # print(media_tags_list,2)
                        else:
                            # print("end ................4")
                            media_cate_item[2]["id"] = a_id
                            media_cate_item[2]["name"] = a_name
                            media_cate_item[1]["id"] = b_id
                            media_cate_item[1]["name"] = b_name
                            media_tags_list.append(media_cate_item)
                            # print(media_tags_list,1)
                            tags_get_flag = 1

            print("----------start 3-----------")
            sql = "SELECT `id`,`name` FROM `wom_district` WHERE `level` = 1"
            source_mysql_tool_2.cursor.execute(sql)
            count_third = 0
            for row_wom_district in source_mysql_tool_2.cursor:
                count_third += 1
                (id_wom_district,name) = row_wom_district
                if len(name[:-1]) <2:
                    continue
                if name[:-1] in merge_str:
                    print("count third: ",count_third)
                    media_cate_item = {1:{"id" : -1,
                                          "name" : ""},
                                       2:{"id" : -1,
                                          "name" : ""}
                                       }
                    if len(media_tags_list) >= 1:
                        item_1_id_list = []
                        # media_tags_copy = media_tags_list[:]
                        for item in media_tags_list:
                            item_1_id_list.append(item[1]["id"])
                        if id_wom_district not in item_1_id_list:
                            item_1_id_list.append(id_wom_district)
                            media_cate_item[1]["id"] = id_wom_district
                            media_cate_item[1]["name"] = name
                            media_tags_list.append(media_cate_item)
                            tags_get_flag = 1
                            # media_tags_list = media_tags_copy[:]
                    else:
                        media_cate_item[1]["id"] = id_wom_district
                        media_cate_item[1]["name"] = name
                        media_tags_list.append(media_cate_item)
                        tags_get_flag = 1
            sql = "UPDATE `weixin_cate` SET `media_tags` = '%s' where `id` = %d" % (json.dumps(media_tags_list, ensure_ascii=False),id_weixin_cate)
            target_mysql_tool.cursor.execute(sql)
            target_mysql_tool.cnx.commit()
            settled_count+=1
            print(media_tags_list)
            print("settled %d accounts" % settled_count)
        source_mysql_tool_1.disconnection()
        source_mysql_tool_2.disconnection()
        source_mysql_tool_3.disconnection()
        target_mysql_tool.disconnection()

    def mongo_media_weixin_real_public_id_updated(self,source_mysql_args,target_mongo_args):
        source_mysql_tool=self.get_mysql_tool(*source_mysql_args)
        target_mongo_tool=self.get_mongo_tool(*target_mongo_args)
        sql = "SELECT `real_public_id` FROM `media_weixin` WHERE `real_public_id` != ''"
        source_mysql_tool.cursor.execute(sql)
        count = 0 
        for row in source_mysql_tool.cursor:
            (real_public_id,) = row
            target_mongo_tool.database['media_weixin'].update(
                        {'public_id': real_public_id}, {'$set': {'real_public_id_updated': 1}})
            count+=1
            print("settled %d accounts" % count)
    def mysql_media_weixin_basic_info_update(self,source_mongo_args,target_mysql_args):
        target_mysql_tool=self.get_mysql_tool(*target_mysql_args)
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        documents = source_mongo_tool.database["media_weixin"].find({}).batch_size(1000)
        count = 0
        for document in documents:
            if document is not None:
                if "public_name" in document:
                    public_name = document["public_name"].replace("'", '"')
                else:
                    public_name = ""

                if "avatar_big_img" in document:
                    avatar_big_img = document[
                        "avatar_big_img"]
                else:
                    avatar_big_img = ""

                if "avatar_small_img" in document:
                    avatar_small_img = document[
                        "avatar_small_img"]
                else:
                    avatar_small_img = ""

                if "qrcode_img" in document:
                    qrcode_img = document["qrcode_img"]
                else:
                    qrcode_img = ""

                if "account_cert" in document:
                    account_cert = document["account_cert"]
                else:
                    account_cert = 0

                if "account_cert_info" in document:
                    account_cert_info = document[
                        "account_cert_info"].replace("'", '"')
                else:
                    account_cert_info = ""

                if "account_short_desc" in document:
                    account_short_desc = document[
                        "account_short_desc"].replace("'", '"')
                else:
                    account_short_desc = ""
                query = ("UPDATE `media_weixin` SET `public_name`='%s',`avatar_big_img`='%s',`avatar_small_img`='%s',`qrcode_img`='%s',`account_cert`=%d,`account_cert_info`='%s' ,`account_short_desc`='%s' WHERE `real_public_id`='%s'" % (
                        public_name, avatar_big_img, avatar_small_img, qrcode_img, account_cert, account_cert_info, account_short_desc, document["public_id"]))
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
            count+=1
            print("settled %d accounts" % count)

if __name__ == '__main__':
    media_weixin_storage = MediaWeiXinStorage()
    # media_weixin_storage.media_weixin_storage_csv()
    media_weixin_storage.media_weixin_storage_mongo()
    # media_weixin_storage.mysql_to_mysql()
