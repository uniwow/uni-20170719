# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_string import strip_and_replace_all
from models.influence_rank_model import DtsMediaWeixinInfluenceRank, DtsMediaWeixinInfluenceRankSlave, DtsInfluenceRankTableTip, MediaWeixinStatistic, influence_data_prod_database
from models.media_weixin_model import MediaWeixin, prod_database
from tools.tool_time import datetime_to_str, datetime_yesterday_to_str, current_time_to_stamp, current_datetime_to_str
import datetime
import re
import json
import math
from models.video_table_model import MediaVideo, VendorVideoBind, VideoPlatformCommonInfo, VideoVendorPrice, VideoVendorBind, VideoMediaBaseInfo, VideoMediaDouyu, VideoMediaHani, VideoMediaHuajiao, VideoMediaMeipai, VideoMediaMiaopai, VideoMediaPanda, VideoMediaTaobao, VideoMediaYingke, VideoMediaYizhibo, MediaVendor
from tools.tool_time import uuid_generator
import pymongo
import requests
from pykafka import KafkaClient
import time
from models.media_vender_bind_model import MediaVendorBind,MediaVendorWeixinPriceList,WeixinVendorBind,DtsMediaSearch,DtsMediaGroupMap,WomAdminBaseConf,WeiboVendorBind,BindVideoVendorPrice,dev_database
import os
from models.sogou_weixin_article_crawl_model import SogouWeixinAccountInitTask,SogouWeixinAccountLatestArticleCrawlTask
from models.dts_media_weixin_kaikai import database_kaikai,MongoMediaWeiXin
from tools.tool_dict import dict2list
from tqdm import tqdm
client = KafkaClient(
    hosts="server-32:9092, server-32:9093, server-32:9094")
topic = client.topics[b'media_weixin_statistic_wordle-1']


class MediaWeiXinStorageORM(object):

    def __init__(self):
        # database.connect()
        pass

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def dts_media_weixin_influence_rank_slave(self, source_mongo_args_1, source_mongo_args_2):

        _calculate_timestamp = current_time_to_stamp()

        source_mongo_tool_1 = self.get_mongo_tool(*source_mongo_args_1)
        source_mongo_tool_2 = self.get_mongo_tool(*source_mongo_args_2)
        current_date_time = datetime.datetime.now()
        current_date_time_to_str = datetime_to_str(current_date_time)
        document_list = source_mongo_tool_1.database[
            "media_weixin_info"].find({})
        month_cycle_type_tuple = ("month", "oneMonth", "twoMonth")
        week_cycle_type_tuple = ("week", "oneWeek", "twoWeek", "threeWeek")
        cycle_type_tuple = ("month", "oneMonth", "twoMonth",
                            "week", "oneWeek", "twoWeek", "threeWeek")
        classify_rank_tuple = (('rank_1000', 'rank_1000_inc', 0), ('rank_1001', 'rank_1001_inc', 1), ('rank_1002', 'rank_1002_inc', 2), ('rank_1003', 'rank_1003_inc', 3), ('rank_1004', 'rank_1004_inc', 4), ('rank_1005', 'rank_1005_inc', 5), ('rank_1006', 'rank_1006_inc', 6), ('rank_1007', 'rank_1007_inc', 7), ('rank_1008', 'rank_1008_inc', 8), ('rank_1009', 'rank_1009_inc', 9), ('rank_1010', 'rank_1010_inc', 10), ('rank_1011', 'rank_1011_inc', 11), ('rank_1012', 'rank_1012_inc', 12), (
            'rank_1013', 'rank_1013_inc', 13), ('rank_1014', 'rank_1014_inc', 14), ('rank_1015', 'rank_1015_inc', 15), ('rank_1016', 'rank_1016_inc', 16), ('rank_1017', 'rank_1017_inc', 17), ('rank_1018', 'rank_1018_inc', 18), ('rank_1019', 'rank_1019_inc', 19), ('rank_1020', 'rank_1020_inc', 20), ('rank_1021', 'rank_1021_inc', 21), ('rank_1022', 'rank_1022_inc', 22), ('rank_1023', 'rank_1023_inc', 23), ('rank_1024', 'rank_1024_inc', 24), ('rank_1025', 'rank_1025_inc', 25), ('rank_1026', 'rank_1026_inc', 26))
        auth_info_type_tuple = (
            "account_cert", "tencent_account_cert", "sina_account_cert",)
        # database.connect()
        write_to = DtsInfluenceRankTableTip.get(
            DtsInfluenceRankTableTip.id == 1).read_from
        read_from = write_to
        # database.close()
        write_to_orm = None
        delete_from_orm = None
        if write_to == "dts_media_weixin_influence_rank":
            write_to = "dts_media_weixin_influence_rank_slave"
            write_to_orm = DtsMediaWeixinInfluenceRankSlave
            delete_from_orm = DtsMediaWeixinInfluenceRank
        elif write_to == "dts_media_weixin_influence_rank_slave":
            write_to = "dts_media_weixin_influence_rank"
            write_to_orm = DtsMediaWeixinInfluenceRank
            delete_from_orm = DtsMediaWeixinInfluenceRankSlave

        write_to_orm.delete().execute()
        settled_count = 0
        total_count = document_list.count()

        hour_of_day_list = ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23")
        day_of_week_list = ("1", "2", "3", "4", "5", "6", "7")
        re_head_view_avg_cnt = "(\d+)_head_view_avg_cnt"
        re_head_like_avg_cnt = "(\d+)_head_like_avg_cnt"

        re_many_second_view_avg_cnt = "(\d+)_many_second_view_avg_cnt"
        re_many_second_like_avg_cnt = "(\d+)_many_second_like_avg_cnt"

        re_day_of_month_article_cnt = "(\d+)_day_of_month_article_cnt"
        re_day_of_week_article_avg_cnt = "(\d+)_day_of_week_article_avg_cnt"
        re_hour_of_day_article_cnt = "(\d+)_hour_of_day_article_cnt"
        re_rank = "rank_(\d+)"

        dts_media_weixin_influence_rank_data_source_list = []
        dts_media_weixin_influence_rank_public_id_list = []
        dts_media_weixin_influence_rank_data_source_count = 0
        for document in document_list:
            print(document["public_id"])
            dts_media_weixin_influence_rank_public_id_list.append(document[
                                                                  "public_id"])
            settled_count += 1
            total_count -= 1
            if "cycle" not in document or current_date_time_to_str not in document['cycle']:
                continue
            """
            更新详情页数据
            """
            _account_keyword = {}
            _article_effect = {}
            _influence_data = {}
            _operation_status = {}
            _public = document["public_id"]
            _update_time = current_datetime_to_str()

            month_article_effect = {}
            operational_status = {}
            month_article_keyword = {}

            month_head_article_trend = {}
            month_next_article_trend = {}

            month_post_trend = {}
            month_post_time_hour_distribute = {}
            month_post_time_week_distribute = {}
            month_rank_trend = {}

            for k, v in document['cycle'][current_date_time_to_str]["month"].items():
                m = re.search(re_head_view_avg_cnt, k)
                if m is not None:
                    month_head_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_head_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_many_second_view_avg_cnt, k)
                if m is not None:
                    month_next_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_many_second_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_day_of_month_article_cnt, k)
                if m is not None:
                    month_post_trend[str(m.group(1))] = document['cycle'][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_hour_of_day_article_cnt, k)
                if m is not None:
                    month_post_time_hour_distribute[str(m.group(1))] = document["cycle"][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_day_of_week_article_avg_cnt, k)
                if m is not None:
                    day_of_week = int(m.group(1))
                    if day_of_week == 0:
                        day_of_week = 7
                    month_post_time_week_distribute[str(day_of_week)] = document["cycle"][
                        current_date_time_to_str]["month"][k]

            month_statistics = {}
            month_statistics["head_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["head_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["head_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["head_10w_article_total_cnt"]
            }
            month_statistics["next_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_10w_article_total_cnt"]
            }
            month_article_effect[
                "month_head_article_trend"] = month_head_article_trend
            month_article_effect[
                "month_next_article_trend"] = month_next_article_trend
            month_article_effect["month_statistics"] = month_statistics
            """
            文章效果
            """
            _article_effect["month_article_effect"] = month_article_effect
            """
            运营状况
            """
            operational_status["month_post_articles_count"] = document["cycle"][
                current_date_time_to_str]["month"]["article_total_cnt"]
            operational_status["month_post_count"] = document["cycle"][
                current_date_time_to_str]["month"]["pub_total_cnt"]
            operational_status["month_article_per_post"] = document["cycle"][
                current_date_time_to_str]["month"]["article_avg_pub_cnt"]
            operational_status["month_post_trend"] = month_post_trend
            operational_status[
                "month_post_time_hour_distribute"] = month_post_time_hour_distribute
            operational_status[
                "month_post_time_week_distribute"] = month_post_time_week_distribute
            operational_status["month_single_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["single_avg_view_cnt"]
            operational_status["month_multi_pos_1_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos1_avg_view_cnt"]
            operational_status["month_multi_pos_2_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos2_avg_view_cnt"]
            operational_status["month_multi_pos_3_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos3_avg_view_cnt"]

            _operation_status["operational_status"] = operational_status
            """
            账户关键词
            """

            if "text_info" in document and current_date_time_to_str in document["text_info"] and "account_wordle" in document['text_info'][current_date_time_to_str]:
                _account_keyword["month_article_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle"]
                _account_keyword["month_top_five_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle_top_5"]
            else:
                _account_keyword["month_article_keyword"] = []
                _account_keyword["month_top_five_keyword"] = []

            """
            影响力数据
            """
            for k, v in document["rank_trend"].items():
                m = re.search(re_rank, k)
                if m is not None:
                    # print(type(m.group(1)))
                    rank_trend_list = []
                    for rank_trend in document["rank_trend"][k][-30:]:
                        rank_trend_dict = {}
                        rank_trend_dict[rank_trend[
                            "name"]] = rank_trend["value"]
                        rank_trend_list.append(rank_trend_dict)
                    month_rank_trend[
                        str(int(m.group(1)) - 1000)] = rank_trend_list

            _influence_data["month_rank_trend"] = month_rank_trend
            month_wom_index_trend_list = []
            for month_wom_index_rank in document["month_wom_index_rank"][-30:]:
                month_wom_index_rank_dict = {}
                month_wom_index_rank_dict[month_wom_index_rank[
                    "name"]] = month_wom_index_rank["value"]
                month_wom_index_trend_list.append(month_wom_index_rank_dict)
            _influence_data[
                "month_wom_index_trend"] = month_wom_index_trend_list
            
            if MediaWeixinStatistic.select().where(MediaWeixinStatistic.public == document["public_id"]).count() == 1:
                # account_keyword=json.dumps(_account_keyword, ensure_ascii=False),
                MediaWeixinStatistic.update(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    update_time=current_datetime_to_str()).where(MediaWeixinStatistic.public == document["public_id"]).execute()
                # print("1")

            else:
                # print("2")
                MediaWeixinStatistic.insert(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    public=document["public_id"],
                    update_time=current_datetime_to_str()).execute()

            # input(",,,,,,,,,,,,,,,,,,,,,")
            # print(str(document))
            _account_short_desc = ''
            _auth_info = ''
            _auth_type = -1
            _nickname = ''
            _follower_num = -1
            _retail_avg_price = 0

            document_media_weixin = source_mongo_tool_2.database[
                "media_weixin"].find_one({"public_id": document["public_id"]})
            if MediaWeixin.select().where(MediaWeixin.real_public == document["public_id"]).count() == 1:

                media_weixin_record = MediaWeixin.get(
                    MediaWeixin.real_public == document["public_id"])
                _follower_num = media_weixin_record.follower_num
                if(media_weixin_record.retail_price_m_1_min == 0):
                    if(media_weixin_record.retail_price_m_2_min == 0):
                        if(media_weixin_record.retail_price_m_3_min == 0):
                            _retail_avg_price = 0
                        else:
                            _retail_avg_price = float(
                                media_weixin_record.retail_price_m_3_min)
                    else:
                        _retail_avg_price = float(
                            media_weixin_record.retail_price_m_2_min)
                else:
                    _retail_avg_price = float(
                        media_weixin_record.retail_price_m_1_min)

            _m_avg_price_pv = 0
            if document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != -1 and document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != 0:
                _m_avg_price_pv = _retail_avg_price / \
                    document['cycle'][current_date_time_to_str][
                        "month"]["head_avg_view_cnt"]

            if document_media_weixin is not None:

                """
                更新榜单数据
                """

                if "account_short_desc" in document_media_weixin:
                    _account_short_desc = document_media_weixin[
                        "account_short_desc"]
                else:
                    _account_short_desc = ''
                for auth_info_type in auth_info_type_tuple:
                    if auth_info_type in document_media_weixin:
                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "account_cert":
                            _auth_type = 0
                            if "account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "tencent_account_cert":
                            _auth_type = 1
                            if "tencent_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "tencent_account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "sina_account_cert":
                            _auth_type = 2
                            if "sina_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "sina_account_cert_info"]

                if "public_name" in document_media_weixin:
                    _nickname = document_media_weixin["public_name"]
            week_or_month = None

            '''
            如果头条平均阅读数<1000或者沃米指数<=2.0，则跳过
            '''
            # if (document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["oneMonth"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["twoMonth"]["head_avg_view_cnt"] < 1000) or (document['cycle'][current_date_time_to_str]["month"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["oneMonth"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["twoMonth"]["wom_index"] <= 2):
            #     continue
            for cycle_type in cycle_type_tuple:
                if cycle_type in month_cycle_type_tuple:
                    week_or_month = 1
                else:
                    week_or_month = 0

                for classify_rank in classify_rank_tuple:
                    # print(current_date_time_to_str)
                    # print(document[current_date_time_to_str])

                    if classify_rank[0] in document['cycle'][current_date_time_to_str][cycle_type]:
                        # print("....................")
                        # database.connect()
                        # print(str(document[current_date_time_to_str][
                        #         cycle_type]["view_median"]))

                        # input("............................")

                        dts_media_weixin_influence_rank_data_source_count += 1
                        dts_media_weixin_influence_rank_data_source_item = {}
                        dts_media_weixin_influence_rank_data_source_item["_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["10w_article_total_cnt"]

                        dts_media_weixin_influence_rank_data_source_item["act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["article_post_end_date"] = document['cycle'][
                            current_date_time_to_str][cycle_type]["time"]
                        dts_media_weixin_influence_rank_data_source_item["article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_pub_count"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_avg_pub_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_median"]
                        dts_media_weixin_influence_rank_data_source_item["like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["max_post_time"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["max_post_time"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["pub_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["pub_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "public"] = document["public_id"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "classify"] = classify_rank[2]
                        dts_media_weixin_influence_rank_data_source_item["rank"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[0]]
                        dts_media_weixin_influence_rank_data_source_item["rank_incr"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[1]]
                        dts_media_weixin_influence_rank_data_source_item["single_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_median"]
                        dts_media_weixin_influence_rank_data_source_item["view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["wom_index"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["wom_index"]
                        '''
                        潜力指标计算
                        '''
                        dts_media_weixin_influence_rank_data_source_item[
                            "potential_index"] = 0
                        if document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] > 0:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][
                                current_date_time_to_str][cycle_type]["wom_index"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] + 1) * 0.4
                            dts_media_weixin_influence_rank_data_source_item[
                                "potential_index"] = math.pow(potential_index, 2)
                        else:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][
                                cycle_type]["wom_index"] + 1) * 0.3 - math.log((document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] * (-1)) + 1) * 0.4
                            if potential_index > 0:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = math.pow(potential_index, 2)
                            else:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = (math.pow((potential_index * (-1)), 2)) * (-1)

                        dts_media_weixin_influence_rank_data_source_item[
                            "cycle_type"] = week_or_month
                        dts_media_weixin_influence_rank_data_source_item[
                            "account_short_desc"] = _account_short_desc
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_type"] = _auth_type
                        dts_media_weixin_influence_rank_data_source_item[
                            "nickname"] = _nickname
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_info"] = _auth_info
                        dts_media_weixin_influence_rank_data_source_item["highest_read_num"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["highest_view_num"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "cost_per_read"] = _m_avg_price_pv
                        dts_media_weixin_influence_rank_data_source_item[
                            "fans_number"] = _follower_num

                        dts_media_weixin_influence_rank_data_source_list.append(
                            dts_media_weixin_influence_rank_data_source_item)
            try:
                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []
            except Exception as e:
                print("delete duplicated items.....................")
                influence_data_prod_database.connect()
                print(str(dts_media_weixin_influence_rank_public_id_list))
                for public_id in dts_media_weixin_influence_rank_public_id_list:
                    print("delete %s" % public_id)
                    write_to_orm.delete().where(write_to_orm.public == public_id).execute()

                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []

            print("settled %d items;%d items lefted" %
                  (settled_count, total_count))
            # input("...........................")
        '''
        导入最后不足100条数据
        '''
        try:
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()
        except Exception as e:
            print("delete duplicated items.....................")
            influence_data_prod_database.connect()
            for public_id in dts_media_weixin_influence_rank_public_id_list:
                write_to_orm.delete().where(write_to_orm.public == public_id).execute()
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()

        _read_from = write_to
        _update_time = current_datetime_to_str()
        # database.connect()
        '''
        如果写入数据超过100K,则切换表
        以免导致没有数据
        '''
        if write_to_orm.select().count() >= 100000:
            DtsInfluenceRankTableTip.update(calculate_timestamp=_calculate_timestamp,
                                            update_time=_update_time,
                                            read_from=_read_from).where(DtsInfluenceRankTableTip.id == 1).execute()
            delete_from_orm.delete().execute()
            delete_from_orm.raw("TRUNCATE TABLE `%s`" % read_from)
        influence_data_prod_database.close()
        source_mongo_tool_1.disconnection()
        source_mongo_tool_2.disconnection()
        # influence_data_prod_database.close()
        while True:
            result=requests.get("http://api.womdata.com/site/after-refresh-rank?access-token=b05a329eb619eb80763a03d50a81c233")
            if int(result.text) == 1:
                print("success!!!")
                break
            else:
                print("fail!!!")
                
        
        
    def video_media_transfer_orm(self):
        """
        迁移视频平台数据
        func_75
        """
        platform_dict = {"platform1": 1, "platform2": 2, "platform3": 3, "platform4": 4,
                         "platform5": 5, "platform6": 6, "platform7": 7, "platform8": 8, "platform9": 9}
        media_video_item_count = 0
        media_video_item_list = []
        media_video_item_dict = {}

        for video_media_base_info in VideoMediaBaseInfo.select():
            media_video_item_count += 1
            media_video_item_dict["address"] = video_media_base_info.area
            media_video_item_dict["coop_remark"] = video_media_base_info.remark
            media_video_item_dict["create_time"] = current_time_to_stamp()
            media_video_item_dict[
                "main_platform"] = video_media_base_info.main_platform
            media_video_item_dict["media_cate"] = video_media_base_info.tag
            media_video_item_dict["nickname"] = video_media_base_info.name_cn
            media_video_item_dict[
                "name"] = video_media_base_info.stage_name  # 真实姓名
            media_video_item_dict["sex"] = video_media_base_info.sex
            media_video_item_dict["update_time"] = current_time_to_stamp()
            media_video_item_dict["uuid"] = video_media_base_info.uuid
            MediaVideo.create(**media_video_item_dict)
            platform_conf_dict = json.loads(
                video_media_base_info.platform_conf)
            print(video_media_base_info.uuid)
            for k, v in platform_dict.items():

                if k in platform_conf_dict:
                    print("%s" % str(platform_conf_dict[k]))
                    video_platform_common_info_dict = {}
                    video_media_platform = None
                    platform_uuid = platform_conf_dict[k]["uuid"]

                    if v == 1:
                        video_media_platform = VideoMediaHuajiao
                        pass
                    elif v == 2:
                        video_media_platform = VideoMediaPanda
                    elif v == 3:
                        video_media_platform = VideoMediaHani
                    elif v == 4:
                        video_media_platform = VideoMediaMeipai
                    elif v == 5:
                        video_media_platform = VideoMediaMiaopai

                    elif v == 6:
                        video_media_platform = VideoMediaDouyu
                    elif v == 7:
                        video_media_platform = VideoMediaYingke
                    elif v == 9:
                        video_media_platform = VideoMediaYizhibo
                    if v == 8:
                        continue
                    if video_media_platform.select().where(video_media_platform.uuid == platform_uuid).count() == 0:
                        continue
                    video_media_platform_item = video_media_platform.get(
                        video_media_platform.uuid == platform_uuid)
                    video_platform_common_info_dict["uuid"] = platform_uuid
                    video_platform_common_info_dict[
                        "video_uuid"] = video_media_base_info.uuid
                    if video_media_platform_item.media is None:
                        continue
                    video_platform_common_info_dict[
                        "account"] = video_media_platform_item.media
                    video_platform_common_info_dict[
                        "account_name"] = video_media_platform_item.media_name
                    video_platform_common_info_dict[
                        "audit_time"] = current_time_to_stamp()
                    if v == 6 or v == 2:
                        video_platform_common_info_dict["auth_type"] = -1
                    else:
                        if video_media_platform_item.verify_status is None:
                            video_platform_common_info_dict["auth_type"] = -1

                        else:
                            video_platform_common_info_dict[
                                "auth_type"] = video_media_platform_item.verify_status

                    video_platform_common_info_dict[
                        "avatar"] = video_media_base_info.avatar_img
                    video_platform_common_info_dict[
                        "avg_watch_num"] = video_media_platform_item.avg_watch_num
                    video_platform_common_info_dict[
                        "create_time"] = current_time_to_stamp()
                    video_platform_common_info_dict[
                        "follower_num"] = video_media_platform_item.follower_num
                    video_platform_common_info_dict[
                        "is_put"] = video_media_platform_item.put_up
                    video_platform_common_info_dict[
                        "person_sign"] = video_media_platform_item.signature
                    video_platform_common_info_dict["platform_type"] = v
                    video_platform_common_info_dict[
                        "remark"] = video_media_platform_item.remark
                    video_platform_common_info_dict[
                        "status"] = video_media_platform_item.status
                    video_platform_common_info_dict[
                        "update_time"] = current_time_to_stamp()
                    video_platform_common_info_dict[
                        "url"] = video_media_platform_item.url
                    VideoPlatformCommonInfo.create(
                        **video_platform_common_info_dict)
                    vendor_uuid_list = video_media_platform_item.total_vendor_uuid.split(
                        ",")

                    for vendor_uuid in vendor_uuid_list:
                        print(vendor_uuid)
                        vendor_video_bind_item_dict = {}
                        if VideoVendorBind.select().where(VideoVendorBind.uuid == vendor_uuid).count() == 0:
                            continue
                        video_vendor_bind = VideoVendorBind.get(
                            VideoVendorBind.uuid == vendor_uuid)
                        vendor_video_bind_item_dict[
                            "active_end_time"] = current_time_to_stamp()
                        vendor_video_bind_item_dict[
                            "create_time"] = current_time_to_stamp()
                        is_pref_vendor = 0
                        if video_media_base_info.pref_bind_uuid == vendor_uuid:
                            is_pref_vendor = 1
                        vendor_video_bind_item_dict[
                            "is_pref_vendor"] = is_pref_vendor
                        vendor_video_bind_item_dict[
                            "status"] = video_vendor_bind.status
                        vendor_video_bind_item_dict[
                            "update_time"] = current_time_to_stamp()
                        vendor_video_bind_item_dict["uuid"] = vendor_uuid
                        if MediaVendor.select().where(MediaVendor.uuid == video_vendor_bind.vendor_uuid).count() != 0:
                            vendor_video_bind_item_dict[
                                "vendor_uuid"] = video_vendor_bind.vendor_uuid
                        else:
                            vendor_video_bind_item_dict[
                                "vendor_uuid"] = "1471856236aY2Iq"

                        vendor_video_bind_item_dict[
                            "video_uuid"] = video_media_base_info.uuid
                        VendorVideoBind.create(**vendor_video_bind_item_dict)
                        video_vendor_price_item_dict = {}
                        video_vendor_price_item_dict[
                            "create_time"] = current_time_to_stamp()
                        is_main_platform = 0
                        print(str(v))
                        # input("...............")
                        if v == video_media_base_info.main_platform:
                            is_main_platform = 1
                        video_vendor_price_item_dict[
                            "is_main_platform"] = is_main_platform
                        video_vendor_price_item_dict["platform_type"] = v
                        video_vendor_price_item_dict[
                            "platform_uuid"] = platform_uuid
                        # video_vendor_price_item_dict["price_config"] = TextField(null=True)
                        # price_execute_one"] = DecimalField()
                        # price_execute_two"] = DecimalField()

                        # print(str(len(price_orig_one)))
                        if "biz_coop_1" in platform_conf_dict[k]:
                            price_orig_one = platform_conf_dict[
                                k]["biz_coop_1"]["orig_price"]
                            # print(str(len(price_orig_one)))
                            if price_orig_one == "":
                                price_orig_one = "0"
                            price_orig_two = platform_conf_dict[
                                k]["biz_coop_2"]["orig_price"]
                            # print(str(len(price_orig_two)))
                            if price_orig_two == "":
                                price_orig_two = "0"
                        else:
                            price_orig_one = platform_conf_dict[
                                k]["biz_coop_3"]["orig_price"]
                            # print(str(len(price_orig_one)))
                            if price_orig_one == "":
                                price_orig_one = "0"

                            price_orig_two = platform_conf_dict[
                                k]["biz_coop_4"]["orig_price"]
                            # print(str(len(price_orig_two)))
                            if price_orig_two == "":
                                price_orig_two = "0"

                        video_vendor_price_item_dict[
                            "price_orig_one"] = price_orig_one
                        video_vendor_price_item_dict[
                            "price_orig_two"] = price_orig_two
                        video_vendor_price_item_dict[
                            "update_time"] = current_time_to_stamp()
                        video_vendor_price_item_dict["uuid"] = uuid_generator()
                        video_vendor_price_item_dict[
                            "vendor_bind_uuid"] = vendor_uuid
                        VideoVendorPrice.create(**video_vendor_price_item_dict)
                        # input(".............")

    def media_weixin_public_id_to_kafka(self, source_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        count = 0
        with topic.get_sync_producer() as producer:
            for line in source_file:
                count += 1
                print(source_file_tool.line_strip_decode_ignore(
                    line).split(",")[0] + "---" + str(count))
                public_id_dict = {}
                public_id_dict["public_id"] = source_file_tool.line_strip_decode_ignore(
                    line).split(",")[0]
                producer.produce(json.dumps(
                    public_id_dict).encode("utf-8", 'ignore'))
        source_file_tool.close_file()

    def media_weixin_statistic_update_from_kafka(self, source_mongo_args):
        consumer = topic.get_balanced_consumer(
            consumer_group=b"uni-1", auto_commit_enable=False, zookeeper_connect="server-32:2001,server-32:2002,server-32:2003")
        for message in consumer:
            public_id_dict = json.loads(
                message.value.decode("utf-8", 'ignore'))
            source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
            documents = source_mongo_tool.database["media_weixin_article"].find(
                {"weixin_id": public_id_dict["public_id"]}, {"title": 1, "content": 1, "digest": 1}).sort("post_date", -1).limit(10)
            if documents.count() is 0:
                continue
            title = ""
            content = ""
            digest = ""
            for document in documents:
                # print(document)
                title = title + strip_and_replace_all(document["title"])
                content = content + strip_and_replace_all(document["content"])
                if document["digest"] != None:
                    digest = digest + strip_and_replace_all(document["digest"])
            wordle_json_orig = requests.post("http://admin:ada1dfa29d@114.215.110.161:8088/wordle", data={
                'title': title,
                'content': content,
                'digest': digest,
            }, timeout=300)
            print(wordle_json_orig.text)
            # print(type(wordle_json_orig.text))
            # input(".....................")
            try:
                wordle_json = json.loads(wordle_json_orig.text)["wordle"]
                wordle = []
                _sum = 0.0
                wordle_json = sorted(dict2list(wordle_json), key=lambda d:d[1], reverse = True)
                print(wordle_json)
                # input("........................................")
                for _tuple in wordle_json:
                    wordle.append({
                        "name": _tuple[0],
                        "value": _tuple[1]
                    })
                    _sum = _sum + float(_tuple[1])
                _account_keyword = {}
                _account_keyword["month_article_keyword"] = wordle
                _account_keyword["month_top_five_keyword"] = []
                for item in wordle[:5]:
                    month_top_five_keyword_item = {}
                    month_top_five_keyword_item["name"] = item["name"]
                    month_top_five_keyword_item["value"] = str(
                        float(item["value"]) / _sum)
                    _account_keyword["month_top_five_keyword"].append(
                        month_top_five_keyword_item)
                # print(_account_keyword)
                if MediaWeixinStatistic.select().where(MediaWeixinStatistic.public == public_id_dict["public_id"]).count() == 1:
                    # account_keyword=json.dumps(_account_keyword, ensure_ascii=False),
                    print("update ......................")
                    MediaWeixinStatistic.update(account_keyword=json.dumps(_account_keyword, ensure_ascii=False)).where(
                        MediaWeixinStatistic.public == public_id_dict["public_id"]).execute()
                    print("update ......................1")
                else:
                    print("insert........................")
                    MediaWeixinStatistic.create(public = public_id_dict["public_id"],account_keyword=json.dumps(_account_keyword, ensure_ascii=False))

                print("current account is %s" % public_id_dict["public_id"])
                source_mongo_tool.disconnection()
                consumer.commit_offsets()
            except Exception as e:
                print("time out ......................")
                continue
                
            

    def media_weixin_statistic_update(self, source_file_args, source_mongo_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        count = 0
        for line in source_file:
            count += 1
            # if count<139:
            #     continue
            print(source_file_tool.line_strip_decode_ignore(
                line).split(",")[0])
            documents = source_mongo_tool.database["media_weixin_article"].find(
                {"weixin_id": source_file_tool.line_strip_decode_ignore(line).split(",")[0]}).sort("post_date", -1).limit(50)
            if documents.count() is 0:
                continue
            title = ""
            content = ""
            digest = ""

            for document in documents:
                # print(document)
                title = title + strip_and_replace_all(document["title"])
                content = content + strip_and_replace_all(document["content"])
                digest = digest + strip_and_replace_all(document["digest"])
            wordle_json_orig = requests.post("http://admin:ada1dfa29d@114.215.110.161:8088/wordle", data={
                'title': title,
                'content': content,
                'digest': digest,
            })
            wordle_json = wordle_json_orig.text[1:-1].split(',')
            print(wordle_json)
            wordle = []
            _sum = 0.0
            for tuple in wordle_json:
                # print(tuple)
                wordle.append({
                    "name": tuple.split(':')[0].strip()[1:-1],
                    "value": tuple.split(':')[1].strip()
                })
                _sum = _sum + float(tuple.split(':')[1].strip())

            _account_keyword = {}
            _account_keyword["month_article_keyword"] = wordle
            _account_keyword["month_top_five_keyword"] = []
            for item in wordle[:5]:
                month_top_five_keyword_item = {}
                month_top_five_keyword_item["name"] = item["name"]
                month_top_five_keyword_item["value"] = str(
                    float(item["value"]) / _sum)
                _account_keyword["month_top_five_keyword"].append(
                    month_top_five_keyword_item)
            # print(_account_keyword)
            if MediaWeixinStatistic.select().where(MediaWeixinStatistic.public == source_file_tool.line_strip_decode_ignore(line).split(",")[0]).count() == 1:
                # account_keyword=json.dumps(_account_keyword, ensure_ascii=False),
                MediaWeixinStatistic.update(account_keyword=json.dumps(_account_keyword, ensure_ascii=False)).where(
                    MediaWeixinStatistic.public == source_file_tool.line_strip_decode_ignore(line).split(",")[0]).execute()
            print("settled %d accounts;current account is %s" % (
                count, source_file_tool.line_strip_decode_ignore(line).split(",")[0]))
            # input("...........................")
        source_mongo_tool.disconnection()

    def media_weixin_account_tags(self, target_file_args_1,target_file_args_2):
        os.system("/usr/bin/rm %s/%s" % (target_file_args_1[0],target_file_args_1[1]))
        os.system("/usr/bin/rm %s/%s" % (target_file_args_2[0],target_file_args_2[1]))
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        target_file_2_head ="public_id,public_name,wom_index,media_area,media_cate,single_price,multi_1_price,multi_2_price,multi_3_price,fans_num,head_view_media_num,head_like_media_num,singly_view_cost,articles,post_count"+"\n"
        count = 0
        for media_weixin in MediaWeixin.select().where(MediaWeixin.real_public != ""):
            # if count<= 367916:
            #     continue
            account_tags_list = []
            public_id = media_weixin.real_public ## 1
            if media_weixin.public_name is None: ## 2
                public_name = ""
            else:
                public_name = str(media_weixin.public_name)
            if media_weixin.m_wmi is None:  ## 3
                wom_index = str(-1)
            else:
                wom_index = str(media_weixin.m_wmi)
            if media_weixin.follower_area is None: ## 4
                media_area = "#0#"
            else:
                media_area = str(media_weixin.follower_area)
            if media_weixin.media_cate is None: ## 5
                _media_cate = "#26#"
            else:
                _media_cate = str(media_weixin.media_cate)
            single_price = str(-1)
            multi_1_price = str(-1)
            multi_2_price = str(-1)
            multi_3_price = str(-1)
            if WeixinVendorBind.select().where((WeixinVendorBind.media_uuid == media_weixin.uuid) & (WeixinVendorBind.is_pref_vendor ==1)).count() ==1:
                # print('1')
                weixin_vendor_bind_record = WeixinVendorBind.get((WeixinVendorBind.media_uuid == media_weixin.uuid) & (WeixinVendorBind.is_pref_vendor ==1))
                if weixin_vendor_bind_record.s_retail_price is None: ## 6
                    single_price = str(-1)
                else:
                    single_price = str(weixin_vendor_bind_record.s_retail_price)
                if weixin_vendor_bind_record.m_1_retail_price is None: ## 7
                    multi_1_price = str(-1)
                else:
                    multi_1_price = str(weixin_vendor_bind_record.m_1_retail_price)
                if weixin_vendor_bind_record.m_2_retail_price is None: ## 8
                    multi_2_price = str(-1)
                else:
                    multi_2_price = str(weixin_vendor_bind_record.m_2_retail_price)
                if weixin_vendor_bind_record.m_3_retail_price is None: ## 9
                    multi_3_price = str(-1)
                else:
                    multi_3_price = str(weixin_vendor_bind_record.m_3_retail_price)
            if media_weixin.follower_num is None:  ## 10
                fans_num = str(-1)
            else:
                fans_num = str(media_weixin.follower_num)
            if media_weixin.head_read_median is None: ## 11
                head_view_media_num = str(-1)
            else:
                head_view_media_num = str(media_weixin.head_read_median)
            if  media_weixin.head_like_median is None: ## 12
                head_like_media_num = str(-1)
            else:
                head_like_media_num = str(media_weixin.head_like_median)
            if media_weixin.m_avg_price_pv is None: ## 13
                singly_view_cost = str(-1)
            else:
                singly_view_cost = str(media_weixin.m_avg_price_pv)
            if media_weixin.total_article_cnt is None: ## 14
                articles = str(-1)
            else:
                articles = str(media_weixin.total_article_cnt)
            if media_weixin.m_release_cnt is None: ## 15
                post_count = str(-1)
            else:
                post_count = str(media_weixin.m_release_cnt)
            for i in range(26):
                account_tags_list.append("0")
            try:
                media_cate_list = media_weixin.media_cate.split("#")
            except Exception as e:
                media_cate_list=['','']
                # if media_weixin.real_public == "rmrbwx":
                #     input(".......................")
                # continue
            
            print(media_cate_list)
            for media_cate in media_cate_list[1:-1]:
                account_tags_list[int(media_cate) - 1] = "1"
            # print(",".join(account_tags_list))
            target_file_tool_1.write_database_line_to_file(
                media_weixin.real_public + "," + ",".join(account_tags_list) + "\n")
            if count==0:
                target_file_tool_2.f.write(target_file_2_head.encode(
                    'gbk', 'ignore'))
            if (media_weixin.put_up ==1) and ("#" in media_area) and ("#" in _media_cate) and (single_price != "-1") and (multi_1_price != str(-1)) and (multi_2_price != str(-1)) and (multi_3_price != str(-1)):

                write_line=public_id+ \
                        ","+public_name+ \
                        ","+wom_index+ \
                        ","+media_area+ \
                        ","+_media_cate+ \
                        ","+single_price+ \
                        ","+multi_1_price+ \
                        ","+multi_2_price+ \
                        ","+multi_3_price+ \
                        ","+fans_num+ \
                        ","+head_view_media_num+ \
                        ","+head_like_media_num+ \
                        ","+singly_view_cost+ \
                        ","+articles+ \
                        ","+post_count+ \
                        "\n"
                target_file_tool_2.f.write(write_line.encode(
                        'gbk', 'ignore'))
            count += 1

            print("settled %d items " % count)

            # input("..............")
        default_media_cate=",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
        count=0
        target_file_tool_1.close_file()
        target_file_tool_2.close_file()

    def media_weixin_vendor_bind_transfer(self):
        count=0
        for media_vendor_bind in MediaVendorBind.select():
            try:
                media_vendor_weixin_price_list=MediaVendorWeixinPriceList.get(MediaVendorWeixinPriceList.bind_uuid==media_vendor_bind.uuid)
            except Exception as e:
                continue
            WeixinVendorBind.create(account_period = media_vendor_bind.pay_period,
                                    active_end_time = media_vendor_weixin_price_list.active_end_time,
                                    belong_type = media_vendor_bind.media_ownership,
                                    cooperate_level = media_vendor_bind.coop_level,
                                    create_time = current_time_to_stamp(),
                                    is_pref_vendor = media_vendor_bind.is_pref_vendor,
                                    m_1_execute_price = media_vendor_weixin_price_list.execute_price_m_1,
                                    m_1_orig_price = media_vendor_weixin_price_list.orig_price_m_1_min,
                                    m_1_pub_type = media_vendor_weixin_price_list.m_1_pub_type,
                                    m_1_retail_price = media_vendor_weixin_price_list.retail_price_m_1_min,
                                    m_2_execute_price = media_vendor_weixin_price_list.execute_price_m_2,
                                    m_2_orig_price = media_vendor_weixin_price_list.orig_price_m_2_min,
                                    m_2_pub_type = media_vendor_weixin_price_list.m_2_pub_type,
                                    m_2_retail_price = media_vendor_weixin_price_list.retail_price_m_2_min,
                                    m_3_execute_price = media_vendor_weixin_price_list.execute_price_m_3,
                                    m_3_orig_price = media_vendor_weixin_price_list.orig_price_m_3_min,
                                    m_3_pub_type = media_vendor_weixin_price_list.m_3_pub_type,
                                    m_3_retail_price = media_vendor_weixin_price_list.retail_price_m_3_min,
                                    media_uuid = media_vendor_bind.media_uuid,
                                    s_execute_price = media_vendor_weixin_price_list.execute_price_s,
                                    s_orig_price = media_vendor_weixin_price_list.orig_price_s_min,
                                    s_pub_type = media_vendor_weixin_price_list.s_pub_type,
                                    s_retail_price = media_vendor_weixin_price_list.retail_price_s_min,
                                    status = media_vendor_bind.status,
                                    update_time = current_time_to_stamp(),
                                    uuid = uuid_generator(),
                                    vendor_uuid = media_vendor_bind.vendor_uuid,
                                    has_direct_pub = media_vendor_bind.has_direct_pub,
                                    has_origin_pub = media_vendor_bind.has_origin_pub)
            count+=1
            print("settled %d items" % count)
    
    def sogou_weixin_crawl_account_search(self,source_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        correct_count=0
        error_count=0
        for sogou_weixin_account_init_task in SogouWeixinAccountInitTask.select().where((SogouWeixinAccountInitTask.account_status == 1) & (SogouWeixinAccountInitTask.task_status == 3)):
            try:
                # print("data not find %s" % sogou_weixin_account_init_task.account)
                sogou_weixin_account_latest_article_crawl_task=SogouWeixinAccountLatestArticleCrawlTask.get((SogouWeixinAccountLatestArticleCrawlTask.account == sogou_weixin_account_init_task.account) & (SogouWeixinAccountLatestArticleCrawlTask.enable_task == 1))
                
                document=source_mongo_tool.database["media_weixin"].find_one({"public_id":sogou_weixin_account_latest_article_crawl_task.account},{"public_id":1,"public_name":1})
                DtsMediaSearch.create(weixin = document["public_id"],
                                      weixin_name= document["public_name"])
                correct_count+=1
                print("correct_count: %d" % correct_count)
            except Exception as e:
                error_count+=1
                print("error_count: %d" % error_count)
                continue
            print("correct_count: %d;error_count: %d" % (correct_count,error_count))
        source_mongo_tool.disconnection()

    def media_prices_binds_alteration(self):
        for wom_admin_base_conf in WomAdminBaseConf.select().where(WomAdminBaseConf.is_update == 0):
            if wom_admin_base_conf.conf_type ==1:
                price_range_list=json.loads(wom_admin_base_conf.conf_json)
                print(price_range_list)
                for weixin_vendor_bind in WeixinVendorBind.select():
                    print("weixin_vendor_bind %.2f; uuid %s" % (float(weixin_vendor_bind.s_orig_price),weixin_vendor_bind.uuid))
                    for price_range in price_range_list:
                        if price_range["max"] == "":
                            price_range["max"] = "999999999"
                        print("decimal %.2f " % float(price_range["decimal"]))
                        if float(weixin_vendor_bind.s_orig_price) >= float(price_range["min"]) and float(weixin_vendor_bind.s_orig_price) < float(price_range["max"]):
                            _s_execute_price = float(weixin_vendor_bind.s_orig_price) * float(price_range["decimal"])
                            _s_retail_price = float(weixin_vendor_bind.s_orig_price) * float(price_range["decimal"])
                        if float(weixin_vendor_bind.m_1_orig_price) >= float(price_range["min"]) and float(weixin_vendor_bind.m_1_orig_price) < float(price_range["max"]):
                            _m_1_execute_price = float(weixin_vendor_bind.m_1_orig_price) * float(price_range["decimal"])
                            _m_1_retail_price = float(weixin_vendor_bind.m_1_orig_price) * float(price_range["decimal"])
                        if float(weixin_vendor_bind.m_2_orig_price) >= float(price_range["min"]) and float(weixin_vendor_bind.m_2_orig_price) < float(price_range["max"]):
                            _m_2_execute_price = float(weixin_vendor_bind.m_2_orig_price) * float(price_range["decimal"])
                            _m_2_retail_price = float(weixin_vendor_bind.m_2_orig_price) * float(price_range["decimal"])
                        if float(weixin_vendor_bind.m_3_orig_price) >= float(price_range["min"]) and float(weixin_vendor_bind.m_3_orig_price) < float(price_range["max"]):
                            _m_3_execute_price = float(weixin_vendor_bind.m_3_orig_price) * float(price_range["decimal"])
                            _m_3_retail_price = float(weixin_vendor_bind.m_3_orig_price) * float(price_range["decimal"])
                    
                    WeixinVendorBind.update(s_execute_price=_s_execute_price,
                                            s_retail_price = _s_retail_price,
                                            m_1_execute_price = _m_1_execute_price,
                                            m_1_retail_price = _m_1_retail_price,
                                            m_2_execute_price = _m_2_execute_price,
                                            m_2_retail_price = _m_2_retail_price,
                                            m_3_execute_price = _m_3_execute_price,
                                            m_3_retail_price = _m_3_retail_price).where(WeixinVendorBind.uuid == weixin_vendor_bind.uuid).execute()
                    # print("weixin_vendor_bind.s_orig_price: %.2f " % float(weixin_vendor_bind.s_orig_price))
                    # print("weixin_vendor_bind.s_execute_price: %.2f " % _s_execute_price)
                    # print("weixin_vendor_bind.s_retail_price: %.2f " % _s_retail_price)

                    # print("weixin_vendor_bind.m_1_orig_price: %.2f " % float(weixin_vendor_bind.m_1_orig_price))
                    # print("weixin_vendor_bind.m_1_execute_price: %.2f " % _m_1_execute_price)
                    # print("weixin_vendor_bind.m_1_retail_price: %.2f " % _m_1_retail_price)

                    # print("weixin_vendor_bind.m_2_orig_price: %.2f " % float(weixin_vendor_bind.m_2_orig_price))
                    # print("weixin_vendor_bind.m_2_execute_price: %.2f " % _m_2_execute_price)
                    # print("weixin_vendor_bind.m_2_retail_price: %.2f " % _m_2_retail_price)

                    # print("weixin_vendor_bind.m_3_orig_price: %.2f " % float(weixin_vendor_bind.m_3_orig_price))
                    # print("weixin_vendor_bind.m_3_execute_price: %.2f " % _m_3_execute_price)
                    # print("weixin_vendor_bind.m_3_retail_price: %.2f " % _m_3_retail_price)
                    # break
                    # weixin_vendor_bind.s_orig_price
                    # weixin_vendor_bind.m_1_orig_price
                    # weixin_vendor_bind.m_2_orig_price
                    # weixin_vendor_bind.m_3_orig_price
            elif wom_admin_base_conf.conf_type ==2:
                price_range_list=json.loads(wom_admin_base_conf.conf_json)
                print(price_range_list)
                for weibo_vendor_bind in WeiboVendorBind.select():
                    print("weibo_vendor_bind %.2f; uuid %s" % (float(weibo_vendor_bind.micro_direct_price_orig),weibo_vendor_bind.uuid))
                    for price_range in price_range_list:
                        if price_range["max"] == "":
                            price_range["max"] = "999999999"
                        if float(weibo_vendor_bind.micro_direct_price_orig) >= float(price_range["min"]) and float(weibo_vendor_bind.micro_direct_price_orig) < float(price_range["max"]):
                            _micro_direct_price_retail = float(weibo_vendor_bind.micro_direct_price_orig) * float(price_range["decimal"])
                            _micro_direct_price_execute = float(weibo_vendor_bind.micro_direct_price_orig) * float(price_range["decimal"])
                        if float(weibo_vendor_bind.micro_transfer_price_orig) >= float(price_range["min"]) and float(weibo_vendor_bind.micro_transfer_price_orig) < float(price_range["max"]):
                            _micro_transfer_price_retail = float(weibo_vendor_bind.micro_transfer_price_orig) * float(price_range["decimal"])
                            _micro_transfer_price_execute = float(weibo_vendor_bind.micro_transfer_price_orig) * float(price_range["decimal"])
                        if float(weibo_vendor_bind.soft_direct_price_orig) >= float(price_range["min"]) and float(weibo_vendor_bind.soft_direct_price_orig) < float(price_range["max"]):
                            _soft_direct_price_retail = float(weibo_vendor_bind.soft_direct_price_orig) * float(price_range["decimal"])
                            _soft_direct_price_execute = float(weibo_vendor_bind.soft_direct_price_orig) * float(price_range["decimal"])
                        if float(weibo_vendor_bind.soft_transfer_price_orig) >= float(price_range["min"]) and float(weibo_vendor_bind.soft_transfer_price_orig) < float(price_range["max"]):
                            _soft_transfer_price_retail = float(weibo_vendor_bind.soft_transfer_price_orig) * float(price_range["decimal"])
                            _soft_transfer_price_execute = float(weibo_vendor_bind.soft_transfer_price_orig) * float(price_range["decimal"])
                    WeiboVendorBind.update(micro_direct_price_retail=_micro_direct_price_retail,
                                            micro_direct_price_execute = _micro_direct_price_execute,
                                            micro_transfer_price_retail = _micro_transfer_price_retail,
                                            micro_transfer_price_execute = _micro_transfer_price_execute,
                                            soft_direct_price_retail = _soft_direct_price_retail,
                                            soft_direct_price_execute = _soft_direct_price_execute,
                                            soft_transfer_price_retail = _soft_transfer_price_retail,
                                            soft_transfer_price_execute = _soft_transfer_price_execute).where(WeiboVendorBind.uuid == weibo_vendor_bind.uuid).execute()
                    # print("weibo_vendor_bind.micro_direct_price_orig: %.2f " % float(weibo_vendor_bind.micro_direct_price_orig))
                    # print("weibo_vendor_bind.micro_direct_price_retail: %.2f " % _micro_direct_price_retail)
                    # print("weibo_vendor_bind.micro_direct_price_execute: %.2f " % _micro_direct_price_execute)
                    
                    # print("weibo_vendor_bind.micro_transfer_price_orig: %.2f " % float(weibo_vendor_bind.micro_transfer_price_orig))
                    # print("weibo_vendor_bind.micro_transfer_price_retail: %.2f " % _micro_transfer_price_retail)
                    # print("weibo_vendor_bind.micro_transfer_price_execute: %.2f " % _micro_transfer_price_execute)
                    
                    # print("weibo_vendor_bind.soft_direct_price_orig: %.2f " % float(weibo_vendor_bind.soft_direct_price_orig))
                    # print("weibo_vendor_bind.soft_direct_price_retail: %.2f " % _soft_direct_price_retail)
                    # print("weibo_vendor_bind.soft_direct_price_execute: %.2f " % _soft_direct_price_execute)
                    
                    # print("weibo_vendor_bind.soft_transfer_price_orig: %.2f " % float(weibo_vendor_bind.soft_transfer_price_orig))
                    # print("weibo_vendor_bind.soft_transfer_price_retail: %.2f " % _soft_transfer_price_retail)
                    # print("weibo_vendor_bind.soft_transfer_price_execute: %.2f " % _soft_transfer_price_execute)
                    # break
                    # weibo_vendor_bind.micro_direct_price_orig
                    # weibo_vendor_bind.micro_transfer_price_orig
                    # weibo_vendor_bind.soft_direct_price_orig
                    # weibo_vendor_bind.soft_transfer_price_orig
            elif wom_admin_base_conf.conf_type ==3:
                price_range_list=json.loads(wom_admin_base_conf.conf_json)
                print(price_range_list)
                for bind_video_vendor_bind in BindVideoVendorPrice.select():
                    print("media_video_bind %.2f; uuid %s" % (float(bind_video_vendor_bind.price_orig_one),bind_video_vendor_bind.uuid))
                    for price_range in price_range_list:
                        if price_range["max"] == "":
                            price_range["max"] = "999999999"
                        # print(str(bind_video_vendor_bind.price_orig_one) +str(price_range["min"]) + str(price_range["max"]))
                        if float(bind_video_vendor_bind.price_orig_one) >= float(price_range["min"]) and float(bind_video_vendor_bind.price_orig_one) < float(price_range["max"]):
                            _price_retail_one = float(bind_video_vendor_bind.price_orig_one) * float(price_range["decimal"])
                            _price_execute_one = float(bind_video_vendor_bind.price_orig_one) * float(price_range["decimal"])
                        if float(bind_video_vendor_bind.price_orig_two) >= float(price_range["min"]) and float(bind_video_vendor_bind.price_orig_two) < float(price_range["max"]):
                            _price_retail_two = float(bind_video_vendor_bind.price_orig_two) * float(price_range["decimal"])
                            _price_execute_two = float(bind_video_vendor_bind.price_orig_two) * float(price_range["decimal"])
                    BindVideoVendorPrice.update(price_retail_one=_price_retail_one,
                                            price_execute_one = _price_execute_one,
                                            price_retail_two = _price_retail_two,
                                            price_execute_two = _price_execute_two).where(BindVideoVendorPrice.uuid == bind_video_vendor_bind.uuid).execute()
                    # print("bind_video_vendor_bind.price_orig_one: %.2f " % float(bind_video_vendor_bind.price_orig_one))
                    # print("bind_video_vendor_bind.price_retail_one: %.2f " % _price_retail_one)
                    # print("bind_video_vendor_bind.price_execute_one: %.2f " % _price_execute_one)
                    
                    # print("bind_video_vendor_bind.price_orig_two: %.2f " % float(bind_video_vendor_bind.price_orig_two))
                    # print("bind_video_vendor_bind.price_retail_two: %.2f " % _price_retail_two)
                    # print("bind_video_vendor_bind.price_execute_two: %.2f " % _price_execute_two)
                    # break
                    # bind_video_vendor_bind.price_orig_one
                    # bind_video_vendor_bind.price_orig_two
        WomAdminBaseConf.update(is_update = 1).execute()
                    # price_execute_one

    def media_weixin_account_tags_top_100(self,target_file_args):
        target_file_tool = self.get_file_tool(*target_file_args)
        for i in range(1,26):
            for dts_media_weixin_influence_rank in DtsMediaWeixinInfluenceRankSlave.select().where((DtsMediaWeixinInfluenceRankSlave.cycle_type == 1) & (DtsMediaWeixinInfluenceRankSlave.article_post_end_date == '20170515') & (DtsMediaWeixinInfluenceRankSlave.classify == i)).order_by(+DtsMediaWeixinInfluenceRankSlave.rank).limit(100):
                line = dts_media_weixin_influence_rank.public + "," + str(i) + "\n"
                target_file_tool.write_database_line_to_file(line)
        for dts_media_weixin_influence_rank in DtsMediaWeixinInfluenceRankSlave.select().where((DtsMediaWeixinInfluenceRankSlave.cycle_type == 1) & (DtsMediaWeixinInfluenceRankSlave.article_post_end_date == '20170515') & (DtsMediaWeixinInfluenceRankSlave.classify == 26)).order_by(+DtsMediaWeixinInfluenceRankSlave.rank).limit(1000):
            line = dts_media_weixin_influence_rank.public + "," + str(26) + "\n"
            target_file_tool.write_database_line_to_file(line)


    def dts_media_weixin_influence_rank_slave_multi(self, source_mongo_args_1, source_mongo_args_2 , source_file_args, queue_tag):
        _calculate_timestamp = current_time_to_stamp()
        source_mongo_tool_1 = self.get_mongo_tool(*source_mongo_args_1)
        source_mongo_tool_2 = self.get_mongo_tool(*source_mongo_args_2)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        current_date_time = datetime.datetime.now()
        current_date_time_to_str = datetime_to_str(current_date_time)
        month_cycle_type_tuple = ("month", "oneMonth", "twoMonth")
        week_cycle_type_tuple = ("week", "oneWeek", "twoWeek", "threeWeek")
        cycle_type_tuple = ("month", "oneMonth", "twoMonth",
                            "week", "oneWeek", "twoWeek", "threeWeek")
        classify_rank_tuple = (('rank_1000', 'rank_1000_inc', 0), ('rank_1001', 'rank_1001_inc', 1), ('rank_1002', 'rank_1002_inc', 2), ('rank_1003', 'rank_1003_inc', 3), ('rank_1004', 'rank_1004_inc', 4), ('rank_1005', 'rank_1005_inc', 5), ('rank_1006', 'rank_1006_inc', 6), ('rank_1007', 'rank_1007_inc', 7), ('rank_1008', 'rank_1008_inc', 8), ('rank_1009', 'rank_1009_inc', 9), ('rank_1010', 'rank_1010_inc', 10), ('rank_1011', 'rank_1011_inc', 11), ('rank_1012', 'rank_1012_inc', 12), (
            'rank_1013', 'rank_1013_inc', 13), ('rank_1014', 'rank_1014_inc', 14), ('rank_1015', 'rank_1015_inc', 15), ('rank_1016', 'rank_1016_inc', 16), ('rank_1017', 'rank_1017_inc', 17), ('rank_1018', 'rank_1018_inc', 18), ('rank_1019', 'rank_1019_inc', 19), ('rank_1020', 'rank_1020_inc', 20), ('rank_1021', 'rank_1021_inc', 21), ('rank_1022', 'rank_1022_inc', 22), ('rank_1023', 'rank_1023_inc', 23), ('rank_1024', 'rank_1024_inc', 24), ('rank_1025', 'rank_1025_inc', 25), ('rank_1026', 'rank_1026_inc', 26))
        auth_info_type_tuple = (
            "account_cert", "tencent_account_cert", "sina_account_cert",)
        # database.connect()
        write_to = DtsInfluenceRankTableTip.get(
            DtsInfluenceRankTableTip.id == 1).read_from
        read_from = write_to
        # database.close()
        write_to_orm = None
        delete_from_orm = None
        if write_to == "dts_media_weixin_influence_rank":
            write_to = "dts_media_weixin_influence_rank_slave"
            write_to_orm = DtsMediaWeixinInfluenceRankSlave
            delete_from_orm = DtsMediaWeixinInfluenceRank
        elif write_to == "dts_media_weixin_influence_rank_slave":
            write_to = "dts_media_weixin_influence_rank"
            write_to_orm = DtsMediaWeixinInfluenceRank
            delete_from_orm = DtsMediaWeixinInfluenceRankSlave
        if queue_tag == 1:
            write_to_orm.delete().execute()
        else:
            print("sleeping 5min.................")
            time.sleep(60)
        settled_count = 0
        total_count = 0
        hour_of_day_list = ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23")
        day_of_week_list = ("1", "2", "3", "4", "5", "6", "7")
        re_head_view_avg_cnt = "(\d+)_head_view_avg_cnt"
        re_head_like_avg_cnt = "(\d+)_head_like_avg_cnt"
        re_many_second_view_avg_cnt = "(\d+)_many_second_view_avg_cnt"
        re_many_second_like_avg_cnt = "(\d+)_many_second_like_avg_cnt"
        re_day_of_month_article_cnt = "(\d+)_day_of_month_article_cnt"
        re_day_of_week_article_avg_cnt = "(\d+)_day_of_week_article_avg_cnt"
        re_hour_of_day_article_cnt = "(\d+)_hour_of_day_article_cnt"
        re_rank = "rank_(\d+)"

        dts_media_weixin_influence_rank_data_source_list = []
        dts_media_weixin_influence_rank_public_id_list = []
        dts_media_weixin_influence_rank_data_source_count = 0
        for line in source_file:
            document = source_mongo_tool_1.database[
                "media_weixin_info"].find_one({"public_id":source_file_tool.line_strip_decode_ignore(line)})
            print(document["public_id"])
            dts_media_weixin_influence_rank_public_id_list.append(document[
                                                                  "public_id"])
            settled_count += 1
            total_count -= 1
            if "cycle" not in document or current_date_time_to_str not in document['cycle']:
                continue
            """
            更新详情页数据
            """
            _account_keyword = {}
            _article_effect = {}
            _influence_data = {}
            _operation_status = {}
            _public = document["public_id"]
            _update_time = current_datetime_to_str()

            month_article_effect = {}
            operational_status = {}
            month_article_keyword = {}

            month_head_article_trend = {}
            month_next_article_trend = {}

            month_post_trend = {}
            month_post_time_hour_distribute = {}
            month_post_time_week_distribute = {}
            month_rank_trend = {}

            for k, v in document['cycle'][current_date_time_to_str]["month"].items():
                m = re.search(re_head_view_avg_cnt, k)
                if m is not None:
                    month_head_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_head_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_many_second_view_avg_cnt, k)
                if m is not None:
                    month_next_article_trend[str(m.group(1))] = {"read_num": document['cycle'][current_date_time_to_str]["month"][k],
                                                                 "like_num": document['cycle'][current_date_time_to_str]["month"][str(m.group(1)) + "_many_second_like_avg_cnt"]}
                    # print(str(m.group(1)))
                m = re.search(re_day_of_month_article_cnt, k)
                if m is not None:
                    month_post_trend[str(m.group(1))] = document['cycle'][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_hour_of_day_article_cnt, k)
                if m is not None:
                    month_post_time_hour_distribute[str(m.group(1))] = document["cycle"][
                        current_date_time_to_str]["month"][k]

                m = re.search(re_day_of_week_article_avg_cnt, k)
                if m is not None:
                    day_of_week = int(m.group(1))
                    if day_of_week == 0:
                        day_of_week = 7
                    month_post_time_week_distribute[str(day_of_week)] = document["cycle"][
                        current_date_time_to_str]["month"][k]

            month_statistics = {}
            month_statistics["head_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["head_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["head_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["head_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["head_10w_article_total_cnt"]
            }
            month_statistics["next_article"] = {
                "month_read_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_view_median"],
                "month_like_num": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_like_median"],
                "month_max_read_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_view_num"],
                "month_max_like_num": document["cycle"][current_date_time_to_str]["month"]["many_second_highest_like_num"],
                "month_gte_10w_articles": document["cycle"][current_date_time_to_str]["month"]["multil_pos2_10w_article_total_cnt"]
            }
            month_article_effect[
                "month_head_article_trend"] = month_head_article_trend
            month_article_effect[
                "month_next_article_trend"] = month_next_article_trend
            month_article_effect["month_statistics"] = month_statistics
            """
            文章效果
            """
            _article_effect["month_article_effect"] = month_article_effect
            """
            运营状况
            """
            operational_status["month_post_articles_count"] = document["cycle"][
                current_date_time_to_str]["month"]["article_total_cnt"]
            operational_status["month_post_count"] = document["cycle"][
                current_date_time_to_str]["month"]["pub_total_cnt"]
            operational_status["month_article_per_post"] = document["cycle"][
                current_date_time_to_str]["month"]["article_avg_pub_cnt"]
            operational_status["month_post_trend"] = month_post_trend
            operational_status[
                "month_post_time_hour_distribute"] = month_post_time_hour_distribute
            operational_status[
                "month_post_time_week_distribute"] = month_post_time_week_distribute
            operational_status["month_single_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["single_avg_view_cnt"]
            operational_status["month_multi_pos_1_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos1_avg_view_cnt"]
            operational_status["month_multi_pos_2_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos2_avg_view_cnt"]
            operational_status["month_multi_pos_3_avg_read_num"] = document["cycle"][
                current_date_time_to_str]["month"]["multil_pos3_avg_view_cnt"]

            _operation_status["operational_status"] = operational_status
            """
            账户关键词
            """

            if "text_info" in document and current_date_time_to_str in document["text_info"] and "account_wordle" in document['text_info'][current_date_time_to_str]:
                _account_keyword["month_article_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle"]
                _account_keyword["month_top_five_keyword"] = document['text_info'][
                    current_date_time_to_str]["account_wordle_top_5"]
            else:
                _account_keyword["month_article_keyword"] = []
                _account_keyword["month_top_five_keyword"] = []

            """
            影响力数据
            """
            for k, v in document["rank_trend"].items():
                m = re.search(re_rank, k)
                if m is not None:
                    # print(type(m.group(1)))
                    rank_trend_list = []
                    for rank_trend in document["rank_trend"][k][-30:]:
                        rank_trend_dict = {}
                        rank_trend_dict[rank_trend[
                            "name"]] = rank_trend["value"]
                        rank_trend_list.append(rank_trend_dict)
                    month_rank_trend[
                        str(int(m.group(1)) - 1000)] = rank_trend_list

            _influence_data["month_rank_trend"] = month_rank_trend
            month_wom_index_trend_list = []
            for month_wom_index_rank in document["month_wom_index_rank"][-30:]:
                month_wom_index_rank_dict = {}
                month_wom_index_rank_dict[month_wom_index_rank[
                    "name"]] = month_wom_index_rank["value"]
                month_wom_index_trend_list.append(month_wom_index_rank_dict)
            _influence_data[
                "month_wom_index_trend"] = month_wom_index_trend_list
            # print("account_keyword :%s" % str(_account_keyword))
            # print("article_effect :%s" % str(_article_effect))
            # print("influence_data :%s" % str(_influence_data))
            # print("operation_status :%s" % str(_operation_status))
            if MediaWeixinStatistic.select().where(MediaWeixinStatistic.public == document["public_id"]).count() == 1:
                # account_keyword=json.dumps(_account_keyword, ensure_ascii=False),
                MediaWeixinStatistic.update(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    update_time=current_datetime_to_str()).where(MediaWeixinStatistic.public == document["public_id"]).execute()
                # print("1")

            else:
                # print("2")
                MediaWeixinStatistic.insert(
                    article_effect=json.dumps(
                        _article_effect, ensure_ascii=False),
                    influence_data=json.dumps(
                        _influence_data, ensure_ascii=False),
                    operation_status=json.dumps(
                        _operation_status, ensure_ascii=False),
                    public=document["public_id"],
                    update_time=current_datetime_to_str()).execute()


            # input(",,,,,,,,,,,,,,,,,,,,,")
            # print(str(document))
            _account_short_desc = ''
            _auth_info = ''
            _auth_type = -1
            _nickname = ''
            _follower_num = -1
            _retail_avg_price = 0

            document_media_weixin = source_mongo_tool_2.database[
                "media_weixin"].find_one({"public_id": document["public_id"]})
            if MediaWeixin.select().where(MediaWeixin.real_public == document["public_id"]).count() == 1:
                media_weixin_record = MediaWeixin.get(
                    MediaWeixin.real_public == document["public_id"])
                _follower_num = media_weixin_record.follower_num
                if WeixinVendorBind.select().where((WeixinVendorBind.media_uuid == media_weixin_record.uuid) & (WeixinVendorBind.is_pref_vendor ==1)).count() ==1:
                    print('1')
                    weixin_vendor_bind_record = WeixinVendorBind.get((WeixinVendorBind.media_uuid == media_weixin_record.uuid) & (WeixinVendorBind.is_pref_vendor ==1))
                    if(weixin_vendor_bind_record.m_1_retail_price == 0):
                        if(weixin_vendor_bind_record.m_2_retail_price == 0):
                            if(weixin_vendor_bind_record.m_3_retail_price == 0):
                                _retail_avg_price = 0
                            else:
                                _retail_avg_price = float(
                                    weixin_vendor_bind_record.m_3_retail_price)
                        else:
                            _retail_avg_price = float(
                                weixin_vendor_bind_record.m_2_retail_price)
                    else:
                        _retail_avg_price = float(
                            weixin_vendor_bind_record.m_1_retail_price)
            _m_avg_price_pv = 0
            if document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != -1 and document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] != 0:
                _m_avg_price_pv = _retail_avg_price / \
                    document['cycle'][current_date_time_to_str][
                        "month"]["head_avg_view_cnt"]
                print(_m_avg_price_pv)
                # input("........................")

            '''
            更新media_weixin数据
            '''
            m_head_avg_view_cnt = document["cycle"][current_date_time_to_str][
                "month"]["head_avg_view_cnt"]
            m_head_avg_like_cnt = document["cycle"][current_date_time_to_str][
                "month"]["head_avg_like_cnt"]
            m_avg_view_cnt = document["cycle"][
                current_date_time_to_str]["month"]["avg_view_cnt"]
            m_avg_like_cnt = document["cycle"][
                current_date_time_to_str]["month"]["avg_like_cnt"]
            m_total_article_cnt = document["cycle"][
                current_date_time_to_str]["month"]["article_total_cnt"]
            m_release_cnt = document["cycle"][
                current_date_time_to_str]["month"]["pub_total_cnt"]
            head_read_median = document["cycle"][
                current_date_time_to_str]["month"]["head_view_median"]
            head_like_median = document["cycle"][
                current_date_time_to_str]["month"]["head_like_median"]
            m_10w_article_total_cnt = document["cycle"][current_date_time_to_str]["month"][
                "10w_article_total_cnt"]
            m_wmi = document["cycle"][
                current_date_time_to_str]["month"]["wom_index"]
            latest_article_post_date = document["cycle"][current_date_time_to_str][
                "month"]["max_post_time"]
            promote_index = 0.7 * math.log(document["cycle"][current_date_time_to_str]["month"]["head_view_median"] + 1) + 0.3 * math.log(
                document["cycle"][current_date_time_to_str]["month"]["head_like_median"] + 1)
            sales_index = 0.5 * math.log(document["cycle"][current_date_time_to_str]["month"]["head_view_median"] + 1) + 0.5 * math.log(
                document["cycle"][current_date_time_to_str]["month"]["head_like_median"] + 1)
            m_avg_price_pv = _m_avg_price_pv
            rank_in_dts = document["cycle"][current_date_time_to_str]["month"]["rank_1000"]
            if rank_in_dts <= 100000:
                MediaWeixin.update(
                    m_head_avg_view_cnt =m_head_avg_view_cnt,
                    m_head_avg_like_cnt =m_head_avg_like_cnt,
                    m_avg_view_cnt =m_avg_view_cnt,
                    m_avg_like_cnt =m_avg_like_cnt,
                    total_article_cnt =m_total_article_cnt,
                    m_release_cnt =m_release_cnt,
                    head_read_median =head_read_median,
                    head_like_median =head_like_median,
                    m_10w_article_total_cnt =m_10w_article_total_cnt,
                    m_wmi =m_wmi,
                    latest_article_post_date =latest_article_post_date,
                    promote_index =promote_index,
                    sales_index =sales_index,
                    m_avg_price_pv =m_avg_price_pv,
                    rank_in_dts =rank_in_dts,
                    show_in_dts_rank=1
                    ).where(MediaWeixin.real_public == document["public_id"]).execute()
            else:
                MediaWeixin.update(
                    m_head_avg_view_cnt =m_head_avg_view_cnt,
                    m_head_avg_like_cnt =m_head_avg_like_cnt,
                    m_avg_view_cnt =m_avg_view_cnt,
                    m_avg_like_cnt =m_avg_like_cnt,
                    total_article_cnt =m_total_article_cnt,
                    m_release_cnt =m_release_cnt,
                    head_read_median =head_read_median,
                    head_like_median =head_like_median,
                    m_10w_article_total_cnt =m_10w_article_total_cnt,
                    m_wmi =m_wmi,
                    latest_article_post_date =latest_article_post_date,
                    promote_index =promote_index,
                    sales_index =sales_index,
                    m_avg_price_pv =m_avg_price_pv,
                    rank_in_dts =rank_in_dts,
                    ).where(MediaWeixin.real_public == document["public_id"]).execute()
            if document_media_weixin is not None:

                """
                更新榜单数据
                """

                if "account_short_desc" in document_media_weixin:
                    _account_short_desc = document_media_weixin[
                        "account_short_desc"]
                else:
                    _account_short_desc = ''
                for auth_info_type in auth_info_type_tuple:
                    if auth_info_type in document_media_weixin:
                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "account_cert":
                            _auth_type = 0
                            if "account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "tencent_account_cert":
                            _auth_type = 1
                            if "tencent_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "tencent_account_cert_info"]

                        if document_media_weixin[auth_info_type] == 1 and auth_info_type == "sina_account_cert":
                            _auth_type = 2
                            if "sina_account_cert_info" in document_media_weixin:
                                _auth_info = document_media_weixin[
                                    "sina_account_cert_info"]

                if "public_name" in document_media_weixin:
                    _nickname = document_media_weixin["public_name"]
            week_or_month = None

            '''
            如果头条平均阅读数<1000或者沃米指数<=2.0，则跳过
            '''
            # if (document['cycle'][current_date_time_to_str]["month"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["oneMonth"]["head_avg_view_cnt"] < 1000 and document['cycle'][current_date_time_to_str]["twoMonth"]["head_avg_view_cnt"] < 1000) or (document['cycle'][current_date_time_to_str]["month"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["oneMonth"]["wom_index"] <= 2 and document['cycle'][current_date_time_to_str]["twoMonth"]["wom_index"] <= 2):
            #     continue
            for cycle_type in cycle_type_tuple:
                if cycle_type in month_cycle_type_tuple:
                    week_or_month = 1
                else:
                    week_or_month = 0

                for classify_rank in classify_rank_tuple:

                    if classify_rank[0] in document['cycle'][current_date_time_to_str][cycle_type]:
                        # print("....................")
                        # database.connect()
                        # print(str(document[current_date_time_to_str][
                        #         cycle_type]["view_median"]))

                        # input("............................")

                        dts_media_weixin_influence_rank_data_source_count += 1
                        dts_media_weixin_influence_rank_data_source_item = {}
                        dts_media_weixin_influence_rank_data_source_item["_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["10w_article_total_cnt"]

                        dts_media_weixin_influence_rank_data_source_item["act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["article_post_end_date"] = document['cycle'][
                            current_date_time_to_str][cycle_type]["time"]
                        dts_media_weixin_influence_rank_data_source_item["article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_pub_count"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["article_avg_pub_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["head_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["head_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["head_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_median"]
                        dts_media_weixin_influence_rank_data_source_item["like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["max_post_time"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["max_post_time"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos1_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos1_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_10w_article_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_10w_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_median"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos2_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos2_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["multil_pos3_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["multil_pos3_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["pub_total_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["pub_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "public"] = document["public_id"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "classify"] = classify_rank[2]
                        dts_media_weixin_influence_rank_data_source_item["rank"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[0]]
                        dts_media_weixin_influence_rank_data_source_item["rank_incr"] = document['cycle'][current_date_time_to_str][
                            cycle_type][classify_rank[1]]
                        dts_media_weixin_influence_rank_data_source_item["single_act_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_act_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_act_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_act_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_like_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_like_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_avg_view_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_avg_view_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_cnt"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_article_total_cnt"]
                        dts_media_weixin_influence_rank_data_source_item["single_comment_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_comment_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_like_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_like_sum"]
                        dts_media_weixin_influence_rank_data_source_item["single_view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["single_view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["view_median"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_median"]
                        dts_media_weixin_influence_rank_data_source_item["view_sum"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["view_sum"]
                        dts_media_weixin_influence_rank_data_source_item["wom_index"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["wom_index"]
                        '''
                        潜力指标计算
                        '''
                        dts_media_weixin_influence_rank_data_source_item[
                            "potential_index"] = 0
                        if document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] > 0:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][
                                current_date_time_to_str][cycle_type]["wom_index"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] + 1) * 0.4
                            dts_media_weixin_influence_rank_data_source_item[
                                "potential_index"] = math.pow(potential_index, 2)
                        else:
                            potential_index = math.log(document['cycle'][current_date_time_to_str][cycle_type]["head_view_median"] + 1) * 0.3 + math.log(document['cycle'][current_date_time_to_str][
                                cycle_type]["wom_index"] + 1) * 0.3 - math.log((document['cycle'][current_date_time_to_str][cycle_type][classify_rank[1]] * (-1)) + 1) * 0.4
                            if potential_index > 0:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = math.pow(potential_index, 2)
                            else:
                                dts_media_weixin_influence_rank_data_source_item[
                                    "potential_index"] = (math.pow((potential_index * (-1)), 2)) * (-1)

                        dts_media_weixin_influence_rank_data_source_item[
                            "cycle_type"] = week_or_month
                        dts_media_weixin_influence_rank_data_source_item[
                            "account_short_desc"] = _account_short_desc
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_type"] = _auth_type
                        dts_media_weixin_influence_rank_data_source_item[
                            "nickname"] = _nickname
                        dts_media_weixin_influence_rank_data_source_item[
                            "auth_info"] = _auth_info
                        dts_media_weixin_influence_rank_data_source_item["highest_read_num"] = document['cycle'][current_date_time_to_str][
                            cycle_type]["highest_view_num"]
                        dts_media_weixin_influence_rank_data_source_item[
                            "cost_per_read"] = _m_avg_price_pv
                        dts_media_weixin_influence_rank_data_source_item[
                            "fans_number"] = _follower_num

                        dts_media_weixin_influence_rank_data_source_list.append(
                            dts_media_weixin_influence_rank_data_source_item)
            try:
                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []
            except Exception as e:
                print("delete duplicated items.....................")
                influence_data_prod_database.connect()
                print(str(dts_media_weixin_influence_rank_public_id_list))
                for public_id in dts_media_weixin_influence_rank_public_id_list:
                    print("delete %s" % public_id)
                    write_to_orm.delete().where(write_to_orm.public == public_id).execute()
                if settled_count % 100 == 0:
                    with influence_data_prod_database.atomic():
                        write_to_orm.insert_many(
                            dts_media_weixin_influence_rank_data_source_list).execute()
                    dts_media_weixin_influence_rank_data_source_list = []
                    dts_media_weixin_influence_rank_public_id_list = []

            print("settled %d items;%d items lefted" %
                  (settled_count, total_count))
            # input("...........................")
        '''
        导入最后不足100条数据
        '''
        try:
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()
        except Exception as e:
            print("delete duplicated items.....................")
            influence_data_prod_database.connect()
            for public_id in dts_media_weixin_influence_rank_public_id_list:
                write_to_orm.delete().where(write_to_orm.public == public_id).execute()
            with influence_data_prod_database.atomic():
                write_to_orm.insert_many(
                    dts_media_weixin_influence_rank_data_source_list).execute()

            # input("...........................")

        _read_from = write_to
        _update_time = current_datetime_to_str()
        # database.connect()
        '''
        如果写入数据超过100K,则切换表
        以免导致没有数据
        '''
        if write_to_orm.select().count() >= 100000:
            DtsInfluenceRankTableTip.update(calculate_timestamp=_calculate_timestamp,
                                            update_time=_update_time,
                                            read_from=_read_from).where(DtsInfluenceRankTableTip.id == 1).execute()
            delete_from_orm.delete().execute()
            delete_from_orm.raw("TRUNCATE TABLE `%s`" % read_from)
        influence_data_prod_database.close()
        source_mongo_tool_1.disconnection()
        source_mongo_tool_2.disconnection()
        # influence_data_prod_database.close()
        while True:
            result=requests.get("http://api.womdata.com/site/after-refresh-rank?access-token=b05a329eb619eb80763a03d50a81c233")
            if int(result.text) == 1:
                print("success!!!")
                break
            else:
                print("fail!!!")

    def media_weixin_public_id_and_digest_to_mysql(self,source_mongo_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        documents = source_mongo_tool.database["media_weixin"].find({},{"public_id":1,"public_name":1,"account_short_desc":1})
        account_count = 0 
        account_list = []
        for document in documents:
            account_count += 1
            print("settled %d items" % account_count)
            account_item = {}
            if "public_id" in document:
                account_item["public"] = document["public_id"]
            else:
                continue
            if "public_name" in document:
                account_item["public_name"] = document["public_name"]
            else:
                continue
            if "account_short_desc" in document:
                account_item["account_short_desc"] = document["account_short_desc"]
            else:
                account_item["account_short_desc"] = ""
            account_list.append(account_item)
            if account_count % 1000 == 0:
                with database_kaikai.atomic():
                    MongoMediaWeiXin.insert_many(account_list).execute()
                    account_list = []
        with database_kaikai.atomic():
            MongoMediaWeiXin.insert_many(account_list).execute()
        source_mongo_tool.disconnection()

    def dts_public_id_classify_by_classify_dict(self,source_file_args,target_file_args,classify):
        target_file_tool = self.get_file_tool(*target_file_args)
        count = 0 
        classify_count = 0
        public_id_updated = ""
        for mongo_media_weixin in MongoMediaWeiXin.select().where(MongoMediaWeiXin.media_cate == "#26#"):
            count += 1
            source_file_tool = self.get_file_tool(*source_file_args)
            source_file = source_file_tool.get_file()
            for line in source_file:
                # print(line.decode("gbk").replace(" ","").replace("\r","").replace("\n","").replace("\t","") in mongo_media_weixin.account_short_desc)
                if line.decode("gbk").replace(" ","").replace("\r","").replace("\n","").replace("\t","") in mongo_media_weixin.account_short_desc:
                    MongoMediaWeiXin.update(media_cate = classify).where(MongoMediaWeiXin.public == mongo_media_weixin.public).execute()
                    target_file_tool.write_database_line_to_file(mongo_media_weixin.public + "\n")
                    public_id_updated = mongo_media_weixin.public
                    classify_count += 1
                    break
            print("settled %d accounts;classified %d accounts;last updated account is %s" % (count,classify_count,public_id_updated))
            source_file_tool.close_file()
    def  dts_account_recovery_to_others(self,source_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        count = 0
        for line in source_file:
            public_id = source_file_tool.line_strip_decode_ignore(line)
            MongoMediaWeiXin.update(media_cate = "#26#").where(MongoMediaWeiXin.public == public_id).execute()
            count+=1
            print("settled %d accounts" % count)
    def dts_account_tags_from_weixin_media_cate(self,target_file_args):
        os.system("/usr/bin/rm %s/%s" % (target_file_args[0],target_file_args[1]))
        target_file_tool = self.get_file_tool(*target_file_args)
        count = 0
        for mongo_media_weixin in MongoMediaWeiXin.select():
            account_tags_list = []
            try:
                media_cate_list = mongo_media_weixin.media_cate.split("#")
            except Exception as e:
                media_cate_list=['','']
            count += 1
            print(media_cate_list,";counts",count)
            for i in range(26):
                account_tags_list.append("0")
            for media_cate in media_cate_list[1:-1]:
                account_tags_list[int(media_cate) - 1] = "1"
            target_file_tool.write_database_line_to_file(
                mongo_media_weixin.public + "," + ",".join(account_tags_list) + "\n")
    def media_weixin_media_cate_to_mongo_media_weixin(self):
        count = 0 
        for media_weixin in MediaWeixin.select().where(MediaWeixin.real_public !=''):
            count+=1
            print("settled ",count," accounts")
            if media_weixin.media_cate is None:
                continue
            MongoMediaWeiXin.update(wom_media_cate = media_weixin.media_cate).where(MongoMediaWeiXin.public == media_weixin.real_public).execute()
    def mongo_media_weixin_to_mysql_media_weixin(self,source_mongo_args,source_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        auth_info_type_tuple = (
            "account_cert", "tencent_account_cert", "sina_account_cert",)
        settled_count = 0 
        inserted_count = 0
        last_inserted_public = ""
        account_list = []
        for line in source_file:
            document = source_mongo_tool.database["media_weixin"].find_one({"public_id":source_file_tool.line_strip_decode_ignore(line)},{"public_id":1,
                                                                        "public_name":1,
                                                                        "account_cert":1,
                                                                        "tencent_account_cert":1,
                                                                        "sina_account_cert":1,
                                                                        "account_cert_info":1,
                                                                        "tencent_account_cert_info":1,
                                                                        "sina_account_cert_info":1,
                                                                        "account_short_desc":1,
                                                                        "avatar_big_img":1,
                                                                        "avatar_small_img":1,
                                                                        "qrcode_img":1})
            settled_count += 1
            print("...........................",settled_count)
            if (MediaWeixin.select().where(MediaWeixin.real_public == document["public_id"]).count() == 0) and (MediaWeixin.select().where(MediaWeixin.public == document["public_id"]).count() == 0):
                account_item = {}
                account_cert = -1
                account_cert_info = ""
                for auth_info_type in auth_info_type_tuple:
                    
                    if auth_info_type in document:
                        if document[auth_info_type] == 1 and auth_info_type == "account_cert":
                            account_cert = 0
                            if "account_cert_info" in document:
                                account_cert_info = document[
                                    "account_cert_info"]

                        if document[auth_info_type] == 1 and auth_info_type == "tencent_account_cert":
                            account_cert = 1
                            if "tencent_account_cert_info" in document:
                                account_cert_info = document[
                                    "tencent_account_cert_info"]

                        if document[auth_info_type] == 1 and auth_info_type == "sina_account_cert":
                            account_cert = 2
                            if "sina_account_cert_info" in document:
                                account_cert_info = document[
                                    "sina_account_cert_info"]
                account_short_desc = ""
                if "account_short_desc" in document:
                    account_short_desc = document["account_short_desc"]

                avatar_big_img =""
                if "avatar_big_img" in document:
                    avatar_big_img = document["avatar_big_img"]
                avatar_small_img = ""
                if "avatar_small_img" in document:
                    avatar_small_img = document["avatar_small_img"]
                public_name = ""
                if "public_name" in document:
                    public_name = document["public_name"]
                qrcode_img = ""
                if "qrcode_img" in document:
                    qrcode_img = document["qrcode_img"]
                account_item["put_up"]=0
                account_item["account_cert"]=account_cert
                account_item["account_cert_info"]=account_cert_info
                account_item["account_short_desc"]=account_short_desc
                account_item["avatar_big_img"]=avatar_big_img
                account_item["avatar_small_img"]=avatar_small_img
                account_item["come_from"]=1
                account_item["create_time"]=current_time_to_stamp()
                account_item["is_need_bd"]=0
                account_item["is_push"]=0
                account_item["is_top"]=0
                account_item["media_level"]=0
                account_item["public"]=document["public_id"]
                account_item["public_name"]=public_name
                account_item["qrcode_img"]=qrcode_img
                account_item["real_public"]=document["public_id"]
                account_item["show_in_51wom"]=0
                account_item["show_in_dts_rank"]=1
                account_item["status"]=0
                account_item["last_update_time"]=current_time_to_stamp()
                account_item["uuid"]=uuid_generator()
                account_item["media_belong_type"]=-1
                account_list.append(account_item)
                last_inserted_public = document["public_id"]
                # print("settled %d accounts;inserted %d accounts;last inserted is %s" % (settled_count,inserted_count,last_inserted_public))  
                # input("..............................................")
                inserted_count += 1
            if inserted_count == 0:
                continue
            if inserted_count % 1000 == 0:
                with prod_database.atomic():
                    # print(account_list)
                    # input("............................")
                    MediaWeixin.insert_many(account_list).execute()
                    account_list = []
            print("settled %d accounts;inserted %d accounts;last inserted is %s" % (settled_count,inserted_count,last_inserted_public))
        with prod_database.atomic():
            MediaWeixin.insert_many(account_list).execute()
            print("settled %d accounts;inserted %d accounts;last inserted is %s" % (settled_count,inserted_count,last_inserted_public))
    def mysql_media_weixin_basic_info_update(self,source_mongo_args):
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        documents = source_mongo_tool.database["media_weixin"].find({}).batch_size(1000)
        count = 0

        for document in documents:
            count+=1
            print("settled %d accounts" % count)
            if count <= 104630:
                continue
            if document is not None:
                if "public_name" in document:
                    public_name = document["public_name"].replace("'", '"')
                else:
                    public_name = ""

                if "avatar_big_img" in document:
                    avatar_big_img = document[
                        "avatar_big_img"]
                else:
                    avatar_big_img = ""

                if "avatar_small_img" in document:
                    avatar_small_img = document[
                        "avatar_small_img"]
                else:
                    avatar_small_img = ""

                if "qrcode_img" in document:
                    qrcode_img = document["qrcode_img"]
                else:
                    qrcode_img = ""

                if "account_cert" in document:
                    account_cert = document["account_cert"]
                else:
                    account_cert = 0

                if "account_cert_info" in document:
                    account_cert_info = document[
                        "account_cert_info"].replace("'", '"')
                else:
                    account_cert_info = ""

                if "account_short_desc" in document:
                    account_short_desc = document[
                        "account_short_desc"].replace("'", '"')
                else:
                    account_short_desc = ""
                MediaWeixin.update(
                    public_name = public_name,
                    avatar_big_img=avatar_big_img,
                    avatar_small_img=avatar_small_img,
                    qrcode_img=qrcode_img,
                    account_cert=account_cert,
                    account_cert_info=account_cert_info,
                    account_short_desc=account_short_desc
                    ).where(MediaWeixin.real_public == document["public_id"]).execute()
    def wom_dev_weixin_cate_tags(self,source_file_args):
        for mongo_media_weixin in MongoMediaWeiXin.select():
            target_str = mongo_media_weixin.public_name+mongo_media_weixin.account_short_desc
            source_file_tool = self.get_file_tool(*source_file_args)
            source_file = source_file_tool.get_file()
            media_tags_list = []
            for line in source_file:
                line = line.decode("utf-8").replace(" ","").replace("\r","").replace("\n","").replace("\t","")
                print(line)
                if line in target_str:
                    media_tags_list.append(line)
            media_tags_str = "#" + "#".join(media_tags_list) + "#"
            print(media_tags_str)
            MongoMediaWeiXin.update(media_tags = media_tags_str).where(MongoMediaWeiXin.public == mongo_media_weixin.public).execute()
    def wom_dev_cates_tags_to_dev_prod(self):
        count = 0
        for dev_weixin_cate in MongoMediaWeiXin.select():
            MediaWeixin.update(follower_area_machine = dev_weixin_cate.media_area,
                                media_cate_machine = dev_weixin_cate.media_cate).where(MediaWeixin.real_public == dev_weixin_cate.public).execute()
            count += 1
            print("settled ",count," accounts")

    def media_weixin_statistic_brand_keyword_update(self,source_mongo_args):
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        documents = source_mongo_tool.database["media_weixin_brands"].find({}).batch_size(1000)
        for document in tqdm(documents):
            public_id = document["public_id"]
            brand_list = []
            brand_segment = document["brand_segment"][0]
            # print(brand_segment)
            # print(sorted(dict2list(brand_segment), key=lambda d:d[1], reverse = True))
            for key,value in sorted(dict2list(brand_segment), key=lambda d:d[1], reverse = True):
                brand_item = {}
                brand_item["name"]=key
                brand_item["value"]=value
                brand_list.append(brand_item)
            MediaWeixinStatistic.update(brand_keyword = json.dumps(brand_list)).where(MediaWeixinStatistic.public == public_id).execute()
            




            



