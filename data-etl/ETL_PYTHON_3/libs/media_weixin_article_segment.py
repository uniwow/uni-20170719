import jieba
import jieba.posseg as pseg # 词性标注
import jieba.analyse # 关键词抽取
from optparse import OptionParser

'''
1.
P(台中) ＜ P(台)×P(中)，“台中”词频不够导致其成词概率较低
解决方法：强制调高词频
jieba.add_word('台中') 或者 jieba.suggest_freq('台中', True)
2.
“今天天气 不错”应该被切成“今天 天气 不错”？（以及类似情况）
解决方法：强制调低词频
jieba.suggest_freq(('今天', '天气'), True)
或者直接删除该词 jieba.del_word('今天天气')
'''
jieba.cut("my str",cut_all = True,HMM =True)
jieba.load_userdict(file_name) # 词语、词频（可省略）、词性（可省略）
jieba.add_word('石墨烯')
jieba.enable_parallel(4) # 开启并行分词模式，参数为并行进程数
jieba.disable_parallel()
jieba.analyse.TFIDF(idf_path=None)
jieba.analyse.set_idf_path(file_name)
jieba.analyse.set_stop_words(file_name)
jieba.set_dictionary("foobar.txt") # 主词典路径
result = pseg.cut(test_sent)

for w in result:
    print(w.word, "/", w.flag, ", ", end=' ')

USAGE = "usage:    python extract_tags.py [file name] -k [top k]"

parser = OptionParser(USAGE)
parser.add_option("-k", dest="topK")
opt, args = parser.parse_args()


if len(args) < 1:
    print(USAGE)
    sys.exit(1)

file_name = args[0]

if opt.topK is None:
    topK = 10
else:
    topK = int(opt.topK)

content = open(file_name, 'rb').read()

tags = jieba.analyse.extract_tags(content, topK=topK ,allowPOS=('ns', 'n'))

print(",".join(tags))
for word, flag in words:
    print('%s %s' % (word, flag))
