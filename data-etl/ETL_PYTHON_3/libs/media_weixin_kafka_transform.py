# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from pykafka import KafkaClient, SslConfig
from tools.settings import WOM_DTS_OPS_MONGO_TUPLE, BACKUP_MACHINE_LEARNING_PATH, BACKUP_INDEX_COMPUTE_PATH, WOM_DEV_MYSQL_TUPLE, BACKUP_CSV_PATH_BIG_DATA, TARGET_COLUMN_TUPLE_KAFKA, ZOOKEEPER, CHECK_CSV_PATH
import json
from tools.tool_string import strip_and_replace_all, strip_and_replace
from pykafka.partition import Partition
from tools.tool_time import datetime_to_str, current_time_to_stamp, current_stamp_to_datetime
import time
import math
import datetime
from my_exceptions.my_exceptions import KafkaCrash
from elasticsearch import Elasticsearch
import sys
import traceback
from pykafka import KafkaClient
import time
import fcntl
import random
import bson

client = KafkaClient(
    hosts="server-32:9092, server-32:9093, server-32:9094") 


ES_INDEX_NAME = "qmg_dts_sogou_weixin"
ES_ARTICLE_TYPE = "media_weixin_article"


# with topic_testj.get_sync_producer() as producer:
#     for i in range(4):
#         producer.produce('test message ' + str(i ** 2))
#         print('test message ' + str(i ** 2))

# consumer
# balanced_consumer = topic.get_balanced_consumer(
#     consumer_group='testgroup',
#     auto_commit_enable=True,  # 设置为Flase的时候不需要添加 consumer_group
#     zookeeper_connect='myZkClusterNode1.com:2181,myZkClusterNode2.com:2181/myZkChroot' # 这里就是连接多个zk
# )


class MediaWeiXinKafkaTransform(object):
    """weixin & kafka"""

    def get_mysql_tool(self, host, db, user, pwd):
        mysql_tool = MysqlTool(host, db, user, pwd)
        return mysql_tool

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def producer_test(self, kafka_args):
        client = KafkaClient(hosts=kafka_args["hosts"])
        topics = client.topics
        if kafka_args["topics"][2] in topics:
            print("topic exists")
            uni_kafka_topic = topics[kafka_args["topics"][2]]
            producer = uni_kafka_topic.get_sync_producer()
            for i in range(4):
                producer.produce(
                    ('test message ' + str(i ** 2)).encode('utf-8', 'ignore'))
                print("product one")

    def consumer_test(self, target_file_args, kafka_args):
        target_file_tool = self.get_file_tool(*target_file_args)
        client = KafkaClient(hosts=kafka_args["hosts"])
        topics = client.topics
        if kafka_args["topics"][2] in topics:
            article_info_crawl_topic = topics[kafka_args["topics"][2]]
            print(str(article_info_crawl_topic.latest_available_offsets()),
                  "-" * 30 + "latest_available_offsets")
            consumer = article_info_crawl_topic.get_simple_consumer(
                consumer_group=b"uniwow")
            # consumer = article_info_crawl_topic.get_balanced_consumer(zookeeper_connect=ZOOKEEPER,
            #                                auto_commit_enable=True,
            #                                consumer_group=kafka_args["topics"][3])
            topic_partition = consumer.partitions
            for partition in topic_partition:
                print(str(type(partition)) + "<-------------------partition")
                # consumer.reset_offsets(partition)
            for message in consumer:
                if message is not None:
                    print(message.offset, message.value.decode("utf-8", 'ignore'))
                    target_file_tool.write_database_line_to_file(message.value.decode(
                        "utf-8", 'ignore') + "#" + str(message.offset) + '\n')
                    consumer.commit_offsets()

    def weixin_article_csv_consumer(self, source_mongo_args, target_mysql_args, kafka_args):
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_INDEX_COMPUTE_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        source_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        es_log_file = self.get_file_tool(
            *(CHECK_CSV_PATH, 'es_error_log.csv', 'ab'))
        es = Elasticsearch(
            hosts="http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.156:9200/")

        # target_file_tool_3 = self.get_file_tool(*target_file_args_3)
        # input("..................")
        settled_count = 0
        while True:
            collection_count = source_mongo_tool.database[
                'kafka_conventional_grow_trend_info'].find({}).count()
            '''
            查看source collection 的数目
            如果为零，等待3秒，再次查询，如果还无数据，则假设kafka成功
            等待3秒是假设mongo collection 插入延时
            '''
            if collection_count == 0:
                time.sleep(10)
                collection_count = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info'].find({}).count()
                if collection_count == 0:
                    break
            document = source_mongo_tool.database[
                'kafka_conventional_grow_trend_info'].find_one({}, {"kafka_message": 1})
            # print(str(document['kafka_message'].decode("utf-8", 'ignore')))
            mongo_object_id = document['_id']
            message_dict = json.loads(
                document['kafka_message'].decode("utf-8", 'ignore'))
            index_compute_item_list = []
            for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                index_compute_item_list.append(
                    strip_and_replace_all(str(message_dict[k])))
            try:
                fcntl.flock(target_file_tool_1.f, fcntl.LOCK_EX)
                target_file_tool_1.write_database_line_to_file(
                    ",".join(index_compute_item_list) + "\n")
                fcntl.flock(target_file_tool_1.f, fcntl.LOCK_UN)
                
            except Exception as e:
                print('........................')
                pass
                #"to do list"
            finally:
                pass

            machine_learning_item_list = []
            for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                machine_learning_item_list.append(
                    strip_and_replace_all(str(message_dict[k])))
            try:
                fcntl.flock(target_file_tool_2.f, fcntl.LOCK_EX)
                target_file_tool_2.write_database_line_to_file(
                    "*|233|*|".join(machine_learning_item_list) + "\n")
                fcntl.flock(target_file_tool_2.f, fcntl.LOCK_UN)
            except Exception as e:
                pass
                #"to do list"
            finally:
                pass
            # weixin_article_item_list = []
            is_new_crawled_article = message_dict[
                "is_new_crawled_article"]
            print("is_new_crawled_article type %s -> %s" %
                  (type(is_new_crawled_article), str(is_new_crawled_article)))
            # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
            #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
            #     weixin_article_item_list.append(
            #         strip_and_replace_all(str(message_dict[k])))
            # print(str(message_dict))
            mongo_obj_id = strip_and_replace_all(
                strip_and_replace(str(message_dict["mongo_object_id"])))
            weixin_id = strip_and_replace_all(
                strip_and_replace(str(message_dict["weixin_id"])))
            title = strip_and_replace_all(
                strip_and_replace(str(message_dict["title"])))
            article_url = strip_and_replace_all(
                strip_and_replace(str(message_dict["fixed_url"])))
            short_desc = strip_and_replace_all(
                strip_and_replace(str(message_dict["digest"])))
            article_type = int(message_dict["article_type"])
            article_pos = int(message_dict["article_pos"])
            read_num = int(message_dict["page_view_num"])
            like_num = int(message_dict["page_like_num"])
            post_time = int(message_dict["post_time"])
            content = strip_and_replace_all(message_dict["content"])

            if "is_origin" in message_dict:
                is_origin = int(message_dict["is_origin"])
            else:
                is_origin = -1
            if "cover" in message_dict:
                cover_img = strip_and_replace_all(message_dict["cover"])
            else:
                cover_img = ''
            article_influence_index = 0.7 * \
                math.log(read_num + 1) + 0.3 * math.log(like_num + 1)
            query = (
                "SELECT `m_wmi` FROM `media_weixin` WHERE `real_public_id`='%s'" % weixin_id)
            source_mysql_tool.cursor.execute(query)
            row = source_mysql_tool.cursor.fetchone()
            if row is not None:
                wom_index, = row
            else:
                wom_index = 0
            article_posted_time = int(
                (int(time.time()) - post_time) / (60 * 60))
            article_hot_index = math.pow(0.5 * math.log(wom_index + 1) + 0.3 * math.log(
                read_num + 1) + 0.2 * math.log(like_num + 1), 2) - math.pow(math.log(article_posted_time + 1), 2)
            query = (
                "SELECT COUNT(*) FROM `weixin_article` WHERE `mongo_obj_id`='%s'" % mongo_obj_id)
            target_mysql_tool.cursor.execute(query)
            row = target_mysql_tool.cursor.fetchone()
            (target_row_exist_flag,) = row
            
            if int(target_row_exist_flag) == 1:
                query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d,`content`='%s',`is_origin`=%d,`cover_img`='%s',`article_influence_index`=%.2f,`article_hot_index`=%.2f WHERE `mongo_obj_id`='%s'" % (
                    weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index, mongo_obj_id))
                print(query)
                # input("..................update")
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    # print(e.)
                    pass
                finally:
                    pass

            elif int(target_row_exist_flag) == 0:
                # print(content)
                query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`,`content`,`is_origin`,`cover_img`,`article_influence_index`,`article_hot_index`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s',%d,'%s',%.2f,%.2f)" % (
                    mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index))
                print(query)
                
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    exc_type, exc_value, exc_tb = sys.exc_info()
                    es_log_file.write_database_line_to_file(
                        "\n".join(traceback.format_exception(exc_type, exc_value, exc_tb)))
                    sys.exit()

                finally:
                    pass

                if weixin_id != "":
                    try:
                        es_result = es.index(index=ES_INDEX_NAME,
                                             doc_type=ES_ARTICLE_TYPE,
                                             id=mongo_obj_id,
                                             body={
                                                 "public_id": weixin_id,
                                                 "public_name": str(message_dict["weixin_name"]),
                                                 "mongo_object_id": mongo_obj_id,
                                                 "title": title,
                                                 "content": content,
                                                 "digest": short_desc,
                                                 "article_pos": article_pos,
                                                 "is_origin": is_origin,
                                                 "post_date": datetime_to_str(current_stamp_to_datetime(post_time)),
                                             },
                                             routing=weixin_id)
                    except Exception as e:
                        es_log_file.write_database_line_to_file("public_id: %s,title: %s,post_date: %s " % (
                            weixin_id, title, datetime_to_str(current_stamp_to_datetime(post_time))))
                else:
                    es_log_file.write_database_line_to_file(
                        "public_id: %s" % weixin_id)
                    sys.exit()
                # print(str(es_result)+"-"*20)
            # input("..................input")
            source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
                {'_id': mongo_object_id})

            settled_count += 1
            # if settled_count == 5:
            #     break
            print('settled %d document' % settled_count)

        try:
            client = KafkaClient(hosts=kafka_args["hosts"])
            # input("..................")
            topics = client.topics
            if kafka_args["topics"][3] in topics:
                # print(
                #     "kafka_args................................................................")
                article_info_crawl_topic = topics[kafka_args["topics"][3]]
                consumer = article_info_crawl_topic.get_balanced_consumer(
                    consumer_group=b"uniwow", auto_commit_enable=False, zookeeper_connect=ZOOKEEPER)
                message_count = 0
                for message in consumer:
                    # print(
                    #     "kafka_args................................................................")
                    if message is not None:
                        message_count += 1
                        # print(message.offset, message.value.decode(
                        #     "utf-8", 'ignore'))
                        message_dict = json.loads(
                            message.value.decode("utf-8", 'ignore'))
                        index_compute_item_list = []

                        if int(message_dict["update_time"]) < 1480262400:
                            continue
                        # print(str(message_dict))
                        # input("................")
                        for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                            index_compute_item_list.append(
                                strip_and_replace_all(str(message_dict[k])))
                        try:
                            fcntl.flock(target_file_tool_1.f, fcntl.LOCK_EX)
                            target_file_tool_1.write_database_line_to_file(
                                ",".join(index_compute_item_list) + "," + str(message.offset) + "\n")
                            fcntl.flock(target_file_tool_1.f, fcntl.LOCK_UN)
                            
                        except Exception as e:
                            pass
                            #"to do list"
                        finally:
                            pass

                        machine_learning_item_list = []
                        for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                            machine_learning_item_list.append(
                                strip_and_replace_all(str(message_dict[k])))
                        try:
                            fcntl.flock(target_file_tool_2.f, fcntl.LOCK_EX)
                            target_file_tool_2.write_database_line_to_file(
                                "*|233|*|".join(machine_learning_item_list) + "\n")
                            fcntl.flock(target_file_tool_2.f, fcntl.LOCK_UN)
                            
                        except Exception as e:
                            pass
                            #"to do list"
                        finally:
                            pass
                        weixin_article_item_list = []
                        is_new_crawled_article = message_dict[
                            "is_new_crawled_article"]
                        print("is_new_crawled_article type %s -> %s" %
                              (type(is_new_crawled_article), str(is_new_crawled_article)))
                        # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
                        #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
                        #     weixin_article_item_list.append(
                        #         strip_and_replace_all(str(message_dict[k])))
                        mongo_obj_id = strip_and_replace_all(
                            strip_and_replace(str(message_dict["mongo_object_id"])))
                        weixin_id = strip_and_replace_all(
                            strip_and_replace(str(message_dict["weixin_id"])))
                        title = strip_and_replace_all(
                            strip_and_replace(str(message_dict["title"])))
                        article_url = strip_and_replace_all(
                            strip_and_replace(str(message_dict["fixed_url"])))
                        short_desc = strip_and_replace_all(
                            strip_and_replace(str(message_dict["digest"])))
                        article_type = int(message_dict["article_type"])
                        article_pos = int(message_dict["article_pos"])
                        read_num = int(message_dict["page_view_num"])
                        like_num = int(message_dict["page_like_num"])
                        post_time = int(message_dict["post_time"])
                        content = strip_and_replace_all(
                            message_dict["content"])

                        if "is_origin" in message_dict:
                            is_origin = int(message_dict["is_origin"])
                        else:
                            is_origin = -1
                        if "cover" in message_dict:
                            cover_img = strip_and_replace_all(
                                message_dict["cover"])
                        else:
                            cover_img = ''
                        article_influence_index = 0.7 * \
                            math.log(read_num + 1) + 0.3 * \
                            math.log(like_num + 1)

                        query = (
                            "SELECT `m_wmi` FROM `media_weixin` WHERE `real_public_id`='%s'" % weixin_id)
                        source_mysql_tool.cursor.execute(query)
                        row = source_mysql_tool.cursor.fetchone()
                        if row is not None:
                            wom_index, = row
                        else:
                            wom_index = 0
                        article_posted_time = int(
                            (int(time.time()) - post_time) / (60 * 60))
                        article_hot_index = math.pow(0.5 * math.log(wom_index + 1) + 0.3 * math.log(
                            read_num + 1) + 0.2 * math.log(like_num + 1), 2) - math.pow(math.log(article_posted_time + 1), 2)
                        query = (
                            "SELECT COUNT(*) FROM `weixin_article` WHERE `mongo_obj_id`='%s'" % mongo_obj_id)
                        target_mysql_tool.cursor.execute(query)
                        row = target_mysql_tool.cursor.fetchone()
                        (target_row_exist_flag,) = row
                        if int(target_row_exist_flag) == 1:
                            query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d,`content`='%s',`is_origin`=%d,`cover_img`='%s',`article_influence_index`=%.2f,`article_hot_index`=%.2f WHERE `mongo_obj_id`='%s'" % (
                                weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index, mongo_obj_id))
                            print(query)
                            # input("..................update")
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                # print(e.)
                                pass
                            finally:
                                pass

                        elif int(target_row_exist_flag) == 0:
                            query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`,`content`,`is_origin`,`cover_img`,`article_influence_index`,`article_hot_index`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s',%d,'%s',%.2f,%.2f)" % (
                                mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index))
                            print(query)
                            # input("..................input")
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                es_log_file.write_database_line_to_file(
                                    "mysql insert error: %s" % mongo_obj_id)
                                pass
                            finally:
                                pass
                            if weixin_id != "":
                                es.index(index=ES_INDEX_NAME,
                                         doc_type=ES_ARTICLE_TYPE,
                                         id=mongo_obj_id,
                                         body={
                                             "public_id": weixin_id,
                                             "public_name": str(message_dict["weixin_name"]),
                                             "mongo_object_id": mongo_obj_id,
                                             "title": title,
                                             "content": content,
                                             "digest": short_desc,
                                             "article_pos": article_pos,
                                             "is_origin": is_origin,
                                             "post_date": datetime_to_str(current_stamp_to_datetime(post_time)),
                                         },
                                         routing=weixin_id)
                            else:
                                es_log_file.write_database_line_to_file(
                                    "public_id: %s" % "1")
                        # 标记消息为已消费
                        consumer.commit_offsets()
                        # print(str(message_dict))
                        # input("................")
                        if datetime_to_str(datetime.datetime.now()) != current_day:
                            source_mongo_tool.disconnection()
                            target_file_tool_1.close_file()
                            target_file_tool_2.close_file()
                            source_mysql_tool.disconnection()
                            target_mysql_tool.disconnection()
                            current_day = datetime_to_str(
                                datetime.datetime.now())
                            target_file = "article_%s.csv" % current_day
                            target_file_args_1 = (
                                BACKUP_INDEX_COMPUTE_PATH, target_file, "ab")
                            target_file_args_2 = (
                                BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
                            source_mongo_tool = self.get_mongo_tool(
                                *source_mongo_args)
                            target_file_tool_1 = self.get_file_tool(
                                *target_file_args_1)
                            target_file_tool_2 = self.get_file_tool(
                                *target_file_args_2)
                            source_mysql_tool = self.get_mysql_tool(
                                *target_mysql_args)
                            target_mysql_tool = self.get_mysql_tool(
                                *target_mysql_args)
                        # target_file_tool_3.write_database_line_to_file(
                        #     "*|233|*|".join(weixin_article_item_list) + "\n")
                        # ducument_cursor=source_mongo_tool.database['kafka_conventional_grow_trend_info'].find({},{"kafka_message":1}).limit(5)
                        # ducument_cursor.batch_size(500)

        except Exception as e:
            pass
            settled_count = 0
            while True:
                collection_count = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info'].find({}).count()
                '''
                查看source collection 的数目
                如果为零，等待3秒，再次查询，如果还无数据，则假设kafka成功
                等待3秒是假设mongo collection 插入延时
                '''
                if collection_count == 0:
                    time.sleep(5)
                    collection_count = source_mongo_tool.database[
                        'kafka_conventional_grow_trend_info'].find({}).count()
                    if collection_count == 0:
                        source_mongo_tool.disconnection()
                        target_file_tool_1.close_file()
                        target_file_tool_2.close_file()
                        source_mysql_tool.disconnection()
                        target_mysql_tool.disconnection()
                        self.weixin_article_csv_consumer(
                            source_mongo_args, target_mysql_args, kafka_args)
                document = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info'].find_one({}, {"kafka_message": 1})
                mongo_object_id = document['_id']
                message_dict = json.loads(
                    document['kafka_message'].decode("utf-8", 'ignore'))
                index_compute_item_list = []
                for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                    index_compute_item_list.append(
                        strip_and_replace_all(str(message_dict[k])))
                try:
                    fcntl.flock(target_file_tool_1.f, fcntl.LOCK_EX)
                    target_file_tool_1.write_database_line_to_file(
                        ",".join(index_compute_item_list) + "\n")
                    fcntl.flock(target_file_tool_1.f, fcntl.LOCK_UN)
                except Exception as e:
                    print('........................')
                    pass
                    #"to do list"
                finally:
                    pass

                machine_learning_item_list = []
                for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                    machine_learning_item_list.append(
                        strip_and_replace_all(str(message_dict[k])))
                try:
                    fcntl.flock(target_file_tool_2.f, fcntl.LOCK_EX)
                    target_file_tool_2.write_database_line_to_file(
                        "*|233|*|".join(machine_learning_item_list) + "\n")
                    fcntl.flock(target_file_tool_2.f, fcntl.LOCK_UN)
                except Exception as e:
                    pass
                    #"to do list"
                finally:
                    pass
                # weixin_article_item_list = []
                is_new_crawled_article = message_dict[
                    "is_new_crawled_article"]
                print("is_new_crawled_article type %s -> %s" %
                      (type(is_new_crawled_article), str(is_new_crawled_article)))
                # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
                #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
                #     weixin_article_item_list.append(
                #         strip_and_replace_all(str(message_dict[k])))
                mongo_obj_id = strip_and_replace_all(
                    strip_and_replace(str(message_dict["mongo_object_id"])))
                weixin_id = strip_and_replace_all(
                    strip_and_replace(str(message_dict["weixin_id"])))
                title = strip_and_replace_all(
                    strip_and_replace(str(message_dict["title"])))
                article_url = strip_and_replace_all(
                    strip_and_replace(str(message_dict["fixed_url"])))
                short_desc = strip_and_replace_all(
                    strip_and_replace(str(message_dict["digest"])))
                article_type = int(message_dict["article_type"])
                article_pos = int(message_dict["article_pos"])
                read_num = int(message_dict["page_view_num"])
                like_num = int(message_dict["page_like_num"])
                post_time = int(message_dict["post_time"])

                content = strip_and_replace_all(message_dict["content"])

                if "is_origin" in message_dict:
                    is_origin = int(message_dict["is_origin"])
                else:
                    is_origin = -1
                if "cover" in message_dict:
                    cover_img = strip_and_replace_all(message_dict["cover"])
                else:
                    cover_img = ''
                article_influence_index = 0.7 * \
                    math.log(read_num + 1) + 0.3 * math.log(like_num + 1)
                query = (
                    "SELECT `m_wmi` FROM `media_weixin` WHERE `real_public_id`='%s'" % weixin_id)
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
                if row is not None:
                    wom_index, = row
                else:
                    wom_index = 0
                article_posted_time = int(
                    (int(time.time()) - post_time) / (60 * 60))
                article_hot_index = math.pow(0.5 * math.log(wom_index + 1) + 0.3 * math.log(
                    read_num + 1) + 0.2 * math.log(like_num + 1), 2) - math.pow(math.log(article_posted_time + 1), 2)
                query = (
                    "SELECT COUNT(*) FROM `weixin_article` WHERE `mongo_obj_id`='%s'" % mongo_obj_id)
                target_mysql_tool.cursor.execute(query)
                row = target_mysql_tool.cursor.fetchone()
                (target_row_exist_flag,) = row
                if int(target_row_exist_flag) == 1:
                    query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d,`content`='%s',`is_origin`=%d,`cover_img`='%s',`article_influence_index`=%.2f,`article_hot_index`=%.2f WHERE `mongo_obj_id`='%s'" % (
                        weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index, mongo_obj_id))
                    print(query)
                    # input("..................update")
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        # print(e.)
                        pass
                    finally:
                        pass

                elif int(target_row_exist_flag) == 0:
                    query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`,`content`,`is_origin`,`cover_img`,`article_influence_index`,`article_hot_index`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s',%d,'%s',%.2f,%.2f)" % (
                        mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, article_hot_index))
                    print(query)
                    # input("..................input")
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        # print(e.)
                        pass
                    finally:
                        pass
                    if weixin_id != "":
                        es.index(index=ES_INDEX_NAME,
                                 doc_type=ES_ARTICLE_TYPE,
                                 id=mongo_obj_id,
                                 body={
                                     "public_id": weixin_id,
                                     "public_name": str(message_dict["weixin_name"]),
                                     "mongo_object_id": mongo_obj_id,
                                     "title": title,
                                     "content": content,
                                     "digest": short_desc,
                                     "article_pos": article_pos,
                                     "is_origin": is_origin,
                                     "post_date": datetime_to_str(current_stamp_to_datetime(post_time)),
                                 },
                                 routing=weixin_id)

                source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
                    {'_id': mongo_object_id})

                settled_count += 1
                # if settled_count == 5:
                #     break
                print('settled %d document' % settled_count)

        finally:
            source_mongo_tool.disconnection()

            target_file_tool_1.close_file()
            target_file_tool_2.close_file()
            source_mysql_tool.disconnection()
            target_mysql_tool.disconnection()
            print("game over..............................")

    def weixin_article_csv_consumer_kafka(self, target_mysql_args, kafka_args):
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                              '/machine_learning', target_file, "ab")
        target_file_tool_1 = FileTool(*target_file_args_1)
        target_file_tool_2 = FileTool(*target_file_args_2)
        target_mysql_tool = MysqlTool(*target_mysql_args)
        # target_file_tool_3 = self.get_file_tool(*target_file_args_3)

        crash_flag = 0
        try:
            client = KafkaClient(hosts=kafka_args["hosts"])
            # input("..................")
            topics = client.topics
            print(str(kafka_args["topics"][3] in topics))
            input("..................kafka crash pre")
            raise KafkaCrash
            input("..................kafka crash ...")
            if kafka_args["topics"][3] in topics:

                # print(
                #     "kafka_args................................................................")
                article_info_crawl_topic = topics[kafka_args["topics"][3]]
                consumer = article_info_crawl_topic.get_balanced_consumer(
                    consumer_group=b"uniwow", auto_commit_enable=False, zookeeper_connect=ZOOKEEPER)
                message_count = 0

                for message in consumer:
                    # print(
                    #     "kafka_args................................................................")
                    # input("..................")
                    if message is not None:
                        message_count += 1
                        # print(message.offset, message.value.decode(
                        #     "utf-8", 'ignore'))
                        message_dict = json.loads(
                            message.value.decode("utf-8", 'ignore'))
                        index_compute_item_list = []
                        for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                            index_compute_item_list.append(
                                strip_and_replace_all(str(message_dict[k])))
                        try:
                            target_file_tool_1.write_database_line_to_file(
                                ",".join(index_compute_item_list) + "," + str(message.offset) + "\n")
                        except Exception as e:
                            pass
                            #"to do list"
                        finally:
                            pass

                        machine_learning_item_list = []
                        for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                            machine_learning_item_list.append(
                                strip_and_replace_all(str(message_dict[k])))
                        try:
                            target_file_tool_2.write_database_line_to_file(
                                "*|233|*|".join(machine_learning_item_list) + "\n")
                        except Exception as e:
                            pass
                            #"to do list"
                        finally:
                            pass
                        # weixin_article_item_list = []
                        is_new_crawled_article = message_dict[
                            "is_new_crawled_article"]
                        print("is_new_crawled_article type %s -> %s" %
                              (type(is_new_crawled_article), str(is_new_crawled_article)))
                        # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
                        #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
                        #     weixin_article_item_list.append(
                        #         strip_and_replace_all(str(message_dict[k])))
                        mongo_obj_id = strip_and_replace_all(
                            strip_and_replace(str(message_dict["mongo_object_id"])))
                        weixin_id = strip_and_replace_all(
                            strip_and_replace(str(message_dict["weixin_id"])))
                        title = strip_and_replace_all(
                            strip_and_replace(str(message_dict["title"])))
                        article_url = strip_and_replace_all(
                            strip_and_replace(str(message_dict["fixed_url"])))
                        short_desc = strip_and_replace_all(
                            strip_and_replace(str(message_dict["digest"])))
                        article_type = int(message_dict["article_type"])
                        article_pos = int(message_dict["article_pos"])
                        read_num = int(message_dict["page_view_num"])
                        like_num = int(message_dict["page_like_num"])
                        post_time = int(message_dict["post_time"])

                        if int(is_new_crawled_article) == 0:
                            query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d WHERE `mongo_obj_id`='%s'" % (
                                weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, mongo_obj_id))
                            print(query)
                            # input("..................")
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                # print(e.)
                                pass
                            finally:
                                pass

                        elif int(is_new_crawled_article) == 1:
                            query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
                                mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
                            print(query)
                            try:
                                target_mysql_tool.cursor.execute(query)
                                target_mysql_tool.cnx.commit()
                            except Exception as e:
                                # print(e.)
                                pass
                            finally:
                                pass
                        # 标记消息为已消费
                        # consumer.commit_offsets()

                        if datetime_to_str(datetime.datetime.now()) != current_day:
                            source_mongo_tool.disconnection()
                            target_file_tool_1.close_file()
                            target_file_tool_2.close_file()
                            target_mysql_tool.disconnection()
                            current_day = datetime_to_str(
                                datetime.datetime.now())
                            target_file = "article_%s.csv" % current_day
                            target_file_args_1 = (BACKUP_CSV_PATH_BIG_DATA +
                                                  '/index_compute', target_file, "ab")
                            target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                                                  '/machine_learning', target_file, "ab")
                            source_mongo_tool = self.get_mongo_tool(
                                *source_mongo_args)
                            target_file_tool_1 = self.get_file_tool(
                                *target_file_args_1)
                            target_file_tool_2 = self.get_file_tool(
                                *target_file_args_2)
                            target_mysql_tool = self.get_mysql_tool(
                                *target_mysql_args)
                        # target_file_tool_3.write_database_line_to_file(
                        #     "*|233|*|".join(weixin_article_item_list) + "\n")
                        # ducument_cursor=source_mongo_tool.database['kafka_conventional_grow_trend_info'].find({},{"kafka_message":1}).limit(5)
                        # ducument_cursor.batch_size(500)

        except Exception as e:
            crash_flag = 1
            print("except crash")
        finally:
            target_file_tool_1.close_file()
            target_file_tool_2.close_file()
            target_mysql_tool.disconnection()
            print("game over..............................kafka")
        return crash_flag

    def weixin_article_csv_consumer_mongo(self, target_mysql_args):
        source_mongo_tool = MongoTool(*WOM_DTS_OPS_MONGO_TUPLE)
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                              '/machine_learning', target_file, "ab")
        target_file_tool_1 = FileTool(*target_file_args_1)
        target_file_tool_2 = FileTool(*target_file_args_2)
        target_mysql_tool = MysqlTool(*target_mysql_args)
        break_flag = 0
        try:
            settled_count = 0
            while True:

                collection_count = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info_test'].find({}).count()
                '''
                查看source collection 的数目
                如果为零，等待3秒，再次查询，如果还无数据，则假设kafka成功
                等待3秒是假设mongo collection 插入延时
                '''
                if collection_count == 0:
                    time.sleep(5)
                    collection_count = source_mongo_tool.database[
                        'kafka_conventional_grow_trend_info'].find({}).count()
                    if collection_count == 0:
                        source_mongo_tool.disconnection()
                        target_file_tool_1.close_file()
                        target_file_tool_2.close_file()
                        target_mysql_tool.disconnection()
                        break_flag = 1
                        break

                document = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info'].find_one({}, {"kafka_message": 1})
                mongo_object_id = document['_id']
                message_dict = json.loads(
                    document['kafka_message'].decode("utf-8", 'ignore'))
                index_compute_item_list = []
                for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                    index_compute_item_list.append(
                        strip_and_replace_all(str(message_dict[k])))
                try:
                    target_file_tool_1.write_database_line_to_file(
                        ",".join(index_compute_item_list) + "\n")
                except Exception as e:
                    print('........................')
                    pass
                    #"to do list"
                finally:
                    pass

                machine_learning_item_list = []
                for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                    machine_learning_item_list.append(
                        strip_and_replace_all(str(message_dict[k])))
                try:
                    target_file_tool_2.write_database_line_to_file(
                        "*|233|*|".join(machine_learning_item_list) + "\n")
                except Exception as e:
                    pass
                    #"to do list"
                finally:
                    pass
                # weixin_article_item_list = []
                is_new_crawled_article = message_dict[
                    "is_new_crawled_article"]
                print("is_new_crawled_article type %s -> %s" %
                      (type(is_new_crawled_article), str(is_new_crawled_article)))
                # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
                #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
                #     weixin_article_item_list.append(
                #         strip_and_replace_all(str(message_dict[k])))
                mongo_obj_id = strip_and_replace_all(
                    strip_and_replace(str(message_dict["mongo_object_id"])))
                weixin_id = strip_and_replace_all(
                    strip_and_replace(str(message_dict["weixin_id"])))
                title = strip_and_replace_all(
                    strip_and_replace(str(message_dict["title"])))
                article_url = strip_and_replace_all(
                    strip_and_replace(str(message_dict["fixed_url"])))
                short_desc = strip_and_replace_all(
                    strip_and_replace(str(message_dict["digest"])))
                article_type = int(message_dict["article_type"])
                article_pos = int(message_dict["article_pos"])
                read_num = int(message_dict["page_view_num"])
                like_num = int(message_dict["page_like_num"])
                post_time = int(message_dict["post_time"])

                if int(is_new_crawled_article) == 0:
                    query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d WHERE `mongo_obj_id`='%s'" % (
                        weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, mongo_obj_id))
                    print(query)
                    # input("..................update")
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        # print(e.)
                        pass
                    finally:
                        pass

                elif int(is_new_crawled_article) == 1:
                    query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
                        mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
                    print(query)
                    # input("..................input")
                    try:
                        target_mysql_tool.cursor.execute(query)
                        target_mysql_tool.cnx.commit()
                    except Exception as e:
                        # print(e.)
                        pass
                    finally:
                        pass
                # source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
                #     {'_id': mongo_object_id})

                settled_count += 1
                # if settled_count == 5:
                #     break
                print('settled %d document' % settled_count)
        except Exception as e:
            pass
        finally:
            source_mongo_tool.disconnection()
            target_file_tool_1.close_file()
            target_file_tool_2.close_file()
            target_mysql_tool.disconnection()
            print("game over..............................mongo")

        return break_flag

    def weixin_article_kafka_mongo_control(self):
        kafka_crash_count = -1
        mongo_break_flag = 0
        kafka_crash_flag = 0
        while True:
            try:
                kafka_crash_count += 1

                print("Welcome to kafka. Work Start! It's your %d times" %
                      kafka_crash_count)
                input("...............1")
                if kafka_crash_count == 0:
                    input("..................6")
                    kafka_crash_flag = self.weixin_article_csv_consumer_kafka(
                        WOM_DEV_MYSQL_TUPLE, KAFKA)
                    input("..................7kaf ")
                    mongo_break_flag == 0
                input("..................2")
                if mongo_break_flag == 1:
                    kafka_crash_flag = self.weixin_article_csv_consumer_kafka(
                        WOM_DEV_MYSQL_TUPLE, KAFKA)
                    mongo_break_flag == 0
                input("..................3")
                if kafka_crash_flag == 1:
                    mongo_break_flag = self.weixin_article_csv_consumer_mongo(
                        WOM_DEV_MYSQL_TUPLE)
                    kafka_crash_flag = 0
                input("..................4")
                if kafka_crash_count == 2:
                    break
                input("...............5")
            except Exception as e:
                print("process_control crash...............")
                pass
            finally:
                pass

    def conventional_grow_trend_info_object_id_to_kafka(self,source_mongo_args,topic_numbers):
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        documents=source_mongo_tool.database["kafka_conventional_grow_trend_info"].find({"consumed":{"$exists":False}},{"_id":1}).limit(250000).batch_size(3000)
        client = KafkaClient(
            hosts="server-32:9092, server-32:9093, server-32:9094")
        topic_names=[b"grow-trend-info-1-uniwow",b"grow-trend-info-2-uniwow",b"grow-trend-info-3-uniwow",b"grow-trend-info-4-uniwow",b"grow-trend-info-5-uniwow",b"grow-trend-info-6-uniwow",b"grow-trend-info-7-uniwow",b"grow-trend-info-8-uniwow",b"grow-trend-info-9-uniwow",b"grow-trend-info-10-uniwow",b"grow-trend-info-11-uniwow",b"grow-trend-info-12-uniwow"]
        productors_in_use=[]
        for topic_name in topic_names[:topic_numbers]:
            productors_in_use.append(client.topics[topic_name].get_producer())
        for productor in productors_in_use:
            productor.start()
        count=0
        for document in documents:
            print("...........................")
            object_id_dict={}
            object_id_dict["_id"]=str(document["_id"])
            productor_in_use=random.choice(productors_in_use)
            productor_in_use.produce(json.dumps(object_id_dict).encode("utf-8","ignore"))
            source_mongo_tool.database["kafka_conventional_grow_trend_info"].update({"_id":document["_id"]},{"$set":{"consumed":1}})
            count+=1
            print("settled %d items" % count)
        for productor in productors_in_use:
            productor.stop()

    def media_weixin_info_object_id_kafka(self,source_mongo_args,topic_numbers):
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        documents=source_mongo_tool.database["test-formal"].find({"consumed":{"$exists":False}}).limit(100000)
        client = KafkaClient(
            hosts="server-32:9092, server-32:9093, server-32:9094")
        topic_names=[b"media-weixin-info-1",b"media-weixin-info-2",b"media-weixin-info-3",b"media-weixin-info-4",b"media-weixin-info-5",b"media-weixin-info-6"]
        productors_in_use=[]
        for topic_name in topic_names[:topic_numbers]:
            productors_in_use.append(client.topics[topic_name].get_producer())
        for productor in productors_in_use:
            productor.start()
        count=0
        for document in documents:
            object_id_dict={}
            object_id_dict["_id"]=str(document["_id"])
            productor_in_use=random.choice(productors_in_use)
            productor_in_use.produce(json.dumps(object_id_dict).encode("utf-8","ignore"))
            source_mongo_tool.database["test-formal"].update({"_id":document["_id"]},{"$set":{"consumed":1}})
            count+=1
            print("settled %d items" % count)
        for productor in productors_in_use:
            productor.stop()


    def weixin_article_consumer_from_grow_trend_info(self,kafka_args,source_mongo_args,target_mysql_args):
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_INDEX_COMPUTE_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        source_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        
        es_log_file = self.get_file_tool(
            *(CHECK_CSV_PATH, 'es_error_log.csv', 'ab'))
        es = Elasticsearch(
            hosts="http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.156:9200/")

        kafka_client = KafkaClient(
            hosts=kafka_args["hosts"])
        topic_in_use=kafka_client.topics[kafka_args["topics"][0]]
        consumer = topic_in_use.get_balanced_consumer(
            consumer_group=b"uni-1", auto_commit_enable=False, zookeeper_connect=kafka_args["zookeepers"])
        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        """
        消费mongo中的文章
        """
        for message in consumer:
            object_id_dict=json.loads(message.value.decode("utf-8","ignore"))
            document=source_mongo_tool.database["kafka_conventional_grow_trend_info"].find_one({"_id":bson.ObjectId(object_id_dict["_id"])})
            if document is None:
                print("consumerd...............................")
                consumer.commit_offsets()
                continue
            message_dict = json.loads(
                document['kafka_message'].decode("utf-8", 'ignore'))
            print(message_dict)
            # input("........................")
            object_id_grow_trend_info=message_dict["mongo_object_id"]
            target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
            self.grow_trend_info_message_handle(es_log_file,str(object_id_grow_trend_info),message_dict,target_file_tool_1,target_file_tool_2,source_mysql_tool,target_mysql_tool,es)
            target_mysql_tool.disconnection()
            consumer.commit_offsets()
            source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
                    {'_id': bson.ObjectId(object_id_dict["_id"])})
        
        
    def weixin_article_consumer_from_kafka(self,kafka_args,target_mysql_args):
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_INDEX_COMPUTE_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        source_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        es_log_file = self.get_file_tool(
            *(CHECK_CSV_PATH, 'es_error_log.csv', 'ab'))
        es = Elasticsearch(
            hosts="http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.156:9200/")
        kafka_client = KafkaClient(
            hosts=kafka_args["hosts"])
        article_info_crawl_topic = kafka_client.topics[kafka_args["topics"][0]]
        consumer = article_info_crawl_topic.get_balanced_consumer(
            consumer_group=b"uniwow", auto_commit_enable=False, zookeeper_connect=ZOOKEEPER)
        message_count = 0
        for message in consumer:
            # print(
            #     "kafka_args................................................................")
            if message is not None:
                message_count += 1
                # print(message.offset, message.value.decode(
                #     "utf-8", 'ignore'))
                message_dict = json.loads(
                    message.value.decode("utf-8", 'ignore'))
                self.grow_trend_info_message_handle(es_log_file,str(message_dict["mongo_object_id"]),message_dict,target_file_tool_1,target_file_tool_2,source_mysql_tool,target_mysql_tool,es)
                consumer.commit_offsets()

    def grow_trend_info_message_handle(self,es_log_file,object_id_grow_trend_info,message_dict,target_file_tool_1,target_file_tool_2,source_mysql_tool,target_mysql_tool,es):
        index_compute_item_list = []
        for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
            index_compute_item_list.append(
                strip_and_replace_all(str(message_dict[k])))
        try:
            fcntl.flock(target_file_tool_1.f, fcntl.LOCK_EX)
            target_file_tool_1.write_database_line_to_file(
                ",".join(index_compute_item_list) + "\n")
            fcntl.flock(target_file_tool_1.f, fcntl.LOCK_UN)
        except Exception as e:
            fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
            es_log_file.write_database_line_to_file("write to index compute error: %s \n" % str(object_id_grow_trend_info) )
            fcntl.flock(es_log_file.f, fcntl.LOCK_UN)
        finally:
            pass

        machine_learning_item_list = []
        for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
            machine_learning_item_list.append(
                strip_and_replace_all(str(message_dict[k])))
        try:
            fcntl.flock(target_file_tool_2.f, fcntl.LOCK_EX)
            target_file_tool_2.write_database_line_to_file(
                "*|233|*|".join(machine_learning_item_list) + "\n")
            fcntl.flock(target_file_tool_2.f, fcntl.LOCK_UN)
        except Exception as e:
            fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
            es_log_file.write_database_line_to_file("write to mechine learning error: %s \n" % str(object_id_grow_trend_info) )
            fcntl.flock(es_log_file.f, fcntl.LOCK_UN)
        finally:
            pass
        
        is_new_crawled_article = message_dict[
            "is_new_crawled_article"]
        print("is_new_crawled_article type %s -> %s" %
              (type(is_new_crawled_article), str(is_new_crawled_article)))
        
        mongo_obj_id = strip_and_replace_all(
            strip_and_replace(str(message_dict["mongo_object_id"])))
        weixin_id = strip_and_replace_all(
            strip_and_replace(str(message_dict["weixin_id"])))
        title = strip_and_replace_all(
            strip_and_replace(str(message_dict["title"])))
        article_url = strip_and_replace_all(
            strip_and_replace(str(message_dict["fixed_url"])))
        short_desc = strip_and_replace_all(
            strip_and_replace(str(message_dict["digest"])))
        article_type = int(message_dict["article_type"])
        article_pos = int(message_dict["article_pos"])
        read_num = -1
        like_num = -1
        if message_dict["page_view_num"] == "--" or message_dict["page_like_num"] == "--":
            read_num = -1
            like_num = -1
        else:
            read_num = int(message_dict["page_view_num"])
            like_num = int(message_dict["page_like_num"])
        post_time = int(message_dict["post_time"])
        content = strip_and_replace_all(message_dict["content"])

        if "is_origin" in message_dict:
            is_origin = int(message_dict["is_origin"])
        else:
            is_origin = -1
        if "cover" in message_dict:
            cover_img = strip_and_replace_all(message_dict["cover"])
        else:
            cover_img = ''
        print("------------------------"+str(like_num))
        if read_num == -1 or like_num == -1:
            article_influence_index = -1
        else:
            article_influence_index = 0.7 * \
                math.log(read_num + 1) + 0.3 * math.log(like_num + 1)
        # query = (
        #     "SELECT `m_wmi` FROM `media_weixin` WHERE `real_public_id`='%s'" % weixin_id)
        # source_mysql_tool.cursor.execute(query)
        # row = source_mysql_tool.cursor.fetchone()
        # if row is not None:
        #     wom_index, = row
        # else:
        #     wom_index = 0
        # article_posted_time = int(
        #     (int(time.time()) - post_time) / (60 * 60))
        # article_hot_index = math.pow(0.5 * math.log(wom_index + 1) + 0.3 * math.log(
        #     read_num + 1) + 0.2 * math.log(like_num + 1), 2) - math.pow(math.log(article_posted_time + 1), 2)
        query = (
            "SELECT COUNT(*) FROM `weixin_article` WHERE `mongo_obj_id`='%s'" % mongo_obj_id)
        print(query)
        target_mysql_tool.cursor.execute(query)
        row = target_mysql_tool.cursor.fetchone()
        (target_row_exist_flag,) = row
        
        if int(target_row_exist_flag) == 1:
            query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d,`content`='%s',`is_origin`=%d,`cover_img`='%s',`article_influence_index`=%.2f WHERE `mongo_obj_id`='%s'" % (
                weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index, mongo_obj_id))
            print(query)
            # input("..................update")
            try:
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
            except Exception as e:
                fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
                es_log_file.write_database_line_to_file("update to weixin article error: %s \n" % str(object_id_grow_trend_info) )
                fcntl.flock(es_log_file.f, fcntl.LOCK_UN)
            finally:
                pass

        elif int(target_row_exist_flag) == 0:
            # print(content)
            query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`,`content`,`is_origin`,`cover_img`,`article_influence_index`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,'%s',%d,'%s',%.2f)" % (
                mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, content, is_origin, cover_img, article_influence_index))
            print(query)
            
            try:
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()
            except Exception as e:
                fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
                es_log_file.write_database_line_to_file("insert to weixin article error: %s \n" % str(object_id_grow_trend_info) )
                fcntl.flock(es_log_file.f, fcntl.LOCK_UN)

            finally:
                pass

            if weixin_id != "":
                try:
                    es_result = es.index(index=ES_INDEX_NAME,
                                         doc_type=ES_ARTICLE_TYPE,
                                         id=mongo_obj_id,
                                         body={
                                             "public_id": weixin_id,
                                             "public_name": str(message_dict["weixin_name"]),
                                             "mongo_object_id": mongo_obj_id,
                                             "title": title,
                                             "content": content,
                                             "digest": short_desc,
                                             "article_pos": article_pos,
                                             "is_origin": is_origin,
                                             "post_date": datetime_to_str(current_stamp_to_datetime(post_time)),
                                         },
                                         routing=weixin_id)
                except Exception as e:
                    fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
                    es_log_file.write_database_line_to_file("update to es error: %s \n" % str(object_id_grow_trend_info))
                    fcntl.flock(es_log_file.f, fcntl.LOCK_UN)
            else:
                fcntl.flock(es_log_file.f, fcntl.LOCK_EX)
                es_log_file.write_database_line_to_file("weixin id not exists error: %s \n" % str(object_id_grow_trend_info))
                fcntl.flock(es_log_file.f, fcntl.LOCK_UN)


        
    def weixin_article_consumer_from_grow_trend_info_random(self,groups_num,group_id,source_mongo_args,target_mysql_args):
        current_day = datetime_to_str(datetime.datetime.now())
        target_file = "article_%s.csv" % current_day
        target_file_args_1 = (BACKUP_INDEX_COMPUTE_PATH, target_file, "ab")
        target_file_args_2 = (BACKUP_MACHINE_LEARNING_PATH, target_file, "ab")
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        source_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        
        es_log_file = self.get_file_tool(
            *(CHECK_CSV_PATH, 'es_error_log.csv', 'ab'))
        es = Elasticsearch(
            hosts="http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.156:9200/")

        source_mongo_tool=self.get_mongo_tool(*source_mongo_args)
        """
        消费mongo中的文章
        """
        documents = source_mongo_tool.database["kafka_conventional_grow_trend_info"].find({},{"_id":1}).limit(10000).batch_size(10000)

        for document in documents:
            inc = str(document["_id"])[:8]
            inc = int('0x'+inc,16)
            if inc % groups_num !=  group_id:
                print("consumerd...............................%d" % (inc % groups_num))
                continue
            document=source_mongo_tool.database["kafka_conventional_grow_trend_info"].find_one({"_id":document["_id"]})
            if document is None:
                print("consumerd...............................")
                continue
            message_dict = json.loads(
                document['kafka_message'].decode("utf-8", 'ignore'))
            print(message_dict)
            # input("........................")
            object_id_grow_trend_info=message_dict["mongo_object_id"]
            target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
            self.grow_trend_info_message_handle(es_log_file,str(object_id_grow_trend_info),message_dict,target_file_tool_1,target_file_tool_2,source_mysql_tool,target_mysql_tool,es)
            target_mysql_tool.disconnection()
            source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
                    {'_id': document["_id"]})
        source_mongo_tool.disconnection()
        target_file_tool_1.close_file()
        target_file_tool_2.close_file()



