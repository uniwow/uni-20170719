# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.tool_string import strip_and_replace_all
from tools.settings import WOM_PROD_1_TUPLE, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO, BACKUP_JSON_PATH, CHECK_CSV_PATH, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_225_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_32_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_249_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_70_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_102_MONGO_TUPLE, CHECK_SQL_PATH, CITY, CATEGORY
from tools.tool_time import datetime_one_month_ago, datetime_yesterday_to_str, current_stamp_to_datetime, str_to_time_stamp, current_datetime_to_str, current_stamp_to_time, datetime_yesterday_to_str_upgrade, current_time_to_stamp
import datetime
import os
import random
import re
from tqdm import tqdm

class MediaWeiXinCheck(object):
    """检查数据错误及其冗余"""

    def __init__(self):
        pass

    def get_mysql_tool(self, host, db, user, pwd):
        mysql_tool = MysqlTool(host, db, user, pwd)
        return mysql_tool

    def get_mongo_tool(self, host, port, db, user, pwd):
        mongo_tool = MongoTool(host, port, db, user, pwd)
        return mongo_tool

    def get_file_tool(self, file_path, file_name, open_status):
        file_tool = FileTool(file_path, file_name, open_status)
        return file_tool

    def get_target_file_tool_list(self, target_file_args_list):
        '''构造结果文件并返回'''
        target_file_tool_1 = None
        target_file_tool_2 = None
        target_file_tool_3 = None
        file_pos = 0
        for target_file_args in target_file_args_list:
            file_pos += 1
            if file_pos == 1:
                if target_file_args[0] == 1:
                    target_file_tool_1 = self.get_file_tool(
                        *target_file_args[1:])
            elif file_pos == 2:
                if target_file_args[0] == 1:
                    target_file_tool_2 = self.get_file_tool(
                        *target_file_args[1:])
            elif file_pos == 3:
                if target_file_args[0] == 1:
                    target_file_tool_3 = self.get_file_tool(
                        *target_file_args[1:])
        return (target_file_tool_1, target_file_tool_2, target_file_tool_3)

    def file_mongo(self, source_file_args, source_mongo_args, source_collection_args, target_file_args_list):
        '''
           file和mongo数据比较
           target_file_1:既在file又在mongo
           target_file_2:在file不在mongo
           target_file_3:在mongo不在file
           source_file_args=['file_path','file_name','open_status']
           target_file_args=[[1,'file_path','file_name','open_status'],[0,],[0,]]
           1表示目标file存在，0表示目标file不存在
        '''
        source_file_tool = self.get_file_tool(*source_file_args)
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_file = source_file_tool.get_file()
        source_file_list_lower = []
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        unique_id_list_mongo_lower = []
        for unique_id_mongo in unique_id_list_mongo:
            unique_id_list_mongo_lower.append(unique_id_mongo.lower())
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for line in source_file:
            '''如果既在file又在mongo'''
            source_file_list_lower.append(
                source_file_tool.line_strip_decode_ignore(line).lower())
            if source_file_tool.line_strip_decode_ignore(line).lower() in unique_id_list_mongo_lower:
                if target_file_tool_1 is not None:
                    if source_file_tool.line_strip_decode_ignore(line) is not u'':

                        target_file_tool_1.write_file_line_to_file(
                            line.strip())
                        count_target_file_1 += 1
                        print(('既在file又在mongo中的数据：%d ' % count_target_file_1) +
                              source_file_tool.line_strip_decode_ignore(line))
            else:
                if target_file_tool_2 is not None:
                    if source_file_tool.line_strip_decode_ignore(line) is not u'':
                        target_file_tool_2.write_file_line_to_file(
                            line.strip())
                        count_target_file_2 += 1
                        print(('在file但不在mongo中的数据：%d ' % count_target_file_2) +
                              source_file_tool.line_strip_decode_ignore(line))
        if target_file_tool_3 is not None:
            for unique_id_mongo in unique_id_list_mongo:
                '''存在于mongo但不存在于file中'''
                if unique_id_mongo.lower() not in source_file_list_lower:
                    if unique_id_mongo is not '':
                        count_target_file_3 += 1
                        target_file_tool_1.write_database_line_to_file(
                            unique_id_mongo.strip())
                        print(('在mongo但不在file中的数据：%d ' % count_target_file_3) +
                              unique_id_mongo)

        print(('既在file又在mongo中的数据总数为：%d ' % count_target_file_1))
        print(('在file但不在mongo中的数据总数为：%d ' % count_target_file_2))
        print(('在mongo但不在file中的数据总数为：%d ' % count_target_file_3))

    def file_mysql(self, source_file_args, source_mysql_args, source_table_args, target_file_args_list):
        '''
           file和mysql数据比较
           target_file_1:既在file又在mysql
           target_file_2:在file不在mysql
           target_file_3:在mysql不在file
           source_file_args=['file_path','file_name','open_status']
           target_file_args=[[1,'file_path','file_name','open_status'],[0,],[0,]]
           1表示目标file存在，0表示目标file不存在
        '''
        source_file_tool = self.get_file_tool(*source_file_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_file = source_file_tool.get_file()
        source_file_list_lower = []
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        # print(str(type(unique_id_list_mysql[0])))

        unique_id_list_mysql_lower = []
        for unique_id_mysql in unique_id_list_mysql:
            unique_id_list_mysql_lower.append(unique_id_mysql.lower())
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for line in source_file:
            '''如果既在file又在mysql'''
            # print(str(type(line)))
            # print(str(type(source_file_tool.line_strip_decode_ignore(line))))
            # print(str(type(u'')))
            # input("...............")
            source_file_list_lower.append(
                source_file_tool.line_strip_decode_ignore(line).lower())

            if source_file_tool.line_strip_decode_ignore(line).lower() in unique_id_list_mysql_lower:
                if target_file_tool_1 is not None:
                    if source_file_tool.line_strip_decode_ignore(line) is not u'':
                        target_file_tool_1.write_file_line_to_file(
                            line.strip())
                        count_target_file_1 += 1
                        print(('既在file又在mysql中的数据：%d ' % count_target_file_1) +
                              source_file_tool.line_strip_decode_ignore(line))
            else:
                if target_file_tool_2 is not None:
                    if source_file_tool.line_strip_decode_ignore(line) is not u'':
                        target_file_tool_2.write_file_line_to_file(
                            line.strip())
                        count_target_file_2 += 1
                        print(('在file但不在mysql中的数据：%d ' % count_target_file_2) +
                              source_file_tool.line_strip_decode_ignore(line))
        if target_file_tool_3 is not None:
            for unique_id_mysql in unique_id_list_mysql:
                '''存在于mysql但不存在于file中'''
                if unique_id_mysql.lower() not in source_file_list_lower:
                    if unique_id_mysql is not '':
                        count_target_file_3 += 1
                        target_file_tool_1.write_database_line_to_file(
                            unique_id_mysql.strip())
                        print(('在mysql但不在file中的数据：%d ' % count_target_file_3) +
                              unique_id_mysql)

        print(('既在file又在mysql中的数据总数为：%d ' % count_target_file_1))
        print(('在file但不在mysql中的数据总数为：%d ' % count_target_file_2))
        print(('在mysql但不在file中的数据总数为：%d ' % count_target_file_3))

    def mongo_mysql(self, source_mongo_args, source_collection_args, source_mysql_args, source_table_args, target_file_args_list):
        '''
           mongo和mysql数据比较
           target_file_1:既在mongo又在mysql
           target_file_2:在mongo不在mysql
           target_file_3:在mysql不在mongo
           source_file_args=['file_path','file_name','open_status']
           target_file_args=[[1,'file_path','file_name','open_status'],[0,],[0,]]
           1表示目标file存在，0表示目标file不存在
        '''
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        unique_id_list_mysql_lower = []
        for unique_id_mysql in unique_id_list_mysql:
            unique_id_list_mysql_lower.append(unique_id_mysql.lower())
        unique_id_list_mongo_lower = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mongo in unique_id_list_mongo:
            '''如果既在mongo又在mysql'''
            unique_id_list_mongo_lower.append(
                unique_id_list_mongo.lower())

            if unique_id_mongo.lower() in unique_id_list_mysql_lower:
                if target_file_tool_1 is not None:
                    if unique_id_mongo is not '':
                        target_file_tool_1.write_database_line_to_file(
                            unique_id_mongo.strip())
                        count_target_file_1 += 1
                        print(('既在mongo又在mysql中的数据：%d ' % count_target_file_1) +
                              unique_id_mongo)
            else:
                if target_file_tool_2 is not None:
                    if unique_id_mongo is not '':
                        target_file_tool_2.write_database_line_to_file(
                            unique_id_mongo.strip())
                        count_target_file_2 += 1
                        print(('在mongo但不在mysql中的数据：%d ' % count_target_file_2) +
                              unique_id_mongo)
        if target_file_tool_3 is not None:
            for unique_id_mysql in unique_id_list_mysql:
                '''存在于mysql但不存在于mongo中'''
                if unique_id_mysql.lower() not in unique_id_list_mongo_lower:
                    if unique_id_mysql is not '':
                        target_file_tool_3.write_database_line_to_file(
                            unique_id_mysql.strip())
                        count_target_file_3 += 1
                        print(('在mysql但不在mongo中的数据：%d ' % count_target_file_3) +
                              unique_id_mysql)

        print(('既在mongo又在mysql中的数据总数为：%d' % count_target_file_1))
        print(('在mongo但不在mysql中的数据总数为：%d' % count_target_file_2))
        print(('在mysql但不在mongo中的数据总数为：%d' % count_target_file_3))

    def file_file(self, source_file_args_1, source_file_args_2, target_file_args_list):
        '''两个file内容相比较'''
        source_file_tool_1 = self.get_file_tool(*source_file_args_1)
        source_file_tool_2 = self.get_file_tool(*source_file_args_2)
        source_file_1 = source_file_tool_1.get_file()
        source_file_2 = source_file_tool_2.get_file()
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        source_file_1_list_lower = []
        source_file_2_list_lower = []
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for line in source_file_2:
            source_file_2_list_lower.append(
                source_file_tool_2.line_strip_decode_ignore(line).lower())
        for line in source_file_1:
            source_file_1_list_lower.append(
                source_file_tool_1.line_strip_decode_ignore(line).lower())
            if source_file_tool_1.line_strip_decode_ignore(line).lower() in source_file_2_list_lower:
                if source_file_tool_1.line_strip_decode_ignore(line) is not u'':
                    count_target_file_1 += 1
                    print(('既在file_1又在file_2中的数据 %d ' % count_target_file_1) +
                          source_file_tool_1.line_strip_decode_ignore(line))
                    if target_file_tool_1 is not None:
                        target_file_tool_1.write_file_line_to_file(line)
            else:
                if source_file_tool_1.line_strip_decode_ignore(line) is not u'':
                    count_target_file_2 += 1
                    print(('存在file_1但不在file_2的数据 %d ' % count_target_file_2) +
                          source_file_tool_1.line_strip_decode_ignore(line))
                    if target_file_tool_2 is not None:
                        target_file_tool_2.write_file_line_to_file(line)
        if target_file_tool_3 is not None:
            for line in source_file_2:
                if source_file_tool_2.line_strip_decode_ignore(line).lower() not in source_file_1_list_lower:
                    if source_file_tool_2.line_strip_decode_ignore(line) is not u'':
                        count_target_file_3 += 1
                        print(('存在file_2但不在file_1的数据 %d ' % count_target_file_3) +
                              source_file_tool_2.line_strip_decode_ignore(line))
                        target_file_tool_3.write_file_line_to_file(line)

    def mysql_mysql(self, source_mysql_args_1, source_table_args_1, source_mysql_args_2, source_table_args_2, target_file_args_list):
        '''两个mysql表比较'''
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        unique_id_list_mysql_1 = source_mysql_tool_1.get_unique_id_list(
            *source_table_args_1)
        unique_id_list_mysql_2 = source_mysql_tool_2.get_unique_id_list(
            *source_table_args_2)
        unique_id_list_mysql_lower_1 = []
        unique_id_list_mysql_lower_2 = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mysql in unique_id_list_mysql_2:
            unique_id_list_mysql_lower_2.append(unique_id_mysql.lower())
        for unique_id_mysql in unique_id_list_mysql_1:
            '''如果既在mysql_1又在mysql_2'''
            unique_id_list_mysql_lower_1.append(
                unique_id_list_mongo.lower())

            if unique_id_mysql.lower() in unique_id_list_mysql_lower_2:
                if unique_id_mongo is not '':
                    count_target_file_1 += 1
                    print(('既在mysql_1又在mysql_2中的数据：%d ' % count_target_file_1) +
                          unique_id_mongo)
                    if target_file_tool_1 is not None:
                        target_file_tool_1.write_database_line_to_file(
                            unique_id_mysql.strip())

            else:
                if unique_id_mongo is not '':
                    count_target_file_2 += 1
                    print(('在mongo但不在mysql中的数据：%d ' % count_target_file_2) +
                          unique_id_mysql)
                    if target_file_tool_2 is not None:
                        target_file_tool_2.write_database_line_to_file(
                            unique_id_mysql.strip())

        if target_file_tool_3 is not None:
            for unique_id_mysql in unique_id_list_mysql_2:
                '''存在于mysql_2但不存在于mysql_1中'''
                if unique_id_mysql.lower() not in unique_id_list_mysql_lower_1:
                    if unique_id_mysql is not '':
                        count_target_file_3 += 1
                        print(('在mysql但不在mongo中的数据：%d ' % count_target_file_3) +
                              unique_id_mysql)
                        target_file_tool_3.write_database_line_to_file(
                            unique_id_mysql.strip())

        print(('既在mysql_1又在mysql_2中的数据总数为：%d' % count_target_file_1))
        print(('在mysql_1但不在mysql_2中的数据总数为：%d' % count_target_file_2))
        print(('在mysql_2但不在mysql_1中的数据总数为：%d' % count_target_file_3))

    def mongo_mongo(self, source_mongo_args_1, source_collection_args_1, source_mongo_args_2, source_collection_args_2, target_file_args_list):
        '''两个mongo表比较'''
        source_mongo_tool_1 = self.get_mongo_tool(*source_mongo_args_1)
        source_mongo_tool_2 = self.get_mongo_tool(*source_mongo_args_2)
        unique_id_list_mongo_1 = source_mongo_tool_1.get_unique_id_list(
            *source_collection_args_1)
        unique_id_list_mongo_2 = source_mongo_tool_2.get_unique_id_list(
            *source_collection_args_2)
        unique_id_list_mongo_lower_1 = []
        unique_id_list_mongo_lower_2 = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mongo in unique_id_list_mongo_2:
            unique_id_list_mongo_lower_2.append(unique_id_mongo.lower())
        for unique_id_mongo in unique_id_list_mongo_1:
            '''如果既在mongo_1又在mongo_2'''
            unique_id_list_mongo_lower_1.append(
                unique_id_list_mongo.lower())

            if unique_id_mongo.lower() in unique_id_list_mongo_lower_2:
                if unique_id_mongo is not '':
                    count_target_file_1 += 1
                    print(('既在mongo_1又在mongo_2中的数据：%d ' % count_target_file_1) +
                          unique_id_mongo)
                    if target_file_tool_1 is not None:
                        target_file_tool_1.write_database_line_to_file(
                            unique_id_mongo.strip())

            else:
                if unique_id_mongo is not '':
                    count_target_file_2 += 1
                    print(('在mongo但不在mongo中的数据：%d ' % count_target_file_2) +
                          unique_id_mongo)
                    if target_file_tool_2 is not None:
                        target_file_tool_2.write_database_line_to_file(
                            unique_id_mongo.strip())

        if target_file_tool_3 is not None:
            for unique_id_mongo in unique_id_list_mongo_2:
                '''存在于mongo_2但不存在于mongo_1中'''
                if unique_id_mongo.lower() not in unique_id_list_mongo_lower_1:
                    if unique_id_mongo is not '':
                        count_target_file_3 += 1
                        print(('在mongo但不在mongo中的数据：%d ' % count_target_file_3) +
                              unique_id_mongo)
                        target_file_tool_3.write_database_line_to_file(
                            unique_id_mongo.strip())

        print(('既在mongo_1又在mongo_2中的数据总数为：%d' % count_target_file_1))
        print(('在mongo_1但不在mongo_2中的数据总数为：%d' % count_target_file_2))
        print(('在mongo_2但不在mongo_1中的数据总数为：%d' % count_target_file_3))

    def file_duplicate(self, source_file_args, target_file_args_list):
        '''检查文件中的重复行'''
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        source_file_list_lower = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for line in source_file:
            '''如果行不存在小写列表中，则加入'''
            if source_file_tool.line_strip_decode_ignore(line).lower() not in source_file_list_lower:
                count_target_file_1 += 1
                source_file_list_lower.append(
                    source_file_tool.line_strip_decode_ignore(line).lower())

                print(('新增一条唯一数据 %d ' % count_target_file_1) +
                      source_file_tool.line_strip_decode_ignore(line).lower())
                if target_file_tool_1 is not None:
                    target_file_tool_1.write_file_line_to_file(line)
            else:
                count_target_file_2 += 1
                print(('新增一条冗余数据 %d ' % count_target_file_2) +
                      source_file_tool.line_strip_decode_ignore(line).lower())
                if target_file_tool_2 is not None:
                    target_file_tool_2.write_file_line_to_file(line)
        print('数据总数为 %d ' % count_target_file_1)
        print('冗余数据数为 %d ' % count_target_file_2)

    def mongo_duplicate(self, source_mongo_args, source_collection_args, target_file_args_list):
        '''检查mongo表中的重复数据'''
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        unique_id_list_mongo_lower = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mongo in unique_id_list_mongo:
            if unique_id_mongo.lower() not in unique_id_list_mongo_lower:
                count_target_file_1 += 1
                unique_id_list_mongo_lower.append(unique_id_mongo.lower())
                print(('新增一条唯一数据 %d ' % count_target_file_1) + unique_id_mongo)
                if target_file_tool_1 is not None:
                    target_file_tool_1.write_database_line_to_file(
                        unique_id_mongo)

            else:
                count_target_file_2 += 1
                print(('新增一条冗余数据 %d ' % count_target_file_2) + unique_id_mongo)
                if target_file_tool_2 is not None:
                    target_file_tool_2.write_database_line_to_file(
                        unique_id_mongo)
        print('数据总数为 %d ' % count_target_file_1)
        print('冗余数据数为 %d ' % count_target_file_2)

    def mysql_duplicate(self, source_mysql_args, source_table_args, target_file_args_list):
        '''检查mysql表中的重复数据'''
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        unique_id_list_mysql_lower = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mysql in unique_id_list_mysql:
            if unique_id_mysql.lower() not in unique_id_list_mysql_lower:
                count_target_file_1 += 1
                unique_id_list_mysql_lower.append(unique_id_mysql.lower())
                print(('新增一条唯一数据 %d ' % count_target_file_1) + unique_id_mysql)
                if target_file_tool_1 is not None:
                    target_file_tool_1.write_database_line_to_file(
                        unique_id_mysql)

            else:
                count_target_file_2 += 1
                print(('新增一条冗余数据 %d ' % count_target_file_2) + unique_id_mysql)
                if target_file_tool_2 is not None:
                    target_file_tool_2.write_database_line_to_file(
                        unique_id_mysql)
        print('数据总数为 %d ' % count_target_file_1)
        print('冗余数据数为 %d ' % count_target_file_2)

    def media_weixin_article_count(self, source_mongo_args, source_file_args, target_file_args_list):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0

        for line in source_file:
            count = source_mongo_tool.database['media_weixin_article'].find(
                {"weixin_id": {'$regex': source_file_tool.line_strip_decode_ignore(line), '$options': 'i'}}).count()
            print('%d' % count)
            # input('...............................')
            target_file_tool_1.write_database_line_to_file(
                source_file_tool.line_strip_decode_ignore(line) + (',%d' % count))

    def media_weixin_1_price_1_duplicate(self, source_mysql_args, target_file_args_list):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        query = ("SELECT `sOpenName` FROM `t_media` WHERE  `sOpenName`!='' AND `iMediaType` = 1  AND (`iPrice1` != 0  OR `iPrice2` != 0  OR `iPrice3` != 0  OR `iPrice4` != 0)  AND `iPut` = 1")
        source_mysql_tool.cursor.execute(query)
        unique_id_list_mysql = []
        for row in source_mysql_tool.cursor:
            (unique_id_mysql,) = row
            unique_id_list_mysql.append(unique_id_mysql)
        unique_id_list_mysql_lower = []
        target_file_tool_1, target_file_tool_2, target_file_tool_3 = self.get_target_file_tool_list(
            target_file_args_list)
        count_target_file_1 = 0
        count_target_file_2 = 0
        count_target_file_3 = 0
        for unique_id_mysql in unique_id_list_mysql:
            if unique_id_mysql.lower() not in unique_id_list_mysql_lower:
                count_target_file_1 += 1
                unique_id_list_mysql_lower.append(unique_id_mysql.lower())
                print(('新增一条唯一数据 %d ' % count_target_file_1) + unique_id_mysql)
                if target_file_tool_1 is not None:
                    target_file_tool_1.write_database_line_to_file(
                        unique_id_mysql)
            else:
                count_target_file_2 += 1
                print(('新增一条冗余数据 %d ' % count_target_file_2) + unique_id_mysql)
                if target_file_tool_2 is not None:
                    target_file_tool_2.write_database_line_to_file(
                        unique_id_mysql)
        print('数据总数为 %d ' % count_target_file_1)
        print('冗余数据数为 %d ' % count_target_file_2)

    def unique_id_duplicate(self, source_mysql_args, source_table_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list(
            *source_table_args)
        count_outer = 0
        find_count = 0
        for unique_id_mysql_outer in unique_id_list_mysql:
            count_outer += 1
            if unique_id_mysql_outer.strip() in " ":
                continue
            print(unique_id_mysql_outer)
            count_inner = 0
            for unique_id_mysql_inner in unique_id_list_mysql[count_outer:]:
                count_inner += 1
                if unique_id_mysql_inner.strip() in " ":
                    continue
                print(unique_id_mysql_inner)
                #print(str(unique_id_mysql_outer.lower() in unique_id_mysql_inner.lower()))
                # input(".......")
                if unique_id_mysql_outer.lower() in unique_id_mysql_inner.lower():
                    print("%s:%s" % (unique_id_mysql_outer.lower(),
                                     unique_id_mysql_inner.lower()))
                    print(str(unique_id_mysql_outer.lower()
                              in unique_id_mysql_inner.lower()))
                    target_file_tool.write_database_line_to_file(
                        unique_id_mysql_outer)
                    find_count += 1
                    print('find one %d : %s' %
                          (find_count, unique_id_mysql_inner))

                if unique_id_mysql_inner.lower() in unique_id_mysql_outer.lower():
                    target_file_tool.write_database_line_to_file(
                        unique_id_mysql_inner)
                    find_count += 1
                    print('find one %d :%s' %
                          (find_count, unique_id_mysql_outer))
                print("outer:%d;inter:%d" % (count_outer, count_inner))

    def unique_id_duplicate_further(self, source_mysql_args, source_file_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_file_tool = self.get_file_tool(*target_file_args)
        for line in source_file:
            query = ("SELECT `public_name`,`public_id` FROM `media_weixin` WHERE LOWER(`public_id`) LIKE '%%%s%%'" %
                     source_file_tool.line_strip_decode_ignore(line).lower())
            source_mysql_tool.cursor.execute(query)

            public_name_id_list = []
            for row in source_mysql_tool.cursor:
                (public_name, public_id) = row
                public_name_id_list.append((public_name, public_id))
            print('public_name_id_list length:%d' % len(public_name_id_list))
            if len(public_name_id_list) >= 2:
                count_outer = 0
                for public_name_id_outer in public_name_id_list:
                    count_outer += 1
                    count_inner = 0
                    public_id_to_write = public_name_id_outer[1]
                    for public_name_id_inner in public_name_id_list[count_outer:]:
                        count_inner += 1
                        if (public_name_id_outer[1] in public_name_id_inner[1]) and ((public_name_id_outer[0] in public_name_id_inner[0]) or (public_name_id_inner[0] in public_name_id_outer[0])):

                            public_id_to_write = public_id_to_write + \
                                "," + public_name_id_inner[1] + ","

                        if (public_name_id_inner[1] in public_name_id_outer[1]) and ((public_name_id_outer[0] in public_name_id_inner[0]) or (public_name_id_inner[0] in public_name_id_outer[0])):

                            public_id_to_write = public_id_to_write + \
                                "," + public_name_id_inner[1] + ","
                        target_file_tool.write_database_line_to_file(
                            public_id_to_write)
                        print("outer:%d;inner:%d \t public_name_id_outer:%s;public_name_id_inner:%s" % (
                            count_outer, count_inner, str(public_name_id_outer), str(public_name_id_inner)))

    def media_weixin_post_time_range(self, source_mongo_args, source_collection_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        target_file = target_file_tool.get_file()
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        for unique_id_mongo in unique_id_list_mongo[:100]:
            source_document = source_mongo_tool.find_one(
                "media_weixin", {"public_id": unique_id_mongo}, ["post_time_law", ])
            post_time_list = source_document["post_time_law"]
            min_post_time = 86400
            max_post_time = 0
            post_time_bar_list = [3600 * 1, 3600 * 2, 3600 * 3, 3600 * 4, 3600 * 5, 3600 * 6, 3600 * 7, 3600 * 8, 3600 * 9, 3600 * 10, 3600 * 11,
                                  3600 * 12, 3600 * 13, 3600 * 14, 3600 * 15, 3600 * 16, 3600 * 17, 3600 * 18, 3600 * 19, 3600 * 20, 3600 * 21, 3600 * 22, 3600 * 23]
            post_time_count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            post_time_count_joint = ""

            for post_time in post_time_list:

                print(str(type(post_time)))
                date_time = current_stamp_to_datetime(int(post_time))
                date_time_hour = date_time.hour * 3600
                date_time_minute = date_time.minute * 60
                date_time_second = date_time.second
                date_time_to_second = date_time_hour + date_time_minute + date_time_second
                if date_time_to_second < min_post_time:
                    min_post_time = date_time_to_second
                if date_time_to_second > max_post_time:
                    max_post_time = date_time_to_second
                if date_time_to_second <= post_time_bar_list[0]:
                    post_time_count_list[0] += 1
                elif date_time_to_second > post_time_bar_list[0] and date_time_to_second <= post_time_bar_list[1]:
                    post_time_count_list[1] += 1
                elif date_time_to_second > post_time_bar_list[1] and date_time_to_second <= post_time_bar_list[2]:
                    post_time_count_list[2] += 1
                elif date_time_to_second > post_time_bar_list[2] and date_time_to_second <= post_time_bar_list[3]:
                    post_time_count_list[3] += 1
                elif date_time_to_second > post_time_bar_list[3] and date_time_to_second <= post_time_bar_list[4]:
                    post_time_count_list[4] += 1
                elif date_time_to_second > post_time_bar_list[4] and date_time_to_second <= post_time_bar_list[5]:
                    post_time_count_list[5] += 1
                elif date_time_to_second > post_time_bar_list[5] and date_time_to_second <= post_time_bar_list[6]:
                    post_time_count_list[6] += 1
                elif date_time_to_second > post_time_bar_list[6] and date_time_to_second <= post_time_bar_list[7]:
                    post_time_count_list[7] += 1
                elif date_time_to_second > post_time_bar_list[7] and date_time_to_second <= post_time_bar_list[8]:
                    post_time_count_list[8] += 1
                elif date_time_to_second > post_time_bar_list[8] and date_time_to_second <= post_time_bar_list[9]:
                    post_time_count_list[9] += 1
                elif date_time_to_second > post_time_bar_list[9] and date_time_to_second <= post_time_bar_list[10]:
                    post_time_count_list[10] += 1
                elif date_time_to_second > post_time_bar_list[10] and date_time_to_second <= post_time_bar_list[11]:
                    post_time_count_list[11] += 1
                elif date_time_to_second > post_time_bar_list[11] and date_time_to_second <= post_time_bar_list[12]:
                    post_time_count_list[12] += 1
                elif date_time_to_second > post_time_bar_list[12] and date_time_to_second <= post_time_bar_list[13]:
                    post_time_count_list[13] += 1
                elif date_time_to_second > post_time_bar_list[13] and date_time_to_second <= post_time_bar_list[14]:
                    post_time_count_list[14] += 1
                elif date_time_to_second > post_time_bar_list[14] and date_time_to_second <= post_time_bar_list[15]:
                    post_time_count_list[15] += 1
                elif date_time_to_second > post_time_bar_list[15] and date_time_to_second <= post_time_bar_list[16]:
                    post_time_count_list[16] += 1
                elif date_time_to_second > post_time_bar_list[16] and date_time_to_second <= post_time_bar_list[17]:
                    post_time_count_list[17] += 1
                elif date_time_to_second > post_time_bar_list[17] and date_time_to_second <= post_time_bar_list[18]:
                    post_time_count_list[18] += 1
                elif date_time_to_second > post_time_bar_list[18] and date_time_to_second <= post_time_bar_list[19]:
                    post_time_count_list[19] += 1
                elif date_time_to_second > post_time_bar_list[19] and date_time_to_second <= post_time_bar_list[20]:
                    post_time_count_list[20] += 1
                elif date_time_to_second > post_time_bar_list[20] and date_time_to_second <= post_time_bar_list[21]:
                    post_time_count_list[21] += 1
                elif date_time_to_second > post_time_bar_list[21] and date_time_to_second <= post_time_bar_list[22]:
                    post_time_count_list[22] += 1
                elif date_time_to_second > post_time_bar_list[22]:
                    post_time_count_list[23] += 1
            for post_time_count in post_time_count_list:
                post_time_count_joint += str(post_time_count) + ","
            target_line = unique_id_mongo + "," + post_time_count_joint + \
                str(min_post_time) + "," + str(max_post_time)
            target_file_tool.write_database_line_to_file(target_line)

    def media_weixin_post_time_range_from_id_file(self, source_mongo_args, source_file_args, target_file_args, target_mysql_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        source_file = source_file_tool.get_file()
        target_file = target_file_tool.get_file()
        count = 0
        for line in source_file:
            count += 1
            source_document = source_mongo_tool.find_one(
                "media_weixin", {"public_id": {"$regex": source_file_tool.line_strip_decode_ignore(line), "$options": "i"}}, ["post_time_law", ])
            if type(source_document) == type({}) and ("post_time_law" in source_document):

                post_time_list = source_document["post_time_law"]
                min_post_time = 86400
                max_post_time = 0
                post_time_bar_list = [3600 * 1, 3600 * 2, 3600 * 3, 3600 * 4, 3600 * 5, 3600 * 6, 3600 * 7, 3600 * 8, 3600 * 9, 3600 * 10, 3600 * 11,
                                      3600 * 12, 3600 * 13, 3600 * 14, 3600 * 15, 3600 * 16, 3600 * 17, 3600 * 18, 3600 * 19, 3600 * 20, 3600 * 21, 3600 * 22, 3600 * 23]
                post_time_count_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                post_time_count_joint = ""

                for post_time in post_time_list:

                    print(str(type(post_time)))
                    date_time = current_stamp_to_datetime(int(post_time))
                    date_time_hour = date_time.hour * 3600
                    date_time_minute = date_time.minute * 60
                    date_time_second = date_time.second
                    date_time_to_second = date_time_hour + date_time_minute + date_time_second
                    if date_time_to_second < min_post_time:
                        min_post_time = date_time_to_second
                    if date_time_to_second > max_post_time:
                        max_post_time = date_time_to_second
                    if date_time_to_second <= post_time_bar_list[0]:
                        post_time_count_list[0] += 1
                    elif date_time_to_second > post_time_bar_list[0] and date_time_to_second <= post_time_bar_list[1]:
                        post_time_count_list[1] += 1
                    elif date_time_to_second > post_time_bar_list[1] and date_time_to_second <= post_time_bar_list[2]:
                        post_time_count_list[2] += 1
                    elif date_time_to_second > post_time_bar_list[2] and date_time_to_second <= post_time_bar_list[3]:
                        post_time_count_list[3] += 1
                    elif date_time_to_second > post_time_bar_list[3] and date_time_to_second <= post_time_bar_list[4]:
                        post_time_count_list[4] += 1
                    elif date_time_to_second > post_time_bar_list[4] and date_time_to_second <= post_time_bar_list[5]:
                        post_time_count_list[5] += 1
                    elif date_time_to_second > post_time_bar_list[5] and date_time_to_second <= post_time_bar_list[6]:
                        post_time_count_list[6] += 1
                    elif date_time_to_second > post_time_bar_list[6] and date_time_to_second <= post_time_bar_list[7]:
                        post_time_count_list[7] += 1
                    elif date_time_to_second > post_time_bar_list[7] and date_time_to_second <= post_time_bar_list[8]:
                        post_time_count_list[8] += 1
                    elif date_time_to_second > post_time_bar_list[8] and date_time_to_second <= post_time_bar_list[9]:
                        post_time_count_list[9] += 1
                    elif date_time_to_second > post_time_bar_list[9] and date_time_to_second <= post_time_bar_list[10]:
                        post_time_count_list[10] += 1
                    elif date_time_to_second > post_time_bar_list[10] and date_time_to_second <= post_time_bar_list[11]:
                        post_time_count_list[11] += 1
                    elif date_time_to_second > post_time_bar_list[11] and date_time_to_second <= post_time_bar_list[12]:
                        post_time_count_list[12] += 1
                    elif date_time_to_second > post_time_bar_list[12] and date_time_to_second <= post_time_bar_list[13]:
                        post_time_count_list[13] += 1
                    elif date_time_to_second > post_time_bar_list[13] and date_time_to_second <= post_time_bar_list[14]:
                        post_time_count_list[14] += 1
                    elif date_time_to_second > post_time_bar_list[14] and date_time_to_second <= post_time_bar_list[15]:
                        post_time_count_list[15] += 1
                    elif date_time_to_second > post_time_bar_list[15] and date_time_to_second <= post_time_bar_list[16]:
                        post_time_count_list[16] += 1
                    elif date_time_to_second > post_time_bar_list[16] and date_time_to_second <= post_time_bar_list[17]:
                        post_time_count_list[17] += 1
                    elif date_time_to_second > post_time_bar_list[17] and date_time_to_second <= post_time_bar_list[18]:
                        post_time_count_list[18] += 1
                    elif date_time_to_second > post_time_bar_list[18] and date_time_to_second <= post_time_bar_list[19]:
                        post_time_count_list[19] += 1
                    elif date_time_to_second > post_time_bar_list[19] and date_time_to_second <= post_time_bar_list[20]:
                        post_time_count_list[20] += 1
                    elif date_time_to_second > post_time_bar_list[20] and date_time_to_second <= post_time_bar_list[21]:
                        post_time_count_list[21] += 1
                    elif date_time_to_second > post_time_bar_list[21] and date_time_to_second <= post_time_bar_list[22]:
                        post_time_count_list[22] += 1
                    elif date_time_to_second > post_time_bar_list[22]:
                        post_time_count_list[23] += 1
                for post_time_count in post_time_count_list:
                    post_time_count_joint += str(post_time_count) + ","
                target_line = source_file_tool.line_strip_decode_ignore(line) + "," + post_time_count_joint + \
                    str(min_post_time) + "," + str(max_post_time)
                target_file_tool.write_database_line_to_file(target_line)
                if len(post_time_list) >= 15:
                    max_count_value = max(post_time_count_list)
                    max_count_value_index = post_time_count_list.index(
                        max_count_value)
                    target_time_str = str((max_count_value_index + 1))
                    if len(target_time_str) == 1:
                        target_time_str = "0" + target_time_str + ":00:00"
                    else:
                        if target_time_str == "24":
                            target_time_str = "00:00:00"
                        else:
                            target_time_str = target_time_str + ":00:00"
                    query = ("UPDATE `sogou_weixin_account_init_task` SET `post_start_time`='%s' WHERE LOWER(`account_id`)='%s'" % (
                        target_time_str, source_file_tool.line_strip_decode_ignore(line).lower()))
                    print(query)
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
            else:
                target_file_tool.write_file_line_to_file(line)
        print("%d   ------" % count)

    def media_weixin_task_check(self, source_mongo_args, source_collection_args, source_file_args, source_mysql_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        target_file = target_file_tool.get_file()
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        unique_id_list_file_lower = []
        for line in source_file:
            unique_id_list_file_lower.append(
                source_file_tool.line_strip_decode_ignore(line).lower())
        unique_id_list_mongo = source_mongo_tool.get_unique_id_list(
            *source_collection_args)
        for unique_id_mongo in unique_id_list_mongo:
            if unique_id_mongo.lower() in unique_id_list_file_lower:

                query = ("SELECT `server_host_name` FROM `sogou_weixin_account_init_task` WHERE LOWER(`account_id`)='%s'" %
                         unique_id_mongo.lower())
                print(query)
                source_mysql_tool.cursor.execute(query)
                row = source_mysql_tool.cursor.fetchone()
                (server_host_name,) = row
                # print(str(type(unique_id_mongo)))
                # print(str(type(server_host_name)))
                # print(str(type(",")))
                # print(target_file_tool.line_strip_decode_ignore(server_host_name))
                # input("..........")
                line = unique_id_mongo + "," + \
                    target_file_tool.line_strip_decode_ignore(server_host_name)
                target_file_tool.write_database_line_to_file(line)

    def media_weixin_article_count(self, source_mongo_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)

        article_count = source_mongo_tool.database[
            "media_weixin_article"].find({}).count()
        count_time = current_datetime_to_str()
        target_file_tool.write_database_line_to_file(
            count_time + ":" + str(article_count))

    def media_weixin_data_backup(self, source_mysql_args, target_file_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        query = ("SELECT `uuid` FROM `media_weixin`")
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for unique_id in unique_id_list_mysql:
            query = ("SELECT `public_id`,`public_name`,`follower_num`,`media_cate`,`retail_price_s_min`,`retail_price_m_1_min`,`retail_price_m_2_min`,`retail_price_m_3_min` FROM `media_weixin` WHERE `uuid` = '%s'" % unique_id)
            public_id, public_name, follower_num, media_cate, retail_price_s_min, retail_price_m_1_min, retail_price_m_2_min, retail_price_m_3_min = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)
            follower_num_wan = (follower_num / 1000)
            media_category = ""
            if (media_cate is None) or (media_cate is '') or (media_cate is "#"):
                pass
            else:
                media_cate_item_list = media_cate.split("#")
                print(str(media_cate_item_list))
                # input("..............")
                for media_cate_item in media_cate_item_list[1:-1]:
                    for k, v in CATEGORY.items():
                        if int(media_cate_item) == v:
                            media_category = media_category + "#" + k

            target_file_tool.write_database_line_to_file(public_id + "," + str(public_name) + "," + str(follower_num) + "," + str(round(follower_num_wan, 1)) + "," + str(
                media_category) + "," + str(retail_price_s_min) + "," + str(retail_price_m_1_min) + "," + str(retail_price_m_2_min) + "," + str(retail_price_m_3_min))

    def media_weixin_pub_type_retail_min(self, source_mysql_args, target_file_args, error_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        query = ("SELECT `public_id` FROM `media_weixin` WHERE (`s_pub_type`=0 and `retail_price_s_min`!=0) or (`m_1_pub_type`=0 and `retail_price_m_1_min`!=0) or (`m_2_pub_type`=0 and `retail_price_m_2_min`!=0) or (`m_3_pub_type`=0 and `retail_price_m_3_min`!=0)")
        unique_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for unique_id in unique_id_list_mysql:
            query = (
                "SELECT `public_name` FROM `media_weixin`WHERE `public_id` = '%s'" % unique_id)
            (public_name,) = source_mysql_tool.find_one_by_sql(
                query, error_file_tool)

            target_file_tool.write_database_line_to_file(
                unique_id + "," + public_name)

    def media_weixn_media_vendor_bind(self, source_mysql_args_1, source_mysql_args_2):
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        query = ("SELECT `uuid` FROM `media_weixin`")
        uuid_media_weixin_list = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        query = ("SELECT `media_uuid` FROM `media_vendor_bind`")
        media_uuid_media_vendor_bind_list = source_mysql_tool_2.get_unique_id_list_by_sql(
            query)
        count = 0
        for media_uuid_media_vendor_bind in media_uuid_media_vendor_bind_list:
            if media_uuid_media_vendor_bind not in uuid_media_weixin_list:
                count += 1

        print("共处理 %d 条记录" % count)

    def t_media_duplicate(self, source_mysql_args, target_file_args_1, target_file_args_2):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        query = ('SELECT `sOpenName` FROM `t_media` WHERE `iMediaType`=1')
        weixin_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        weixin_id_list_lower = []
        for weixin_id in weixin_id_list:
            weixin_id_list_lower.append(weixin_id.lower())
        weixin_id_list_distinct = []
        count_distinct = 0
        count_duplicate = 0
        for weixin_id in weixin_id_list:
            if weixin_id.lower() in weixin_id_list_distinct:
                count_duplicate += 1
                target_file_tool_2.write_database_line_to_file(
                    weixin_id.lower())
            else:
                count_distinct += 1
                weixin_id_list_distinct.append(weixin_id.lower())
                target_file_tool_1.write_database_line_to_file(
                    weixin_id.lower())
        print("distinct records %d; duplicate records %d" %
              (count_distinct, count_duplicate))

    def t_media_i_user_id_not_in_user_id(self, source_mysql_args_1, source_mysql_args_2, target_file_args):
        source_mysql_tool_1 = self.get_mysql_tool(*source_mysql_args_1)
        source_mysql_tool_2 = self.get_mysql_tool(*source_mysql_args_2)
        target_file_tool = self.get_file_tool(*target_file_args)
        query = ("SELECT DISTINCT(`iUserID`) FROM `t_media`")
        t_media_user_id_list = source_mysql_tool_1.get_unique_id_list_by_sql(
            query)
        query = ("SELECT DISTINCT(`iUserID`) FROM `t_user` WHERE `iType`=2")
        t_user_user_id_list = source_mysql_tool_2.get_unique_id_list_by_sql(
            query)
        count = 0
        for user_id in t_media_user_id_list:
            if user_id not in t_user_user_id_list:
                count += 1
                target_file_tool.write_database_line_to_file(str(user_id))
        print('settled %d records' % count)

    def media_weixin_1_price_1_duplicate_data(self, source_file_args, source_mysql_args, target_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        source_file = source_file_tool.get_file()
        count = 0
        for line in source_file:
            count += 1
            query = ("SELECT `iStatus`,`iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14`,`dPrice1L`,`dPrice1H`,`dPrice2L`,`dPrice2H`,`dPrice3L`,`dPrice3H`,`dPrice4L`,`dPrice4H`,`dPrice5L`,`dPrice5H`,`dPrice6L`,`dPrice6H`,`dPrice7L`,`dPrice7H`,`dPrice8L`,`dPrice8H`,`dPrice11`,`dPrice12`,`dPrice13`,`dPrice14`,`iReadNum`,`iCreateType`,`iPublishType`,`iEffectiveTime`,`iWomSectTop`,`iBeizhu`,`sIntroduction`,`iAuditperson`,`iPutExplain`,`iAuthState`,`sCooperateLevelInfo`,`iExplain`,`iUpdateTime`,`iPersonCharge`,`sTypeInfo`,`iCreateTime` FROM `t_media` WHERE LOWER(`sOpenName`)= '%s' " %
                     source_file_tool.line_strip_decode_ignore(line))
            source_mysql_tool.cursor.execute(query)
            for row in source_mysql_tool.cursor:
                i_status, i_media_id, i_user_id, s_media_name, i_follower_num, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14, d_price_1_l, d_price_1_h, d_price_2_l, d_price_2_h, d_price_3_l, d_price_3_h, d_price_4_l, d_price_4_h, d_price_5_l, d_price_5_h, d_price_6_l, d_price_6_h, d_price_7_l, d_price_7_h, d_price_8_l, d_price_8_h, d_price_11, d_price_12, d_price_13, d_price_14, i_read_num, i_create_type, i_publish_type, i_effective_time, i_wom_sect_top, i_beizhu, i_instroduction, i_audit_person, i_put_explain, i_auth_status, s_cooperate_level_info, i_explain, i_update_time, i_person_charge, s_type_info, i_create_time = row
                if i_effective_time is None:
                    i_effective_time = -1

                '''
                行号
                '''
                i_media_id_convert = i_media_id
                '''
                状态
                '''
                i_status_convert = None
                if i_status == 0:
                    i_status_convert = '已删除'
                elif i_status == 1:
                    i_status_convert = '已审核'
                elif i_status == 2:
                    i_status_convert = '未审核'
                elif i_status == 3:
                    i_status_convert = '审核未通过'
                '''
                供应商编号
                '''
                i_user_id_convert = i_user_id
                '''
                微信昵称
                '''
                s_media_name_convert = s_media_name
                '''
                粉丝数
                '''
                i_follower_num_convert = i_follower_num
                '''
                直投账号单图文平台合作价
                '''
                i_price_1_convert = round(float(i_price_1), 2)
                '''
                直投账号多图文第一条平台合作价
                '''
                i_price_2_convert = round(float(i_price_2), 2)
                '''
                直投账号多图文第二条平台合作价
                '''
                i_price_3_convert = round(float(i_price_3), 2)
                '''
                直投账号多图文第3~N条平台合作价
                '''
                i_price_4_convert = round(float(i_price_4), 2)
                '''
                直投账号单图文零售价
                '''
                i_price_5_convert = round(float(i_price_5), 2)
                '''
                直投账号多图文第一条零售价
                '''
                i_price_6_convert = round(float(i_price_6), 2)
                '''
                直投账号多图文第二条零售价
                '''
                i_price_7_convert = round(float(i_price_7), 2)
                '''
                直投账号多图文3~N条零售价
                '''
                i_price_8_convert = round(float(i_price_8), 2)
                '''
                直投账号单图文执行价
                '''
                i_price_11_convert = round(float(i_price_11), 2)
                '''
                直投账号多图文第一条执行价
                '''
                i_price_12_convert = round(float(i_price_12), 2)
                '''
                直投账号多图文第二条执行价
                '''
                i_price_13_convert = round(float(i_price_13), 2)
                '''
                直投账号多图文3~N条执行价
                '''
                i_price_14_convert = round(float(i_price_14), 2)
                '''
                预约账号单图文平台合作价
                '''
                d_price_1_l_convert = round(float(d_price_1_l), 2)
                d_price_1_h_convert = round(float(d_price_1_h), 2)
                '''
                预约账号多图文第一条平台合作价
                '''
                d_price_2_l_convert = round(float(d_price_2_l), 2)
                d_price_2_h_convert = round(float(d_price_2_h), 2)
                '''
                预约账号多图文第二条平台合作价
                '''
                d_price_3_l_convert = round(float(d_price_3_l), 2)
                d_price_3_h_convert = round(float(d_price_3_h), 2)
                '''
                预约账号多图文第3~N条平台合作价
                '''
                d_price_4_l_convert = round(float(d_price_4_l), 2)
                d_price_4_h_convert = round(float(d_price_4_h), 2)
                '''
                预约账号单图文零售价
                '''
                d_price_5_l_convert = round(float(d_price_5_l), 2)
                d_price_5_h_convert = round(float(d_price_5_h), 2)
                '''
                预约账号多图文第一条零售价
                '''
                d_price_6_l_convert = round(float(d_price_6_l), 2)
                d_price_6_h_convert = round(float(d_price_6_h), 2)
                '''
                预约账号多图文第二条零售价
                '''
                d_price_7_l_convert = round(float(d_price_7_l), 2)
                d_price_7_h_convert = round(float(d_price_7_h), 2)
                '''
                预约账号多图文3~N条零售价
                '''
                d_price_8_l_convert = round(float(d_price_8_l), 2)
                d_price_8_h_convert = round(float(d_price_8_h), 2)
                '''
                预约账号单图文执行价
                '''
                d_price_11_convert = round(float(d_price_11), 2)

                '''
                预约账号多图文第一条执行价
                '''
                d_price_12_convert = round(float(d_price_12), 2)
                '''
                预约账号多图文第二条执行价
                '''
                d_price_13_convert = round(float(d_price_13), 2)

                '''
                预约账号多图文3~N条执行价
                '''
                d_price_14_convert = round(float(d_price_14), 2)
                '''
                账号类型
                '''
                i_create_type_convert = None
                if i_create_type == 0:
                    i_create_type_convert = '直投'
                elif i_status_convert == 1:
                    i_create_type_convert = '预约'
                '''
                发布类型
                '''
                i_publish_type_convert = None
                if i_publish_type == 0:
                    i_publish_type_convert = '纯发布'
                elif i_publish_type == 1:
                    i_publish_type_convert = '撰写+发布'
                '''
                有效期
                '''
                i_effective_time_convert = None
                if i_effective_time == -1:
                    i_effective_time_convert = '无数据'
                elif i_effective_time > 1:
                    i_effective_time_convert = current_stamp_to_time(
                        i_effective_time)

                '''
                wom置顶标记（1-5）
                '''
                i_wom_sect_top_convert = i_wom_sect_top
                '''
                备注
                '''
                i_beizhu_convert = None
                if i_beizhu is not None:
                    i_beizhu_convert = i_beizhu.replace(",", ";;")
                else:
                    i_beizhu_convert = '无备注信息'
                '''
                介绍
                '''
                i_instroduction_convert = None

                if i_instroduction is not None:
                    i_instroduction_convert = i_instroduction.replace(
                        ",", ";;")
                else:
                    i_instroduction_convert = "无介绍信息"
                i_open_name_convert = source_file_tool.line_strip_decode_ignore(
                    line)
                '''
                审核人
                '''

                i_audit_person_convert = ''
                i_admin_name = None
                if i_audit_person == 0:
                    i_audit_person_convert = '未知'
                else:
                    query = (
                        "SELECT `sAdminName` FROM `t_admin` WHERE `iAdminID`=%d" % i_audit_person)
                    source_mysql_tool.cursor.execute(query)
                    (i_admin_name,) = source_mysql_tool.cursor.fetchone()
                    i_audit_person_convert = i_admin_name
                '''
                上下架原因
                '''
                i_put_explain_convert = None
                if i_put_explain is None:
                    i_put_explain_convert = "未知"
                else:
                    i_put_explain_convert = i_put_explain
                '''
                微信认证状态
                '''
                i_auth_status_convert = None
                if i_auth_status is None:
                    i_auth_status_convert = '未知'
                elif i_auth_status == 0:
                    i_auth_status_convert = '未认证'
                elif i_auth_status == 1:
                    i_auth_status_convert = '已认证'
                '''
                合作等级
                '''
                s_cooperate_level_info_convert = None
                if s_cooperate_level_info is None:
                    s_cooperate_level_info_convert = '未知'
                else:
                    s_cooperate_level_info_convert = s_cooperate_level_info
                '''
                审核不通过原因
                '''
                i_explain_convert = None
                if i_explain is None:
                    i_explain_convert = ''
                else:
                    i_explain_convert = i_explain
                '''
                最新审核时间
                '''
                i_update_time_convert = None
                if i_update_time is None:
                    i_update_time_convert = '未知'
                else:
                    i_update_time_convert = current_stamp_to_time(
                        i_update_time)
                '''
                媒介专员
                '''
                i_person_charge_convert = None
                if i_person_charge is None:
                    i_person_charge_convert = '未知'
                elif i_person_charge == 0:
                    i_person_charge_convert = '未知'
                else:
                    query = (
                        "SELECT `sAdminName` FROM `t_admin` WHERE `iAdminID`=%d" % i_person_charge)
                    source_mysql_tool.cursor.execute(query)
                    (i_admin_name,) = source_mysql_tool.cursor.fetchone()
                    i_person_charge_convert = i_admin_name
                '''
                资源分类
                '''
                s_type_info_convert = None
                if s_type_info is None:
                    s_type_info_convert = '未知'
                else:
                    s_type_info_convert = s_type_info
                '''
                入驻时间
                '''
                i_create_time_convert = None
                if i_create_time is None:
                    i_create_time_convert = '未知'
                else:
                    i_create_time_convert = current_stamp_to_time(
                        i_create_time)

                write_joint = ''
                if count == 1:

                    write_joint = '行号' + ',' + '账号id' + ',' + '状态' + ',' + '供应商编号' + ',' + '微信昵称' + ',' + '粉丝数' + ',' + '直投账号单图文平台合作价' + ',' + '直投账号多图文第一条平台合作价' + ',' + '直投账号多图文第2条平台合作价' + ',' + '直投账号多图文第3~N条平台合作价' + ',' + '直投账号单图文零售价' + ',' + '直投账号多图文第一条零售价' + ',' + '直投账号多图文第2条零售价' + ',' + '直投账号多图文第3-N条零售价' + ',' + '直投账号单图文执行价' + ',' + '直投账号多图文第一条执行价' + ',' + '直投账号多图文第2条执行价' + ',' + '直投账号多图文第3-N条执行价' + ',' + '预约账号单图文平台合作价下限' + ',' + '预约账号单图文平台合作价上限' + ',' + '预约账号多图文第一条平台合作价下限' + ',' + '预约账号多图文第一条平台合作价上限' + ',' + '预约账号多图文第2条平台合作价下限' + ',' + '预约账号多图文第2条平台合作价上限' + ',' + \
                        '预约账号多图文第3~N条平台合作价下限' + ',' + '预约账号多图文第3~N条平台合作价上限' + ',' + '预约账号单图文零售价下限' + ',' + '预约账号单图文零售价上限' + ',' + '预约账号多图文第一条零售价下限' + ',' + '预约账号多图文第一条零售价上限' + ',' + '预约账号多图文第2条零售价下限' + ',' + '预约账号多图文第2条零售价上限' + ',' + '预约账号多图文第3-N条零售价下限' + ',' + '预约账号多图文第3-N条零售价上限' + ',' + \
                        '预约账号单图文执行价' + ',' + '预约账号多图文第一条执行价' + ',' + '预约账号多图文第2条执行价' + ',' + \
                        '预约账号多图文3~N条执行价' + ',' + '账号类型' + ',' + \
                        '发布类型' + ',' + '有效期' + ',' + \
                        'wom置顶标记(1-5)' + ',' + '备注' + ',' + '介绍' + ',' + '审核人' + ',' + '上下架原因' + ',' + '微信认证状态' + \
                                 ',' + '合作等级' + ',' + '审核不通过原因' + ',' + '最新审核时间' + \
                        ',' + '媒介专员' + ',' + '资源分类' + ',' + '入驻时间'
                    print(write_joint)
                    target_file_tool.write_database_line_to_file(write_joint)
                write_joint = str(i_media_id_convert) + ',' + i_open_name_convert + ',' + str(i_status_convert) + ',' + str(i_user_id_convert) + ',' + str(s_media_name_convert) + ',' + str(i_follower_num_convert) + ',' + str(i_price_1_convert) + ',' + str(i_price_2_convert) + ',' + str(i_price_3_convert) + ',' + str(i_price_4_convert) + ',' + str(i_price_5_convert) + ',' + str(i_price_6_convert) + ',' + str(i_price_7_convert) + ',' + str(i_price_8_convert) + ',' + str(i_price_11_convert) + ',' + str(i_price_12_convert) + ',' + str(i_price_13_convert) + ',' + str(i_price_14_convert) + ',' + str(d_price_1_l_convert) + ',' + str(d_price_1_h_convert) + ',' + str(d_price_2_l_convert) + ',' + str(d_price_2_h_convert) + ',' + str(d_price_3_l_convert) + \
                    ',' + str(d_price_3_h_convert) + ',' + str(d_price_4_l_convert) + ',' + str(d_price_4_h_convert) + ',' + str(d_price_5_l_convert) + ',' + str(d_price_5_h_convert) + ',' + str(d_price_6_l_convert) + ',' + str(d_price_6_h_convert) + ',' + str(d_price_7_l_convert) + ',' + str(d_price_7_h_convert) + ',' + str(d_price_8_l_convert) + ',' + str(
                        d_price_8_h_convert) + ',' + str(d_price_11_convert) + ',' + str(d_price_12_convert) + ',' + str(d_price_13_convert) + ',' + str(d_price_14_convert) + ',' + str(i_create_type_convert) + ',' + str(i_publish_type_convert) + ',' + str(i_effective_time_convert) + ',' + str(i_wom_sect_top_convert) + ',' + str(i_beizhu_convert) + ',' + str(i_instroduction_convert) + ',' + str(i_audit_person_convert) + ',' + str(i_put_explain_convert) + ',' + str(i_auth_status_convert) + ',' + str(s_cooperate_level_info_convert) + ',' + str(i_explain_convert) + ',' + str(i_update_time_convert) + ',' + str(i_person_charge_convert) + ',' + str(s_type_info_convert) + ',' + str(i_create_time_convert)
                print(write_joint)
                target_file_tool.write_database_line_to_file(write_joint)

    def settled_yuyue_public_id(self, source_file_args_1, source_file_args_2, source_mysql_args, error_file_args):
        source_file_tool_1 = self.get_file_tool(*source_file_args_1)
        source_file_tool_2 = self.get_file_tool(*source_file_args_2)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        error_file_tool = self.get_file_tool(*error_file_args)
        source_file_1 = source_file_tool_1.get_file()
        source_file_2 = source_file_tool_2.get_file()

        source_file_2_line_list = []
        query = ("SELECT DISTINCT(`sOpenName`) FROM `t_media` WHERE `iCreateType`=1 AND (`dPrice1L`!=0 or `dPrice2L`!=0 or `dPrice3L`!=0 or `dPrice4L`!=0)")
        distinct_open_name_list = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        distinct_open_name_list_lower = []
        for open_name in distinct_open_name_list:
            if open_name.lower() not in distinct_open_name_list_lower:
                distinct_open_name_list_lower.append(open_name.lower())

        for line in source_file_2:
            source_file_2_line_list.append(
                source_file_tool_2.line_strip_decode_ignore(line))
        count = 0
        for line in source_file_1:
            if (source_file_tool_1.line_strip_decode_ignore(line) in distinct_open_name_list_lower) and (source_file_tool_1.line_strip_decode_ignore(line) not in source_file_2_line_list):
                count += 1
                print(source_file_tool_1.line_strip_decode_ignore(line))
                error_file_tool.write_file_line_to_file(line)
        print('settled %d records' % count)

    def media_weixin_price_errors(self, source_mysql_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        query = ("SELECT `public_id` FROM `media_weixin` WHERE (`orig_price_s_min`>`orig_price_s_max`) OR (`orig_price_m_1_min`>`orig_price_m_1_max`) OR (`orig_price_m_2_min`>`orig_price_m_2_max`) OR (`orig_price_m_3_min`>`orig_price_m_3_max`) OR (`retail_price_s_min`>`retail_price_s_max`) OR (`retail_price_m_1_min`>`retail_price_m_1_max`) OR (`retail_price_m_2_min`>`retail_price_m_2_max`) OR (`retail_price_m_3_min`>`retail_price_m_3_max`)")
        source_unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for unique_id in source_unique_id_list:
            target_file_tool.write_database_line_to_file(unique_id)

    def media_weixin_has_direct_or_origin(self, source_mysql_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        query = (
            "SELECT `public_id` FROM `media_weixin` WHERE `has_origin_pub`=-1 OR `has_direct_pub`=-1")
        unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        count = 0
        for unique_id in unique_id_list:
            count += 1
            target_file_tool.write_database_line_to_file(unique_id)
        print("settled %d records" % count)

    def weibo_account_duplicate(self, source_mysql_args, target_file_args_1, target_file_args_2):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        url_list = []
        query = (
            "SELECT `sUrl` FROM `t_media` WHERE `sUrl` LIKE '%%weibo.com/%%'")
        url_list = source_mysql_tool.get_unique_id_list_by_sql(query)

        unique_url_list = []
        duplicate_url_list = []
        unique_url_count = 0
        duplicate_url_count = 0
        try:
            for url in url_list:
                if url not in unique_url_list:
                    unique_url_list.append(url)
                else:
                    duplicate_url_list.append(url)
            for url in unique_url_list:
                if url not in duplicate_url_list:
                    pass
                    unique_url_count += 1
                    target_file_tool_1.write_database_line_to_file(url)
                else:

                    query = (
                        "SELECT `iMediaID`,`iUserID`,`sMediaName`,`iFollowerNum`,`sFollowerImg`,`sUrl`,`sAvatar`,`sQRCode`,`iReadNum`,`sBottomPutExplain`,`iBeizhu`,`iVerifyState`,`sIntroduction`,`iStatus`,`iPut`,`iExplain`,`iWomSectTop`,`iEffectiveTime`,`iPrice1`,`iPrice2`,`iPrice3`,`iPrice4`,`iPrice5`,`iPrice6`,`iPrice7`,`iPrice8`,`iPrice11`,`iPrice12`,`iPrice13`,`iPrice14` FROM `t_media` WHERE `sUrl`='%s'" % url)
                    source_mysql_tool.cursor.execute(query)

                    for row in source_mysql_tool.cursor:
                        duplicate_url_count += 1
                        i_media_id, i_user_id, s_media_name, i_follower_num, s_follower_img, s_url, s_avater, s_qrcode, i_read_num, s_bottom_put_explain, i_beizhu, i_verify_status, s_introduction, i_status, i_put, i_explain, i_wom_sect_top, i_effective_time, i_price_1, i_price_2, i_price_3, i_price_4, i_price_5, i_price_6, i_price_7, i_price_8, i_price_11, i_price_12, i_price_13, i_price_14 = row
                        '媒体ID,归属帐号,帐号名称,粉丝数量,粉丝截图,微博地址,头像,二维码,阅读量,下架原因,备注,认证状态(),简介,账号状态,上下架,审核不通过原因,沃米置顶排序,有效时间,软广直发平台合作价,软广转发平台合作价,微任务直发执行价,微任务转发执行价,软广直发零售价,软广转发零售价,微任务直发零售价,微任务转发零售价,软广直发执行价,软广转发执行价,微任务直发执行价,微任务转发执行价'

                        if i_verify_status == 126:
                            i_verify_status = "未认证"
                        elif i_verify_status == 127:
                            i_verify_status = "蓝V"
                        elif i_verify_status == 128:
                            i_verify_status = "黄V"
                        elif i_verify_status == 129:
                            i_verify_status = "达人"
                        elif i_verify_status == 130:
                            i_verify_status = "草根"

                        if i_status == 0:
                            i_status = "已删除"
                        elif i_status == 1:
                            i_status = "已审核"
                        elif i_status == 2:
                            i_status = "未审核"
                        elif i_status == 3:
                            i_status = "未审核通过"

                        if i_put == 0:
                            i_put = "下架"
                        elif i_put == 1:
                            i_put = "上架"
                        if duplicate_url_count == 1:
                            target_file_tool_2.write_database_line_to_file(
                                '媒体ID,归属帐号,帐号名称,粉丝数量,粉丝截图,微博地址,头像,二维码,阅读量,下架原因,备注,认证状态(),简介,账号状态,上下架,审核不通过原因,沃米置顶排序,有效时间,软广直发平台合作价,软广转发平台合作价,微任务直发执行价,微任务转发执行价,软广直发零售价,软广转发零售价,微任务直发零售价,微任务转发零售价,软广直发执行价,软广转发执行价,微任务直发执行价,微任务转发执行价')
                        input_joint = (str(i_media_id).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_user_id).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_media_name).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_follower_num).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_follower_img).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_url).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_avater).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_qrcode).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_read_num).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_bottom_put_explain).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_beizhu).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_verify_status).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(s_introduction).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_status).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_put).strip(
                        ).replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_explain).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_wom_sect_top).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_effective_time).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_1).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_2).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_3).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_4).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_5).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_6).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_7).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_8).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_11).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_12).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + (str(i_price_13).strip().replace(",", ";").replace("\r", '').replace('\n', '')) + "," + str(i_price_14)
                        target_file_tool_2.write_database_line_to_file(
                            input_joint)
        finally:
            source_mysql_tool.disconnection()
            target_file_tool_1.close_file()
            target_file_tool_2.close_file()

        print('unique_url_count %d;duplicate_url_count %d' %
              (unique_url_count, duplicate_url_count))

    def file_encode_exchange(self, source_file_args, target_file_args):
        source_file_tool = self.get_file_tool(*source_file_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        source_file = source_file_tool.get_file()
        for line in source_file:
            target_file_tool.f.write((source_file_tool.line_strip_decode_ignore(
                line) + '\n').encode("gbk", 'ignore'))

    def public_name_duplicate(self, source_mysql_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        query = ("SELECT `public_name` FROM `media_weixin`")
        public_name_list = source_mysql_tool.get_unique_id_list_by_sql(query)
        public_name_list_unique = []
        public_name_list_duplicate = []
        item_count = 0
        public_name_count = 0
        for public_name in public_name_list:
            if public_name not in public_name_list_unique:
                public_name_list_unique.append(public_name)
            elif public_name not in public_name_list_duplicate:
                public_name_list_duplicate.append(public_name)
                query = (
                    "SELECT `public_id`,`public_name` FROM `media_weixin` WHERE `public_name` ='%s'" % public_name)
                source_mysql_tool.cursor.execute(query)
                rows = source_mysql_tool.cursor
                for row in rows:
                    item_count += 1
                    public_id, public_name = row
                    target_file_tool.f.write(
                        (public_id + "," + public_name + "\n").encode("gbk", 'ignore'))
                    print("ITEM COUNT %d" % item_count)
                public_name_count += 1
                print("PUBLIC NAME COUNT %d" % public_name_count)

    def transer_data_to_hdfs(self):
        one_month_ago = datetime_one_month_ago()
        os.system(
            "/usr/local/hadoop27/bin/hadoop fs  -rm hdfs://spider-host-003:9000/spark_csv_in/article_%s*" % one_month_ago)
        os.system(
            "/usr/local/hadoop27/bin/hadoop fs  -rm hdfs://spider-host-003:9000/account_classify/*")

        yesterday = datetime_yesterday_to_str()
        _yesterday = datetime_yesterday_to_str_upgrade()
        os.system("/usr/bin/scp admin@server-32:/alidata1/dts/export_from_mongo/index_compute/article_%s.csv /alidata1/dts/export_from_mongo/index_compute" % yesterday)
        os.system("/usr/bin/scp admin@server-32:/alidata1/dts/export_from_mongo/article_wordle/wordle-%s.csv /alidata1/dts/export_from_mongo/account_wordle" % _yesterday)
        os.system("/usr/bin/scp admin@server-32:/home/admin/ETL_PYTHON_3/csvs/account_tags.csv /usr/local/spark/spark_test/")
        os.system("/usr/local/hadoop27/bin/hadoop fs  -put /alidata1/dts/export_from_mongo/index_compute/article_%s.csv hdfs://spider-host-003:9000/spark_csv_in" % yesterday)
        os.system("/usr/local/hadoop27/bin/hadoop fs  -put /alidata1/dts/export_from_mongo/account_wordle/wordle-%s.csv hdfs://spider-host-003:9000/media_weixin_account_info" % _yesterday)
        os.system("/usr/local/hadoop27/bin/hadoop fs  -put /usr/local/spark/spark_test/account_tags.csv hdfs://spider-host-003:9000/account_classify")

    def media_weixin_public_id_mysql_not_in_wom_info(self, source_mongo_args, source_mysql_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        public_id_list_mongo_dict = source_mongo_tool.database[
            "media_weixin_wom_info"].find({}, {"public_id": 1})
        target_file_tool = self.get_file_tool(*target_file_args)
        public_id_list_mongo = []
        for document in public_id_list_mongo_dict:
            public_id_list_mongo.append(document["public_id"])
        query = "SELECT `real_public_id` FROM `media_weixin` WHERE `real_public_id` !=''"
        public_id_list_mysql = source_mysql_tool.get_unique_id_list_by_sql(
            query)
        for public_id_mysql in public_id_list_mysql:
            if public_id_mysql not in public_id_list_mongo:
                target_file_tool.write_database_line_to_file(
                    public_id_mysql + '\n')

    def video_media_id_csv(self, source_mysql_args, target_file_args):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        target_file_tool = self.get_file_tool(*target_file_args)

    def transfer_public_id_to_file(self, source_mongo_args, target_file_args):
        '''
        截取进程分片
        '''
        os.system(
            "/usr/bin/rm /home/admin/ETL_PYTHON_3/csvs/media_weixin_info_public_id_*")
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        documents = source_mongo_tool.database[
            "media_weixin_info"].find({}, {"public_id": 1})
        document_count = 0
        file_suffix = 1
        for document in documents:

            document_count += 1
            print("settled %d items" % document_count)
            target_file_tool.write_database_line_to_file(
                document['public_id'] + '\n')
            if document_count % 50000 == 0:
                file_suffix += 1
                target_file_tool.close_file()
                target_file_args_list = list(target_file_args)
                target_file_args_list[
                    1] = "media_weixin_info_public_id_%d.csv" % file_suffix
                target_file_tool = self.get_file_tool(
                    *tuple(target_file_args_list))
        target_file_tool.close_file()
        source_mongo_tool.disconnection()

    def delete_weixin_article_two_month_ago(self, target_mysql_args):
        target_mysql_tool = self.get_mysql_tool(*target_mysql_args)
        current_time = current_time_to_stamp() - 2 * 30 * 24 * 60 * 60
        print("delete data two month ago:%d" % current_time)
        query = ("DELETE FROM `weixin_article` WHERE `post_time`<%d" %
                 current_time)
        print(query)
        target_mysql_tool.cursor.execute(query)
        target_mysql_tool.cnx.commit()
        target_mysql_tool.disconnection()
        print("work over............")

    def media_weixin_head_view_and_like_select(self, source_mysql_args, source_file_args, target_file_args_1, target_file_args_2):
        source_mysql_tool = self.get_mysql_tool(*source_mysql_args)
        source_file_tool = self.get_file_tool(*source_file_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        source_file = source_file_tool.get_file()
        count = 0
        for line in source_file:
            count += 1
            print("settled %d items" % count)
            query = ("SELECT `account_type`,`m_head_avg_view_cnt`,`m_head_avg_like_cnt` FROM `media_weixin` WHERE `real_public_id`='%s'" %
                     source_file_tool.line_strip_decode_ignore(line))
            print(query)
            source_mysql_tool.cursor.execute(query)
            row = source_mysql_tool.cursor.fetchone()
            if row is not None:
                account_type, m_head_avg_view_cnt, m_head_avg_like_cnt = row
                target_file_tool_1.write_database_line_to_file(source_file_tool.line_strip_decode_ignore(
                    line) + "," + str(account_type) + "," + str(m_head_avg_view_cnt) + "," + str(m_head_avg_like_cnt) + "\n")
            else:
                target_file_tool_2.write_database_line_to_file(
                    source_file_tool.line_strip_decode_ignore(line) + "\n")

    def media_weixin_wom_avg_view_cnt_gte_6000(self, source_mongo_args, target_file_args_1,target_file_args_2,target_file_args_3,target_file_args_4,target_file_args_5,target_file_args_6,target_file_args_7,target_file_args_8,target_file_args_9,target_file_args_10):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        target_file_tool_3 = self.get_file_tool(*target_file_args_3)
        target_file_tool_4 = self.get_file_tool(*target_file_args_4)
        target_file_tool_5 = self.get_file_tool(*target_file_args_5)
        target_file_tool_6 = self.get_file_tool(*target_file_args_6)
        target_file_tool_7 = self.get_file_tool(*target_file_args_7)
        target_file_tool_8 = self.get_file_tool(*target_file_args_8)
        target_file_tool_9 = self.get_file_tool(*target_file_args_9)
        target_file_tool_10 = self.get_file_tool(*target_file_args_10)
        
        documents = source_mongo_tool.database["media_weixin"].find(
            {},{"public_id":1})
        count = 0
        for document in documents:
            chooce = random.choice([1,2,3,4,5,6,7,8,9,10])
            if chooce == 1:
                target_file_tool_1.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 2:
                target_file_tool_2.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 3:
                target_file_tool_3.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 4:
                target_file_tool_4.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 5:
                target_file_tool_5.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 6:
                target_file_tool_6.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 7:
                target_file_tool_7.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 8:
                target_file_tool_8.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 9:
                target_file_tool_9.write_database_line_to_file(
                    document["public_id"] + "\n")
            elif chooce == 10:
                target_file_tool_10.write_database_line_to_file(
                    document["public_id"] + "\n")
            count += 1
            print("settled %d items;current public id: %s" %
                  (count, document["public_id"]))

    def media_weixin_mongo_avg_view_cnt_gte_6000(self, source_mongo_args, source_file_args, target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        source_file_tool = self.get_file_tool(*source_file_args)
        source_file = source_file_tool.get_file()
        target_file_tool = self.get_file_tool(*target_file_args)
        count = 0
        for line in source_file:
            document = source_mongo_tool.database["media_weixin"].find_one(
                {"public_id": source_file_tool.line_strip_decode_ignore(line)})
            public_id = document["public_id"]
            public_name = strip_and_replace_all(document["public_name"])
            account_short_desc = strip_and_replace_all(document[
                "account_short_desc"].replace(",", ";"))
            account_cert = "0"
            if document["tencent_account_cert"] == 1 or document["sina_account_cert"] == 1 or document["account_cert"] == 1:
                account_cert = "1"
            document = source_mongo_tool.database["media_weixin_info"].find_one(
                {"public_id": source_file_tool.line_strip_decode_ignore(line)})
            avg_view_cnt = document["cycle"][
                "20170207"]["month"]["avg_view_cnt"]
            input_str = public_id + "," + public_name + "," + account_short_desc + \
                "," + account_cert + "," + str(avg_view_cnt) + "\n"
            target_file_tool.f.write(
            input_str.encode("gbk", 'ignore'))
            count += 1
            print("settled %d items;current public id: %s" %
                  (count, source_file_tool.line_strip_decode_ignore(line)))
    def media_weixin_info_accounts_split_to_five_files(self,source_mongo_args,file_num):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        os.system("/usr/bin/rm /home/admin/ETL_PYTHON_3/csvs/media_weixin_info_influence_public_id*")
        target_file_tool_list = ["first",]
        file_num_list = list(range(1,file_num+1))
        target_file_args = [CHECK_CSV_PATH,"","ab"]
        for i in file_num_list:
            target_file_args[1] = "media_weixin_info_influence_public_id_%d.csv" % i
            target_file_tool_list.append(self.get_file_tool(*tuple(target_file_args)))
        documents = source_mongo_tool.database["media_weixin_info"].find(
            {},{"public_id":1}).batch_size(10000)
        count = 0
        for document in documents:
            file_choosed = random.choice(file_num_list)
            target_file_tool_list[file_choosed].write_database_line_to_file(document["public_id"] + "\n")
            count += 1
            print("settled %d items;target %d ;current public id: %s" %
                  (count, file_choosed, document["public_id"]))
    def brand_warehouse_qmg_by_level_csv(self,source_mongo_args,target_file_args_1,target_file_args_2):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool_1 = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        documents = source_mongo_tool.database["brand_warehouse_qmg"].find({},{"brand_name_cn":1,"brand_name_en":1})
        for document in documents:
            name_cn = document["brand_name_cn"]
            name_en = document["brand_name_en"]
            if name_cn !="未知" and name_en != "未知":
                if len(name_cn)==2:
                    target_file_tool_2.f.write((name_cn+"\n").encode("utf-8","ignore"))
                elif len(name_cn) ==1:
                    pass
                else:
                    target_file_tool_1.f.write((name_cn+"\n").encode("utf-8","ignore"))
                if len(name_en)==2:
                    target_file_tool_2.f.write((name_en+"\n").encode("utf-8","ignore"))
                elif len(name_cn) ==1:
                    pass
                else:
                    target_file_tool_1.f.write((name_en+"\n").encode("utf-8","ignore"))
            elif name_cn !="未知" and name_en == "未知":
                if len(name_cn)<=2:
                    target_file_tool_2.f.write((name_cn+"\n").encode("utf-8","ignore"))
                elif len(name_cn) ==1:
                    pass
                else:
                    target_file_tool_1.f.write((name_cn+"\n").encode("utf-8","ignore"))
            elif name_cn =="未知" and name_en != "未知":
                if len(name_en)<=2:
                    target_file_tool_2.f.write((name_en+"\n").encode("utf-8","ignore"))
                elif len(name_cn) ==1:
                    pass
                else:
                    target_file_tool_1.f.write((name_en+"\n").encode("utf-8","ignore"))
    def brand_warehouse_qmg_csv(self,source_mongo_args,target_file_args_1,target_file_args_2):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args_1)
        target_file_tool_2 = self.get_file_tool(*target_file_args_2)
        documents = source_mongo_tool.database["brand_warehouse_qmg"].find({},{"brand_name_cn":1,"brand_name_en":1,"brand_cate":1})
        for document in tqdm(documents):
            if document["brand_cate"][0]["sup_catid"] not in ["7","9"]:
                name_cn = document["brand_name_cn"]
                name_en = document["brand_name_en"]
                line_list = []
                # print(name_cn != "未知")
                # if name_cn != "未知" and len(name_cn)<=2:
                #     target_file_tool_2.f.write((name_cn+"\n").encode("utf-8","ignore"))
                #     continue
                if name_cn != "未知" and len(name_cn)>=2:
                    line_list.append(name_cn)
                if name_en != "unknown" and name_en != "未知" and len(name_en)>=2:
                    line_list.append(name_en)
                if len(line_list) == 2:
                    if name_en.isdigit():
                        # print("\n".join(line_list))
                        write_line = name_en+name_cn + "\n"
                        target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                        target_file_tool.f.flush()
                    elif len(name_en) <=2:
                        if len(name_en) ==2 and name_en[0] !=name_en[1]:
                            write_line = name_en +"\n"+ name_cn + "\n"
                            target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                            target_file_tool.f.flush()
                        else:
                            write_line = name_en+name_cn + "\n"
                            target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                            target_file_tool.f.flush()
                    else:
                        # print("\n".join(line_list))
                        write_line = "\n".join(line_list) + "\n"
                        target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                        target_file_tool.f.flush()
                elif len(line_list) ==1:
                    if (re.match('^[a-zA-Z]+$',line_list[0]) and len(line_list[0])>=3):
                        # print("\n".join(line_list))
                        write_line = "\n".join(line_list) + "\n"
                        target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                        target_file_tool.f.flush()
                    elif not line_list[0].isdigit():
                        # print("\n".join(line_list))
                        write_line = "\n".join(line_list) + "\n"
                        target_file_tool.f.write(write_line.encode("utf-8","ignore"))
                        target_file_tool.f.flush()

                        
            else:
                pass
                # print("current item is not my need")

    def sogou_scel_dicts_csv(self,source_mongo_args,target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        documents = source_mongo_tool.database["sogou_scel_dicts"].find({},{"words":1})
        for document in documents:
            write_line = "\n".join(document["words"]) + "\n"
            target_file_tool.f.write(write_line.encode("utf-8","ignore"))
            target_file_tool.f.flush()
    def media_weixin_mongo_public_id_export(self,source_mongo_args,target_file_args):
        source_mongo_tool = self.get_mongo_tool(*source_mongo_args)
        target_file_tool = self.get_file_tool(*target_file_args)
        documents = source_mongo_tool.database["media_weixin"].find({},{"public_id":1})
        for document in tqdm(documents):
            write_line = document["public_id"] + "\n"
            target_file_tool.f.write(write_line.encode("utf-8","ignore"))
            target_file_tool.f.flush()

if __name__ == '__main__':
    media_weixin_check = MediaWeiXinCheck()
