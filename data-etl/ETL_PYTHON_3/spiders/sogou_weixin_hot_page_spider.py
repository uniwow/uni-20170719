# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.tool_string import my_decode, my_strip
import re
import requests
import json
from lxml import etree
from tools.settings import MEIPAI_HOT_URL_DICT, WOM_PROD_1_TUPLE, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, CHECK_CSV_PATH

def sogou_weixin_hot_page(target_url):
	mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
	collection = mongo_tool.database['sogou_weixin_hot_page']
	response = requests.get(
        target_url)
	