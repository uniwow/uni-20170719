import requests
import json
from tools.tool_file import FileTool
import aiohttp
import asyncio
import fcntl
import random 



async def fetch(client,url):
    abuyun_proxy = {
        "server": "proxy.abuyun.com",
        "port": 9010,
        "tunnels": [
            {
                "user": "H7U2365U2IZPJ6XP",
                "password": "694E0E53ED72AAD0"
            },
            {
                "user": "HZQ776SX9D33D39P",
                "password": "CE63E0B1198FDB3A"
            },
            {
                "user": "HT7127N93262415P",
                "password": "FFD7FA05C02F8B7F"
            },
            {
                "user": "HWPV08959D4IDE4P",
                "password": "10C2528F907410AA"
            },
            {
                "user": "HA46E6800VLFS3BP",
                "password": "D827AC1228FE9261"
            }
        ],
    }
    proxyHost = abuyun_proxy["server"]
    proxyPort = abuyun_proxy["port"]
    proxy_user= abuyun_proxy["tunnels"][random.choice([0,1,2,3,4])]
    proxyUser = proxy_user["user"]
    proxyPass = proxy_user["password"]
    proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
          "host" : proxyHost,
          "port" : proxyPort,
          "user" : proxyUser,
          "pass" : proxyPass,
        }
    async with client.get(url,allow_redirects=True,proxy=proxyMeta) as resp:
        assert resp.status == 200
        return  await resp.text()


async def main(loop,headers,cookies,url,target_file_args,public_id):
    async with aiohttp.ClientSession(loop=loop,headers=headers,
            cookies=cookies) as client:
        html = await fetch(client,url)
        # print(html)
        # print(json.loads(list(filter(lambda l : "var jsonData" in l ,html.split("\n")))[0].replace("\n", "").replace(";","").replace("\r", "").replace("\t", "").replace("\\", "").split(" = ")[1].replace('"[', "[").replace('"{', "{").replace('}"', "}").replace(']"', "]"))["typeRanking"])
        type_ranking = [type_rank["rankName"] for type_rank in json.loads(list(filter(lambda l : "var jsonData" in l ,html.split("\n")))[0].replace("\n", "").replace(";","").replace("\r", "").replace("\t", "").replace("\\", "").split(" = ")[1].replace('"[', "[").replace('"{', "{").replace('}"', "}").replace(']"', "]"))["typeRanking"]]
        # print(type_ranking)
        line = public_id + "," + ",".join(type_ranking) + "\n"
        print(line)
        target_file_tool = get_file_tool(*target_file_args)
        fcntl.flock(target_file_tool.f, fcntl.LOCK_EX)
        target_file_tool.f.write(line.encode('gbk', 'ignore'))
        fcntl.flock(target_file_tool.f, fcntl.LOCK_UN)
        target_file_tool.close_file()

def get_file_tool(file_path, file_name, open_status):
    file_tool = FileTool(file_path, file_name, open_status)
    return file_tool

def bluemc_media_weixin_account_tags_spider(headers, cookies, public_id, target_file_args):
    headers[
        "Referer"] = "http//bang.bluemc.cn/billBoard/detail.do?urn=3136949-%s" % public_id
    url = "http://bang.bluemc.cn/billBoard/detail.do?urn=3136949-%s" % public_id
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop,headers,cookies,url,target_file_args,public_id))

def newrank_media_weixin_account_tags_spider(public_id,target_file_args):
    url = "http://www.newrank.cn/public/info/detail.html?account=%s" % public_id
    # url = "http://www.newrank.cn/public/info/detail.html?account=woshitongdao"
    abuyun_proxy = {
        "server": "proxy.abuyun.com",
        "port": 9010,
        "tunnels": [
            {
                "user": "H7U2365U2IZPJ6XP",
                "password": "694E0E53ED72AAD0"
            },
            {
                "user": "HZQ776SX9D33D39P",
                "password": "CE63E0B1198FDB3A"
            },
            {
                "user": "HT7127N93262415P",
                "password": "FFD7FA05C02F8B7F"
            },
            {
                "user": "HWPV08959D4IDE4P",
                "password": "10C2528F907410AA"
            },
            {
                "user": "HA46E6800VLFS3BP",
                "password": "D827AC1228FE9261"
            }
        ],
    }
    proxyHost = abuyun_proxy["server"]
    proxyPort = abuyun_proxy["port"]
    proxy_user= abuyun_proxy["tunnels"][random.choice([0,1,2,3,4])]
    proxyUser = proxy_user["user"]
    proxyPass = proxy_user["password"]
    proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
          "host" : proxyHost,
          "port" : proxyPort,
          "user" : proxyUser,
          "pass" : proxyPass,
        }
    proxies = {
        "http" : proxyMeta,
    }
    response= requests.get(url,proxies=proxies)
    response_dict = json.loads(list(filter(lambda l : "var fgkcdg" in l,response.text.split("\n")))[0].split(" = ")[1].replace(';',''))
    line = public_id + "," + response_dict["type"] + "\n"
    print(line)
    target_file_tool = get_file_tool(*target_file_args)
    fcntl.flock(target_file_tool.f, fcntl.LOCK_EX)
    target_file_tool.f.write(line.encode('gbk', 'ignore'))
    fcntl.flock(target_file_tool.f, fcntl.LOCK_UN)
    target_file_tool.close_file()