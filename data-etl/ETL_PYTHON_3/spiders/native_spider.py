# coding=utf-8
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
from tools.tool_mysql import MysqlTool
from tools.settings import WOM_PROD_MYSQL_TUPLE, MEIPAI_HOT_URL_DICT, WOM_PROD_1_TUPLE, DTS_OPT_MYSQL_TUPLE, DEV_WOM2_MYSQL_TUPLE, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, CHECK_CSV_PATH
import os
from tools.tool_time import datetime_to_str, current_time_to_stamp
import datetime
from tools.tool_string import my_decode, my_strip
import re
import requests
import json
from lxml import etree


def get_file_tool(file_path, file_name, open_status):
    file_tool = FileTool(file_path, file_name, open_status)
    return file_tool


def huajiao_video_crawler(target_url, uuid):

    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    collection = mongo_tool.database['huajiao_user_info']
    response = requests.get(
        target_url)
    # print(response.json()['data']['feeds'][0])
    target_dict = response.json()['data']
    if type(target_dict) == type({}):
        feeds = target_dict['feeds']
        print(str(type(feeds)))
        user_info = {}
        user_info['media_id'] = None
        for feed in feeds:
            try:
                user_info['media_id'] = feed['author']['uid']
            except Exception as e:
                continue
            user_info['nickname'] = None
            try:
                user_info['nickname'] = feed['author']['nickname']
            except Exception as e:
                user_info['nickname'] = ''
            user_info['avatar'] = None
            try:
                user_info['avatar'] = feed['author']['avatar']
            except Exception as e:
                user_info['avatar'] = ''

            user_info['description'] = None
            try:
                user_info['description'] = feed['author'][
                    'verifiedinfo']['credentials']
            except Exception as e:
                user_info['description'] = ''
            user_info['experience'] = None
            try:
                user_info['experience'] = feed['author']['exp']
            except Exception as e:
                user_info['experience'] = -1
            user_info['level'] = None
            try:
                user_info['level'] = feed['author']['level']
            except Exception as e:
                user_info['level'] = -1
            user_info_trend = {}
            user_info_trend['experience'] = user_info['experience']
            user_info_trend['level'] = user_info['level']

            media_info = {}
            media_info['image'] = None
            try:
                media_info['image'] = feed['feed']['image']
            except Exception as e:
                media_info['image'] = ''
            media_info['location'] = None
            try:
                media_info['location'] = feed['feed']['location']
            except Exception as e:
                media_info['location'] = ''
            media_info["media_id"] = None
            try:
                media_info["media_id"] = feed['feed']['relateid']
            except Exception as e:
                continue
            target_document = collection.find_one(
                {"media_id": user_info['media_id'], "media_info.media_id": media_info["media_id"]})

            media_info["title"] = None
            try:
                media_info["title"] = my_strip(feed['feed']['title'])
            except Exception as e:
                media_info["title"] = ''
            media_info["duration"] = None
            try:
                media_info["duration"] = feed['feed']['duration']
            except Exception as e:
                media_info["duration"] = -1
            media_info["publish_time"] = None
            try:
                media_info["publish_time"] = feed['feed']['publishtimestamp']
            except Exception as e:
                media_info["publish_time"] = -1
            media_info["collection_time"] = current_time_to_stamp()
            media_info["update_time"] = current_time_to_stamp()
            media_info["viewers"] = None
            try:
                media_info["viewers"] = feed['feed']['watches']
            except Exception as e:
                media_info["viewers"] = -1
            media_info["praise_number"] = None
            try:
                media_info["praise_number"] = feed['feed']['praises']
            except Exception as e:
                media_info["praise_number"] = -1
            media_info_trend = {}
            media_info_trend["viewers"] = media_info["viewers"]
            media_info_trend["praise_number"] = media_info["praise_number"]
            media_info_trend["time"] = current_time_to_stamp()
            if type(target_document) == type({}):
                collection.update({"media_id": user_info['media_id'], "media_info.media_id": media_info["media_id"]}, {"$set": {
                                  "experience": user_info['experience'], "level": user_info['level'], "media_info.$.praise_number": media_info["praise_number"], "media_info.$.viewers": media_info["viewers"], "media_info.$.collection_time": current_time_to_stamp(), "media_info.$.update_time": current_time_to_stamp(), }}, True)
                collection.update({"media_id": user_info['media_id'], "media_info.media_id": media_info["media_id"]}, {
                                  "$addToSet": {"media_info.$.trend_data": media_info_trend}}, True)

            else:
                print('hello else')
                collection.update({"media_id": user_info['media_id']}, {
                                  "$set": {"level": user_info['level'], "experience": user_info['experience'], "description": user_info['description'], "avatar": user_info["avatar"], "media_id": user_info['media_id'], "nickname": user_info['nickname']},  "$addToSet": {"media_info": media_info}}, True)
                collection.update({"media_id": user_info['media_id'], "media_info.media_id": media_info["media_id"]}, {
                                  "$addToSet": {"media_info.$.trend_data": media_info_trend}}, True)
        target_url = "http://webh.huajiao.com/User/getUserFeeds?uid=%s&offset=%s" % (
            user_info['media_id'], target_dict['offset'])
        # huajiao_video_crawler(target_url,uuid)
        # print(str(type(offset)))
        target_mysql_tool = MysqlTool(*(WOM_PROD_MYSQL_TUPLE))
        query = ("UPDATE `video_platform_common_info` SET `avatar`='%s' WHERE `url`='%s'" % (
            user_info["avatar"], url))
        target_mysql_tool.cursor.execute(query)
        target_mysql_tool.cnx.commit()

        # if 'avatar' in user_info and user_info['avatar'] is not '':
        #     pic= requests.get(user_info['avatar'], timeout=10)
        #     pic_name = '%s.jpg' % uuid
        #     fp = open("/home/admin/ETL_PYTHON_3/pics/"+pic_name,'wb')
        #     fp.write(pic.content)
        #     fp.close()


def huajiao_fans_crawler(target_url, target_user_id):
    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    collection = mongo_tool.database['huajiao_user_info']
    cookies = {'__guid': '139759029.193467078783620450.1472025008821.1423',
               'CNZZDATA1255745025': '1444655122-1472023124-null%7C1477456368', 'logid': '4d889ddcb9c0f77f315ff4418841b4ff'}
    response = requests.get(
        target_url, headers={'referer': 'http://www.huajiao.com/', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
    target_list = response.json()['data']
    print(str(target_list))
    if len(target_list) != 0:
        for data in target_list:
            fans_info = {}
            fans_info['user_id'] = None
            try:
                fans_info['user_id'] = data['uid']
            except Exception as e:
                continue
            fans_info['nickname'] = None
            try:
                fans_info['nickname'] = data['name']
            except Exception as e:
                fans_info[''] = ''
            fans_info['avatar'] = None
            try:
                fans_info['avatar'] = data['avatar']
            except Exception as e:
                fans_info['avatar'] = ''
            fans_info['score'] = None
            try:
                fans_info['score'] = data['score']
            except Exception as e:
                fans_info['score'] = -1
            fans_info['description'] = None
            try:
                fans_info['description'] = data['verifiedinfo']['verifiedinfo']
            except Exception as e:
                fans_info['description'] = ''
            target_document = collection.find_one(
                {"media_id": target_user_id, "fans_info.user_id": data['uid']})
            if type(target_document) == type({}):
                collection.update({"media_id": target_user_id, "fans_info.user_id": data['uid']}, {"$set": {
                                  'fans_info.$.description': fans_info['description'], "fans_info.$.nickname": fans_info['nickname'], "fans_info.$.score": fans_info['score'], 'fans_info.$.avatar': fans_info['avatar']}}, True)
            else:
                print('hello else')
                collection.update({"media_id": target_user_id}, {
                                  "$set": {"media_id": target_user_id},  "$addToSet": {"fans_info": fans_info}}, True)


def huajiao_account_crawler(target_url, target_user_id):
    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    collection = mongo_tool.database['huajiao_user_info']
    response = requests.get(
        target_url, headers={'referer': 'http://www.huajiao.com/', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
    # print(response.text)
    tree = etree.HTML(response.text)
    personal_signature = None
    try:
        personal_signature = tree.xpath(
            "//p[@class='about']/text()")[0]
    except Exception as e:
        personal_signature = ''
    follower_number = None
    try:
        follower_number = int(tree.xpath(
            "//div[@class='info']/div/ul/li[1]/p/text()")[0])
    except Exception as e:
        follower_number = -1

    fan_number = None
    try:
        fan_number = int(tree.xpath(
            "//div[@class='info']/div/ul/li[2]/p/text()")[0])
    except Exception as e:
        fan_number = -1
    praise_number = None
    try:
        praise_number = int(tree.xpath(
            "//div[@class='info']/div/ul/li[3]/p/text()")[0])
    except Exception as e:
        praise_number = -1
    trend_data = {}
    trend_data['follower_number'] = follower_number
    trend_data['fan_number'] = fan_number
    trend_data['praise_number'] = praise_number
    trend_data['time'] = current_time_to_stamp()
    collection.update({"media_id": target_user_id}, {
        "$set": {"personal_signature": personal_signature, "follower_number": follower_number, "fan_number": fan_number, "praise_number": praise_number}, "$addToSet": {"trend_data": trend_data}}, True)


def meipai_hot_account_crawler(target_url):
    source_mysql_tool = MysqlTool(*DTS_OPT_MYSQL_TUPLE)
    query = ("SELECT `user_id` FROM `video_collection_info` WHERE `platform`=2 AND `id_status`=1 AND `from` IS NOT NULL")
    unique_id_list = source_mysql_tool.get_unique_id_list_by_sql(query)
    target_mysql_tool = MysqlTool(*DTS_OPT_MYSQL_TUPLE)
    response = requests.get(
        target_url)
    tree = etree.HTML(response.text)
    user_list = tree.xpath("//div[@class='feed-user pr']/a[1]/@href")
    print(str(user_list))
    for user in user_list:
        m = re.search('\d+', user)
        target_user_id = m.group(0)
        if target_user_id in unique_id_list:
            query = (
                "SELECT `id`,`other_from` FROM `video_collection_info` WHERE `user_id`='%s' AND `from` IS NOT NULL" % target_user_id)
            source_mysql_tool.cursor.execute(query)
            row = source_mysql_tool.cursor.fetchone()
            (row_id, other_from) = row
            other_from_list = other_from.split("#")
            if MEIPAI_HOT_URL_DICT[target_url] not in other_from_list:
                other_from_list.append(MEIPAI_HOT_URL_DICT[target_url])
                query = ("UPDATE `video_collection_info` SET `other_from`='%s' WHERE `id`=%d" % (
                    "#".join(other_from_list), row_id))
                print(query)
                target_mysql_tool.cursor.execute(query)
                target_mysql_tool.cnx.commit()

        else:
            unique_id_list.append(target_user_id)
            query = ("INSERT INTO `video_collection_info`(`user_id`,`platform`,`from`,`other_from`) VALUES('%s',%d,'%s','%s')" % (
                target_user_id, 2, MEIPAI_HOT_URL_DICT[target_url], MEIPAI_HOT_URL_DICT[target_url]))
            print(query)
            target_mysql_tool.cursor.execute(query)
            target_mysql_tool.cnx.commit()


def miaopai_video_crawler(target_user_id, url):
    mongo_tool = MongoTool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
    collection = mongo_tool.database['miaopai_user_info']
    target_mysql_tool = MysqlTool(*(WOM_PROD_MYSQL_TUPLE))
    """
    更新基本信息
    第一次更新到数据库
    """
    target_url = "http://www.miaopai.com/u/%s/relation/follow.htm" % target_user_id
    response = requests.get(
        target_url, headers={'referer': target_url, 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
    target_msg = response.text
    tree = etree.HTML(target_msg)
    if len(tree.xpath("//div[@class='pers_nav']")) == 0:
        return
    item = tree.xpath("//div[@class='pers_nav']")[0]
    suid = item.xpath(".//button/@suid")[0]
    print(suid)
    # input("........................")
    user_info = {}
    user_info["media_id"] = target_user_id
    # print(user_info["media_id"])
    user_info["nickname"] = item.xpath(".//div[@class='nav_div1']/a/@title")[0]
    # print(user_info["nickname"])
    user_info["avatar"] = item.xpath(".//div[@class='nav_div1']/a/img/@src")[0]
    # print(user_info["avatar"])
    query = ("UPDATE `video_platform_common_info` SET `avatar`='%s' WHERE `url`='%s'" % (
        user_info["avatar"], url))
    target_mysql_tool.cursor.execute(query)
    target_mysql_tool.cnx.commit()

    # if 'avatar' in user_info and user_info['avatar'] is not '':
    #     pic= requests.get(user_info['avatar'], timeout=10)
    #     pic_name = '%s.jpg' % uuid
    #     fp = open("/home/admin/ETL_PYTHON_3/pics/"+pic_name,'wb')
    #     fp.write(pic.content)
    #     fp.close()
    user_info["follower_num"] = item.xpath(".//ol/li[3]/a/text()")[0]
    # print(user_info["follower_num"])
    user_info["forward_num"] = item.xpath(".//ol/li[1]/a/text()")[0]
    # print(user_info["forward_num"])
    user_info["address"] = item.xpath(
        ".//div[@class='nav_div3']/h3[1]/text()")[0]
    # print(user_info["address"])
    user_info["personal_signature"] = item.xpath(
        ".//div[@class='nav_div3']/h3[2]/text()")[0]
    # print(user_info["personal_signature"])
    user_info["video_num"] = item.xpath(
        ".//div[@class='nav_bottom']//ul/li[1]//a/text()")[0]
    # print(user_info["video_num"])
    user_info["trans_num"] = item.xpath(
        ".//div[@class='nav_bottom']//ul/li[2]//a/text()")[0]
    # print(user_info["trans_num"])
    user_info["like_num"] = item.xpath(
        ".//div[@class='nav_bottom']//ul/li[3]//a/text()")[0]
    # print(user_info["like_num"])
    # input("..................................")
    user_info_trend = {}
    user_info_trend["time"] = current_time_to_stamp()
    user_info_trend["follower_num"] = user_info["follower_num"]
    user_info_trend["forward_num"] = user_info["forward_num"]
    user_info_trend["video_num"] = user_info["video_num"]
    user_info_trend["trans_num"] = user_info["trans_num"]
    user_info_trend["like_num"] = user_info["like_num"]

    page_count = 0
    forward_list = []

    while 1 == 2:

        page_count += 1
        target_url = "http://www.yixia.com/gu/follow?page=%d&suid=%s" % (
            page_count, suid)
        print(target_url)
        response = requests.get(
            target_url, headers={'referer': target_url, 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
        target_msg = response.json()['msg']
        print(target_msg)
        if target_msg == "":
            break
        tree = etree.HTML(target_msg)
        print(len(tree.xpath("//div[@class='box']")))
        for item in tree.xpath("//div[@class='box']"):
            follower_info = {}
            follower_info["follower_nickname"] = item.xpath(
                "./div[@class='box_top clearfix']/a/@title")[0]
            print(follower_info["follower_nickname"])
            follower_info["follower_user_id"] = item.xpath(
                "./div[@class='box_top clearfix']/a/@href")[0]
            print(follower_info["follower_user_id"])
            follower_info["follower_avatar"] = item.xpath(
                "./div[@class='box_top clearfix']/a/img/@src")[0]
            print(follower_info["follower_avatar"])
            weibo_vip_lebal = item.xpath(
                "./div[@class='box_top clearfix']/div[@class='top_txt']/h1/img/@src")
            print(str(weibo_vip_lebal))
            if len(weibo_vip_lebal) != 0:
                follower_info["weibo_vip_lebal"] = item.xpath(
                    "./div[@class='box_top clearfix']/div[@class='top_txt']/h1/img/@src")[0]
                print(follower_info["weibo_vip_lebal"])
            # input("..........................")
            follower_info["video_num"] = item.xpath(".//li[1]/a/text()")[0]
            print(item.xpath(".//li[1]/a/text()")[0])
            follower_info["forward_number"] = item.xpath(
                ".//li[2]/a/text()")[0]
            print(item.xpath(".//li[2]/a/text()")[0])
            follower_info["follower_number"] = item.xpath(
                ".//li[3]/a/text()")[0]
            print(item.xpath(".//li[3]/a/text()")[0])
            follower_info["personal_signature"] = item.xpath("./p/b/text()")[0]
            print(item.xpath("./p/b/text()")[0])
            forward_list.append(follower_info)
            # input("...........................")
    page_count = 0
    follower_list = []
    while 1 == 2:
        page_count += 1
        target_url = "http://www.yixia.com/gu/fans?page=%d&suid=%s" % (
            page_count, suid)
        response = requests.get(
            target_url, headers={'referer': target_url, 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
        target_msg = response.json()['msg']
        print(target_msg)
        if target_msg == "":
            break
        tree = etree.HTML(target_msg)
        for item in tree.xpath("//div[@class='box']"):
            follower_info = {}
            follower_info["follower_nickname"] = item.xpath(
                "./div[@class='box_top clearfix']/a/@title")[0]
            print(follower_info["follower_nickname"])
            follower_info["follower_user_id"] = item.xpath(
                "./div[@class='box_top clearfix']/a/@href")[0]
            print(follower_info["follower_user_id"])
            follower_info["follower_avatar"] = item.xpath(
                "./div[@class='box_top clearfix']/a/img/@src")[0]
            print(follower_info["follower_avatar"])
            weibo_vip_lebal = item.xpath(
                "./div[@class='box_top clearfix']/div[@class='top_txt']/h1/img/@src")
            print(str(weibo_vip_lebal))
            if len(weibo_vip_lebal) != 0:
                follower_info["weibo_vip_lebal"] = item.xpath(
                    "./div[@class='box_top clearfix']/div[@class='top_txt']/h1/img/@src")[0]
                print(follower_info["weibo_vip_lebal"])
            # input("..........................")
            follower_info["video_num"] = item.xpath(".//li[1]/a/text()")[0]
            print(item.xpath(".//li[1]/a/text()")[0])
            follower_info["forward_number"] = item.xpath(
                ".//li[2]/a/text()")[0]
            print(item.xpath(".//li[2]/a/text()")[0])
            follower_info["follower_number"] = item.xpath(
                ".//li[3]/a/text()")[0]
            print(item.xpath(".//li[3]/a/text()")[0])
            follower_info["personal_signature"] = item.xpath("./p/b/text()")[0]
            print(item.xpath("./p/b/text()")[0])
            follower_list.append(follower_info)
    collection.update({"media_id": target_user_id}, {
                      "$set": {"media_id": user_info["media_id"], "nickname": user_info["nickname"], "avatar": user_info["avatar"], "follower_num": user_info["follower_num"], "forward_num": user_info["forward_num"], "address": user_info["address"], "personal_signature": user_info["personal_signature"], "video_num": user_info["video_num"], "trans_num": user_info["trans_num"], "like_num": user_info["like_num"], "forward_info": forward_list, "follower_info": follower_list}, "$addToSet": {"trend_data": user_info_trend}}, True)
    """
    更新视频信息
    """

    page_count = 0
    while 1 == 2:
        page_count += 1
        target_url = "http://www.yixia.com/gu/u?page=%d&suid=%s" % (
            page_count, suid)
        print(target_url)
        response = requests.get(
            target_url, headers={'referer': 'http://www.yixia.com/u/%s' % target_user_id, 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
        target_msg = response.json()['msg']
        if target_msg == "":
            break
        print(target_msg)
        tree = etree.HTML(target_msg)
        for item in tree.xpath("//div[@class='D_video']"):
            media_info = {}
            # print(str(item))
            media_info["media_id"] = item.xpath("./@data-scid")[0]
            # print(scid)
            media_info["media_img"] = item.xpath(
                "./div[@class='video_img']/img/@src")[0]
            # print(media_img)
            media_info["media_flash"] = item.xpath(
                "./div[@class='video_flash']/@va")[0]
            # print(media_flash)
            media_info["publish_time"] = item.xpath(
                ".//div[@class='D_head_name']/h2//text()")[0]
            # print(publish_time)
            media_info["viewers"] = item.xpath(
                ".//div[@class='D_head_name']/h2//text()")[1].strip()
            # print(viewers)
            media_info["media_description"] = item.xpath(
                ".//div[@class='introduction']/p/text()")[0]
            # print(media_description)
            media_info["praise_number"] = item.xpath(
                ".//a[@title='赞']/text()")[0]
            # print(praise_number)
            media_info["comment_number"] = item.xpath(
                ".//a[@title='评论']/text()")[0]
            update_time = current_time_to_stamp()
            media_info_trend = {"time": current_time_to_stamp(), "viewers": media_info["viewers"], "praise_number": media_info[
                "praise_number"], "comment_number": media_info["comment_number"]}

            target_document = collection.find_one(
                {"media_id": target_user_id, "media_info.media_id": media_info["media_id"]})

            if type(target_document) == type({}):
                collection.update({"media_id": target_user_id, "media_info.media_id": media_info["media_id"]}, {"$set": {
                                  "media_info.$.comment_number": media_info["comment_number"], "media_info.$.praise_number": media_info["praise_number"], "media_info.$.viewers": media_info["viewers"], "media_info.$.update_time": update_time}}, True)
                collection.update({"media_id": target_user_id, "media_info.media_id": media_info["media_id"]}, {
                                  "$addToSet": {"media_info.$.trend_data": media_info_trend}}, True)

            else:
                print('hello else')
                collection.update({"media_id": target_user_id}, {
                                  "$set": {"media_id": target_user_id},  "$addToSet": {"media_info": media_info}}, True)
                collection.update({"media_id": target_user_id, "media_info.media_id": media_info["media_id"]}, {
                                  "$addToSet": {"media_info.$.trend_data": media_info_trend}}, True)
            target_url = "http://www.miaopai.com/miaopai/get_v2_comments?scid=%s&per=100" % media_info[
                "media_id"]
            print(target_url)
            response = requests.get(target_url, headers={'referer': 'http://www.miaopai.com/show/%s.htm' % media_info[
                                    "media_id"], 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
            target_msg = response.text
            tree = None
            try:
                tree = etree.HTML(target_msg)
            except Exception as e:
                continue
            finally:
                pass

            print(len(tree.xpath("//li")))
            comment_list = []
            for item in tree.xpath("//li"):
                comment_info = {}
                # print(item.text)
                comment_info["comment_user_id"] = item.xpath("./a/@href")[0]
                print(comment_info["comment_user_id"])
                comment_info["comment_user_nickname"] = item.xpath(
                    "./a/@title")[0]
                print(comment_info["comment_user_nickname"])

                comment_info["comment_user_avatar"] = item.xpath(
                    "./a/img/@src")[0]
                print(comment_info["comment_user_avatar"])
                comment_info["comment_time"] = item.xpath(
                    "./div[@class='pername']/strong[1]/text()")[0]
                print(comment_info["comment_time"])
                comment_info["comment_detail"] = item.xpath(
                    "./div[@class='pername']/span/text()")[0]
                print(comment_info["comment_detail"])
                comment_list.append(comment_info)
                # input("............................")
            collection.update({"media_id": target_user_id}, {
                              "$set": {"comment_info.%s" % media_info["media_id"]: comment_list}}, True)


def sogou_pinyin_dict_download(target_url,target_file_args):
    response = requests.get(
        target_url, headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
    target_file_tool=get_file_tool(*target_file_args)
    target_file_tool.f.write(response.text.encode("utf-8","ignore"))
    