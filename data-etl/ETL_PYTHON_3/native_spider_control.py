from tools.tool_mongo import MongoTool
from tools.tool_mysql import MysqlTool
from tools.tool_file import FileTool
from tools.settings import MEIPAI_HOT_URL_DICT,WOM_PROD_1_TUPLE, DTS_OPT_MYSQL, DEV_WOM2_MYSQL, WOM_PROD_MYSQL, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO, WOM_DTS_DATAWAREHOSE_225_MONGO, WOM_DTS_DATAWAREHOSE_32_MONGO, WOM_DTS_DATAWAREHOSE_249_MONGO, WOM_DTS_DATAWAREHOSE_70_MONGO, WOM_DTS_DATAWAREHOSE_102_MONGO, BACKUP_JSON_PATH, CHECK_CSV_PATH, DTS_OPT_MYSQL_TUPLE, DTS_OPT_MYSQL_TUPLE, WOM_PROD_MYSQL_TUPLE, WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_225_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_32_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_249_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_70_MONGO_TUPLE, WOM_DTS_DATAWAREHOSE_102_MONGO_TUPLE, CHECK_SQL_PATH, UNI_MONGO_TUPLE, ERROR_FILE_ARGS
import multiprocessing
import time
from libs.media_weixin_check import MediaWeiXinCheck
from libs.media_weixin_storage import MediaWeiXinStorage
from libs.media_weixin_backups import MediaWeixinBackups
import os
import sys
import getopt
from tools.tool_time import current_time_to_stamp, str_to_time_stamp
import datetime
from spiders.native_spider import huajiao_video_crawler, huajiao_fans_crawler, huajiao_account_crawler,meipai_hot_account_crawler,miaopai_video_crawler,sogou_pinyin_dict_download
from spiders.media_weixin_account_tags import bluemc_media_weixin_account_tags_spider,newrank_media_weixin_account_tags_spider
from models.media_weixin_model import MediaWeixin

def usage():
    print('help me............')


def get_mysql_tool(host, db, user, pwd):
    mysql_tool = MysqlTool(host, db, user, pwd)
    return mysql_tool


def get_file_tool(file_path, file_name, open_status):
    file_tool = FileTool(file_path, file_name, open_status)
    return file_tool

def get_mongo_tool(host, port, db, user, pwd):
    mongo_tool = MongoTool(host, port, db, user, pwd)
    return mongo_tool

def main(argv):
    spider_name = None
    try:
        opts, args = getopt.getopt(argv[1:], 'h', ['help=', 'spider_name='])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ('-h', '--help'):
            sys.exit(1)
        elif o in ('-f', '--spider_name'):
            spider_name = a
    if spider_name == "huajiao_video":
        mysql_tool = get_mysql_tool(*DTS_OPT_MYSQL_TUPLE)
        query = ("SELECT `uuid` FROM `video_collection_info` WHERE `platform`=1 AND `crawling_status`=0 AND `id_status`=1  AND `from` IS NOT NULL")
        unique_id_list = mysql_tool.get_unique_id_list_by_sql(query)
        count = 0
        for uuid in unique_id_list:
            query = ("SELECT `user_id` FROM `video_collection_info` WHERE `uuid`='%s'" % uuid)
            mysql_tool.cursor.execute(query)
            (unique_id,)=mysql_tool.cursor.fetchone()
            count += 1
            target_url = "http://webh.huajiao.com/User/getUserFeeds?uid=%s" % unique_id
            huajiao_video_crawler(target_url,uuid)
            print("total tasks:%d;settled tasks:%d;settled rate:%.2f" %
                  (len(unique_id_list), count, (count / len(unique_id_list))))
    elif spider_name == "huajiao_fans":
        mysql_tool = get_mysql_tool(*DTS_OPT_MYSQL_TUPLE)
        query = ("SELECT `user_id` FROM `video_collection_info` WHERE `platform`=1 AND `crawling_status`=0 AND `id_status`=1  AND `from` IS NOT NULL")
        unique_id_list = mysql_tool.get_unique_id_list_by_sql(query)
        count = 0
        for unique_id in unique_id_list:
            for i in range(1, 5):
                offset = int((i - 1) * 15)
                target_url = "http://webh.huajiao.com/fansrank?uid=%s&offset=%d" % (
                    unique_id, offset)
                print(target_url)
                #input('............%d' % offset)
                huajiao_fans_crawler(target_url, unique_id)
            count += 1
            print("total tasks:%d;settled tasks:%d;settled rate:%.2f" %
                  (len(unique_id_list), count, (count / len(unique_id_list))))
    elif spider_name=="huajiao_account":
        mysql_tool = get_mysql_tool(*DTS_OPT_MYSQL_TUPLE)
        query = ("SELECT `user_id` FROM `video_collection_info` WHERE `platform`=1 AND `crawling_status`=0 AND `id_status`=1  AND `from` IS NOT NULL")
        unique_id_list = mysql_tool.get_unique_id_list_by_sql(query)
        count = 0
        for unique_id in unique_id_list:
            target_url = "http://www.huajiao.com/user/%s" % unique_id
            print(target_url)
            #input('............%d' % offset)
            huajiao_account_crawler(target_url,unique_id)
            count += 1
            print("total tasks:%d;settled tasks:%d;settled rate:%.2f" %
                  (len(unique_id_list), count, (count / len(unique_id_list))))
    elif spider_name == "meipai_hot_account":
        for k,v in MEIPAI_HOT_URL_DICT.items():
            meipai_hot_account_crawler(k)
    elif spider_name =="miaopai_video":
        # query=("SELECT `user_id`,`uuid` FROM `video_collection_info` WHERE `platform`=3 AND `id_status`=1 ")
        query=("SELECT `url` FROM `video_platform_common_info` WHERE `platform_type`=5")
        # source_mysql_tool.cursor.execute(query)

        source_mysql_tool = MysqlTool(*WOM_PROD_MYSQL_TUPLE)
        source_mysql_tool.cursor.execute(query)
        unique_id_list=[]
        for row in source_mysql_tool.cursor:
            (url,)=row

            unique_id_list.append((url,url.split("/")[-1]))
            # (user_id,uuid)=row
            # unique_id_list.append((user_id,uuid))
        source_mysql_tool.disconnection()
        for unique_id in unique_id_list:
            (url,user_id)=unique_id
            miaopai_video_crawler(user_id,url)
    elif spider_name=="sogou_pinyin":
        target_url = "http://download.pinyin.sogou.com/dict/download_cell.php?id=15201&name=%E9%A5%AE%E9%A3%9F%E5%A4%A7%E5%85%A8%E3%80%90%E5%AE%98%E6%96%B9%E6%8E%A8%E8%8D%90%E3%80%91"
        sogou_pinyin_dict_download(target_url,(CHECK_CSV_PATH, 'sogou_pinyin.csv', 'ab'))
    elif spider_name == "bluemc":
        error_file_tool = get_file_tool(*(CHECK_CSV_PATH, 'bluemc_account_tags_not_crawled.csv', 'ab'))
        source_mongo_tool = get_mongo_tool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)

        headers = {
            "Host": "bang.bluemc.cn",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Referer": "http//bang.bluemc.cn/billBoard/detail.do?urn=3136949-yetingfm",
            "Accept-Encoding": "gzip, deflate, sdch",
            "Accept-Language": "zh-CN,zh;q=0.8"
        }
        cookies = {
            "route": "9f512c79ab17c4d77bc715b14cdac17e",
            "Hm_lvt_7661f5a78a72e3a4e7aab72d9b7fb6d9": "1494235935,1494293819,1494321312,1494405219",
            "Hm_lpvt_7661f5a78a72e3a4e7aab72d9b7fb6d9": "1494405984",
            "JSESSIONID": "11CA497D46AFBE689A09F4E12D68663C"
        }
        correct_count=0
        error_count=0
        for document in source_mongo_tool.database["media_weixin"].find({},{"public_id":1}):
            # public_id = "hzsmiuo"
            public_id=document["public_id"]
            correct_count+=1
            if correct_count <= 87513:
                continue
            try:
                bluemc_media_weixin_account_tags_spider(headers,cookies,public_id,(CHECK_CSV_PATH, 'bluemc_account_tags_scrawed.csv', 'ab'))
            except Exception as e:
                error_count+=1
                error_file_tool.write_database_line_to_file(public_id + "\n")
            print("settled %d accounts;error %d accounts;current account is %s" % (correct_count,error_count,public_id))
            # time.sleep(1)
        error_file_tool.close_file()
    elif spider_name == "newrank":
        error_file_tool = get_file_tool(*(CHECK_CSV_PATH, 'newrank_account_tags_not_crawled.csv', 'ab'))
        source_mongo_tool = get_mongo_tool(*WOM_DTS_DATAWAREHOSE_CLUSTER_MONGO_TUPLE)
        correct_count=0
        error_count=0
        for document in source_mongo_tool.database["media_weixin"].find({},{"public_id":1}):
            # public_id = "hzsmiuo"
            public_id=document["public_id"]
            correct_count+=1
            if correct_count <= 174702:
                continue
            try:
                newrank_media_weixin_account_tags_spider(public_id,(CHECK_CSV_PATH, 'newrank_account_tags_scrawed.csv', 'ab'))
            except Exception as e:
                error_count+=1
                error_file_tool.write_database_line_to_file(public_id + "\n")
            print("settled %d accounts;error %d accounts;current account is %s" % (correct_count,error_count,public_id))
            

if __name__ == '__main__':
    main(sys.argv)
