# coding=utf-8
import time
import signal
from tools.tool_mysql import MysqlTool
from tools.tool_mongo import MongoTool
from tools.tool_file import FileTool
# import os
# from time import sleep
from multiprocessing import Process, Queue
from pykafka import KafkaClient, SslConfig
from tools.settings import ZOOKEEPER,TARGET_COLUMN_TUPLE_KAFKA, WOM_DEV_MYSQL_TUPLE, KAFKA, WOM_DTS_OPS_MONGO_TUPLE, BACKUP_CSV_PATH_BIG_DATA
from tools.tool_string import strip_and_replace_all, strip_and_replace
from pykafka.partition import Partition
from tools.tool_time import datetime_to_str, current_time_to_stamp
import time
import os
import datetime
import json
# def onsignal_term(a, b):
#     print('收到SIGTERM信号')

# # 这里是绑定信号处理函数，将SIGTERM绑定在函数onsignal_term上面
# signal.signal(signal.SIGTERM, onsignal_term)


# def onsignal_usr1(a, b):
#     print('收到SIGUSR1信号')
# # 这里是绑定信号处理函数，将SIGUSR1绑定在函数onsignal_term上面
# signal.signal(signal.SIGUSR1, onsignal_usr1)

# while 1:
#     print('我的进程id是 %d' % os.getpid())
#     sleep(10)
def weixin_article_csv_consumer_kafka(target_mysql_args, kafka_args):
    current_day = datetime_to_str(datetime.datetime.now())
    target_file = "article_%s.csv" % current_day
    target_file_args_1 = (BACKUP_CSV_PATH_BIG_DATA +
                          '/index_compute', target_file, "ab")
    target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                          '/machine_learning', target_file, "ab")
    target_file_tool_1 = FileTool(*target_file_args_1)
    target_file_tool_2 = FileTool(*target_file_args_2)
    target_mysql_tool = MysqlTool(*target_mysql_args)
    # target_file_tool_3 = self.get_file_tool(*target_file_args_3)
    
    crash_flag = 0
    try:
        client = KafkaClient(hosts=kafka_args["hosts"])
        
        # input("..................")
        topics = client.topics
        print(str(kafka_args["topics"][3] in topics))
        

        if kafka_args["topics"][3] in topics:
            
            # print(
            #     "kafka_args................................................................")
            article_info_crawl_topic = topics[kafka_args["topics"][3]]
            consumer = article_info_crawl_topic.get_balanced_consumer(
                consumer_group=b"uniwow",auto_commit_enable=False,zookeeper_connect=ZOOKEEPER)
            message_count = 0

            for message in consumer:
                # print(
                #     "kafka_args................................................................")
                # input("..................")
                if message is not None:
                    message_count += 1
                    # print(message.offset, message.value.decode(
                    #     "utf-8", 'ignore'))
                    message_dict = json.loads(
                        message.value.decode("utf-8", 'ignore'))
                    index_compute_item_list = []
                    for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                        index_compute_item_list.append(
                            strip_and_replace_all(str(message_dict[k])))
                    try:
                        target_file_tool_1.write_database_line_to_file(
                            ",".join(index_compute_item_list) + "," + str(message.offset) + "\n")
                    except Exception as e:
                        pass
                        #"to do list"
                    finally:
                        pass

                    machine_learning_item_list = []
                    for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                        machine_learning_item_list.append(
                            strip_and_replace_all(str(message_dict[k])))
                    try:
                        target_file_tool_2.write_database_line_to_file(
                            "*|233|*|".join(machine_learning_item_list) + "\n")
                    except Exception as e:
                        pass
                        #"to do list"
                    finally:
                        pass
                    # weixin_article_item_list = []
                    is_new_crawled_article = message_dict[
                        "is_new_crawled_article"]
                    print("is_new_crawled_article type %s -> %s" %
                          (type(is_new_crawled_article), str(is_new_crawled_article)))
                    # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
                    #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
                    #     weixin_article_item_list.append(
                    #         strip_and_replace_all(str(message_dict[k])))
                    mongo_obj_id = strip_and_replace_all(
                        strip_and_replace(str(message_dict["mongo_object_id"])))
                    weixin_id = strip_and_replace_all(
                        strip_and_replace(str(message_dict["weixin_id"])))
                    title = strip_and_replace_all(
                        strip_and_replace(str(message_dict["title"])))
                    article_url = strip_and_replace_all(
                        strip_and_replace(str(message_dict["fixed_url"])))
                    short_desc = strip_and_replace_all(
                        strip_and_replace(str(message_dict["digest"])))
                    article_type = int(message_dict["article_type"])
                    article_pos = int(message_dict["article_pos"])
                    read_num = int(message_dict["page_view_num"])
                    like_num = int(message_dict["page_like_num"])
                    post_time = int(message_dict["post_time"])

                    if int(is_new_crawled_article) == 0:
                        query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d WHERE `mongo_obj_id`='%s'" % (
                            weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, mongo_obj_id))
                        print(query)
                        # input("..................")
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                        except Exception as e:
                            # print(e.)
                            pass
                        finally:
                            pass

                    elif int(is_new_crawled_article) == 1:
                        query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
                            mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
                        print(query)
                        try:
                            target_mysql_tool.cursor.execute(query)
                            target_mysql_tool.cnx.commit()
                        except Exception as e:
                            # print(e.)
                            pass
                        finally:
                            pass
                    # 标记消息为已消费
                    # consumer.commit_offsets()

                    if datetime_to_str(datetime.datetime.now()) != current_day:
                        source_mongo_tool.disconnection()
                        target_file_tool_1.close_file()
                        target_file_tool_2.close_file()
                        target_mysql_tool.disconnection()
                        current_day = datetime_to_str(datetime.datetime.now())
                        target_file = "article_%s.csv" % current_day
                        target_file_args_1 = (BACKUP_CSV_PATH_BIG_DATA +
                                              '/index_compute', target_file, "ab")
                        target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                                              '/machine_learning', target_file, "ab")
                        source_mongo_tool = self.get_mongo_tool(
                            *source_mongo_args)
                        target_file_tool_1 = self.get_file_tool(
                            *target_file_args_1)
                        target_file_tool_2 = self.get_file_tool(
                            *target_file_args_2)
                        target_mysql_tool = self.get_mysql_tool(
                            *target_mysql_args)
                    # target_file_tool_3.write_database_line_to_file(
                    #     "*|233|*|".join(weixin_article_item_list) + "\n")
                    # ducument_cursor=source_mongo_tool.database['kafka_conventional_grow_trend_info'].find({},{"kafka_message":1}).limit(5)
                    # ducument_cursor.batch_size(500)
    except Exception as e:
        crash_flag = 1

    finally:
        target_file_tool_1.close_file()
        target_file_tool_2.close_file()
        target_mysql_tool.disconnection()
        print("game over..............................kafka")
    return crash_flag


def weixin_article_csv_consumer_mongo(target_mysql_args):
    source_mongo_tool = MongoTool(*WOM_DTS_OPS_MONGO_TUPLE)
    current_day = datetime_to_str(datetime.datetime.now())
    target_file = "article_%s.csv" % current_day
    target_file_args_1 = (BACKUP_CSV_PATH_BIG_DATA +
                          '/index_compute', target_file, "ab")
    target_file_args_2 = (BACKUP_CSV_PATH_BIG_DATA +
                          '/machine_learning', target_file, "ab")
    target_file_tool_1 = FileTool(*target_file_args_1)
    target_file_tool_2 = FileTool(*target_file_args_2)
    target_mysql_tool = MysqlTool(*target_mysql_args)
    break_flag = 0
    try:
        settled_count = 0
        while True:

            collection_count = source_mongo_tool.database[
                'kafka_conventional_grow_trend_info'].find({}).count()
            '''
            查看source collection 的数目
            如果为零，等待3秒，再次查询，如果还无数据，则假设kafka成功
            等待3秒是假设mongo collection 插入延时
            '''
            if collection_count == 0:
                time.sleep(5)
                collection_count = source_mongo_tool.database[
                    'kafka_conventional_grow_trend_info'].find({}).count()
                if collection_count == 0:
                    source_mongo_tool.disconnection()
                    target_file_tool_1.close_file()
                    target_file_tool_2.close_file()
                    target_mysql_tool.disconnection()
                    break_flag = 1
                    break

            document = source_mongo_tool.database[
                'kafka_conventional_grow_trend_info'].find_one({}, {"kafka_message": 1})
            mongo_object_id = document['_id']
            message_dict = json.loads(
                document['kafka_message'].decode("utf-8", 'ignore'))
            index_compute_item_list = []
            for k in TARGET_COLUMN_TUPLE_KAFKA[0]:
                index_compute_item_list.append(
                    strip_and_replace_all(str(message_dict[k])))
            try:
                target_file_tool_1.write_database_line_to_file(
                    ",".join(index_compute_item_list) + "\n")
            except Exception as e:
                print('........................')
                pass
                #"to do list"
            finally:
                pass

            machine_learning_item_list = []
            for k in TARGET_COLUMN_TUPLE_KAFKA[1]:
                machine_learning_item_list.append(
                    strip_and_replace_all(str(message_dict[k])))
            try:
                target_file_tool_2.write_database_line_to_file(
                    "*|233|*|".join(machine_learning_item_list) + "\n")
            except Exception as e:
                pass
                #"to do list"
            finally:
                pass
            # weixin_article_item_list = []
            is_new_crawled_article = message_dict[
                "is_new_crawled_article"]
            print("is_new_crawled_article type %s -> %s" %
                  (type(is_new_crawled_article), str(is_new_crawled_article)))
            # for k in TARGET_COLUMN_TUPLE_KAFKA[2]:
            #     "weixin_id","title","fixed_url","digest","article_pos","page_view_num","page_like_num","post_time"
            #     weixin_article_item_list.append(
            #         strip_and_replace_all(str(message_dict[k])))
            mongo_obj_id = strip_and_replace_all(
                strip_and_replace(str(message_dict["mongo_object_id"])))
            weixin_id = strip_and_replace_all(
                strip_and_replace(str(message_dict["weixin_id"])))
            title = strip_and_replace_all(
                strip_and_replace(str(message_dict["title"])))
            article_url = strip_and_replace_all(
                strip_and_replace(str(message_dict["fixed_url"])))
            short_desc = strip_and_replace_all(
                strip_and_replace(str(message_dict["digest"])))
            article_type = int(message_dict["article_type"])
            article_pos = int(message_dict["article_pos"])
            read_num = int(message_dict["page_view_num"])
            like_num = int(message_dict["page_like_num"])
            post_time = int(message_dict["post_time"])

            if int(is_new_crawled_article) == 0:
                query = ("UPDATE `weixin_article` SET `weixin_id`='%s',`title`='%s',`article_url`='%s',`short_desc`='%s',`article_type`=%d,`article_pos`=%d,`read_num`=%d,`like_num`=%d,`post_time`=%d WHERE `mongo_obj_id`='%s'" % (
                    weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time, mongo_obj_id))
                print(query)
                # input("..................update")
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    # print(e.)
                    pass
                finally:
                    pass

            elif int(is_new_crawled_article) == 1:
                query = ("INSERT INTO `weixin_article`(`mongo_obj_id`,`weixin_id`,`title`,`article_url`,`short_desc`,`article_type`,`article_pos`,`read_num`,`like_num`,`post_time`) VALUES('%s','%s','%s','%s','%s',%d,%d,%d,%d,%d)" % (
                    mongo_obj_id, weixin_id, title, article_url, short_desc, article_type, article_pos, read_num, like_num, post_time))
                print(query)
                # input("..................input")
                try:
                    target_mysql_tool.cursor.execute(query)
                    target_mysql_tool.cnx.commit()
                except Exception as e:
                    # print(e.)
                    pass
                finally:
                    pass
            # source_mongo_tool.database['kafka_conventional_grow_trend_info'].remove(
            #     {'_id': mongo_object_id})

            settled_count += 1
            # if settled_count == 5:
            #     break
            print('settled %d document' % settled_count)
    except Exception as e:
        pass
    finally:
        source_mongo_tool.disconnection()
        target_file_tool_1.close_file()
        target_file_tool_2.close_file()
        target_mysql_tool.disconnection()
        print("game over..............................mongo")

    return break_flag


def process_control():
    kafka_crash_count = -1
    mongo_break_flag = 0
    kafka_crash_flag = 0
    while True:
        try:
            kafka_crash_count += 1

            print("Welcome to kafka. Work Start! It's your %d times" %
                  kafka_crash_count)
            if kafka_crash_count == 0:
                kafka_crash_flag = weixin_article_csv_consumer_kafka(
                    WOM_DEV_MYSQL_TUPLE, KAFKA)
                mongo_break_flag == 0
            if mongo_break_flag == 1:
                kafka_crash_flag = weixin_article_csv_consumer_kafka(
                    WOM_DEV_MYSQL_TUPLE, KAFKA)
                mongo_break_flag == 0
            if kafka_crash_flag == 1:
                mongo_break_flag = weixin_article_csv_consumer_mongo(
                    WOM_DEV_MYSQL_TUPLE)
                kafka_crash_flag = 0
            if kafka_crash_count==5:
            	break
        except Exception as e:
            print("process_control crash...............")
            input("........................")
            pass
        finally:
            pass


def func_1(name, processName, q):
    print('Process[%s]  hello %s' % (processName, name))
    print('sub pid: %d, ppid: %d' % (os.getpid(), os.getppid()))
    q.put("get out1")


def func_2(name, processName, q):
    time.sleep(1)
    print('Process[%s]  hello %s' % (processName, name))
    print('sub pid: %d, ppid: %d' % (os.getpid(), os.getppid()))
    print("q empty %s" % str(q.empty()))
    if not q.empty():
        print("get .............1%s" % q.get())
    if not q.empty():
        print("get .............2%s" % q.get())


def main():
    q = Queue(1)
    print('main pid: %d, ppid: %d' % (os.getpid(), os.getppid()))
    processList = []
    pro = Process(target=func_1, args=(1, 'Process-' + str(1), q))
    pro.start()
#         pro.join()      # 不要在此处阻塞子进程，否则子进程都将是线性顺序执行
    processList.append(pro)
    pro = Process(target=func_2, args=(2, 'Process-' + str(2), q))
    pro.start()

    for pro in processList:
        pro.join()      # 在此处阻塞子进程，可实现异步执行效果，直至子进程全部完成后再继续执行父进程

# 测试
if __name__ == '__main__':
    process_control()
    print('end.')
