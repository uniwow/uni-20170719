#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
微信舆情，根据条件统计文章和账号数
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: wx_public_option.py
@time: 2017/4/10 16:36
"""

import json
import pprint

from tornado.web import RequestHandler
from elasticsearch import Elasticsearch

from QMGPlatformService import ES_CLUSTER_PUBLIC


class WXPublicOption(RequestHandler):

    def get(self):
        self.write(json.dumps(self.__count()))

    def post(self):
        self.write(json.dumps(self.__count()))

    def __count(self) -> dict:
        """
        根据条件统计账号的文章和账号数量
        :return: 
        """

        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, HEAD')

        result = {
            "success": True,
            "data": {
                "article": 0,
                "account": 0
            }
        }

        excluded_query_words = self.get_argument("excluded_query_words", "").strip()
        required_query_words = self.get_argument("required_query_words", "").strip()
        optional_query_words = self.get_argument("optional_query_words", "").strip()
        period_start = self.get_argument("period_start", "").replace("-", "").strip()
        period_end = self.get_argument("period_end", "").replace("-", "")

        if excluded_query_words or required_query_words or optional_query_words:

            query_condition = {
                "query": {
                    "bool": {
                        "must_not": [],
                        "must": [],
                    }
                },
                "aggs": {
                    "account_count": {
                        "cardinality": {
                            "field": "public_id.raw"
                        }
                    }
                }
            }
            range_ = {"range": {"post_date": {}}}
            if period_end:
                range_["range"]["post_date"]["lte"] = period_end
            if period_start:
                range_["range"]["post_date"]["gte"] = period_start
            if len(range_["range"]["post_date"]) > 0:
                query_condition["query"]["bool"]["must"].append(range_)

            query_string = ""

            if len(required_query_words) > 0:
                query_string += "(" + " AND ".join(
                    [term for term in required_query_words.split(" ") if len(term) > 0]) + ")"

            if optional_query_words:
                query_string += " AND " if query_string != "" else ""
                query_string += "(" + " OR ".join(
                    [term for term in optional_query_words.split(" ") if len(term) > 0]) + ")"

            if excluded_query_words:
                query_condition["query"]["bool"]["must_not"] = [
                    {
                        "query_string": {
                            "default_field": "content",
                            "query": excluded_query_words,
                            "default_operator": "AND",
                            "split_on_whitespace": True
                        }
                    }
                ]

            if query_string != "":
                query_condition["query"]["bool"]["must"].append({
                        "query_string": {
                            "default_field": "content",
                            "query": query_string,
                            "split_on_whitespace": True
                        }
                    })

            pprint.pprint(query_condition)
            es_cnn = Elasticsearch(hosts=ES_CLUSTER_PUBLIC)
            response = es_cnn.search(index="qmg_dts_sogou_weixin",
                                     body=query_condition,
                                     doc_type="media_weixin_article",
                                     filter_path="aggregations,hits.total"
                                     )
            result["data"]["article"] = response["hits"]["total"]
            result["data"]["account"] = response["aggregations"]["account_count"]["value"]

        return result
