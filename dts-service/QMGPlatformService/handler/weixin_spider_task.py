#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
用于将微信请求账号历史页文章的请求写到任务队列里面
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: weixin_spider_task.py
@time: 2017/5/3 13:21
"""
from datetime import datetime
from urllib import parse
import json
import pytz

from tornado.web import RequestHandler
from peewee import DoesNotExist

from QMGPlatformService.common.pyredis import Redis
from QMGPlatformService.common.ops_models import WeixinCookiePool
from QMGPlatformService import ConfigReader

timezone = pytz.timezone("Asia/Shanghai")

TASK_QUEUE_NAME = "WeixinRequestTaskQueue"


class WeixinRequestTaskHandler(RequestHandler):

    def post(self):
        """
        微信cookie pool 构建
        :return: 
        """

        method = self.get_argument("method")
        url = self.get_argument("url")
        headers = self.get_argument("headers")
        body = self.get_argument("body")
        headers = dict(list(map(lambda item: tuple(item.split("=")), headers.split("&"))))
        url_parse = parse.urlparse(url)
        url_parse_params = parse.parse_qs(url_parse.query)
        url_parse_params = {key: value[0] for key, value in url_parse_params.items()}

        write_result = {
            "success": True
        }

        if "action" in url_parse_params and url_parse_params["action"] == "home":

            pass_ticket = url_parse_params["pass_ticket"]
            redis = Redis(**dict(ConfigReader.items("redis")))
            redis.connection.rpush(TASK_QUEUE_NAME, json.dumps({
                "headers": headers,
                "url": url,
                "method": method,
                "body": body,
                "pass_ticket": pass_ticket
            }))

            # 请求进入cookie池用作文章监控使用
            # try:
            #     model = WeixinCookiePool.get(WeixinCookiePool.pass_ticket == pass_ticket)
            # except DoesNotExist:
            #     model = WeixinCookiePool()
            #
            # model.pass_ticket = pass_ticket
            # model.url = url
            # model.method = method
            # model.is_expired = 0
            # model.headers = json.dumps(headers)
            # model.created_at = int(datetime.now(tz=timezone).timestamp())
            # model.save()

        self.write(write_result)
