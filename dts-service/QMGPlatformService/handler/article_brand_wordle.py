# encoding: utf-8

from threading import Thread
import json
import collections
import time

from tornado.web import RequestHandler
import jieba.analyse
import jieba.posseg as pseg

from QMGPlatformService import package_name
from tornado.escape import json_decode

STATIC_FILE_DIR = "./%s/static/wordle" % package_name
jieba.load_userdict("%s/brand_dict_utf_remained_posseg.txt" % STATIC_FILE_DIR)
# jieba.analyse.set_stop_words("%s/brand_stopwords.txt" % STATIC_FILE_DIR)


class BrandWordleHandler(RequestHandler):
    def post(self):
        # 需要分词的文本
        # reqests_data = tornado.escape.json_decode(self.request.body)
        # print(str(self.request.body,encoding = "utf8"))
        # print(self.get_body_argument("title"))
        # data = json.loads(str(self.request.body,encoding = "utf8"))
        title = self.get_body_argument("title")
        content = self.get_body_argument("content")
        digest = self.get_body_argument("digest")

        # 返回的结果数
        if len(title + content + digest) > 0:
            text = (title + " ") + (content + " ") + (digest + " ")
            print(text)
            words = pseg.cut(text,HMM=False)
            word_list = []
            for w in words:
                print(w)
                if w.flag == "brand":
                    word_list.append(w.word)
            word_count_list = []
            for key,value in collections.Counter(word_list).most_common():
                word_count_item = {}
                word_count_item["name"] = key
                word_count_item["value"] = value
                word_count_list.append(word_count_item)
            self.write(json.dumps({
                "success": True,
                "wordle": word_count_list
            }, ensure_ascii=False))
