#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: weixin_cookie_pool.py
@time: 2017/5/10 16:07
"""

from urllib import parse
import json

from tornado.web import RequestHandler
from peewee import DoesNotExist, RawQuery

from QMGPlatformService.common.ops_models import WeixinCookiePool as WeixinCookiePoolModel
from QMGPlatformService.common.authenticate import authenticate


class WeixinCookiePool(RequestHandler):

    def delete(self):
        pass_ticket = self.get_argument("pass_ticket")
        result = {
            "error": False
        }
        try:
            model = WeixinCookiePoolModel.get(WeixinCookiePoolModel.pass_ticket == pass_ticket)
            model.is_expired = 1
            model.save()
        except DoesNotExist:
            result["error"] = True
            result["msg"] = "the record does'n exist!"

        self.write(result)

    @authenticate(module_name="cookie-pool")
    def get(self):
        """
        返回一条可用cookie
        :return: 
        """
        rq = RawQuery(WeixinCookiePoolModel,
                      "SELECT id, pass_ticket, url, headers "
                      "FROM weixin_cookie_pool "
                      "WHERE "
                      "    is_expired = 0 "
                      "ORDER BY created_at desc, rand() "
                      "LIMIT 1")
        results = rq.tuples()
        data = {
            "success": True,
            "cookies": [],
        }

        for id_, pass_ticket, url, headers in results:
            data["cookies"].append({
                "id": id_,
                "pass_ticket": pass_ticket,
                "url": parse.unquote(url),
                "headers": json.loads(parse.unquote(headers))
            })

        self.write(data)
