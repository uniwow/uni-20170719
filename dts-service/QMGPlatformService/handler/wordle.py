# encoding: utf-8

from threading import Thread
import json
import collections
import time

from tornado.web import RequestHandler
import jieba.analyse

from QMGPlatformService.common.authenticate import authenticate
from QMGPlatformService import package_name

STATIC_FILE_DIR = "./%s/static/wordle" % package_name

jieba.analyse.set_stop_words("%s/stopwords.txt" % STATIC_FILE_DIR)
jieba.analyse.set_idf_path("%s/idf_dict.txt" % STATIC_FILE_DIR)
FLAG_LIST = ('n', 'nt', 'nz', 'nl', 'nr', 'ng', 'nz', 'v', 'vn', 'vi', 's', 'j', 'x', 'a', 'l', 't', 'i', 'd')


def load_dict():
    with open("%s/dict.txt" % STATIC_FILE_DIR, encoding="utf-8") as f:
        print("%s 开始加载词典" % time.ctime())
        for line in f:
            try:
                params = line.replace("\n", "").split(" ")
                if len(params) >= 2:
                    params[1] = int(params[1])
                jieba.add_word(*tuple(params))
            except (KeyError, ValueError, MemoryError):
                pass
        print("%s 词典加载完成" % time.ctime())

# 为了不影响主线程正常执行，使用daemon线程加载词典
thread = Thread(target=load_dict)
thread.setDaemon(True)
thread.start()


class WordleHandler(RequestHandler):

    @authenticate(module_name="wordle")
    def post(self):

        # 需要分词的文本
        title = self.get_argument("title", "")
        content = self.get_argument("content")
        digest = self.get_argument("digest", "")

        # 返回的结果数
        word_number = int(self.get_argument("word_number", 50))

        # 是否美化返回的结果
        pretty = bool(self.get_query_argument("pretty", default=False))

        # 加载自定义的额外词，以逗号分隔开
        extra_words = self.get_argument("extra_words", "")
        if len(extra_words) > 0:
            for word in extra_words.strip().split(","):
                if len(word) > 0:
                    jieba.add_word(word)

        wordle = collections.OrderedDict()
        if len(title + content + digest) > 0:
            text = (title + " ") * 5 + (content + " ") + (digest + " ")
            results = jieba.analyse.extract_tags(text, topK=500, withWeight=True, withFlag=True, allowPOS=FLAG_LIST)
            for (word, property_), value in results:
                if len(wordle) <= word_number:
                    wordle[word] = value
                else:
                    break

        self.write(json.dumps({
            "success": True,
            "wordle": wordle
        }, ensure_ascii=False, indent=None if pretty is False else 4))
