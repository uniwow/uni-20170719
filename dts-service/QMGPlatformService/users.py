#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8


USERS = {
    # cookie池
    "cookie-pool": {
        ("admin", "lufohce2l6"),
    },
    # 词云接口用户
    "wordle": [
        ("admin", "ada1dfa29d"),
    ],
    # 竞品分析模块用户安全配置文件
    "recommend": [
        ("admin", "ada1dfa29d")
    ],
    # 默认用户
    "default": [
        ("admin", "admin")
    ],
}

