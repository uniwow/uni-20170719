#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

import pkgutil
from configparser import ConfigParser

__version__ = "0.1"

package_name = "QMGPlatformService"
module_name = "WeixinIntelligentRecommend"

ConfigReader = ConfigParser(allow_no_value=True)
ConfigReader.read_string(pkgutil.get_data(package_name, 'config.ini').decode("utf-8"))

ES_CLUSTER_PUBLIC = [
    "http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.156:9200/",
    "http://elastic:5f9a3b47e63829260b179bd1667607dc@114.215.45.176:9200/"
]

