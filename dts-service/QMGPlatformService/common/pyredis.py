#!/usr/bin/env /usr/local/bin/python3
# encoding: utf-8

"""
@version: 0.1
@author: Gamelfie
@contact: fudenglong1417@gmail.com
@software: PyCharm
@file: pyredis.py
@time: 2017/5/5 9:20
"""

import redis


class Redis(object):

    def __init__(self, *, host, port, password=None, **kwargs):

        self.__redis = redis.StrictRedis(host=host, port=port, password=password)

    @property
    def connection(self):
        return self.__redis

    def __getattr__(self, item):
        return getattr(self.__redis, item)

