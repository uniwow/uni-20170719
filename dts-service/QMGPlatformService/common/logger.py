#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2016-12-15 23:25:49
# @Author  : Gamelife (fudenglong1417@gmail.com)
# @Link    : https://github.com/gamelife1314
# @Version : 0.1

import logging
import logging.config

from QMGPlatformService import package_name, module_name

"""
    aioauera log 组件
"""


class Logger(object):

    """Auera logger component"""

    def __init__(self, log_file="%s.log" % package_name, app_name=module_name, message_prefix="", app_type=0,
                 task_number="0", **kwargs):
        """
        初始化logger
        :param log_file: 保存的文件名称
        :param app_name: 应用程序名称
        :param prefix: 消息前缀
        :param app_type: 应用类型，0为服务类型应用，1为普通类型应用
        :param task_number: 任务编号，服务类型应用编号默认为0
        """
        self.__app_type = "service" if app_type == 0 else "common"
        self.__message_prefix = message_prefix
        self.__log_file = log_file
        self.__app_name = app_name
        self.__task_number = task_number
        self.__config()

    def __config(self):
        logger_config = {
            "version": 1,
            "disable_existing_loggers": True,
            "formatters": {
                "verbose": {
                    "format": "%(asctime)s %(levelname)s %(process)d "
                              + self.__app_name + " "
                              + str(self.__app_type) + " "
                              + str(self.__task_number) + " "
                              + self.message_prefix + "@%(message)s",
                    "datefmt": "%Y/%m/%d-%H:%M:%S",
                }
            },
            "handlers": {
                'null': {
                    'level': 'DEBUG',
                    'class': 'logging.NullHandler',
                },
                "console": {
                    "level": "DEBUG",
                    "class": "logging.StreamHandler",
                    "formatter": "verbose",
                    "stream": "ext://sys.stdout",
                },
                "file": {
                    "level": "ERROR",
                    "class": "cloghandler.ConcurrentRotatingFileHandler",
                    "maxBytes": 1024 * 1024 * 10,
                    "backupCount": 20,
                    "delay": False,
                    "filename": "%s/runtime/log/%s" % (package_name, self.__log_file),
                    "formatter": "verbose"
                },
            },
            "loggers": {
            },
        }

        logger_config["loggers"][self.__app_name] = {
            "handlers": ["console", "file"],
            "level": "DEBUG"
        }

        logging.config.dictConfig(logger_config)

        self.log = logging.getLogger(self.__app_name)

    @property
    def message_prefix(self):
        return self.__message_prefix

    @message_prefix.setter
    def message_prefix(self, value):
        self.__message_prefix = value
        self.__config()

    def __getattr__(self, item):
        if hasattr(self.log, item):
            return getattr(self.log, item)

        raise AttributeError("'%s' object has no attribute %s" % (self.__class__.__name__, item))


if __name__ == '__main__':
    # from multiprocessing.pool import Pool
    #
    # def write_log():
    #     logger = Logger()
    #     for i in range(10):
    #         logger.error("Hello %s", i)
    #
    # pool = Pool()
    # for i in range(4):
    #     pool.apply_async(func=write_log, args=())
    #
    # pool.close()
    # pool.join()
    logger = Logger()
    logger.info("hello world")
    logger.message_prefix = "前缀:"
    logger.info("hello world")

