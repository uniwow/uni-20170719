from peewee import *
from playhouse.shortcuts import RetryOperationalError


class AutoReconnectMysql(RetryOperationalError, MySQLDatabase):
    pass

database = AutoReconnectMysql('dts_ops', **{
    'user': 'wom',
    'host': 'rdsvwq3t80d6dyy39hk3o.mysql.rds.aliyuncs.com',
    'password': 'Qmg2016mw#semys$',
    'charset': 'utf8mb4'
})


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class SogouWeixinAccountLatestArticleCrawlTask(BaseModel):
    account = CharField(db_column='account_id', unique=True)
    account_priority = IntegerField(index=True)
    crawled_today_post = IntegerField(index=True)
    create_time = IntegerField()
    enable_task = IntegerField(index=True)
    enqueue_status = IntegerField()
    execute_count = IntegerField(index=True)
    execute_time = TextField()
    last_task_end_time = IntegerField(null=True)
    last_task_start_time = IntegerField()
    server_host_name = CharField()
    server_ip = CharField()
    server_process = IntegerField(db_column='server_process_id')
    task_status = IntegerField(index=True)
    today_execute_count = IntegerField(index=True)
    total_success_count = IntegerField()
    update_time = DateTimeField()

    class Meta:
        db_table = 'sogou_weixin_account_latest_article_crawl_task'


class SogouWeixinAccountInitTask(BaseModel):
    account = CharField(db_column='account_id', unique=True)
    account_level = IntegerField(index=True)
    account_priority = IntegerField()
    account_source = IntegerField()
    account_status = IntegerField(index=True)
    article_num = IntegerField()
    crawled_latest_article_post_time = IntegerField()
    create_time = IntegerField()
    enable_task = IntegerField(index=True)
    enqueue_status = IntegerField()
    execute_count = IntegerField()
    last_recheck_end_time = IntegerField()
    last_task_end_time = IntegerField(null=True)
    last_task_start_time = IntegerField()
    post_end_time = CharField()
    post_start_time = CharField()
    recheck_execute_count = IntegerField()
    recheck_status = IntegerField()
    server_host_name = CharField()
    server_ip = CharField()
    server_process = IntegerField(db_column='server_process_id')
    sync_info_from_mongo = IntegerField()
    task_status = IntegerField(index=True)
    update_time = DateTimeField()

    class Meta:
        db_table = 'sogou_weixin_account_init_task'


class WeixinCookiePool(BaseModel):
    created_at = IntegerField()
    headers = TextField()
    is_expired = IntegerField()
    method = CharField()
    pass_ticket = CharField(unique=True)
    readable_created_time = DateTimeField()
    url = TextField()

    class Meta:
        db_table = 'weixin_cookie_pool'
