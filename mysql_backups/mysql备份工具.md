# Mysql 备份工具上线流程  

## 环境 ：Centos7 

## 工具简介  

### mysqldump 
``` 
mysqldump是mysql自带的用于转存储数据库的实用程序。它主要产生一个SQL脚本，其中包含从头重新创建数据库所必需的命令CREATE TABLE INSERT等。由此可见mysqldump是逻辑备份工具 。支持远程备份*
```

### Percona 备份工具  
```
Percona Server 是基于mysql 的改进版本。percona xtrabackupx 是Percona 提供的数据库备份工具，包括 xtrabackup 和 innobackupex，后者支持 inno 引擎。xtrabackupx 是物理备份工具，需要指定环境目录  ，故不支持远程备份
```
## 安装流程 

### mysqldump 
```
mysql 自带导入导出工具，无需安装
```

### xtrabackupx 
```
1.下载yum源文件(yum install http://www.percona.com/downloads/percona-release/redhat/0.1-4/percona-release-0.1-4.noarch.rpm);
2.安装备份工具 (yum install percona-xtrabackup-24) 
```
## 实现原理 

### mysqldump 
```
mysqldump通过连接数据库，导出表结构信息以及插入  语句。属于逻辑备份 。因为基于登陆数据库 ，故可以实现远程备份
```

### Percona 备份工具 
```
xtrabackup 是 percona 提供的备份工具 。基于本地配置文件以及文件 存储路径。 xtrabackup 属于物理备份工具，不支持远程备份  ，一般用于本地备份后通过管道传输到  远程服务器.
```
## 优缺点 

### mysqldump 
`优点：1.支持远程备份 。缺点：1.只支持逻辑备份 。` 

### Percona 备份工具  
`优点：1.支持物理备份 。缺点：1.不支持远程备份。` 

## 备份方式 

### mysqldump 
```
mysqldump -u数据库账号 -p数据库密码 -h 数据库IP 数据库名称 > 文件路径。详细参数请参考官方文档
```

### Percona 备份工具  
```
innobackupex --defaults-file=/etc/my.cnf --port 3306 --host 192.168.0.116 --user=admin --socket=/var/lib/mysql/mysql.sock   --password=admin1234 ./
```

## 恢复方式 

### mysqldump 
```
mysql -u数据库账号 -p数据库密码 -h 数据库IP 数据库名称 <文件路径。详细参数请参考官方文档
```

### Percona 备份工具  
```
innobackupex --defaults-file=/etc/my.cnf --port 3306 --host 192.168.0.116 --user=admin --socket=/var/lib/mysql/mysql.sock   --password=admin1234  --copy-back ./文件名
```

## 参考资料  
[mysqldump官方文档](https://dev.mysql.com/doc/refman/5.6/en/mysqldump.html)  
[xtrabackup 官方文档](https://www.percona.com/doc/percona-xtrabackup/2.2/innobackupex/innobackupex_option_reference.html)  
[xtrabackup 使用案例](http://blog.csdn.net/stubborn_cow/article/details/50072965)  