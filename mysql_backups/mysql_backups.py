import argparse
import subprocess
import datetime



def get_args():
    parse =argparse.ArgumentParser()
    parse.add_argument('-t',type=str) # 所用工具
    parse.add_argument('-d',type=str) # 数据库名称
    args = parse.parse_args()
    return vars(args)

def get_href():
    args = get_args()
    backup_tool = args['t']
    backup_db = args['d']
    if backup_tool == "mysql_dump":
        mysql_dump(backup_db)
    elif backup_tool == "inno_backup":
        inno_backup()


def mysql_dump(backup_db):
    start_time = str(datetime.datetime.now())
    p = subprocess.Popen('/usr/bin/mysqldump -uadmin -padmin1234 -h 192.168.0.116 qmg > /data/mysql_backup/%s_%s.sql' % (backup_db,str(datetime.datetime.now().strftime('%y%m%d%H%M%S'))),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
    stdout,stderr = p.communicate()
    if b"Using a password" in stderr:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " mysql dump correct \n"
            f.write(line.encode("utf-8","ignore"))
    else:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " mysql dump error \n"
            f.write(line.encode("utf-8","ignore"))

def mysql_recovery(backup_db):
    start_time = str(datetime.datetime.now())
    p = subprocess.Popen('/usr/bin/mysql -uadmin -padmin1234 -h 192.168.0.116 qmg_copy < /data/mysql_backup/%s_%s.sql' % (backup_db,str(datetime.datetime.now().strftime('%y%m%d%H%M%S'))),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
    stdout,stderr = p.communicate()
    if b"Using a password" in stderr:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " mysql dump correct \n"
            f.write(line.encode("utf-8","ignore"))
    else:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " mysql dump error \n"
            f.write(line.encode("utf-8","ignore"))


def inno_backup():
    start_time = str(datetime.datetime.now())
    p = subprocess.Popen('/usr/bin/innobackupex --defaults-file=/etc/my.cnf --port 3306 --host 192.168.0.116 --user=admin --socket=/var/lib/mysql/mysql.sock   --password=admin1234 /data/mysql_bakup/',stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
    stdout,stderr = p.communicate()
    if b"completed OK" in stderr:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " inno backup correct \n"
            f.write(line.encode("utf-8","ignore"))
    else:
        with open("./mysql_dump.log","ab") as f:
            line = start_time + "---" + str(datetime.datetime.now()) + " inno backup error \n"
            f.write(line.encode("utf-8","ignore"))


    

if __name__ == '__main__':
    get_href()