# -*- coding: utf-8 -*-

import re
import time
import requests
from lxml import etree
from pymongo import MongoClient
from bson import ObjectId
import collections
import pyquery
import grequests
import os
from pypinyin import lazy_pinyin

mongo_dict = {
    'host': '118.190.175.5',
    'port': 27011,
    'db': 'wom-dts-datawarehouse',
    'user': 'dts-datawarehouse-admin',
    'password': '6ghednsdf1xde0bqq',
}

def sogou_dict_cate_crawl():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    response_pyquery = pyquery.PyQuery(url = "http://pinyin.sogou.com/dict")
    classify_list = []
    for item in response_pyquery("div.dict_category_list_title"):
        classify_dict = {}
        classify_dict["classify"] = item[0].attrib["href"].split("?")[0]
        classify_dict["desc"] = item[0].text_content()
        classify_list.append(classify_dict)
    print(classify_list)
    for classift_dict in classify_list:
        response_pyquery = pyquery.PyQuery(url = "http://pinyin.sogou.com"+ classift_dict["classify"])
        sub_classify_list = []
        if classift_dict["desc"] == "城市信息大全":
            for item in response_pyquery("a.citylist"):
                sub_classify_list.append({
                        "classify":item.attrib["href"],
                        "desc":item.text_content()
                    })
            classift_dict["sub_classify"] = sub_classify_list
        else:
            for item in response_pyquery("div.no_select"):
                sub_classify_list.append({
                        "classify":item[0].attrib["href"],
                        "desc":item[0].text
                    })
            classift_dict["sub_classify"] = sub_classify_list

    print(classify_list)

    
    mongo_database["brand_classify_qmg"].insert({"classify_dimension":"tags",
                                                "desc":"安照标签分类",
                                                "target":"http://pinyin.sogou.com/dict/",
                                                "classify":classify_list})


def request_error_handler(request,exception):
    print("resuest error...")
    
def sogou_dict_detail_crawl():
    mongo_client = MongoClient(mongo_dict["host"], mongo_dict["port"])
    mongo_client[mongo_dict["db"]].authenticate(mongo_dict["user"], mongo_dict["password"])
    mongo_database = mongo_client[mongo_dict["db"]]
    document = mongo_database["brand_classify_qmg"].find_one({"target":"http://pinyin.sogou.com/dict/","classify_dimension":"tags"},{"classify":1})
    for classify in document["classify"][:]:
        classify_path = "/mnt/shared-dir/"+"".join(lazy_pinyin(classify["desc"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*","")))
        # if not os.path.exists(classify_path):
        #     os.system("/usr/bin/mkdir %s" % classify_path)
        # else:
        #     os.system("/usr/bin/rm -rf %s" % classify_path)
        #     os.system("/usr/bin/mkdir %s" % classify_path)

        for sub_classify in classify["sub_classify"]:
            sub_classify_path = classify_path + "/" +"".join(lazy_pinyin(sub_classify["desc"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*","")))
            # if not os.path.exists(sub_classify_path):
            #     os.system("/usr/bin/mkdir %s" % sub_classify_path)
            # else:
            #     os.system("/usr/bin/rm -rf %s" % sub_classify_path)
            #     os.system("/usr/bin/mkdir %s" % sub_classify_path)


            page_count = 1
            while True:
                response_pyquery = pyquery.PyQuery(url = "http://pinyin.sogou.com" + sub_classify["classify"] + "/default/%d" % page_count)
                response_list = []
                if len(response_pyquery("div.detail_title>a")) == 0:
                    break
                for item in response_pyquery("div.detail_title>a"):
                    item_dict = {}
                    item_dict["title"] = item.text_content()
                    item_dict["detail"] = item.attrib["href"]
                    response_list.append(item_dict)
                item_count = 0
                for item in response_pyquery("div.dict_dl_btn>a"):
                    response_list[item_count]["target"] = item.attrib["href"]
                    item_count += 1
                headers = {
                        "Host":"pinyin.sogou.com",
                        "Connection":"keep-alive",
                        "Accept":"*/*",
                        "X-Requested-With":"XMLHttpRequest",
                        "User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36",
                        "Referer":"http://pinyin.sogou.com/dict/detail/index/15191",
                        "Accept-Encoding":"gzip, deflate, sdch",
                        "Accept-Language":"zh-CN,zh;q=0.8"
                    }
                cookies = {
                        "SUV":"00BA7A57655135E7591028D8C8601125",
                        "CXID":"0AAE7EB9489E280479A4CCC03D913FEF",
                        "_ga":"GA1.2.427300628.1494297158",
                        "ssuid":"5336228900",
                        "dt_ssuid":"5915942300",
                        "pgv_pvi":"4892420096",
                        "ad":"Llllllllll2B9jdklllllV60T3klllllBq9CiZllll9lllllVZlll5@@@@@@@@@@",
                        "SUID":"E73551655412940A00000000591028D7",
                        "SMYUV":"1496633317303784",
                        "UM_distinctid":"15c764c17bcd1-0cf0c7773d2a3b-541a321f-15f900-15c764c17bdad8",
                        "PHPSESSID":"ennlp8dd0ed5nq8q810slldfp0",
                        "SNUID":"BE2BD4433732647185102CE9370A1C34",
                        "pgv_si":"s9247672320",
                        "sct":"219",
                        "ld":"MZllllllll2B9BVVlllllVO4S7GlllllaU8VJZllllolllll9Zlll5@@@@@@@@@@",
                        "LSTMV":"205%2C358",
                        "LCLKINT":"6093",
                        "sw_uuid":"7880404070",
                        "sg_uuid":"9166314400",
                        "IPLOC":"US",
                        "CNZZDATA1261059312":"1325543815-1496632511-null%7C1497923448",
                        "CNZZDATA1253526839":"904740595-1496630722-http%253A%252F%252Fpinyin.sogou.com%252F%7C1497920749"
                }
                rs = []
                for response in response_list:
                    headers["Referer"] = response["detail"]
                    rs.append(grequests.get("http://pinyin.sogou.com/dict/dialog/word_list/"+response["detail"].split("/")[-1],headers = headers,cookies = cookies))


                # rs = (grequests.get("http://pinyin.sogou.com/dict/dialog/word_list/"+response["detail"].split("/")[-1],headers = headers,cookies = cookies) for response in response_list)
                dict_content_list = grequests.map(rs,exception_handler = request_error_handler)
                item_count = 0
                for dict_content in dict_content_list:
                    # file_path = sub_classify_path +"/"+ "".join(lazy_pinyin(response_list[item_count]["title"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*",""))) + ".scel"
                    # file = open(file_path,"ab")

                    # if dict_content is not None:
                    #     file.write(dict_content.text.encode("utf-8","ignore"))
                    #     file.flush()
                    #     file.close()
                    
                    mongo_database["sogou_scel_dicts"].insert({
                                                            "classify_cn":sub_classify["desc"],
                                                            "classify_py":"".join(lazy_pinyin(sub_classify["desc"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*",""))),
                                                            "sup_classify_cn":classify["desc"],
                                                            "sup_classify_py":"".join(lazy_pinyin(classify["desc"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*",""))),
                                                            "file_name_cn":response_list[item_count]["title"],
                                                            "file_name_py":"".join(lazy_pinyin(response_list[item_count]["title"].replace("】","").replace("【","").replace(" ","").replace("\t","").replace("\n","").replace(".","").replace("?","").replace("，","").replace(",","").replace("、","").replace("(","").replace(")","").replace("（","").replace("）","").replace("/","").replace("\\","").replace('"',"").replace("&","").replace(":","").replace("：","").replace("|","").replace("<","").replace(">","").replace("*",""))),
                                                            "words":pyquery.PyQuery(dict_content.text)("div#words").text().split(" "),
                                                            })

                    item_count += 1
                page_count += 1
                print("current classify is %s;" % sub_classify["desc"],"current sup_classify is %s;" % classify["desc"], "current page is %d" % page_count)
                # print(urls)
                # input("....................................")



    



            


if __name__ == '__main__':
    sogou_dict_detail_crawl()



